package cz.muni.fi.pa165.winerymanagement.production.data.model;

import cz.muni.fi.pa165.winerymanagement.core.converter.QuantityJsonConverter;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * GrapeContainer entity represents a container of grapes with information about
 * its grape type, harvest year, and quantity.
 */
@Entity
@Table(name = "grape_container")
public class GrapeContainer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_grape_container")
    private Long id;
    @Column(name = "grape_type")
    private GrapeType grapeType;
    @Column(name = "harvest_year")
    private int harvestYear;
    @Column(name = "quantity", columnDefinition = "TEXT")
    @Convert(converter = QuantityJsonConverter.class)
    private WeightQuantity quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeType getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeType grapeType) {
        this.grapeType = grapeType;
    }

    public int getHarvestYear() {
        return harvestYear;
    }

    public void setHarvestYear(int harvestYear) {
        this.harvestYear = harvestYear;
    }

    public WeightQuantity getQuantity() {
        return quantity;
    }

    public void setQuantity(WeightQuantity quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GrapeContainer that)) return false;
        return Objects.equals(getGrapeType(), that.getGrapeType()) && Objects.equals(getHarvestYear(), that.getHarvestYear()) && Objects.equals(getQuantity(), that.getQuantity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGrapeType(), getHarvestYear(), getQuantity());
    }

    @Override
    public String toString() {
        return "GrapeContainer{" +
                "id=" + id +
                ", grapeType=" + grapeType +
                ", harvestYear=" + harvestYear +
                ", quantity=" + quantity +
                '}';
    }
}
