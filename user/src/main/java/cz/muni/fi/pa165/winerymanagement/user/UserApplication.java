package cz.muni.fi.pa165.winerymanagement.user;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "PA165 Winery management",
                version = "1.0",
                description = "API describing module for registering and authorizing users and managing their reviews"
        ),
        tags = {
                @Tag(name = "Review", description = "API for managing user reviews"),
                @Tag(name = "User", description = "API for managing users")
        }
)
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

    /**
     * Configure access restrictions to the API.
     *
     * Introspection of opaque access token is configured, introspection endpoint is defined in application.yml.
     */
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests()
                .requestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**")).permitAll()
                .and()
                .csrf().ignoringRequestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**"))
                .and()
                .headers(headers -> headers.frameOptions().sameOrigin());
        http.csrf().disable();
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/actuator/**", "/h2-console/**", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                        .requestMatchers("/api/v1/user/register").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/v1/reviews/bottle/**", "/api/v1/reviews/user/**").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/v1/user/clear", "/api/v1/user/seed").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers("/api/v1/reviews").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers(HttpMethod.PUT, "/api/v1/reviews/*").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers(HttpMethod.DELETE, "/api/v1/reviews/*").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .anyRequest().denyAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return http.build();
    }
}
