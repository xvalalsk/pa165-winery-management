package cz.muni.fi.pa165.winerymanagement.core.model;

public enum ModuleEnum {
    HARVEST(8081),
    PRODUCTION(8082),
    STORE(8083),
    USER(8084);

    private final int defaultPort;

    ModuleEnum(int defaultPort) {
        this.defaultPort = defaultPort;
    }

    public int getDefaultPort() {
        return defaultPort;
    }

    public String getDockerName() {
        return this.name().toLowerCase();
    }

    public String getPortName() {
        return this.name().toUpperCase() + "_PORT";
    }
}
