package cz.muni.fi.pa165.winerymanagement.user.repository;

import cz.muni.fi.pa165.winerymanagement.user.UserApplication;
import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.ReviewRepository;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserApplication.class)
@Transactional
public class ReviewRepositoryTest {
    @Autowired
    private EntityManager em;
    @Autowired
    private ReviewRepository reviewRepository;
    private Review review;
    private User user;

    @BeforeEach
    void setUp() {
        user = new User("MUNI", "My_external_id");
        em.persist(user);

        review = new Review(2222, user, 1, "Comment");
        em.persist(review);
    }

    @Test
    void findById() {
        var foundReview = reviewRepository.findById(review.getId());
        assertThat(foundReview).isPresent();
        assertThat(foundReview.get()).isEqualTo(review);
    }

    @Test
    void findByNonExistingId() {
        assertThat(reviewRepository.findById(-1L)).isEmpty();
    }

    @Test
    void findWineBottleReviews() {
        assertEquals(1, reviewRepository.findReviewsByWineBottleId(review.getWineBottleId()).size());
    }

    @Test
    void findNonExistingWineBottleReviews() {
        assertEquals(0, reviewRepository.findReviewsByWineBottleId(-1L).size());
    }


    @Test
    void findUserReviews() {
        assertEquals(1, reviewRepository.findReviewsByUserId(review.getUser().getId()).size());
    }

    @Test
    void findNonExistingUserReviews() {
        assertEquals(0, reviewRepository.findReviewsByUserId(-1L).size());
    }

    @Test
    void addReview() {
        Review newReview = new Review(1111, user, 2, "Comment 2");

        reviewRepository.save(newReview);
        assertEquals(2, reviewRepository.findReviewsByUserId(review.getUser().getId()).size());
    }

    @Test
    void updateReview() {
        String newComment = "Updated comments";

        Review reviewForUpdate = new Review();
        reviewForUpdate.setId(review.getId());
        reviewForUpdate.setUser(review.getUser());
        reviewForUpdate.setRating(review.getRating());
        reviewForUpdate.setWineBottleId(review.getWineBottleId());
        reviewForUpdate.setComment(newComment);

        reviewRepository.save(reviewForUpdate);
        Optional<Review> updatedReview = reviewRepository.findById(review.getId());
        assertEquals(newComment, updatedReview.get().getComment());
    }

    @Test
    void deleteReview() {
        reviewRepository.deleteById(review.getId());
        assertEquals(0, reviewRepository.findReviewsByUserId(review.getUser().getId()).size());
    }
}
