package cz.muni.fi.pa165.winerymanagement.production.rest;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.api.IngredientDto;
import cz.muni.fi.pa165.winerymanagement.production.facade.IngredientFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/production")
@Tag(name = "Ingredients", description = "API for managing ingredients")
@CrossOrigin(origins = "*")
public class IngredientRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IngredientRestController.class);
    private final IngredientFacade ingredientFacade;

    @Autowired
    public IngredientRestController(IngredientFacade ingredientFacade) {
        this.ingredientFacade = ingredientFacade;
    }

    @Operation(summary = "Get a list of all ingredients", description = "Returns a list of all ingredients.")
    @ApiResponse(responseCode = "200", description = "List of all ingredients returned successfully.")
    @GetMapping("/ingredients")
    public ResponseEntity<List<IngredientDto>> getAllIngredients() {
        LOGGER.info("Getting a list of all ingredients");
        return ResponseEntity.ok(ingredientFacade.findAll());
    }

    @Operation(summary = "Get an ingredient by ID", description = "Returns an ingredient with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient with the specified ID returned successfully."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified ID was not found.")
    })
    @GetMapping("/ingredients/{id}")
    public ResponseEntity<IngredientDto> getIngredientById(@PathVariable(value = "id") Long id) {
        LOGGER.info("Getting an ingredient by ID: {}", id);
        IngredientDto ingredient = ingredientFacade.findById(id);
        if (ingredient == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(ingredient);
        }
    }

    @Operation(summary = "Get an ingredient by name", description = "Returns an ingredient with the specified name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient with the specified name returned successfully."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified name was not found.")
    })
    @GetMapping("/ingredients/name/{name}")
    public ResponseEntity<IngredientDto> getIngredientByName(@PathVariable(value = "name") String name) {
        LOGGER.info("Getting an ingredient by name: {}", name);
        IngredientDto ingredient = ingredientFacade.findByName(name);
        if (ingredient == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(ingredient);
        }
    }

    @Operation(summary = "Add an ingredient", description = "Adds a new ingredient.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Ingredient added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "409", description = "Ingredient with this name already exists.")
    })
    @PostMapping("/ingredients")
    public ResponseEntity<IngredientDto> createIngredient(@Valid @RequestBody IngredientDto ingredient) {
        LOGGER.info("Creating an ingredient: {}", ingredient);
        IngredientDto ingredientDto = ingredientFacade.add(ingredient);
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON).body(ingredientDto);
    }

    @Operation(summary = "Update an ingredient by ID", description = "Updates an ingredient with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient with the specified ID updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified ID was not found.")
    })
    @PutMapping("/ingredients/{id}")
    public ResponseEntity<IngredientDto> updateIngredientById(@PathVariable(value = "id") Long id, @Valid @RequestBody IngredientDto updatedIngredient) {
        LOGGER.info("Updating an ingredient by ID: {} with new data: {}", id, updatedIngredient);
        IngredientDto ingredient = ingredientFacade.updateById(id, updatedIngredient);
        return ResponseEntity.ok(ingredient);
    }

    @Operation(summary = "Update an ingredient by name", description = "Updates an ingredient with the specified name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient with the specified name updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified name was not found.")
    })
    @PutMapping("/ingredients/name/{name}")
    public ResponseEntity<IngredientDto> updateIngredientByName(@PathVariable(value = "name") String name, @Valid @RequestBody IngredientDto updatedIngredient) {
        LOGGER.info("Updating an ingredient by name: {} with new data: {}", name, updatedIngredient);
        IngredientDto ingredient = ingredientFacade.updateByName(name, updatedIngredient);
        return ResponseEntity.ok(ingredient);
    }

    @Operation(summary = "Add quantity to an ingredient by ID", description = "Adds the specified quantity to an ingredient with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity from ingredient with the specified ID added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified ID was not found.")
    })
    @PostMapping("/ingredients/{id}/add-quantity")
    public ResponseEntity<IngredientDto> addQuantityToIngredientById(@PathVariable(value = "id") Long id, @RequestBody Quantity quantity) {
        LOGGER.info("Adding quantity: {} to an ingredient by ID: {}", quantity, id);
        IngredientDto ingredient = ingredientFacade.findById(id);
        return ResponseEntity.ok(ingredientFacade.addQuantity(ingredient.getId(), quantity));
    }

    @Operation(summary = "Add quantity to an ingredient by name", description = "Adds the specified quantity to an ingredient with the specified name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of ingredient with the specified name added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified name was not found.")
    })
    @PostMapping("/ingredients/name/{name}/add-quantity")
    public ResponseEntity<IngredientDto> addQuantityToIngredientByName(@PathVariable(value = "name") String name, @RequestBody Quantity quantity) {
        LOGGER.info("Adding quantity: {} to an ingredient by name: {}", quantity, name);
        IngredientDto ingredient = ingredientFacade.findByName(name);
        return ResponseEntity.ok(ingredientFacade.addQuantity(ingredient.getId(), quantity));
    }

    @Operation(summary = "Subtract quantity to an ingredient by ID", description = "Subtracts the specified value to an ingredient with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity from ingredient with the specified ID subtracted successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified ID was not found.")
    })
    @PostMapping("/ingredients/{id}/subtract-quantity")
    public ResponseEntity<IngredientDto> subtractQuantityToIngredientById(@PathVariable(value = "id") Long id, @Valid @RequestBody Quantity quantity) {
        LOGGER.info("Subtracting quantity: {} from an ingredient by ID: {}", quantity, id);
        IngredientDto ingredient = ingredientFacade.findById(id);
        return ResponseEntity.ok(ingredientFacade.subtractQuantity(ingredient.getId(), quantity));
    }

    @Operation(summary = "Subtract quantity to an ingredient by name", description = "Subtracts the specified value to an ingredient with the specified name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity from ingredient with the specified name subtracted successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified name was not found.")
    })
    @PostMapping("/ingredients/name/{name}/subtract-quantity")
    public ResponseEntity<IngredientDto> subtractQuantityToIngredientByName(@PathVariable(value = "name") String name, @RequestBody Quantity quantity) {
        LOGGER.info("Subtracting quantity: {} from an ingredient by name: {}", quantity, name);
        IngredientDto ingredient = ingredientFacade.findByName(name);
        return ResponseEntity.ok(ingredientFacade.subtractQuantity(ingredient.getId(), quantity));
    }

    @Operation(summary = "Delete an ingredient by ID", description = "Deletes an ingredient with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Ingredient with the specified ID deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified ID was not found.")
    })
    @DeleteMapping("/ingredients/{id}")
    public ResponseEntity<IngredientDto> deleteIngredientById(@PathVariable(value = "id") Long id) {
        LOGGER.info("Deleting an ingredient by ID: {}", id);
        return ResponseEntity.ok(ingredientFacade.deleteById(id));
    }

    @Operation(summary = "Delete an ingredient by name", description = "Deletes an ingredient with the specified name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Ingredient with the specified name deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Ingredient with the specified name was not found.")
    })
    @DeleteMapping("/ingredients/name/{name}")
    public ResponseEntity<IngredientDto> deleteIngredientByName(@PathVariable(value = "name") String name) {
        LOGGER.info("Deleting an ingredient by name: {}", name);
        return ResponseEntity.ok(ingredientFacade.deleteByName(name));
    }
}
