package cz.muni.fi.pa165.winerymanagement.store.data.repository;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WineBottleRepository extends JpaRepository<WineBottle, Long> {
	WineBottle findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear);
    WineBottle findByGrapeTypeAndHarvestYearAndVolume(GrapeType grapeType, int harvestYear, VolumeQuantity bottleVolume);
}