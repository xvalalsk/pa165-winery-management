package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductionItemRepository extends JpaRepository<ProductionItem, Long> {
    ProductionItem findByGrape_GrapeTypeAndYear(GrapeType grapeType, int harvestYear);
}
