package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.GrapeContainerRepository;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerWithGrapeTypeAndYearNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerWithIdNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GrapeContainerServiceTest {

	@Mock
	private GrapeContainerRepository grapeContainerRepository;

	@InjectMocks
	private GrapeContainerService grapeContainerService;

	private GrapeContainer grapeContainer1;
	private GrapeContainer grapeContainer2;

	private GrapeType grapeType1;

	private GrapeType grapeType2;
	@BeforeEach
	public void setUp() {
		grapeType1 = new GrapeType();
		grapeType1.setName("Souvignon");
		grapeType1.setColor(GrapeType.Color.RED);
		grapeType1.setDescription("description");
		grapeType1.setSweetness(GrapeType.Sweetness.SWEET);

		grapeType2 = new GrapeType();
		grapeType2.setName("Merlot");
		grapeType2.setColor(GrapeType.Color.WHITE);
		grapeType2.setDescription("Merlot_description");
		grapeType2.setSweetness(GrapeType.Sweetness.DRY);

		grapeContainer1 = new GrapeContainer();
		grapeContainer1.setId(1L);
		grapeContainer1.setGrapeType(grapeType1);
		grapeContainer1.setHarvestYear(2020);
		grapeContainer1.setQuantity(new WeightQuantity(100.0, WeightQuantity.WeightUnit.KILOGRAM));

		grapeContainer2 = new GrapeContainer();
		grapeContainer2.setId(2L);
		grapeContainer2.setGrapeType(grapeType2);
		grapeContainer2.setHarvestYear(2021);
		grapeContainer2.setQuantity(new WeightQuantity(200.0, WeightQuantity.WeightUnit.KILOGRAM));
	}

	@Test
	void testFindAll() {
		when(grapeContainerRepository.findAll()).thenReturn(Arrays.asList(grapeContainer1, grapeContainer2));

		List<GrapeContainer> grapeContainers = grapeContainerService.findAll();

		assertEquals(2, grapeContainers.size());
		assertTrue(grapeContainers.contains(grapeContainer1));
		assertTrue(grapeContainers.contains(grapeContainer2));
	}

	@Test
	void testFindById() {
		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.of(grapeContainer1));

		GrapeContainer foundGrapeContainer = grapeContainerService.findById(1L);

		assertEquals(grapeContainer1, foundGrapeContainer);
	}

	@Test
	void testFindByIdNotFound() {
		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.empty());

		assertThrows(GrapeContainerWithIdNotFoundException.class, () -> grapeContainerService.findById(1L));
	}

	@Test
	void testFindByGrapeTypeAndHarvestYear() {
		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(grapeContainer1);

		GrapeContainer foundGrapeContainer = grapeContainerService.findByGrapeTypeAndHarvestYear(grapeType1, 2020);

		assertEquals(grapeContainer1, foundGrapeContainer);
	}

	@Test
	void testFindByGrapeTypeAndHarvestYearNotFound() {
		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(null);

		assertThrows(GrapeContainerWithGrapeTypeAndYearNotFoundException.class, () -> grapeContainerService.findByGrapeTypeAndHarvestYear(grapeType1, 2020));
	}

	@Test
	void testAdd() {
		GrapeContainer newGrapeContainer = new GrapeContainer();
		newGrapeContainer.setGrapeType(grapeType1);
		newGrapeContainer.setHarvestYear(2021);
		newGrapeContainer.setQuantity(new WeightQuantity(100.0, WeightQuantity.WeightUnit.KILOGRAM));

		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2021)).thenReturn(null);
		when(grapeContainerRepository.save(newGrapeContainer)).thenReturn(newGrapeContainer);

		GrapeContainer addedGrapeContainer = grapeContainerService.add(newGrapeContainer);

		assertEquals(newGrapeContainer, addedGrapeContainer);
	}

	@Test
	void testAddGrapeContainerAlreadyExists() {
		GrapeContainer newGrapeContainer = new GrapeContainer();
		newGrapeContainer.setGrapeType(grapeType1);
		newGrapeContainer.setHarvestYear(2020);
		newGrapeContainer.setQuantity(new WeightQuantity(100.0, WeightQuantity.WeightUnit.KILOGRAM));

		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(grapeContainer1);

		assertThrows(GrapeContainerAlreadyExistsException.class, () -> grapeContainerService.add(newGrapeContainer));
	}
	@Test
	void testUpdateById() {
		GrapeContainer updatedGrapeContainer = new GrapeContainer();
		updatedGrapeContainer.setGrapeType(grapeType2);
		updatedGrapeContainer.setHarvestYear(2021);
		updatedGrapeContainer.setQuantity(new WeightQuantity(300.0, WeightQuantity.WeightUnit.KILOGRAM));

		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.of(grapeContainer1));
		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType2, 2021)).thenReturn(null);
		when(grapeContainerRepository.save(grapeContainer1)).thenReturn(grapeContainer1);

		GrapeContainer result = grapeContainerService.updateById(1L, updatedGrapeContainer);

		assertEquals(grapeType2, result.getGrapeType());
		assertEquals(2021, result.getHarvestYear());
		assertEquals(300.0, result.getQuantity().getValue());
	}

	@Test
	void testUpdateByGrapeTypeAndHarvestYear() {
		GrapeContainer updatedGrapeContainer = new GrapeContainer();
		updatedGrapeContainer.setGrapeType(grapeType2);
		updatedGrapeContainer.setHarvestYear(2021);
		updatedGrapeContainer.setQuantity(new WeightQuantity(300.0, WeightQuantity.WeightUnit.KILOGRAM));

		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(grapeContainer1);
		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType2, 2021)).thenReturn(null);
		when(grapeContainerRepository.save(grapeContainer1)).thenReturn(grapeContainer1);

		GrapeContainer result = grapeContainerService.updateByGrapeTypeAndHarvestYear(grapeType1, 2020, updatedGrapeContainer);

		assertEquals(grapeType2, result.getGrapeType());
		assertEquals(2021, result.getHarvestYear());
		assertEquals(300.0, result.getQuantity().getValue());
	}

	@Test
	void testAddQuantity() {
		WeightQuantity additionalQuantity = new WeightQuantity(50.0, WeightQuantity.WeightUnit.KILOGRAM);

		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.of(grapeContainer1));

		GrapeContainer result = grapeContainerService.addQuantity(1L, additionalQuantity);

		assertEquals(150000.0, result.getQuantity().getValue());
	}

	@Test
	void testSubtractQuantity() {
		WeightQuantity subtractedQuantity = new WeightQuantity(50.0, WeightQuantity.WeightUnit.KILOGRAM);

		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.of(grapeContainer1));

		GrapeContainer result = grapeContainerService.subtractQuantity(1L, subtractedQuantity);

		assertEquals(50000.0, result.getQuantity().getValue());
	}

	@Test
	void testTrySubtractQuantity() {
		WeightQuantity subtractedQuantity = new WeightQuantity(50.0, WeightQuantity.WeightUnit.KILOGRAM);

		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.of(grapeContainer1));

		double remainingQuantity = grapeContainerService.trySubtractQuantity(1L, subtractedQuantity);

		assertEquals(50000, remainingQuantity);
	}

	@Test
	void testDeleteById() {
		when(grapeContainerRepository.findById(1L)).thenReturn(Optional.of(grapeContainer1));
		grapeContainerService.deleteById(1L);
		verify(grapeContainerRepository).deleteById(1L);
	}

	@Test
	void testDeleteByGrapeTypeAndHarvestYear() {
		when(grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(grapeContainer1);
		grapeContainerService.deleteByGrapeTypeAndHarvestYear(grapeType1, 2020);
		verify(grapeContainerRepository).deleteById(1L);
	}
}