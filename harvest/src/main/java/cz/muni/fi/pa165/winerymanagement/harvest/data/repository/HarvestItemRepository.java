package cz.muni.fi.pa165.winerymanagement.harvest.data.repository;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeQuality;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.HarvestItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface HarvestItemRepository extends JpaRepository<HarvestItem, Long> {
    List<HarvestItem> findHarvestItemsByHarvestId(Long harvestId);
    @Query("SELECT item FROM HarvestItem item WHERE item.harvest = ?1 AND item.grape = ?2 AND item.date = ?3 AND item.quantity = ?4 AND item.quality = ?5")
    HarvestItem findSpecificHarvestItem(Harvest harvest, GrapeType grapeType, Date date, double quantity, GrapeQuality quality);
}
