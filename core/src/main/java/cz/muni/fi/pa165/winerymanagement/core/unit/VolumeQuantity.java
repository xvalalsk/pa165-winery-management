package cz.muni.fi.pa165.winerymanagement.core.unit;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.WrongQuantityTypeException;

@JsonTypeName("volume")
public class VolumeQuantity extends Quantity {

    public enum VolumeUnit implements Unit {
        LITER("l"),
        MILLILITER("ml");

        private final String symbol;

        VolumeUnit(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }

    private VolumeUnit volumeUnit;

    public VolumeQuantity() {
        super();
    }

    public VolumeQuantity(double value, VolumeUnit volumeUnit) {
        super(value, volumeUnit.getSymbol());
        this.volumeUnit = volumeUnit;
        this.value = value;
    }

    public VolumeUnit getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(VolumeUnit volumeUnit) {
        this.volumeUnit = volumeUnit;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String getSymbol() {
        return volumeUnit.getSymbol();
    }

    public void convertTo(VolumeUnit newVolumeUnit) {
        double newValue;
        switch (newVolumeUnit) {
            case LITER:
                newValue = toLiter();
                break;
            case MILLILITER:
                newValue = toMilliliter();
                break;
            default:
                throw new IllegalArgumentException("Invalid unit: " + newVolumeUnit);
        }
        super.value = newValue;
    }

    public double convertValueTo(VolumeUnit volumeUnit) {
        double newValue;
        switch (volumeUnit) {
            case LITER:
                newValue = toLiter();
                break;
            case MILLILITER:
                newValue = toMilliliter();
                break;
            default:
                throw new IllegalArgumentException("Invalid unit: " + volumeUnit);
        }
        return newValue;
    }

    private double toLiter() {
        switch (volumeUnit) {
            case LITER:
                return getValue();
            case MILLILITER:
                return getValue() / 1000;
            default:
                throw new IllegalStateException("Unknown unit: " + volumeUnit);
        }
    }

    private double toMilliliter() {
        switch (volumeUnit) {
            case LITER:
                return getValue() * 1000;
            case MILLILITER:
                return getValue();
            default:
                throw new IllegalStateException("Unknown unit: " + volumeUnit);
        }
    }

    @Override
    public Quantity add(Quantity other) {
        if (!(other instanceof VolumeQuantity otherVolume)) {
            throw new IllegalArgumentException("Cannot add volume to weight or vice versa");
        }
        double newValue = this.toMilliliter() + otherVolume.toMilliliter();
        return new VolumeQuantity(newValue, VolumeUnit.MILLILITER);
    }

    @Override
    public Quantity subtract(Quantity other) {
        if (!(other instanceof VolumeQuantity otherVolume)) {
            throw new IllegalArgumentException("Cannot add volume to weight or vice versa");
        }
        double newValue = this.toMilliliter() - otherVolume.toMilliliter();
        return new VolumeQuantity(newValue, VolumeUnit.MILLILITER);
    }

    @Override
    public double calculateSubtractValue(Quantity other) throws NotEnoughQuantityException {
        VolumeQuantity otherVolume = checkQuantityType(other);
        double newValue = this.toMilliliter() - otherVolume.toMilliliter();
        if (newValue < 0) {
            throw new NotEnoughQuantityException("volume", this.toMilliliter(), "ml", otherVolume.toMilliliter(), "ml");
        }
        return newValue;
    }

    private static VolumeQuantity checkQuantityType(Quantity other) {
        if (!(other instanceof VolumeQuantity otherVolume)) {
            throw new WrongQuantityTypeException();
        }
        return otherVolume;
    }
}