package cz.muni.fi.pa165.winerymanagement.core.exceptions;


import java.util.ResourceBundle;

public class NotEnoughQuantityException extends RuntimeException {

    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");

    private final String quantityType;
    private final double requiredQuantity;
    private final String requiredQuantityType;
    private final double availableQuantity;
    private final String availableQuantityType;

    public NotEnoughQuantityException(String quantityType, double requiredQuantity, String requiredQuantityType, double availableQuantity, String availableQuantityType) {
        this.quantityType = quantityType;
        this.requiredQuantity = requiredQuantity;
        this.requiredQuantityType = requiredQuantityType;
        this.availableQuantity = availableQuantity;
        this.availableQuantityType = availableQuantityType;
    }

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        String message = messages.getString(key);
        return String.format(message, quantityType, requiredQuantity, requiredQuantityType, availableQuantity, availableQuantityType);
    }
}

