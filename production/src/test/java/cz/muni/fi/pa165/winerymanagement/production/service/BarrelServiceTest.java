package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Barrel;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.BarrelRepository;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelWithGrapeTypeAndYearNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelWithIdNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BarrelServiceTest {

	@InjectMocks
	private BarrelService barrelService;

	@Mock
	private BarrelRepository barrelRepository;

	private Barrel barrel1;
	private Barrel barrel2;

	private GrapeType grapeType1;
	private GrapeType grapeType2;

	@BeforeEach
	void setUp() {
		grapeType1 = new GrapeType();
		grapeType1.setName("Souvignon");
		grapeType1.setColor(GrapeType.Color.RED);
		grapeType1.setDescription("description");
		grapeType1.setSweetness(GrapeType.Sweetness.SWEET);

		grapeType2 = new GrapeType();
		grapeType2.setName("Merlot");
		grapeType2.setColor(GrapeType.Color.WHITE);
		grapeType2.setDescription("Merlot_description");
		grapeType2.setSweetness(GrapeType.Sweetness.DRY);

		barrel1 = new Barrel();
		barrel1.setId(1L);
		barrel1.setGrapeType(grapeType1);
		barrel1.setHarvestYear(2018);
		barrel1.setQuantity(new VolumeQuantity(100.0, VolumeQuantity.VolumeUnit.LITER));

		barrel2 = new Barrel();
		barrel2.setId(2L);
		barrel2.setGrapeType(grapeType2);
		barrel2.setHarvestYear(2019);
		barrel2.setQuantity(new VolumeQuantity(200.0, VolumeQuantity.VolumeUnit.LITER));
	}

	@Test
	void testFindAll() {
		when(barrelRepository.findAll()).thenReturn(Arrays.asList(barrel1, barrel2));

		List<Barrel> barrels = barrelService.findAll();

		assertEquals(2, barrels.size());
		assertTrue(barrels.contains(barrel1));
		assertTrue(barrels.contains(barrel2));
	}

	@Test
	void testFindById() {
		when(barrelRepository.findById(1L)).thenReturn(Optional.of(barrel1));

		Barrel foundBarrel = barrelService.findById(1L);

		assertEquals(barrel1, foundBarrel);
	}

	@Test
	void testFindByIdNotFound() {
		when(barrelRepository.findById(3L)).thenReturn(Optional.empty());

		assertThrows(BarrelWithIdNotFoundException.class, () -> barrelService.findById(3L));
	}

	@Test
	void testFindByGrapeTypeAndHarvestYear() {
		when(barrelRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2018)).thenReturn(barrel1);

		Barrel foundBarrel = barrelService.findByGrapeTypeAndHarvestYear(grapeType1, 2018);

		assertEquals(barrel1, foundBarrel);
	}

	@Test
	void testFindByGrapeTypeAndHarvestYearNotFound() {
		when(barrelRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(null);

		assertThrows(BarrelWithGrapeTypeAndYearNotFoundException.class, () -> barrelService.findByGrapeTypeAndHarvestYear(grapeType1, 2020));
	}

	@Test
	void testAdd() {
		Barrel newBarrel = new Barrel();
		newBarrel.setGrapeType(grapeType1);
		newBarrel.setHarvestYear(2020);
		newBarrel.setQuantity(new VolumeQuantity(300.0, VolumeQuantity.VolumeUnit.LITER));

		when(barrelRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(null);
		when(barrelRepository.save(newBarrel)).thenReturn(newBarrel);

		Barrel addedBarrel = barrelService.add(newBarrel);

		assertEquals(newBarrel, addedBarrel);
	}

	@Test
	void testAddAlreadyExists() {
		when(barrelRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2018)).thenReturn(barrel1);

		assertThrows(BarrelAlreadyExistsException.class, () -> barrelService.add(barrel1));
	}

	@Test
	void testUpdateById() {
		Barrel updatedBarrel = new Barrel();
		updatedBarrel.setGrapeType(grapeType1);
		updatedBarrel.setHarvestYear(2018);
		updatedBarrel.setQuantity(new VolumeQuantity(150.0, VolumeQuantity.VolumeUnit.LITER));

		when(barrelRepository.findById(1L)).thenReturn(Optional.of(barrel1));
		when(barrelRepository.save(barrel1)).thenReturn(updatedBarrel);

		Barrel result = barrelService.updateById(1L, updatedBarrel);

		assertEquals(updatedBarrel, result);
	}

	@Test
	void testUpdateByIdNotFound() {
		Barrel updatedBarrel = new Barrel();
		updatedBarrel.setGrapeType(grapeType1);
		updatedBarrel.setHarvestYear(2018);
		updatedBarrel.setQuantity(new VolumeQuantity(150.0, VolumeQuantity.VolumeUnit.LITER));

		when(barrelRepository.findById(3L)).thenReturn(Optional.empty());

		assertThrows(BarrelWithIdNotFoundException.class, () -> barrelService.updateById(3L, updatedBarrel));
	}

	@Test
	void testUpdateByGrapeTypeAndHarvestYear() {
		Barrel updatedBarrel = new Barrel();
		updatedBarrel.setGrapeType(grapeType1);
		updatedBarrel.setHarvestYear(2018);
		updatedBarrel.setQuantity(new VolumeQuantity(150.0, VolumeQuantity.VolumeUnit.LITER));

		when(barrelRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2018)).thenReturn(barrel1);
		when(barrelRepository.save(barrel1)).thenReturn(updatedBarrel);

		Barrel result = barrelService.updateByGrapeTypeAndHarvestYear(grapeType1, 2018, updatedBarrel);

		assertEquals(updatedBarrel, result);
	}

	@Test
	void testUpdateByGrapeTypeAndHarvestYearNotFound() {
		Barrel updatedBarrel = new Barrel();
		updatedBarrel.setGrapeType(grapeType1);
		updatedBarrel.setHarvestYear(2018);
		updatedBarrel.setQuantity(new VolumeQuantity(150.0, VolumeQuantity.VolumeUnit.LITER));

		when(barrelRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020)).thenReturn(null);

		assertThrows(BarrelWithGrapeTypeAndYearNotFoundException.class, () -> barrelService.updateByGrapeTypeAndHarvestYear(grapeType1, 2020, updatedBarrel));
	}

	@Test
	void testAddQuantity() {
		when(barrelRepository.findById(1L)).thenReturn(Optional.of(barrel1));

		Barrel result = barrelService.addQuantity(1L, new VolumeQuantity(50.0, VolumeQuantity.VolumeUnit.LITER));

		assertEquals(150000.0, result.getQuantity().getValue());
	}

	@Test
	void testSubtractQuantity() {
		when(barrelRepository.findById(1L)).thenReturn(Optional.of(barrel1));

		Barrel result = barrelService.subtractQuantity(1L, new VolumeQuantity(50.0, VolumeQuantity.VolumeUnit.LITER));

		assertEquals(50000.0, result.getQuantity().getValue());
	}

	@Test
	public void testDeleteById_success() {
		Long barrelId = 1L;

		when(barrelRepository.findById(barrelId)).thenReturn(Optional.of(barrel1));

		Barrel deletedBarrel = barrelService.deleteById(barrelId);

		verify(barrelRepository, times(1)).findById(barrelId);
		verify(barrelRepository, times(1)).deleteById(barrelId);

		assertEquals(barrel1, deletedBarrel);
	}

	@Test
	public void testDeleteById_notFound() {
		Long barrelId = 1L;

		when(barrelRepository.findById(barrelId)).thenReturn(Optional.empty());

		assertThrows(BarrelWithIdNotFoundException.class, () -> barrelService.deleteById(barrelId));

		verify(barrelRepository, times(1)).findById(barrelId);
		verify(barrelRepository, never()).deleteById(anyLong());
	}
}