package cz.muni.fi.pa165.winerymanagement.harvest.data.models;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name="harvest_items")
public class HarvestItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date date;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grape_id")
    private GrapeType grape;
    private double quantity;    // Amount in kg
    private GrapeQuality quality;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "harvest_id")
    private Harvest harvest;

    public HarvestItem() {}

    public HarvestItem(Harvest harvest, Date date, double quantity, GrapeQuality quality, GrapeType grape) {
        this.harvest = harvest;
        this.date = date;
        this.grape = grape;
        this.quantity = quantity;
        this.quality = quality;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeType getGrape() {
        return grape;
    }

    public void setGrape(GrapeType grape) {
        this.grape = grape;
    }

    public GrapeQuality getQuality() {
        return quality;
    }

    public void setQuality(GrapeQuality quality) {
        this.quality = quality;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Harvest getHarvest() {
        return harvest;
    }

    public void setHarvest(Harvest harvest) {
        this.harvest = harvest;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        HarvestItem that = (HarvestItem) obj;
        return Objects.equals(this.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "HarvestItem{" +
                "id=" + id +
                ", date=" + date +
                ", quantity=" + quantity +
                ", quality=" + quality +
                '}';
    }
}
