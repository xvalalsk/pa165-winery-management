package cz.muni.fi.pa165.winerymanagement.harvest.api;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Color;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Sweetness;

public class GrapeTypeInputDto {
    private String name;
    private String description;
    private Color color;
    private Sweetness sweetness;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Sweetness getSweetness() {
        return sweetness;
    }

    public void setSweetness(Sweetness sweetness) {
        this.sweetness = sweetness;
    }

    @Override
    public String toString() {
        return "GrapeInputDto{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", color=" + color +
                ", sweetness=" + sweetness +
                '}';
    }
}
