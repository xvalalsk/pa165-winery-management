package cz.muni.fi.pa165.winerymanagement.harvest.facade;

import cz.muni.fi.pa165.winerymanagement.harvest.api.GrapeTypeDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.GrapeTypeInputDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestInputDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemCreateDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemUpdateDto;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.GrapeTypeAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.GrapeTypeNotExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.HarvestAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.HarvestItemAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.HarvestItemNotExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.HarvestNotExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.mappers.GrapeTypeMapper;
import cz.muni.fi.pa165.winerymanagement.harvest.mappers.HarvestItemMapper;
import cz.muni.fi.pa165.winerymanagement.harvest.mappers.HarvestMapper;
import cz.muni.fi.pa165.winerymanagement.harvest.service.HarvestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HarvestFacade {
    private final HarvestService harvestService;
    private final HarvestMapper harvestMapper;
    private final HarvestItemMapper harvestItemMapper;
    private final GrapeTypeMapper grapeTypeMapper;

    @Autowired
    public HarvestFacade(HarvestService service, HarvestMapper harvestMapper, HarvestItemMapper harvestItemMapper, GrapeTypeMapper grapeTypeMapper) {
        this.harvestService = service;
        this.harvestMapper = harvestMapper;
        this.harvestItemMapper = harvestItemMapper;
        this.grapeTypeMapper = grapeTypeMapper;
    }

    @Transactional(readOnly = true)
    public List<HarvestDto> findAllHarvests() {
        return this.harvestMapper.mapToHarvestDtoList(this.harvestService.findAllHarvests());
    }

    @Transactional(readOnly = true)
    public List<HarvestItemDto> findAllItemsForHarvest(Long harvestId) throws HarvestNotExistsException {
        return this.harvestItemMapper.mapToHarvestItemDtoList(this.harvestService.findAllItemsForHarvest(harvestId));
    }

    @Transactional(readOnly = true)
    public List<HarvestItemDto> findAllHarvestItems() {
        return this.harvestItemMapper.mapToHarvestItemDtoList(this.harvestService.findAllHarvestItems());
    }

    @Transactional(readOnly = true)
    public List<GrapeTypeDto> findAllGrapeTypes() {
        return this.grapeTypeMapper.mapToGrapeTypeDtoList(this.harvestService.findAllGrapeTypes());
    }

    @Transactional(readOnly = true)
    public HarvestDto findHarvestById(Long id) throws HarvestNotExistsException {
        return this.harvestMapper.mapToHarvestDto(this.harvestService.findHarvestById(id));
    }

    @Transactional(readOnly = true)
    public HarvestItemDto findHarvestItemById(Long id) throws HarvestItemNotExistsException {
        return this.harvestItemMapper.mapToHarvestItemDto(this.harvestService.findHarvestItemById(id));
    }

    @Transactional(readOnly = true)
    public GrapeTypeDto findGrapeTypeById(Long id) throws GrapeTypeNotExistsException {
        return this.grapeTypeMapper.mapToGrapeTypeDto(this.harvestService.findGrapeTypeById(id));
    }

    public HarvestDto addHarvest(HarvestInputDto harvestInputDto) throws HarvestAlreadyExistsException {
        return this.harvestMapper.mapToHarvestDto(this.harvestService.addHarvest(this.harvestMapper.mapFromHarvestInputDto(harvestInputDto)));
    }

    public HarvestItemDto addHarvestItem(HarvestItemCreateDto harvestItemDto) throws HarvestNotExistsException, HarvestItemAlreadyExistsException {
        return this.harvestItemMapper.mapToHarvestItemDto(this.harvestService.addHarvestItem(this.harvestItemMapper.mapFromHarvestItemCreateDto(harvestItemDto)));
    }

    public GrapeTypeDto addGrapeType(GrapeTypeInputDto grapeTypeInputDto) throws GrapeTypeAlreadyExistsException {
        return this.grapeTypeMapper.mapToGrapeTypeDto(this.harvestService.addGrapeType(this.grapeTypeMapper.mapFromGrapeTypeInputDto(grapeTypeInputDto)));
    }

    public HarvestDto updateHarvest(Long id, HarvestInputDto harvestInputDto) throws HarvestNotExistsException {
        return this.harvestMapper.mapToHarvestDto(this.harvestService.updateHarvest(id, this.harvestMapper.mapFromHarvestInputDto(harvestInputDto)));
    }

    public HarvestItemDto updateHarvestItem(Long id, HarvestItemUpdateDto harvestItemUpdateDto) throws HarvestItemNotExistsException {
        return this.harvestItemMapper.mapToHarvestItemDto(this.harvestService.updateHarvestItem(id, this.harvestItemMapper.mapFromHarvestItemUpdateDto(harvestItemUpdateDto)));
    }

    public GrapeTypeDto updateGrapeType(Long id, GrapeTypeInputDto grapeTypeInputDto) throws GrapeTypeNotExistsException {
        return this.grapeTypeMapper.mapToGrapeTypeDto(this.harvestService.updateGrapeType(id, this.grapeTypeMapper.mapFromGrapeTypeInputDto(grapeTypeInputDto)));
    }

    public HarvestDto deleteHarvestById(Long id) throws HarvestNotExistsException {
        return this.harvestMapper.mapToHarvestDto(this.harvestService.deleteHarvestById(id));
    }

    public HarvestItemDto deleteHarvestItemById(Long id) throws HarvestItemNotExistsException {
        return this.harvestItemMapper.mapToHarvestItemDto(this.harvestService.deleteHarvestItemById(id));
    }

    public GrapeTypeDto deleteGrapeTypeById(Long id) throws GrapeTypeNotExistsException {
        return this.grapeTypeMapper.mapToGrapeTypeDto(this.harvestService.deleteGrapeTypeById(id));
    }
}