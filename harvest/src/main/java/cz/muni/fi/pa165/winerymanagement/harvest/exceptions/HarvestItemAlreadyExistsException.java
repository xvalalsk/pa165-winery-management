package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.HarvestItem;

/**
 * Thrown when the Harvest Item already exists.
 */
public class HarvestItemAlreadyExistsException extends RuntimeException {
    public HarvestItemAlreadyExistsException() {
        super();
    }

    public HarvestItemAlreadyExistsException(HarvestItem item) {
        super(item + " already exists");
    }
}
