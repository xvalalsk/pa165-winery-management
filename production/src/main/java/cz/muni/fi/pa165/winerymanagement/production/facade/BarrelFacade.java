package cz.muni.fi.pa165.winerymanagement.production.facade;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.api.BarrelDto;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.mappers.BarrelMapper;
import cz.muni.fi.pa165.winerymanagement.production.service.BarrelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BarrelFacade {
    private final BarrelService barrelService;
    private final BarrelMapper barrelMapper;

    @Autowired
    public BarrelFacade(BarrelService barrelService, BarrelMapper barrelMapper) {
        this.barrelService = barrelService;
        this.barrelMapper = barrelMapper;
    }

    @Transactional(readOnly = true)
    public List<BarrelDto> findAll() {
        return barrelMapper.mapToList(barrelService.findAll());
    }

    @Transactional(readOnly = true)
    public BarrelDto findById(Long id) {
        return barrelMapper.mapToDto(barrelService.findById(id));
    }

    @Transactional(readOnly = true)
    public BarrelDto findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        return barrelMapper.mapToDto(barrelService.findByGrapeTypeAndHarvestYear(grapeType, harvestYear));
    }

    public BarrelDto add(BarrelDto newBarrel) {
        return barrelMapper.mapToDto(barrelService.add(barrelMapper.mapFromDto(newBarrel)));
    }

    public BarrelDto updateById(Long id, BarrelDto updatedBarrel) {
        return barrelMapper.mapToDto(barrelService.updateById(id, barrelMapper.mapFromDto(updatedBarrel)));
    }

    public BarrelDto updateByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear, BarrelDto updatedBarrel) {
        return barrelMapper.mapToDto(barrelService.updateByGrapeTypeAndHarvestYear(grapeType, harvestYear, barrelMapper.mapFromDto(updatedBarrel)));
    }

    public BarrelDto addQuantity(Long id, Quantity quantity) {
        return barrelMapper.mapToDto(barrelService.addQuantity(id, quantity));
    }

    public BarrelDto subtractQuantity(Long id, Quantity quantity) {
        return barrelMapper.mapToDto(barrelService.subtractQuantity(id, quantity));
    }

    public BarrelDto deleteById(Long id) {
        return barrelMapper.mapToDto(barrelService.deleteById(id));
    }

    public BarrelDto deleteByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        return barrelMapper.mapToDto(barrelService.deleteByGrapeTypeAndHarvestYear(grapeType, harvestYear));
    }
}
