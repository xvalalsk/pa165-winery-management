package cz.muni.fi.pa165.winerymanagement.harvest.api;

import cz.muni.fi.pa165.winerymanagement.harvest.annotations.CurrentYearOrBefore;
import jakarta.validation.constraints.Min;

public class HarvestInputDto {

    @Min(1998L)
    @CurrentYearOrBefore
    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "HarvestInputDto{" +
                "year=" + year +
                '}';
    }
}
