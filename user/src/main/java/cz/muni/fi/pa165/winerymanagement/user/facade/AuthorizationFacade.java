package cz.muni.fi.pa165.winerymanagement.user.facade;

import cz.muni.fi.pa165.winerymanagement.user.api.UserRegistrationDto;
import cz.muni.fi.pa165.winerymanagement.user.mappers.UserMapper;
import cz.muni.fi.pa165.winerymanagement.user.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuthorizationFacade {
    private final AuthorizationService authorizationService;
    private final UserMapper userMapper;

    @Autowired
    public AuthorizationFacade(AuthorizationService authorizationService, UserMapper userMapper) {
        this.authorizationService = authorizationService;
        this.userMapper = userMapper;
    }

    public long register(UserRegistrationDto userRegistrationDto) {
        return this.authorizationService.register(this.userMapper.mapFromRegistrationDto(userRegistrationDto)).getId();
    }
}
