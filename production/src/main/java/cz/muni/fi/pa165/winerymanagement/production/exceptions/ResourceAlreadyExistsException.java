package cz.muni.fi.pa165.winerymanagement.production.exceptions;

public class ResourceAlreadyExistsException extends ProductionException {

    public ResourceAlreadyExistsException() {
    }

    @Override
    public String getMessage() {
        return "Resource already exists";
    }
}
