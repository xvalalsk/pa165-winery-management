package cz.muni.fi.pa165.winerymanagement.user.facade;

import cz.muni.fi.pa165.winerymanagement.user.api.ReviewCreateDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewUpdateDto;
import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.mappers.ReviewMapper;
import cz.muni.fi.pa165.winerymanagement.user.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReviewFacade {
    private final ReviewService reviewService;
    private final ReviewMapper reviewMapper;

    @Autowired
    public ReviewFacade(ReviewService reviewService, ReviewMapper reviewMapper) {
        this.reviewService = reviewService;
        this.reviewMapper = reviewMapper;
    }

    public ReviewDto addReview(ReviewCreateDto reviewCreateDto) {
        Review review = this.reviewMapper.mapToReview(reviewCreateDto);
        return this.reviewMapper.mapToReviewDto(this.reviewService.addReview(review));
    }

    public ReviewDto updateReview(ReviewUpdateDto reviewUpdateDto) {
        Review review = this.reviewMapper.mapToReview(reviewUpdateDto);
        return this.reviewMapper.mapToReviewDto(this.reviewService.updateReview(review));
    }

    public void deleteReview(Long id) {
        this.reviewService.deleteReview(id);
    }

    public List<ReviewDto> getWineBottleReviews(Long wineBottleId) {
        return this.reviewService.getWineBottleReviews(wineBottleId).stream()
                .map(this.reviewMapper::mapToReviewDto)
                .toList();
    }

    public List<ReviewDto> getUserReviews(Long userId) {
        return this.reviewService.getUserReviews(userId).stream()
                .map(this.reviewMapper::mapToReviewDto)
                .toList();
    }
}
