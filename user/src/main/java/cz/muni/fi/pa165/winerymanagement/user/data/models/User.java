package cz.muni.fi.pa165.winerymanagement.user.data.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"issuer", "externalId"})
})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    private String issuer;
    @NotBlank
    private String externalId;

    @OneToMany(fetch = FetchType.LAZY , mappedBy = "user", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Review> reviews;

    public User() {
    }

    public User(String issuer, String externalId) {
        this.issuer = issuer;
        this.externalId = externalId;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return issuer.equals(user.issuer) && externalId.equals(user.externalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(issuer, externalId);
    }
}
