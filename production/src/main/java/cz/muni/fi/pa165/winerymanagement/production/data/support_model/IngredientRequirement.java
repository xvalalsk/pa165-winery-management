package cz.muni.fi.pa165.winerymanagement.production.data.support_model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.muni.fi.pa165.winerymanagement.core.converter.QuantityJsonConverter;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents an ingredient requirement entity for a Production Item.
 */
@Entity
@Table(name = "ingredient_requirement")
public class IngredientRequirement implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ingredient_requirement")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity", columnDefinition = "TEXT")
    @Convert(converter = QuantityJsonConverter.class)
    private Quantity quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "production_item_id")
    @JsonIgnore
    private ProductionItem productionItem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public ProductionItem getProductionItem() {
        return productionItem;
    }

    public void setProductionItem(ProductionItem productionItem) {
        this.productionItem = productionItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IngredientRequirement that)) return false;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "IngredientRequirement{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
