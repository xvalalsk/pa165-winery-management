package cz.muni.fi.pa165.winerymanagement.production.exceptions;

public class ResourceNotCreatedException extends ProductionException {

    public ResourceNotCreatedException() {
    }

    @Override
    public String getMessage() {
        return "Resource already exists";
    }
}
