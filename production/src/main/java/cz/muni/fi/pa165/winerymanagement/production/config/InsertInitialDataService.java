package cz.muni.fi.pa165.winerymanagement.production.config;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Barrel;
import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.*;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import cz.muni.fi.pa165.winerymanagement.production.mappers.grape_container.GrapeContainerInfoMapper;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * This class is responsible for inserting dummy data into the application.
 * It is a SpringBoot service that is executed during the application startup.
 */
@Service
@Transactional
public class InsertInitialDataService {
    private final BarrelRepository barrelRepository;
    private final GrapeContainerRepository grapeContainerRepository;
    private final IngredientRepository ingredientRepository;
    private final ProductionItemRepository productionItemRepository;

    private final GrapeContainerInfoMapper grapeContainerInfoMapper;

    private final IngredientRequirementRepository ingredientRequirementRepository;

    @Autowired
    public InsertInitialDataService(BarrelRepository barrelRepository, GrapeContainerRepository grapeContainerRepository, IngredientRepository ingredientRepository, ProductionItemRepository productionItemRepository, GrapeContainerInfoMapper grapeContainerInfoMapper, IngredientRequirementRepository ingredientRequirementRepository) {
        this.barrelRepository = barrelRepository;
        this.grapeContainerRepository = grapeContainerRepository;
        this.ingredientRepository = ingredientRepository;
        this.productionItemRepository = productionItemRepository;
        this.grapeContainerInfoMapper = grapeContainerInfoMapper;
        this.ingredientRequirementRepository = ingredientRequirementRepository;
    }

    /**
     * This method inserts dummy data into the application.
     * It is executed automatically after the bean is created.
     */
    @PostConstruct
    public void insertDummyData() {
        // Insert Ingredients

        Ingredient ingredient1 = new Ingredient();
        ingredient1.setName("sugar");
        ingredient1.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));
        ingredientRepository.save(ingredient1);

        Ingredient ingredient2 = new Ingredient();
        ingredient2.setName("salt");
        ingredient2.setQuantity(new WeightQuantity(500, WeightQuantity.WeightUnit.GRAM));
        ingredientRepository.save(ingredient2);

        Ingredient ingredient3 = new Ingredient();
        ingredient3.setName("water");
        ingredient3.setQuantity(new VolumeQuantity(500, VolumeQuantity.VolumeUnit.LITER));
        ingredientRepository.save(ingredient3);


        // Insert GrapeType
        GrapeType grapeType1 = new GrapeType();
        grapeType1.setName("Chardonnay");
        grapeType1.setDescription("Chardonnay white sweet wine description");
        grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType1.setColor(GrapeType.Color.WHITE);

        GrapeType grapeType2 = new GrapeType();
        grapeType2.setName("Zweigeltrebe");
        grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
        grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
        grapeType2.setColor(GrapeType.Color.ROSE);

        GrapeType grapeType3 = new GrapeType();
        grapeType3.setName("Merlot");
        grapeType3.setDescription("Very good grape");
        grapeType3.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType3.setColor(GrapeType.Color.ROSE);


        // Insert GrapeContainer
        GrapeContainer grapeContainer1 = new GrapeContainer();
        grapeContainer1.setHarvestYear((2011));
        grapeContainer1.setGrapeType(grapeType1);
        grapeContainer1.setQuantity(new WeightQuantity(100, WeightQuantity.WeightUnit.KILOGRAM));
        grapeContainerRepository.save(grapeContainer1);

        GrapeContainer grapeContainer2 = new GrapeContainer();
        grapeContainer2.setHarvestYear((2018));
        grapeContainer2.setGrapeType(grapeType2);
        grapeContainer2.setQuantity(new WeightQuantity(200, WeightQuantity.WeightUnit.KILOGRAM));
        grapeContainerRepository.save(grapeContainer2);

        GrapeContainer grapeContainer3 = new GrapeContainer();
        grapeContainer3.setHarvestYear((2022));
        grapeContainer3.setGrapeType(grapeType3);
        grapeContainer3.setQuantity(new WeightQuantity(300, WeightQuantity.WeightUnit.KILOGRAM));
        grapeContainerRepository.save(grapeContainer3);

        // Insert ProductionItem
        ProductionItem productionItem1 = new ProductionItem();
        productionItem1.setGrape(grapeContainerInfoMapper.mapToDto(grapeContainer1));
        productionItem1.setGrapeQuantity(new WeightQuantity(100, WeightQuantity.WeightUnit.KILOGRAM));
        productionItem1.setYear((2020));
        IngredientRequirement productionItem1Ingredient1 = new IngredientRequirement();
        productionItem1Ingredient1.setName(ingredient1.getName());
        productionItem1Ingredient1.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));
        productionItem1Ingredient1.setProductionItem(productionItem1);
        IngredientRequirement productionItem1Ingredient2 = new IngredientRequirement();
        productionItem1Ingredient2.setName(ingredient2.getName());
        productionItem1Ingredient2.setQuantity(new WeightQuantity(5, WeightQuantity.WeightUnit.GRAM));
        productionItem1Ingredient2.setProductionItem(productionItem1);
        IngredientRequirement productionItem1Ingredient3 = new IngredientRequirement();
        productionItem1Ingredient3.setName(ingredient3.getName());
        productionItem1Ingredient3.setQuantity(new VolumeQuantity(10, VolumeQuantity.VolumeUnit.LITER));
        productionItem1Ingredient3.setProductionItem(productionItem1);
        productionItem1.setIngredients(List.of(productionItem1Ingredient1, productionItem1Ingredient2, productionItem1Ingredient3));
        productionItemRepository.save(productionItem1);
        ingredientRequirementRepository.save(productionItem1Ingredient1);
        ingredientRequirementRepository.save(productionItem1Ingredient2);
        ingredientRequirementRepository.save(productionItem1Ingredient3);

        ProductionItem productionItem2 = new ProductionItem();
        productionItem2.setGrape(grapeContainerInfoMapper.mapToDto(grapeContainer2));
        productionItem2.setGrapeQuantity(new WeightQuantity(100, WeightQuantity.WeightUnit.KILOGRAM));
        productionItem2.setYear((2018));
        IngredientRequirement productionItem2Ingredient1 = new IngredientRequirement();
        productionItem2Ingredient1.setName(ingredient1.getName());
        productionItem2Ingredient1.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));
        productionItem2Ingredient1.setProductionItem(productionItem2);
        IngredientRequirement productionItem2Ingredient2 = new IngredientRequirement();
        productionItem2Ingredient2.setName(ingredient2.getName());
        productionItem2Ingredient2.setQuantity(new WeightQuantity(5, WeightQuantity.WeightUnit.GRAM));
        productionItem2Ingredient2.setProductionItem(productionItem2);
        IngredientRequirement productionItem2Ingredient3 = new IngredientRequirement();
        productionItem2Ingredient3.setName(ingredient3.getName());
        productionItem2Ingredient3.setQuantity(new VolumeQuantity(10, VolumeQuantity.VolumeUnit.LITER));
        productionItem2Ingredient3.setProductionItem(productionItem2);
        productionItem2.setIngredients(List.of(productionItem2Ingredient1, productionItem2Ingredient2, productionItem2Ingredient3));
        productionItemRepository.save(productionItem2);
        ingredientRequirementRepository.save(productionItem2Ingredient1);
        ingredientRequirementRepository.save(productionItem2Ingredient2);
        ingredientRequirementRepository.save(productionItem2Ingredient3);

        ProductionItem productionItem3 = new ProductionItem();
        productionItem3.setGrape(grapeContainerInfoMapper.mapToDto(grapeContainer3));
        productionItem3.setGrapeQuantity(new WeightQuantity(100, WeightQuantity.WeightUnit.KILOGRAM));
        productionItem3.setYear((2019));
        IngredientRequirement productionItem3Ingredient1 = new IngredientRequirement();
        productionItem3Ingredient1.setName(ingredient1.getName());
        productionItem3Ingredient1.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));
        productionItem3Ingredient1.setProductionItem(productionItem3);
        IngredientRequirement productionItem3Ingredient2 = new IngredientRequirement();
        productionItem3Ingredient2.setName(ingredient2.getName());
        productionItem3Ingredient2.setQuantity(new WeightQuantity(5, WeightQuantity.WeightUnit.GRAM));
        productionItem3Ingredient2.setProductionItem(productionItem3);
        IngredientRequirement productionItem3Ingredient3 = new IngredientRequirement();
        productionItem3Ingredient3.setName(ingredient3.getName());
        productionItem3Ingredient3.setQuantity(new VolumeQuantity(10, VolumeQuantity.VolumeUnit.LITER));
        productionItem3Ingredient3.setProductionItem(productionItem3);
        productionItem3.setIngredients(List.of(productionItem3Ingredient1, productionItem3Ingredient2, productionItem3Ingredient3));
        productionItemRepository.save(productionItem3);
        ingredientRequirementRepository.save(productionItem3Ingredient1);
        ingredientRequirementRepository.save(productionItem3Ingredient2);
        ingredientRequirementRepository.save(productionItem3Ingredient3);

        // Insert Barrel
        Barrel barrel1 = new Barrel();
        barrel1.setGrapeType(productionItem1.getGrape().getGrapeType());
        barrel1.setHarvestYear(productionItem1.getGrape().getHarvestYear());
        barrel1.setQuantity(new VolumeQuantity(80, VolumeQuantity.VolumeUnit.LITER));
        barrelRepository.save(barrel1);

        Barrel barrel2 = new Barrel();
        barrel2.setGrapeType(productionItem2.getGrape().getGrapeType());
        barrel2.setHarvestYear(productionItem2.getGrape().getHarvestYear());
        barrel2.setQuantity(new VolumeQuantity(90, VolumeQuantity.VolumeUnit.LITER));
        barrelRepository.save(barrel2);

        Barrel barrel3 = new Barrel();
        barrel3.setGrapeType(productionItem3.getGrape().getGrapeType());
        barrel3.setHarvestYear(productionItem3.getGrape().getHarvestYear());
        barrel3.setQuantity(new VolumeQuantity(85, VolumeQuantity.VolumeUnit.LITER));
        barrelRepository.save(barrel3);
    }

    public void deleteAllData() {
        barrelRepository.deleteAll();
        ingredientRequirementRepository.deleteAll();
        productionItemRepository.deleteAll();
        grapeContainerRepository.deleteAll();
        ingredientRepository.deleteAll();
    }
}
