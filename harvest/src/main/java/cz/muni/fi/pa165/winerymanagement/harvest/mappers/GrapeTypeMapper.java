package cz.muni.fi.pa165.winerymanagement.harvest.mappers;

import cz.muni.fi.pa165.winerymanagement.harvest.api.GrapeTypeDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.GrapeTypeInputDto;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GrapeTypeMapper {
    GrapeTypeDto mapToGrapeTypeDto(GrapeType grapeType);
    List<GrapeTypeDto> mapToGrapeTypeDtoList(List<GrapeType> grapeTypes);
    GrapeType mapFromGrapeTypeDto(GrapeTypeDto grapeTypeDto);
    GrapeType mapFromGrapeTypeInputDto(GrapeTypeInputDto grapeTypeInputDto);
}
