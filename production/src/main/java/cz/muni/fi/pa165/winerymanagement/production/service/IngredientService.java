package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.IngredientRepository;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientNotCreatedException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientWithNameNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.rest.ProductionItemRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class IngredientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(IngredientService.class);
    private final IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Transactional(readOnly = true)
    public List<Ingredient> findAll() {
        LOGGER.info("Retrieving all ingredients from repository");
        return ingredientRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Ingredient findById(Long id) {
        LOGGER.info("Retrieving ingredient with id {} from repository", id);
        return ingredientRepository.findById(id).orElseThrow(() -> new IngredientWithIdNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public Ingredient findByName(String name) {
        LOGGER.info("Retrieving ingredient with name {} from repository", name);
        return ingredientRepository.findByName(name).orElseThrow(() -> new IngredientWithNameNotFoundException(name));
    }

    public Ingredient add(Ingredient newIngredient) {
        LOGGER.info("Adding new ingredient to repository: {}", newIngredient);
        isNameNotBlank(newIngredient.getName());
        isNameAvailable(newIngredient.getName());
        isQuantityPositive(newIngredient.getQuantity().getValue());
        return ingredientRepository.save(newIngredient);
    }

    public Ingredient updateById(Long id, Ingredient updatedIngredient) {
        LOGGER.info("Updating ingredient with id {} in repository by new data: {}", id, updatedIngredient);
        Ingredient ingredient = ingredientRepository.findById(id).orElseThrow(() -> new IngredientWithIdNotFoundException(id));
        ingredient.setName(updatedIngredient.getName());
        ingredient.setQuantity(updatedIngredient.getQuantity());
        ingredientRepository.save(ingredient);
        return ingredient;
    }

    public Ingredient updateByName(String name, Ingredient updatedIngredient) {
        LOGGER.info("Updating ingredient with name {} in repository by new data: {}", name, updatedIngredient);
        Ingredient ingredient = ingredientRepository.findByName(name).orElseThrow(() -> new IngredientWithNameNotFoundException(name));
        ingredient.setName(updatedIngredient.getName());
        ingredient.setQuantity(updatedIngredient.getQuantity());
        ingredientRepository.save(ingredient);
        return ingredient;
    }

    public Ingredient addQuantity(Long id, Quantity quantity) {
        LOGGER.info("Adding quantity {} to ingredient with id {} in repository", quantity, id);
        Ingredient ingredient = ingredientRepository.findById(id).orElseThrow(() -> new IngredientWithIdNotFoundException(id));
        isQuantityPositive(quantity.getValue());
        ingredient.setQuantity(ingredient.getQuantity().add(quantity));
        return ingredient;
    }

    public Ingredient subtractQuantity(Long id, Quantity quantity) {
        LOGGER.info("Subtracting quantity {} from ingredient with id {} in repository", quantity, id);
        Ingredient ingredient = ingredientRepository.findById(id).orElseThrow(() -> new IngredientWithIdNotFoundException(id));
        isQuantityPositive(quantity.getValue());
        ingredient.setQuantity(ingredient.getQuantity().subtract(quantity));
        return ingredient;
    }

    @Transactional(readOnly = true)
    public double trySubtractQuantity(Long id, Quantity quantity) {
        LOGGER.info("Trying to subtract quantity {} from ingredient with id {} in repository", quantity, id);
        Ingredient ingredient = ingredientRepository.findById(id).orElseThrow(() -> new IngredientWithIdNotFoundException(id));
        isQuantityPositive(quantity.getValue());
        return ingredient.getQuantity().calculateSubtractValue(quantity);
    }

    public Ingredient deleteById(Long id) {
        LOGGER.info("Deleting ingredient with id {} from repository", id);
        Ingredient ingredient = ingredientRepository.findById(id).orElseThrow(() -> new IngredientWithIdNotFoundException(id));
        ingredientRepository.deleteById(id);
        return ingredient;
    }

    public Ingredient deleteByName(String name) {
        LOGGER.info("Deleting ingredient with name {} from repository", name);
        Ingredient ingredient = ingredientRepository.findByName(name).orElseThrow(() -> new IngredientWithNameNotFoundException(name));
        ingredientRepository.deleteById(ingredient.getId());
        return ingredient;
    }

    private boolean isNameNotBlank(String name) {
        if (name.isBlank()) {
            throw new IngredientNotCreatedException("Ingredient's name cannot be blank.");
        }
        return true;
    }

    private boolean isNameAvailable(String newName) {
        if (ingredientRepository.findByName(newName).isPresent()) {
            throw new IngredientAlreadyExistsException(newName);
        }
        return true;
    }


    private boolean isQuantityPositive(double value) {
        if (value < 0.0) {
            throw new IngredientNotCreatedException("Ingredient's value cannot be negative.");
        }
        return true;
    }
}
