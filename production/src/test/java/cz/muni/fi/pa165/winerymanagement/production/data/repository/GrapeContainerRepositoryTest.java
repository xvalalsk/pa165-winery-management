package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.ProductionApplication;
import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ProductionApplication.class)
@Transactional
public class GrapeContainerRepositoryTest {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private GrapeContainerRepository grapeContainerRepository;

	private GrapeType grapeType1;
	private GrapeType grapeType2;
	private GrapeContainer grapeContainer1;
	private GrapeContainer grapeContainer2;

	@BeforeEach
	public void setUp() {
		grapeType1 = new GrapeType();
		grapeType1.setName("Type1");
		grapeType1.setDescription("Description1");
		grapeType1.setColor(GrapeType.Color.RED);
		grapeType1.setSweetness(GrapeType.Sweetness.DRY);

		grapeType2 = new GrapeType();
		grapeType2.setName("Type2");
		grapeType2.setDescription("Description2");
		grapeType2.setColor(GrapeType.Color.WHITE);
		grapeType2.setSweetness(GrapeType.Sweetness.SWEET);

		grapeContainer1 = new GrapeContainer();
		grapeContainer1.setGrapeType(grapeType1);
		grapeContainer1.setHarvestYear(2020);

		grapeContainer2 = new GrapeContainer();
		grapeContainer2.setGrapeType(grapeType2);
		grapeContainer2.setHarvestYear(2021);
	}

	@Test
	void saveAndFindById() {
		entityManager.persist(grapeContainer1);
		GrapeContainer foundGrapeContainer = grapeContainerRepository.findById(grapeContainer1.getId()).orElse(null);

		assertThat(foundGrapeContainer).isEqualTo(grapeContainer1);
	}

	@Test
	void findByGrapeTypeAndHarvestYear() {
		entityManager.persist(grapeContainer1);
		entityManager.persist(grapeContainer2);
		entityManager.flush();

		GrapeContainer foundGrapeContainer = grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType1, 2020);

		assertThat(foundGrapeContainer).isNotNull();
		assertThat(foundGrapeContainer.getGrapeType()).isEqualTo(grapeType1);
		assertThat(foundGrapeContainer.getHarvestYear()).isEqualTo(2020);
	}

	@Test
	void findAll() {
		entityManager.persist(grapeContainer1);
		entityManager.persist(grapeContainer2);
		entityManager.flush();

		List<GrapeContainer> grapeContainers = grapeContainerRepository.findAll();

		assertThat(grapeContainers).contains(grapeContainer1, grapeContainer2);
	}

	@Test
	void deleteById() {
		entityManager.persist(grapeContainer1);
		entityManager.persist(grapeContainer2);
		entityManager.flush();

		grapeContainerRepository.deleteById(grapeContainer1.getId());
		List<GrapeContainer> grapeContainers = grapeContainerRepository.findAll();

		assertThat(grapeContainers).doesNotContain(grapeContainer1);
		assertThat(grapeContainers).contains(grapeContainer2);
	}
}