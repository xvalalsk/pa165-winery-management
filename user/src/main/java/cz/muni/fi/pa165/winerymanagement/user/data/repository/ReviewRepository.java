package cz.muni.fi.pa165.winerymanagement.user.data.repository;

import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    Set<Review> findReviewsByWineBottleId(Long wineBottleId);

    Set<Review> findReviewsByUserId(Long userId);
}
