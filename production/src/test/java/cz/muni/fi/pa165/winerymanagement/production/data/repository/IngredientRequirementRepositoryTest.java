package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.production.ProductionApplication;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ProductionApplication.class)
@Transactional
public class IngredientRequirementRepositoryTest {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private IngredientRequirementRepository ingredientRequirementRepository;

	private IngredientRequirement ingredientRequirement;

	@BeforeEach
	public void setUp() {
		ingredientRequirement = new IngredientRequirement();
		ingredientRequirement.setName("Test Ingredient");
	}

	@Test
	void saveIngredientRequirementTest() {
		IngredientRequirement savedIngredientRequirement = ingredientRequirementRepository.save(ingredientRequirement);
		assertThat(savedIngredientRequirement).isNotNull();
		assertThat(savedIngredientRequirement.getId()).isNotNull();
	}

	@Test
	void findIngredientRequirementByIdTest() {
		entityManager.persist(ingredientRequirement);
		entityManager.flush();

		IngredientRequirement foundIngredientRequirement = ingredientRequirementRepository.findById(ingredientRequirement.getId()).orElse(null);
		assertThat(foundIngredientRequirement).isNotNull();
		assertThat(foundIngredientRequirement.getId()).isEqualTo(ingredientRequirement.getId());
	}

	@Test
	void updateIngredientRequirementTest() {
		entityManager.persist(ingredientRequirement);
		entityManager.flush();

		ingredientRequirement.setName("Updated Ingredient");

		ingredientRequirementRepository.save(ingredientRequirement);

		IngredientRequirement updatedIngredientRequirement = entityManager.find(IngredientRequirement.class, ingredientRequirement.getId());
		assertThat(updatedIngredientRequirement).isNotNull();
		assertThat(updatedIngredientRequirement.getName()).isEqualTo("Updated Ingredient");
	}

	@Test
	void deleteIngredientRequirementByIdTest() {
		entityManager.persist(ingredientRequirement);
		entityManager.flush();

		ingredientRequirementRepository.deleteById(ingredientRequirement.getId());

		IngredientRequirement deletedIngredientRequirement = entityManager.find(IngredientRequirement.class, ingredientRequirement.getId());
		assertThat(deletedIngredientRequirement).isNull();
	}

	@Test
	void findAllIngredientRequirementsTest() {
		entityManager.persist(ingredientRequirement);
		entityManager.flush();

		List<IngredientRequirement> ingredientRequirements = ingredientRequirementRepository.findAll();
		assertThat(ingredientRequirements).isNotEmpty();
		assertThat(ingredientRequirements).contains(ingredientRequirement);
	}
}