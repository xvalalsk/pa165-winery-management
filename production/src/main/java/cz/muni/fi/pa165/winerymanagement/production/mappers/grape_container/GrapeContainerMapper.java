package cz.muni.fi.pa165.winerymanagement.production.mappers.grape_container;

import cz.muni.fi.pa165.winerymanagement.production.api.GrapeContainerDto;
import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GrapeContainerMapper {
    GrapeContainerDto mapToDto(GrapeContainer grapeContainer);

    List<GrapeContainerDto> mapToList(List<GrapeContainer> barrels);

    GrapeContainer mapFromDto(GrapeContainerDto grapeContainer);
}
