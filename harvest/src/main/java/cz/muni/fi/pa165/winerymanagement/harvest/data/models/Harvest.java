package cz.muni.fi.pa165.winerymanagement.harvest.data.models;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="harvests")
public class Harvest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int harvestYear;
    @OneToMany(fetch = FetchType.LAZY , mappedBy = "harvest", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private Set<HarvestItem> items;

    public Harvest() {}

    public Harvest(int harvestYear) {
        this.harvestYear = harvestYear;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return harvestYear;
    }

    public void setYear(int harvestYear) {
        this.harvestYear = harvestYear;
    }

    public Set<HarvestItem> getItems() {
        return items;
    }

    public void setItems(Set<HarvestItem> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Harvest that = (Harvest) obj;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Harvest{" +
                "id=" + id +
                ", harvestYear=" + harvestYear +
                '}';
    }
}
