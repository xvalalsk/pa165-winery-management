package cz.muni.fi.pa165.winerymanagement.harvest.annotations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CurrentYearOrBeforeValidator.class)
@Documented
public @interface CurrentYearOrBefore {
    String message() default "The year must not be in the future.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
