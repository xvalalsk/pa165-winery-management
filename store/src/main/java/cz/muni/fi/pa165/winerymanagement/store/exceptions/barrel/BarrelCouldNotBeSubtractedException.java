package cz.muni.fi.pa165.winerymanagement.store.exceptions.barrel;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.StoreException;

import java.util.ResourceBundle;

public class BarrelCouldNotBeSubtractedException extends StoreException {
	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");

	private final GrapeType grapeType;
	private final int year;
	private final VolumeQuantity volumeQuantity;
	public BarrelCouldNotBeSubtractedException(GrapeType grapeType, int year, VolumeQuantity volumeQuantity) {
		this.grapeType = grapeType;
		this.year = year;
		this.volumeQuantity = volumeQuantity;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, grapeType.toString(), year, volumeQuantity.toString());
	}
}