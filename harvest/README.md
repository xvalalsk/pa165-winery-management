# PA165 - Winery Management

## Harvest module

### WineBottle

#### Supported operations:



> ##### Create new harvest item
>> To create a new harvest item send a POST request to /api/v1/harvests/items endpoint.
>>
>> Note: You need to have the harvest created and the production module running
>>
>> In this example we are passing following data to the POST request:
>> ```json
>> {
>>     "id": 1,
>>     "grape": {
>>         "id": 1,
>>         "name": "Merlot",
>>         "description": "Very good grape",
>>         "color": "ROSE",
>>         "sweetness": "SWEET"
>>     },
>>     "quality": "EXCELLENT",
>>     "quantity": 2343.0,
>>     "date": "2023-04-23T22:35:17.276+00:00",
>>     "harvestId": 1
>> }
>>```
>>
>
>> Run:
>> ```shell
>> curl -X 'POST' \
>> 'http://localhost:8081/api/v1/harvests/items' \
>> -H 'accept: */*' \
>> -H 'Content-Type: application/json' \
>> -d ' {
>> "id": 1,
>> "grape": {
>> "id": 1,
>> "name": "Merlot",
>> "description": "Very good grape",
>> "color": "ROSE",
>> "sweetness": "SWEET"
>> },
>> "quality": "EXCELLENT",
>> "quantity": 2343.0,
>> "date": "2023-04-23T22:35:17.276+00:00",
>> "harvestId": 1
>> }'
>>```
>>
>
>> Result:
>> ```json
>> {
>>     "id": 1,
>>     "grape": {
>>         "id": 1,
>>         "name": "Merlot",
>>         "description": "Very good grape",
>>         "color": "ROSE",
>>         "sweetness": "SWEET"
>>     },
>>     "quality": "EXCELLENT",
>>     "quantity": 2343.0,
>>     "date": "2023-04-23T22:35:17.276+00:00",
>>     "harvestId": 1
>> }
>> ```
>> 
>> 
>
>> Note: Check the production module and notice that a grape container containing requested grape and harvest year was updated 

