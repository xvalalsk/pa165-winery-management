package cz.muni.fi.pa165.winerymanagement.store.api;

import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Represents a data transfer object for a Stock of Wine Bottles.
 */
@Schema(
		name = "WineBottleStockDto",
		description = "Data transfer object representing a barrel used in the production of wine",
		example = """
                  {
                    "id": 1,
                    "wineBottle": {
						"id": 1,
						"grapeType": {
						  "name": "Chardonnay",
						  "description": "Chardonnay white sweet wine description",
						  "color": "WHITE",
						  "sweetness": "SWEET"
						},
						"harvestYear": 2020,
						"quantity": {
						  "type": "volume",
						  "value": 0.75,
						  "volumeUnit": "LITER"
						}
					},
                    "count": 250,
                    "price": 8.99
                  }
                  """
)
public class WineBottleStockDto {
	private Long id;
	private WineBottle wineBottle;
	private int count;
	private double price;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WineBottle getWineBottle() {
		return wineBottle;
	}

	public void setWineBottle(WineBottle wineBottle) {
		this.wineBottle = wineBottle;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "WineBottleStockDto{" +
				"id=" + id +
				", wineBottle=" + wineBottle +
				", count=" + count +
				", price=" + price +
				'}';
	}
}

