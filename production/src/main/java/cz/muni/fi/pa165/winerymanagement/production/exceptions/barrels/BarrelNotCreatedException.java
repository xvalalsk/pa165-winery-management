package cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels;

import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceNotCreatedException;

public class BarrelNotCreatedException extends ResourceNotCreatedException {
    private final String message;

    public BarrelNotCreatedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
