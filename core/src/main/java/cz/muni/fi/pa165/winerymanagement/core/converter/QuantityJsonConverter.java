package cz.muni.fi.pa165.winerymanagement.core.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.io.IOException;

/**
 * JPA converter for converting Quantity objects to and from JSON strings.
 */
@Converter
public class QuantityJsonConverter implements AttributeConverter<Quantity, String> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(Quantity quantity) {
        try {
            return objectMapper.writeValueAsString(quantity);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Error serializing quantity to JSON", e);
        }
    }

    @Override
    public Quantity convertToEntityAttribute(String dbData) {
        try {
            return objectMapper.readValue(dbData, Quantity.class);
        } catch (IOException e) {
            throw new IllegalArgumentException("Error deserializing quantity from JSON", e);
        }
    }
}
