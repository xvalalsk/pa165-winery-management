package cz.muni.fi.pa165.winerymanagement.production.api;

import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.GrapeContainerInfo;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * Represents a data transfer object for a Production Item.
 */
@Schema(
        name = "ProductionItemDto",
        description = "Data transfer object representing a production item used as a first step of creating a barrel of wine",
        example = """
                  {
                    "id": 1,
                    "grape": {
                      "id": 1,
                      "grapeType": {
                        "name": "Chardonnay",
                        "description": "Chardonnay white sweet wine description",
                        "color": "WHITE",
                        "sweetness": "SWEET"
                      },
                      "harvestYear": 2020
                    },
                    "grapeQuantity": {
                      "type": "weight",
                      "value": 100.0,
                      "weightUnit": "KILOGRAM"
                    },
                    "ingredients": [
                      {
                        "name": "sugar",
                        "quantity": {
                          "type": "weight",
                          "value": 10.0,
                          "weightUnit": "KILOGRAM"
                        }
                      },
                      {
                        "name": "salt",
                        "quantity": {
                          "type": "weight",
                          "value": 5.0,
                          "weightUnit": "GRAM"
                        }
                      },
                      {
                        "name": "water",
                        "quantity": {
                          "type": "volume",
                          "value": 10.0,
                          "volumeUnit": "LITER"
                        }
                      }
                    ],
                    "year": 2020
                  }
                  """
)
public class ProductionItemDto {
    private Long id;
    private GrapeContainerInfo grape;
    private WeightQuantity grapeQuantity;
    private List<IngredientRequirement> ingredients;
    private int year;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeContainerInfo getGrape() {
        return grape;
    }

    public void setGrape(GrapeContainerInfo grape) {
        this.grape = grape;
    }

    public WeightQuantity getGrapeQuantity() {
        return grapeQuantity;
    }

    public void setGrapeQuantity(WeightQuantity grapeQuantity) {
        this.grapeQuantity = grapeQuantity;
    }

    public List<IngredientRequirement> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientRequirement> ingredients) {
        this.ingredients = ingredients;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "ProductionItemDto{" +
                "id=" + id +
                ", grape=" + grape +
                ", grapeQuantity=" + grapeQuantity +
                ", ingredients=" + ingredients +
                ", year=" + year +
                '}';
    }
}
