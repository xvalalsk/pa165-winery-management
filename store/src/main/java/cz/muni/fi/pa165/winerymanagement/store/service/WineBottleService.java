package cz.muni.fi.pa165.winerymanagement.store.service;

import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleRepository;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleWithIdNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WineBottleService {
	private static final Logger LOGGER = LoggerFactory.getLogger(WineBottleService.class);
	private final WineBottleRepository wineBottleRepository;

	@Autowired
	public WineBottleService(WineBottleRepository wineBottleRepository) {
		this.wineBottleRepository = wineBottleRepository;
	}

	@Transactional(readOnly = true)
	public List<WineBottle> findAll() {
		LOGGER.info("Retrieving all wineBottles from repository.");
		return wineBottleRepository.findAll();
	}

	@Transactional(readOnly = true)
	public WineBottle findById(Long id) {
		LOGGER.info("Retrieving wineBottle with id: {} from repository.", id);
		return wineBottleRepository.findById(id).orElseThrow(() -> new WineBottleWithIdNotFoundException(id));
	}

	public WineBottle add(WineBottle newWineBottle) {
		LOGGER.info("Adding new wineBottle: {} to repository", newWineBottle);
		WineBottle wineBottle = wineBottleRepository.findByGrapeTypeAndHarvestYear(newWineBottle.getGrapeType(), newWineBottle.getHarvestYear());
		if (wineBottle != null){
			LOGGER.warn("WineBottle: {} already exists in the repository with id: {}!", newWineBottle, wineBottle.getId());
			throw new WineBottleAlreadyExistsException(wineBottle.getId());
		}
		return wineBottleRepository.save(newWineBottle);

	}

	public WineBottle updateById(Long id, WineBottle updatedWineBottle) {
		LOGGER.info("Updating wineBottle with id: {} from repository with following wineBottle: {}.", id, updatedWineBottle);
		WineBottle wineBottle = wineBottleRepository.findById(id).orElseThrow(() -> new WineBottleWithIdNotFoundException(id));
		wineBottle.setGrapeType(updatedWineBottle.getGrapeType());
		wineBottle.setHarvestYear(updatedWineBottle.getHarvestYear());
		wineBottle.setVolume(updatedWineBottle.getVolume());
		return wineBottleRepository.save(wineBottle);
	}

	public WineBottle deleteById(Long id) {
		LOGGER.info("Deleting wineBottle with id: {}.", id);
		WineBottle wineBottle = wineBottleRepository.findById(id).orElseThrow(() -> new WineBottleWithIdNotFoundException(id));
		wineBottleRepository.deleteById(id);
		return wineBottle;
	}
}
