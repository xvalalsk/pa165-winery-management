package cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.ResourceNotFoundException;

import java.util.ResourceBundle;

public class WineBottleWithGrapeTypeHarvestYearVolumeNotFoundException extends ResourceNotFoundException {
	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
	private final GrapeType grapeType;
	private final int harvestYear;
	private final VolumeQuantity bottleVolume;

	public WineBottleWithGrapeTypeHarvestYearVolumeNotFoundException(GrapeType grapeType, int harvestYear, VolumeQuantity bottleVolume) {
		this.grapeType = grapeType;
		this.harvestYear = harvestYear;
		this.bottleVolume = bottleVolume;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, grapeType.toString(), harvestYear, bottleVolume.toString());
	}

}
