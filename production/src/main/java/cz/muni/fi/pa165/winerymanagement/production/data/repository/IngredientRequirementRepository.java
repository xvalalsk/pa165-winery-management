package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRequirementRepository extends JpaRepository<IngredientRequirement, Long> {

}
