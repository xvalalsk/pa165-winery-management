package cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle;

import cz.muni.fi.pa165.winerymanagement.store.exceptions.StoreException;

import java.util.ResourceBundle;

public class NotEnoughWineBottlesException extends StoreException {
	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
	private final int count;

	public NotEnoughWineBottlesException(int count) {
		this.count = count;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, count);
	}
}