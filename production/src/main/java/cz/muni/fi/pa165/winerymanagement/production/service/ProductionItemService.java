package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.ProductionItemRepository;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.production_items.ProductionItemWithGrapeTypeAndYearNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.production_items.ProductionItemWithIdNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductionItemService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductionItemService.class);
    private final ProductionItemRepository productionItemRepository;
    private final IngredientService ingredientService;
    private final GrapeContainerService grapeContainerService;

    @Autowired
    public ProductionItemService(ProductionItemRepository productionItemRepository, IngredientService ingredientService, GrapeContainerService grapeContainerService) {
        this.productionItemRepository = productionItemRepository;
        this.ingredientService = ingredientService;
        this.grapeContainerService = grapeContainerService;
    }

    @Transactional(readOnly = true)
    public List<ProductionItem> findAll() {
        LOGGER.info("Retrieving all production items from repository");
        return productionItemRepository.findAll();
    }

    @Transactional(readOnly = true)
    public ProductionItem findById(Long id) {
        LOGGER.info("Retrieving production item with id {} from repository", id);
        return productionItemRepository.findById(id).orElseThrow(() -> new ProductionItemWithIdNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public ProductionItem findByGrapeTypeAndYear(GrapeType grapeType, int harvestYear) {
        LOGGER.info("Retrieving production item with grape type {} and harvest year {} from repository", grapeType, harvestYear);
        ProductionItem productionItem = productionItemRepository.findByGrape_GrapeTypeAndYear(grapeType, harvestYear);
        if (productionItem == null) {
            throw new ProductionItemWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        return productionItem;
    }

    public ProductionItem add(ProductionItem newProductionItem) throws NotEnoughQuantityException {
        LOGGER.info("Adding new production item {} to repository", newProductionItem);
        // check if ingredients exists and have available quantity
        for (IngredientRequirement ingredientRequirement : newProductionItem.getIngredients()) {
            Ingredient ingredient = ingredientService.findByName(ingredientRequirement.getName());
            ingredientService.trySubtractQuantity(ingredient.getId(), ingredientRequirement.getQuantity());
        }
        // check if grape container exists and have available quantity
        grapeContainerService.trySubtractQuantity(grapeContainerService.findByGrapeTypeAndHarvestYear(newProductionItem.getGrape().getGrapeType(), newProductionItem.getGrape().getHarvestYear()).getId(), newProductionItem.getGrapeQuantity());

        // process ingredients
        for (IngredientRequirement ingredientRequirement : newProductionItem.getIngredients()) {
            Ingredient ingredient = ingredientService.findByName(ingredientRequirement.getName());
            ingredientService.subtractQuantity(ingredient.getId(), ingredientRequirement.getQuantity());
        }
        // process grape container
        grapeContainerService.subtractQuantity(grapeContainerService.findByGrapeTypeAndHarvestYear(newProductionItem.getGrape().getGrapeType(), newProductionItem.getGrape().getHarvestYear()).getId(), newProductionItem.getGrapeQuantity());
        return productionItemRepository.save(newProductionItem);
    }

    public ProductionItem updateById(Long id, ProductionItem updatedProductionItem) {
        LOGGER.info("Updating production item with id {} in repository by new data: {}", id, updatedProductionItem);
        ProductionItem productionItem = productionItemRepository.findById(id).orElseThrow(() -> new ProductionItemWithIdNotFoundException(id));
        productionItem.setYear(updatedProductionItem.getYear());
        productionItem.setGrape(updatedProductionItem.getGrape());
        productionItem.setGrapeQuantity(updatedProductionItem.getGrapeQuantity());
        productionItem.setIngredients(updatedProductionItem.getIngredients());
        return productionItemRepository.save(productionItem);
    }

    public ProductionItem deleteById(Long id) {
        LOGGER.info("Deleting production item with id {} from repository", id);
        ProductionItem productionItem = productionItemRepository.findById(id).orElseThrow(() -> new ProductionItemWithIdNotFoundException(id));
        productionItemRepository.deleteById(id);
        return productionItem;
    }
}
