package cz.muni.fi.pa165.winerymanagement.store.data.repository;

import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottleStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WineBottleStockRepository extends JpaRepository<WineBottleStock, Long> {
	WineBottleStock findByWineBottle(WineBottle wineBottle);
}

