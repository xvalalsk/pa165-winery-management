package cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle;

import cz.muni.fi.pa165.winerymanagement.store.exceptions.ResourceAlreadyExistsException;

import java.util.ResourceBundle;

public class WineBottleAlreadyExistsException extends ResourceAlreadyExistsException {

	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
	private final Long id;

	public WineBottleAlreadyExistsException(Long id) {
		this.id = id;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, id);
	}


}
