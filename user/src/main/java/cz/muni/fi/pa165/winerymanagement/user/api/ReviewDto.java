package cz.muni.fi.pa165.winerymanagement.user.api;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        description = "Review DTO",
        example = """
        {
          "rating": 5,
          "comment": "This wine is great!"
        }
        """
)
public class ReviewDto {
    private int rating;
    private String comment;

    // TODO add user name to this dto


    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ReviewDto{" +
                "rating=" + rating +
                ", comment='" + comment + '\'' +
                '}';
    }
}
