package cz.muni.fi.pa165.winerymanagement.store.exceptions;

public class ResourceAlreadyExistsException extends StoreException {

	public ResourceAlreadyExistsException() {
	}

	@Override
	public String getMessage() {
		return "Resource already exists";
	}

}
