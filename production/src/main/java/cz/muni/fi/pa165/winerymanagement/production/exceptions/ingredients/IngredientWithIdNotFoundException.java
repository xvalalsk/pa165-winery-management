package cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients;

import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceNotFoundException;

import java.util.ResourceBundle;

public class IngredientWithIdNotFoundException extends ResourceNotFoundException {

    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
    private final Long id;

    public IngredientWithIdNotFoundException(Long id) {
        this.id = id;
    }

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        String message = messages.getString(key);
        return String.format(message, id);
    }
}
