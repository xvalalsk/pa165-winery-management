package cz.muni.fi.pa165.winerymanagement.production.api;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Represents a data transfer object for an Ingredient.
 */

@Schema(
        name = "IngredientDto",
        description = "Data transfer object representing an ingredient used in the production of wine",
        example = """
                  {
                    "id": 1,
                    "quantity": {
                      "type": "weight",
                      "value": 10.0,
                      "weightUnit": "KILOGRAM"
                    },
                    "name": "sugar"
                  }
                  """
)
public class IngredientDto {
    private Long id;
    private Quantity quantity;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "IngredientDto{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", name='" + name + '\'' +
                '}';
    }
}
