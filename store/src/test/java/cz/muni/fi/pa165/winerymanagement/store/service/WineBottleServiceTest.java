package cz.muni.fi.pa165.winerymanagement.store.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleRepository;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleWithIdNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WineBottleServiceTest {

    @Mock
    private WineBottleRepository wineBottleRepository;
    @InjectMocks
    private WineBottleService wineBottleService;

    private WineBottle wineBottle1;
    private WineBottle wineBottle2;

    private GrapeType grapeType1;
    private GrapeType grapeType2;

    @BeforeEach
    public void setUp() {
        grapeType1 = new GrapeType();
        grapeType1.setName("Chardonnay");
        grapeType1.setDescription("description");
        grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType1.setColor(GrapeType.Color.WHITE);

        grapeType2 = new GrapeType();
        grapeType2.setName("Merlot");
        grapeType2.setDescription("description");
        grapeType2.setSweetness(GrapeType.Sweetness.DRY);
        grapeType2.setColor(GrapeType.Color.RED);


        wineBottle1 = new WineBottle();
        wineBottle1.setId(1L);
        wineBottle1.setVolume(new VolumeQuantity(100.0, VolumeQuantity.VolumeUnit.LITER));
        wineBottle1.setHarvestYear(2020);
        wineBottle1.setGrapeType(grapeType1);

        wineBottle2 = new WineBottle();
        wineBottle2.setId(2L);
        wineBottle2.setVolume(new VolumeQuantity(150.0, VolumeQuantity.VolumeUnit.LITER));
        wineBottle2.setHarvestYear(2018);
        wineBottle2.setGrapeType(grapeType2);
    }

    @Test
    void testFindAll() {
        when(wineBottleRepository.findAll()).thenReturn(Arrays.asList(wineBottle1, wineBottle2));

        List<WineBottle> wineBottles = wineBottleService.findAll();

        assertEquals(2, wineBottles.size());
        assertTrue(wineBottles.contains(wineBottle1));
        assertTrue(wineBottles.contains(wineBottle2));
    }

    @Test
    void testFindById() {
        when(wineBottleRepository.findById(1L)).thenReturn(Optional.of(wineBottle1));

        WineBottle foundWineBottle = wineBottleService.findById(1L);

        assertNotNull(foundWineBottle);
        assertEquals(wineBottle1, foundWineBottle);
    }

    @Test
    void testFindByIdNotFound() {
        when(wineBottleRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(WineBottleWithIdNotFoundException.class, () -> wineBottleService.findById(1L));
    }

    @Test
    void testAdd() {
        when(wineBottleRepository.findByGrapeTypeAndHarvestYear(any(GrapeType.class), anyInt())).thenReturn(null);
        when(wineBottleRepository.save(any(WineBottle.class))).thenReturn(wineBottle1);

        WineBottle addedWineBottle = wineBottleService.add(wineBottle1);

        assertNotNull(addedWineBottle);
        assertEquals(wineBottle1, addedWineBottle);
    }

    @Test
    void testAddAlreadyExists() {
        when(wineBottleRepository.findByGrapeTypeAndHarvestYear(any(GrapeType.class), anyInt())).thenReturn(wineBottle1);

        assertThrows(WineBottleAlreadyExistsException.class, () -> wineBottleService.add(wineBottle1));
    }

    @Test
    void testUpdateById() {
        when(wineBottleRepository.findById(1L)).thenReturn(Optional.of(wineBottle1));
        when(wineBottleRepository.save(any(WineBottle.class))).thenReturn(wineBottle1);

        WineBottle updatedWineBottle = wineBottleService.updateById(1L, wineBottle1);

        assertNotNull(updatedWineBottle);
        assertEquals(wineBottle1, updatedWineBottle);
    }

    @Test
    void testUpdateByIdNotFound() {
        when(wineBottleRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(WineBottleWithIdNotFoundException.class, () -> wineBottleService.updateById(1L, wineBottle1));
    }

    @Test
    void testDeleteById() {
        when(wineBottleRepository.findById(1L)).thenReturn(Optional.of(wineBottle1));

        WineBottle deletedWineBottle = wineBottleService.deleteById(1L);

        assertNotNull(deletedWineBottle);
        assertEquals(wineBottle1, deletedWineBottle);
        verify(wineBottleRepository).deleteById(1L);
    }

    @Test
    void testDeleteByIdNotFound() {
        when(wineBottleRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(WineBottleWithIdNotFoundException.class, () -> wineBottleService.deleteById(1L));
    }
}