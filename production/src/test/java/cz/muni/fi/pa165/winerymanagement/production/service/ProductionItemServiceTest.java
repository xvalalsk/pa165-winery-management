package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.ProductionItemRepository;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.GrapeContainerInfo;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.production_items.ProductionItemWithGrapeTypeAndYearNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.production_items.ProductionItemWithIdNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductionItemServiceTest {

	@Mock
	private ProductionItemRepository productionItemRepository;

	@Mock
	private IngredientService ingredientService;

	@Mock
	private GrapeContainerService grapeContainerService;

	@InjectMocks
	private ProductionItemService productionItemService;

	private ProductionItem productionItem1;
	private ProductionItem productionItem2;
	private IngredientRequirement ingredientRequirement1;
	private IngredientRequirement ingredientRequirement2;

	private GrapeType grapeType1;
	private GrapeType grapeType2;

	private GrapeContainerInfo grapeContainerInfo1;
	private GrapeContainerInfo grapeContainerInfo2;


	@BeforeEach
	public void setUp() {
		grapeType1 = new GrapeType();
		grapeType1.setName("Souvignon");
		grapeType1.setColor(GrapeType.Color.RED);
		grapeType1.setDescription("description");
		grapeType1.setSweetness(GrapeType.Sweetness.SWEET);

		grapeType2 = new GrapeType();
		grapeType2.setName("Merlot");
		grapeType2.setColor(GrapeType.Color.WHITE);
		grapeType2.setDescription("Merlot_description");
		grapeType2.setSweetness(GrapeType.Sweetness.DRY);

		grapeContainerInfo1 = new GrapeContainerInfo();
		grapeContainerInfo1.setGrapeType(grapeType1);
		grapeContainerInfo1.setHarvestYear(2020);

		grapeContainerInfo2 = new GrapeContainerInfo();
		grapeContainerInfo2.setGrapeType(grapeType2);
		grapeContainerInfo2.setHarvestYear(2022);

		productionItem1 = new ProductionItem();
		productionItem1.setId(1L);
		productionItem1.setYear(2020);
		productionItem1.setGrape(grapeContainerInfo1);
		productionItem1.setGrapeQuantity(new WeightQuantity(100.0, WeightQuantity.WeightUnit.KILOGRAM));

		productionItem2 = new ProductionItem();
		productionItem2.setId(2L);
		productionItem2.setYear(2021);
		productionItem2.setGrape(grapeContainerInfo2);
		productionItem2.setGrapeQuantity(new WeightQuantity(200.0, WeightQuantity.WeightUnit.KILOGRAM));

		ingredientRequirement1 = new IngredientRequirement();
		ingredientRequirement1.setName("Ingredient1");
		ingredientRequirement1.setQuantity(new WeightQuantity(100.0, WeightQuantity.WeightUnit.KILOGRAM));

		ingredientRequirement2 = new IngredientRequirement();
		ingredientRequirement2.setName("Ingredient2");
		ingredientRequirement2.setQuantity(new VolumeQuantity(100.0, VolumeQuantity.VolumeUnit.LITER));

		productionItem1.setIngredients(Collections.singletonList(ingredientRequirement1));
		productionItem2.setIngredients(Collections.singletonList(ingredientRequirement2));
	}

	@Test
	void findAllTest() {
		when(productionItemRepository.findAll()).thenReturn(Arrays.asList(productionItem1, productionItem2));

		List<ProductionItem> productionItems = productionItemService.findAll();

		assertEquals(2, productionItems.size());
		assertTrue(productionItems.contains(productionItem1));
		assertTrue(productionItems.contains(productionItem2));

		verify(productionItemRepository, times(1)).findAll();
	}

	@Test
	void findByIdTest() {
		when(productionItemRepository.findById(1L)).thenReturn(Optional.of(productionItem1));

		ProductionItem foundProductionItem = productionItemService.findById(1L);

		assertEquals(productionItem1, foundProductionItem);
		verify(productionItemRepository, times(1)).findById(1L);
	}

	@Test
	void findByIdNotFoundTest() {
		when(productionItemRepository.findById(1L)).thenReturn(Optional.empty());

		assertThrows(ProductionItemWithIdNotFoundException.class, () -> productionItemService.findById(1L));
		verify(productionItemRepository, times(1)).findById(1L);
	}

	@Test
	void findByGrapeTypeAndYearTest() {
		when(productionItemRepository.findByGrape_GrapeTypeAndYear(grapeType1, 2020)).thenReturn(productionItem1);

		ProductionItem foundProductionItem = productionItemService.findByGrapeTypeAndYear(grapeType1, 2020);

		assertEquals(productionItem1, foundProductionItem);
		verify(productionItemRepository, times(1)).findByGrape_GrapeTypeAndYear(grapeType1, 2020);
	}

	@Test
	void findByGrapeTypeAndYearNotFoundTest() {
		when(productionItemRepository.findByGrape_GrapeTypeAndYear(grapeType1, 2020)).thenReturn(null);

		assertThrows(ProductionItemWithGrapeTypeAndYearNotFoundException.class, () -> productionItemService.findByGrapeTypeAndYear(grapeType1, 2020));
		verify(productionItemRepository, times(1)).findByGrape_GrapeTypeAndYear(grapeType1, 2020);
	}

	@Test
	void addTest() throws NotEnoughQuantityException {
		Ingredient ingredient1 = new Ingredient();
		ingredient1.setId(1L);
		ingredient1.setName("Ingredient1");
		ingredient1.setQuantity(new WeightQuantity(20.0, WeightQuantity.WeightUnit.KILOGRAM));

		GrapeContainer grapeContainer = new GrapeContainer();
		grapeContainer.setId(1L);
		grapeContainer.setGrapeType(grapeType1);
		grapeContainer.setQuantity(new WeightQuantity(100.0, WeightQuantity.WeightUnit.KILOGRAM));
		grapeContainer.setHarvestYear(2022);

		when(ingredientService.findByName("Ingredient1")).thenReturn(ingredient1);
		when(grapeContainerService.findByGrapeTypeAndHarvestYear(productionItem1.getGrape().getGrapeType(), productionItem1.getGrape().getHarvestYear())).thenReturn(grapeContainer);
		when(productionItemRepository.save(any(ProductionItem.class))).thenReturn(productionItem1);

		ProductionItem addedProductionItem = productionItemService.add(productionItem1);

		assertEquals(productionItem1, addedProductionItem);
		verify(ingredientService, times(2)).findByName("Ingredient1");
		verify(grapeContainerService, times(2)).findByGrapeTypeAndHarvestYear(grapeType1, 2020);
		verify(productionItemRepository, times(1)).save(any(ProductionItem.class));
	}

	@Test
	void updateByIdTest() {
		when(productionItemRepository.findById(1L)).thenReturn(Optional.of(productionItem1));
		when(productionItemRepository.save(any(ProductionItem.class))).thenReturn(productionItem2);

		ProductionItem updatedProductionItem = productionItemService.updateById(1L, productionItem2);

		assertEquals(productionItem2, updatedProductionItem);
		verify(productionItemRepository, times(1)).findById(1L);
		verify(productionItemRepository, times(1)).save(any(ProductionItem.class));
	}

	@Test
	void deleteByIdTest() {
		when(productionItemRepository.findById(1L)).thenReturn(Optional.of(productionItem1));

		ProductionItem deletedProductionItem = productionItemService.deleteById(1L);

		assertEquals(productionItem1, deletedProductionItem);
		verify(productionItemRepository, times(1)).findById(1L);
		verify(productionItemRepository, times(1)).deleteById(1L);
	}
}