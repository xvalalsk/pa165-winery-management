package cz.muni.fi.pa165.winerymanagement.user.service;

import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuthorizationService {
    private final UserRepository userRepository;

    @Autowired
    public AuthorizationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User register(User newUser) {
        return this.userRepository.findByIssuerAndExternalId(newUser.getIssuer(), newUser.getExternalId()).orElseGet(() -> this.userRepository.save(newUser));
    }
}
