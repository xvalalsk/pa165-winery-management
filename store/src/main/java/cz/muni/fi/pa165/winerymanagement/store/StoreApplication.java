package cz.muni.fi.pa165.winerymanagement.store;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "PA165 Winery management",
                version = "1.0",
                description = "API describing module for storing, selling and bottling wines"
        )
)
public class StoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
        };
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests()
                .requestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**")).permitAll()
                .and()
                .csrf().ignoringRequestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**"))
                .and()
                .headers(headers -> headers.frameOptions().sameOrigin());
        http.csrf().disable();
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/actuator/**", "/h2-console/**", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/v1/store/wine-bottles/**").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/v1/store/clear", "/api/v1/store/seed").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers(HttpMethod.POST, "/api/v1/store/wine-bottles").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers(HttpMethod.PUT, "/api/v1/store/wine-bottles/*").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers(HttpMethod.DELETE, "/api/v1/store/wine-bottles/*").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                        .requestMatchers("/api/v1/store/wine-bottles-in-stock/**").hasAuthority("SCOPE_test_2")
                        .anyRequest().denyAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return http.build();
    }

}
