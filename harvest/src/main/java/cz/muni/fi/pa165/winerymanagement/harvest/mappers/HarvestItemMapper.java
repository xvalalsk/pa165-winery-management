package cz.muni.fi.pa165.winerymanagement.harvest.mappers;

import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemCreateDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemUpdateDto;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.HarvestItem;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HarvestItemMapper {

    @Mapping(source = "harvest.id", target = "harvestId")
    HarvestItemDto mapToHarvestItemDto(HarvestItem harvestItem);
    List<HarvestItemDto> mapToHarvestItemDtoList(List<HarvestItem> harvestItems);
    @InheritInverseConfiguration(name = "mapToHarvestItemDto")
    HarvestItem mapFromHarvestItemDto(HarvestItemDto harvestItemDto);
    @Mapping(source = "harvest.id", target = "harvestId")
    HarvestItemCreateDto mapToHarvestItemCreateDto(HarvestItem harvestItemCreateDto);
    @InheritInverseConfiguration(name = "mapToHarvestItemCreateDto")
    HarvestItem mapFromHarvestItemCreateDto(HarvestItemCreateDto harvestItemCreateDto);
    HarvestItem mapFromHarvestItemUpdateDto(HarvestItemUpdateDto harvestItemUpdateDto);

}



