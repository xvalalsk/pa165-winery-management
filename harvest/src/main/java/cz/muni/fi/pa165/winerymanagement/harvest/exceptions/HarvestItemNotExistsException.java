package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;

/**
 * Thrown when the Harvest Item has not been found.
 */
public class HarvestItemNotExistsException extends RuntimeException {
    public HarvestItemNotExistsException() {
        super();
    }

    public HarvestItemNotExistsException(Long id) {
        super("Harvest item with id " + id + " does not exist");
    }
}
