package cz.muni.fi.pa165.winerymanagement.store.rest;

import cz.muni.fi.pa165.winerymanagement.store.config.InsertInitialStoreDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/store")
public class DataController {

    private final InsertInitialStoreDataService insertInitialDataService;

    @Autowired
    public DataController(InsertInitialStoreDataService insertInitialDataService) {
        this.insertInitialDataService = insertInitialDataService;
    }

    @PostMapping("/seed")
    public void seedData() {
        insertInitialDataService.insertDummyData();
    }

    @PostMapping("/clear")
    public void clearData() {
        insertInitialDataService.deleteAllData();
    }
}
