package cz.muni.fi.pa165.winerymanagement.production.mappers;

import cz.muni.fi.pa165.winerymanagement.production.api.IngredientDto;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IngredientMapper {
    IngredientDto mapToDto(Ingredient ingredient);

    List<IngredientDto> mapToList(List<Ingredient> ingredients);

    Ingredient mapFromDto(IngredientDto ingredientDto);
}
