package cz.muni.fi.pa165.winerymanagement.store.exceptions.barrel;

import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.StoreException;

import java.util.ResourceBundle;

public class BarrelQuantityNotAvailableException extends StoreException {
	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");

	private final double availableQuantity;
	private final double requestedQuantity;
	public BarrelQuantityNotAvailableException(double availableQuantity, double requestedQuantity) {
		this.availableQuantity = availableQuantity;
		this.requestedQuantity = requestedQuantity;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, availableQuantity, requestedQuantity);
	}
}