package cz.muni.fi.pa165.winerymanagement.store.rest.exceptionhandling;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.WrongQuantityTypeException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.ResourceAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.StoreException;
import cz.muni.fi.pa165.winerymanagement.store.service.WineBottleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomRestGlobalExceptionHandling {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomRestGlobalExceptionHandling.class);
	@ExceptionHandler(ResourceAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	@ResponseBody
	public ErrorMessage handleConflict(ResourceAlreadyExistsException ex) {
		LOGGER.error("Throwing exception: {} with 409 response status", ex.getMessage());
		return new ErrorMessage(ex.getMessage());
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorMessage handleNotFound(ResourceNotFoundException ex) {
		LOGGER.error("Throwing exception: {} with 404 response status", ex.getMessage());
		return new ErrorMessage(ex.getMessage());
	}

	@ExceptionHandler({StoreException.class, NotEnoughQuantityException.class, WrongQuantityTypeException.class})
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	@ResponseBody
	public ErrorMessage handleNotAcceptable(Exception ex) {
		LOGGER.error("Throwing exception: {} with 406 response status", ex.getMessage());
		return new ErrorMessage(ex.getMessage());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorMessage handleException(Exception ex) {
		LOGGER.error("Throwing exception: {} with 500 response status", ex.getMessage());
		return new ErrorMessage("Internal server error occurred");
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class ErrorMessage {

		private String message;

		public ErrorMessage(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}
}
