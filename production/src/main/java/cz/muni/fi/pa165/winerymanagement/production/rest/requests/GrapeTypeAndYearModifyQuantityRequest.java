package cz.muni.fi.pa165.winerymanagement.production.rest.requests;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "GrapeTypeAndYearModifyQuantityRequest",
        description = "Request object to modify the quantity of a specific grape type and harvest year.",
        example = """
                    {
                        "grapeTypeAndYearGetRequest": {
                            "grapeType": {
                                "name": "Chardonnay",
                                "description": "Chardonnay white sweet wine description",
                                "color": "WHITE",
                                "sweetness": "SWEET"
                            },
                            "harvestYear": 2020
                        },
                        "quantity": {
                            "type": "weight",
                            "value": 100.0,
                            "weightUnit": "KILOGRAM"
                        }
                    }
                """
)
public class GrapeTypeAndYearModifyQuantityRequest {
    private GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest;
    private Quantity quantity;

    public GrapeTypeAndYearGetRequest getGrapeTypeAndYearGetRequest() {
        return grapeTypeAndYearGetRequest;
    }

    public void setGrapeTypeAndYearGetRequest(GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        this.grapeTypeAndYearGetRequest = grapeTypeAndYearGetRequest;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }
}
