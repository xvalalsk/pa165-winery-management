package cz.muni.fi.pa165.winerymanagement.store.exceptions;

public class StoreException extends RuntimeException {

	public StoreException() {
	}

	@Override
	public String getMessage() {
		return "Something went wrong";
	}

}
