package cz.muni.fi.pa165.winerymanagement.harvest.data.repository;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HarvestRepository extends JpaRepository<Harvest, Long> {
    Harvest findByHarvestYear(int harvestYear);
}
