package cz.muni.fi.pa165.winerymanagement.user.rest;

import cz.muni.fi.pa165.winerymanagement.user.UserApplication;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewCreateDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewUpdateDto;
import cz.muni.fi.pa165.winerymanagement.user.exceptions.InvalidReviewException;
import cz.muni.fi.pa165.winerymanagement.user.facade.ReviewFacade;
import cz.muni.fi.pa165.winerymanagement.user.rest.ReviewController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureJsonTesters
@WebMvcTest(ReviewController.class)
@ContextConfiguration(classes = UserApplication.class)
class ReviewControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewFacade reviewFacade;

    @Autowired
    private JacksonTester<ReviewCreateDto> jsonReviewCreateDto;
    private ReviewCreateDto reviewCreateDto;

    @BeforeEach
    void setUp() {
        reviewCreateDto = new ReviewCreateDto();
        reviewCreateDto.setWineBottleId(1);
        reviewCreateDto.setComment("test");
        reviewCreateDto.setRating(1);
        reviewCreateDto.setUserId(1);
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_1"})
    void addReview() throws Exception {
        mockMvc.perform(post("/api/v1/reviews")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonReviewCreateDto.write(reviewCreateDto).getJson()))
                .andExpect(status().isCreated());

        var reviewCaptor = ArgumentCaptor.forClass(ReviewCreateDto.class);
        verify(reviewFacade).addReview(reviewCaptor.capture());

        assertThat(reviewCaptor.getValue().getComment()).isEqualTo("test");
        assertThat(reviewCaptor.getValue().getRating()).isEqualTo(1);
    }

    void addReviewUnauthorized() throws Exception {
        mockMvc.perform(post("/api/v1/reviews")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonReviewCreateDto.write(reviewCreateDto).getJson()))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void addReviewException() throws Exception {
        when(reviewFacade.addReview(any())).thenThrow(new InvalidReviewException("Invalid message"));

        mockMvc.perform(post("/api/v1/reviews")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonReviewCreateDto.write(reviewCreateDto).getJson()))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_1"})
    void updateReview() throws Exception {
        mockMvc.perform(put("/api/v1/reviews/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonReviewCreateDto.write(reviewCreateDto).getJson()))
                .andExpect(status().isOk());

        var reviewCaptor = ArgumentCaptor.forClass(ReviewUpdateDto.class);
        verify(reviewFacade).updateReview(reviewCaptor.capture());

        assertThat(reviewCaptor.getValue().getComment()).isEqualTo("test");
        assertThat(reviewCaptor.getValue().getRating()).isEqualTo(1);
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_1"})
    void updateReviewException() throws Exception {
        when(reviewFacade.updateReview(any())).thenThrow(new InvalidReviewException("Invalid message"));

        mockMvc.perform(put("/api/v1/reviews/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonReviewCreateDto.write(reviewCreateDto).getJson()))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_1"})
    void deleteReview() throws Exception {
        mockMvc.perform(delete("/api/v1/reviews/1")).andExpect(status().isOk());

        verify(reviewFacade, times(1)).deleteReview(1L);
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_1"})
    void getReviewsForUser() throws Exception {
        mockMvc.perform(get("/api/v1/reviews/user/5")).andExpect(status().isOk());

        verify(reviewFacade, times(1)).getUserReviews(5L);
    }

    @Test
    void getReviewsForBottle() throws Exception {
        mockMvc.perform(get("/api/v1/reviews/bottle/7")).andExpect(status().isOk());

        verify(reviewFacade, times(1)).getWineBottleReviews(7L);
    }
}
