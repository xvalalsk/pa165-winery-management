import {useEffect, useState} from 'react';
import {NavBar} from "@/pages";
import Cookies from "js-cookie";

const ProductionItems = () => {
    const [token, setToken] = useState(null);
    const [productionItems, setProductionItems] = useState([]);
    const [error, setError] = useState('');
    const [grapeId, setGrapeId] = useState('');
    const [grapeQuantityValue, setGrapeQuantityValue] = useState('');
    const [grapeContainers, setGrapeContainers] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [selectedGrapeContainer, setSelectedGrapeContainer] = useState(null);
    const [selectedIngredients, setSelectedIngredients] = useState([]);
    const [selectedIngredient, setSelectedIngredient] = useState(null);
    const [grapeQuantityUnit, setGrapeQuantityUnit] = useState('KILOGRAM');
    const [selectedIngredientUnits, setSelectedIngredientUnits] = useState([]);


    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        fetchProductionItems();
        fetchGrapeContainers();
        fetchGIngredients();
    }, []);

    useEffect(() => {
        setSelectedIngredientUnits(ingredients.map(ingredient => ({
            ingredientID: ingredient.id,
            unit: ingredient.quantity.type === 'weight' ? ingredient.quantity.weightUnit : ingredient.quantity.volumeUnit
        })));
    }, [ingredients]);


    const handleAddIngredientUnit = (ingredientID, unit) => {
        setSelectedIngredientUnits(
            selectedIngredientUnits.map((selectedUnit) =>
                selectedUnit && selectedUnit.ingredientID === ingredientID
                    ? {...selectedUnit, unit}
                    : selectedUnit
            )
        );
    };


    const handleIngredientChange = (e) => {
        const selectedId = parseInt(e.target.value);
        const selected = ingredients.find((ingredient) => ingredient.id === selectedId);
        setSelectedIngredients(selectedIngredients.concat(selected));
    };

    const handleGrapeContainerChange = (e) => {
        const selectedId = parseInt(e.target.value);
        const selected = grapeContainers.find((container) => container.id === selectedId);
        setSelectedGrapeContainer(selected);
    };

    const handleAddIngredient = (ingredientId, quantity) => {
        if (selectedIngredients.find((ingredient) => ingredient.id === ingredientId)) {
            return;
        }
        const ingredient = ingredients.find((ingredient) => ingredient.id === ingredientId);
        setSelectedIngredients([
            ...selectedIngredients,
            {
                ...ingredient,
                quantity: {
                    ...ingredient.quantity,
                    value: parseFloat(quantity),
                },
            },
        ]);
    };

    const handleRemoveIngredient = (ingredientId) => {
        setSelectedIngredients(selectedIngredients.filter((ingredient) => ingredient.id !== ingredientId));
    };
    const handleAddIngredientValue = (ingredientID, value, unit) => {
        const originalIngredient = ingredients.find((ingredient) => ingredient.id === ingredientID);

        setSelectedIngredients(
            selectedIngredients.map((ingredient) =>
                ingredient.id === ingredientID
                    ? {
                        ...ingredient,
                        quantity: {
                            ...ingredient.quantity,
                            value: parseFloat(value),
                            ...(originalIngredient.quantity.weightUnit && {weightUnit: originalIngredient.quantity.weightUnit}),
                            ...(originalIngredient.quantity.volumeUnit && {volumeUnit: originalIngredient.quantity.volumeUnit}),
                        },
                    }
                    : ingredient
            )
        );
        setSelectedIngredientUnits(
            selectedIngredientUnits.map((selectedUnit) =>
                selectedUnit.ingredientID === ingredientID
                    ? {...selectedUnit, unit}
                    : selectedUnit
            )
        );
    };


    const handleAddProductionItem = (e) => {
        e.preventDefault();

        const {quantity, ...selectedGrapeContainerWithoutQuantity} = selectedGrapeContainer
        const newProductionItem = {
            grape: selectedGrapeContainerWithoutQuantity,
            grapeQuantity: {
                type: 'weight',
                value: parseFloat(grapeQuantityValue),
                weightUnit: grapeQuantityUnit,
            },
            year: new Date().getFullYear(),
            ingredients: selectedIngredients.map((ingredient, index) => {
                const {id, ...rest} = ingredient;
                const selectedUnit = selectedIngredientUnits.find(
                    (selectedUnit) => selectedUnit.ingredientID === id
                );
                const isWeightUnit = ['KILOGRAM', 'GRAM'].includes(selectedUnit.unit);
                const isVolumeUnit = ['LITER', 'MILLILITER'].includes(selectedUnit.unit);
                return {
                    ...rest,
                    quantity: {
                        ...rest.quantity,
                        ...(isWeightUnit ? {weightUnit: selectedUnit.unit} : {}),
                        ...(isVolumeUnit ? {volumeUnit: selectedUnit.unit} : {}),
                    },
                };
            }),
        };

        fetch('/api/v1/production/production-items', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(newProductionItem),
        })
            .then(async (response) => {
                if (!response.ok) {
                    const res = await response.json()
                    throw new Error(res.message);
                }
                fetchProductionItems();
                fetchGrapeContainers();
                fetchGIngredients();
                setSelectedGrapeContainer(null);
                setSelectedIngredients([]);
                setError("");
            })
            .catch((error) => {
                setError(error.message)
            });
    };


    const fetchProductionItems = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/production-items', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                setProductionItems(data)
            })
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const fetchGrapeContainers = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/grape-containers', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setGrapeContainers(data))
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const fetchGIngredients = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/ingredients', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setIngredients(data))
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const handleDelete = (id) => {
        fetch(`/api/v1/production/production-items/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Failed to delete the production item');
                }
                fetchProductionItems(); // Refresh the production items list after deletion
            })
            .catch((error) => setError(error.message));
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar/>
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Production Items</h1>
                <div className="mt-4">
                    <table className="table table-striped table-hover table-bordered">
                        <thead className="bg-secondary bg-gradient">
                        <tr>
                            <th scope="col">Grape Type</th>
                            <th scope="col">Description</th>
                            <th scope="col">Color</th>
                            <th scope="col">Sweetness</th>
                            <th scope="col">Harvest Year</th>
                            <th scope="col">Grape Quantity</th>
                            <th scope="col">Used Ingredients</th>
                            <th scope="col">Year</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {productionItems?.map((item) => (
                            <tr key={item.id}>
                                <td>{item.grape.grapeType.name}</td>
                                <td>{item.grape.grapeType.description}</td>
                                <td>{item.grape.grapeType.color}</td>
                                <td>{item.grape.grapeType.sweetness}</td>
                                <td>{item.grape.harvestYear}</td>
                                <td>{`${item.grapeQuantity.value} ${item.grapeQuantity.weightUnit}`}</td>
                                <td>
                                    <ul>
                                        {item.ingredients.map((ingredient) => (
                                            <li key={ingredient.name}>
                                                {ingredient.name}: {ingredient.quantity.value}{' '}
                                                {ingredient.quantity.symbol}
                                            </li>
                                        ))}
                                    </ul>
                                </td>
                                <td>{item.year}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => handleDelete(item.id)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>

                    <div className="card mt-4">
                        <div className="card-body">
                            <h5 className="card-title">Add a new production item</h5>
                            <form onSubmit={handleAddProductionItem}>
                                <div className="mb-3">
                                    <label htmlFor="grapeId" className="form-label">
                                        Grape Container
                                    </label>
                                    <select
                                        className="form-select"
                                        id="grapeId"
                                        value={selectedGrapeContainer?.id || ''}
                                        onChange={handleGrapeContainerChange}
                                        required
                                    >
                                        <option value="">Choose a grape container</option>
                                        {grapeContainers.map((container) => (
                                            <option key={container.id} value={container.id}>
                                                {container.grapeType.name} ({container.harvestYear})
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                {selectedGrapeContainer && (
                                    <>
                                        <div className="mb-3">
                                            <label htmlFor="grapeQuantity" className="form-label">
                                                Grape Quantity
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                id="grapeQuantity"
                                                placeholder={`Enter quantity for ${selectedGrapeContainer.grapeType.name}`}
                                                onChange={(e) => setGrapeQuantityValue(e.target.value)}
                                                required
                                            />
                                        </div>
                                        <div className="mb-3">
                                            <label htmlFor="grapeQuantityUnit" className="form-label">
                                                Grape Quantity Unit
                                            </label>
                                            <select
                                                className="form-select"
                                                id="grapeQuantityUnit"
                                                value={grapeQuantityUnit}
                                                onChange={(e) => setGrapeQuantityUnit(e.target.value)}
                                                required
                                            >
                                                <option value="KILOGRAM">Kilogram</option>
                                                <option value="GRAM">Gram</option>
                                            </select>
                                        </div>
                                    </>
                                )}
                                <div className="mb-3">
                                    <label htmlFor="ingredientId" className="form-label">
                                        Ingredient
                                    </label>
                                    <select
                                        className="form-select"
                                        id="ingredientId"
                                        value={selectedIngredient?.id || ''}
                                        onChange={handleIngredientChange}
                                    >
                                        <option value="">Choose an ingredient</option>
                                        {ingredients
                                            .filter(
                                                (ingredient) =>
                                                    !selectedIngredients.some(
                                                        (selectedIngredient) => selectedIngredient.id === ingredient.id
                                                    )
                                            )
                                            .map((ingredient) => (
                                                <option key={ingredient.id} value={ingredient.id}>
                                                    {ingredient.name}
                                                </option>
                                            ))}
                                    </select>
                                </div>
                                {selectedIngredients.map((selectedIngredient, index) => (
                                    <div key={index} className="mb-3">
                                        <label htmlFor={`ingredient-${selectedIngredient.id}`} className="form-label">
                                            {selectedIngredient.name}
                                        </label>
                                        <input
                                            type="number"
                                            className="form-control"
                                            id={`ingredient-${selectedIngredient.id}`}
                                            placeholder={`Enter quantity for ${selectedIngredient.name}`}
                                            value={selectedIngredients[index].value}
                                            required
                                            onChange={(e) => handleAddIngredientValue(selectedIngredient.id, e.target.value)}
                                        />
                                        <select
                                            className="form-select"
                                            value={
                                                selectedIngredientUnits.find(
                                                    (selectedUnit) => selectedUnit.ingredientID === selectedIngredient.id
                                                )?.unit ?? ''
                                            }
                                            onChange={(e) => {
                                                handleAddIngredientUnit(selectedIngredient.id, e.target.value);
                                            }
                                            }
                                            required
                                        >
                                            {selectedIngredient.quantity.type === "weight" ? (
                                                <>
                                                    <option value="GRAM">Gram</option>
                                                    <option value="KILOGRAM">Kilogram</option>
                                                </>
                                            ) : (
                                                <>
                                                    <option value="MILLILITER">Milliliter</option>
                                                    <option value="LITER">Liter</option>
                                                </>
                                            )}
                                        </select>
                                        <button
                                            type="button"
                                            className="btn btn-danger mt-2"
                                            onClick={() => handleRemoveIngredient(selectedIngredient.id)}
                                        >
                                            Remove {selectedIngredient.name}
                                        </button>
                                    </div>
                                ))}
                                <button type="submit" className="btn btn-primary">
                                    Add Production Item
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                {error && (
                    <div className="alert alert-danger mt-3" role="alert">
                        {error}
                    </div>
                )}
            </div>
        </>
    );
};

export default ProductionItems;