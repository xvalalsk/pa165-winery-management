package cz.muni.fi.pa165.winerymanagement.harvest.config;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Color;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeQuality;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.HarvestItem;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Sweetness;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.GrapeTypeRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestItemRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Transactional
public class InsertInitialDataService {
    private final HarvestRepository harvestRepository;
    private final HarvestItemRepository harvestItemRepository;
    private final GrapeTypeRepository grapeTypeRepository;

    @Autowired
    public InsertInitialDataService(HarvestRepository harvestRepository, HarvestItemRepository harvestItemRepository, GrapeTypeRepository grapeTypeRepository) {
        this.harvestRepository = harvestRepository;
        this.harvestItemRepository = harvestItemRepository;
        this.grapeTypeRepository = grapeTypeRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        // Grape types
        GrapeType grapeType1 = new GrapeType("Merlot", "Very good grape", Color.ROSE, Sweetness.SWEET);
        GrapeType grapeType2 = new GrapeType("Concord", "Some other grape", Color.RED, Sweetness.DRY);

        this.grapeTypeRepository.save(grapeType1);
        this.grapeTypeRepository.save(grapeType2);

        // Harvests
        Harvest harvest1 = new Harvest(2022);
        Harvest harvest2 = new Harvest(2023);

        this.harvestRepository.save(harvest1);
        this.harvestRepository.save(harvest2);

        // Harvest items
        HarvestItem harvestItem1 = new HarvestItem(harvest1, new Date(), 2343.0, GrapeQuality.EXCELLENT, grapeType1);
        HarvestItem harvestItem2 = new HarvestItem(harvest1, new Date(), 45.6, GrapeQuality.POOR, grapeType2);

        this.harvestItemRepository.save(harvestItem1);
        this.harvestItemRepository.save(harvestItem2);
    }

    public void deleteAllData() {
        // Delete data from repositories
        harvestItemRepository.deleteAll();
        harvestRepository.deleteAll();
        grapeTypeRepository.deleteAll();
    }
}
