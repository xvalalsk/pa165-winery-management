# PA165 - Winery Management

## Production module

### Ingredients

#### Supported operations:

| Method     | Endpoint                                                    | Description                                |
|------------|-------------------------------------------------------------|--------------------------------------------|
| **GET**    | `/api/v1/production/ingredients`                               | Get a list of all ingredients              |
| **GET**    | `/api/v1/production/ingredients/{id}`                          | Get an ingredient by ID                    |
| **GET**    | `/api/v1/production/ingredients/name/{name}`                   | Get an ingredient by name                  |
| **PUT**    | `/api/v1/production/ingredients/{id}`                          | Update an ingredient by ID                 |
| **PUT**    | `/api/v1/production/ingredients/name/{name}`                   | Update an ingredient by name               |
| **POST**   | `/api/v1/production/ingredients`                               | Add an ingredient                          |
| **POST**   | `/api/v1/production/ingredients/{id}/subtract-quantity`        | Subtract quantity to an ingredient by ID   |
| **POST**   | `/api/v1/production/ingredients/{id}/add-quantity`             | Add quantity to an ingredient by ID        |
| **POST**   | `/api/v1/production/ingredients/name/{name}/subtract-quantity` | Subtract quantity to an ingredient by name |
| **POST**   | `/api/v1/production/ingredients/name/{name}/add-quantity`      | Add quantity to an ingredient by name      |
| **DELETE** | `/api/v1/production/ingredients/{id}`                          | Delete an ingredient by ID                 |
| **DELETE** | `/api/v1/production/ingredients/name/{name}`                   | Delete an ingredient by name               |

#### Sample data:

For better testing purposes there sample data are currently initialized (this may be removed in following milestones).

```json
[
  {
    "id": 1,
    "quantity": {
      "type": "weight",
      "value": 10.0,
      "weightUnit": "KILOGRAM"
    },
    "name": "sugar"
  },
  {
    "id": 2,
    "quantity": {
      "type": "weight",
      "value": 500.0,
      "weightUnit": "GRAM"
    },
    "name": "salt"
  },
  {
    "id": 3,
    "quantity": {
      "type": "volume",
      "value": 500.0,
      "volumeUnit": "LITER"
    },
    "name": "water"
  }
]
```

#### Example scenarios:

Each following scenarios expect to have tha sample data in place to work.

> ##### List all ingredients
>> To get list of all available ingredients send a GET request to /api/v1/production/ingredients endpoint.
>
>> Run:
>> ```shell
>> curl http://localhost:8082/api/v1/production/ingredients
>> ```
>
>> Result:
>> ```json
>> [
>>   {
>>     "id": 1,
>>     "quantity": {
>>       "type": "weight",
>>       "value": 10.0,
>>       "weightUnit": "KILOGRAM"
>>     },
>>     "name": "sugar"
>>   },
>>   {
>>     "id": 2,
>>     "quantity": {
>>       "type": "weight",
>>       "value": 500.0,
>>       "weightUnit": "GRAM"
>>     },
>>     "name": "salt"
>>   },
>>   {
>>     "id": 3,
>>     "quantity": {
>>       "type": "volume",
>>       "value": 500.0,
>>       "volumeUnit": "LITER"
>>     },
>>     "name": "water"
>>   }
>> ]
>> ```

> ##### Get ingredient by ID
>> To get information only about one ingredient with specific ID, send a GET request to /api/v1/production/ingredients/{id}
> > endpoint.
>>
>> In following example we want to get an ingredient with id: 1
>
>> Run:
>> ```shell
>> curl http://localhost:8082/api/v1/production/ingredients/1
>> ```
>
>> Result:
>> ```json
>> {
>>   "id": 1,
>>   "quantity": {
>>     "type": "weight",
>>     "value": 10.0,
>>     "weightUnit": "KILOGRAM"
>>   },
>>   "name": "sugar"
>> }
>> ```

> ##### Get ingredient by name
>> To get information only about one ingredient with specific name, send a GET request to
> > /api/v1/production/ingredients/name/{name} endpoint.
>>
>> In following example we want to get an ingredient with name: sugar.
>
>> Run:
>> ```shell
>> curl http://localhost:8082/api/v1/production/ingredients/name/sugar
>> ```
>
>> Result:
>> ```json
>> {
>>   "id": 1,
>>   "quantity": {
>>     "type": "weight",
>>     "value": 10.0,
>>     "weightUnit": "KILOGRAM"
>>   },
>>   "name": "sugar"
>> }
>> ```

> ##### Update ingredient by ID
>> To update ingredient with specific ID, send a PUT request to /api/v1/production/ingredients/{id} endpoint.
>>
>> In following example we want to update an ingredient with id: 1
>
>> Run:
>> ```shell
>> curl --header "Content-Type: application/json" \
>>      --request PUT \
>>      --data '{"quantity": {"type": "weight","value": 42.0,"weightUnit": "KILOGRAM"},"name": "sugar"}' \
>>      http://localhost:8082/api/v1/production/ingredients/1
>> ```
>
>> Result:
>> ```json
>> {
>>   "id": 1,
>>   "quantity": {
>>     "type": "weight",
>>     "value": 42.0,
>>     "weightUnit": "KILOGRAM"
>>   },
>>   "name": "sugar"
>> }
>> ```
>> We can list all ingredients again to see that the ingredient with id: 1 was updated.
>
>> Run:
>> ```shell
>> curl http://localhost:8082/api/v1/production/ingredients
>> ```
>
>> Result:
>> ```json
>> [
>>     {
>>         "id": 1,
>>         "quantity": {
>>             "type": "weight",
>>             "value": 42.0,
>>             "weightUnit": "KILOGRAM"
>>         },
>>         "name": "sugar"
>>     },
>>     {
>>         "id": 2,
>>         "quantity": {
>>             "type": "weight",
>>             "value": 500.0,
>>             "weightUnit": "GRAM"
>>         },
>>         "name": "salt"
>>     },
>>     {
>>         "id": 3,
>>         "quantity": {
>>             "type": "volume",
>>             "value": 500.0,
>>             "volumeUnit": "LITER"
>>         },
>>         "name": "water"
>>     }
>> ]
>> ```
>>
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository
>>

> ##### Update ingredient by name
>> To update ingredient with specific name, send a PUT request to /api/v1/production/ingredients/name/{name} endpoint.
>>
>>In following example we want to update an ingredient with name: sugar.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request PUT \
>>     --data '{"quantity": {"type": "weight","value": 42.0,"weightUnit": "KILOGRAM"},"name": "sugar"}' \
>>     http://localhost:8082/api/v1/production/ingredients/name/sugar
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 42.0,
>>    "weightUnit": "KILOGRAM"
>>  },
>>  "name": "sugar"
>>}
>>```
>>We can list all ingredients again to see that the ingredient with name: sugar was updated.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/ingredients
>> ```
>
>> Result:
>>```json
>>[
>>    {
>>        "id": 1,
>>        "quantity": {
>>            "type": "weight",
>>            "value": 42.0,
>>            "weightUnit": "KILOGRAM"
>>        },
>>        "name": "sugar"
>>    },
>>    {
>>        "id": 2,
>>        "quantity": {
>>            "type": "weight",
>>            "value": 500.0,
>>            "weightUnit": "GRAM"
>>        },
>>        "name": "salt"
>>    },
>>    {
>>        "id": 3,
>>        "quantity": {
>>            "type": "volume",
>>            "value": 500.0,
>>            "volumeUnit": "LITER"
>>        },
>>        "name": "water"
>>    }
>>]
>>```
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository

> ##### Add new Ingredient
>> To add new Ingredient, send a POST request to /api/v1/production/ingredients endpoint.
>>
>>In following example want to create new Ingredient with following data:
>>```json
>>{
>>  "quantity": {
>>    "type": "volume",
>>    "value": 500.0,
>>    "volumeUnit": "LITER"
>>  },
>>  "name": "ethanol"
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"quantity": {"type": "volume","value": 500.0,"volumeUnit": "LITER"},"name": "ethanol"}' \
>>     http://localhost:8082/api/v1/production/ingredients
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 4,
>>  "quantity": {
>>    "type": "volume",
>>    "value": 500.0,
>>    "volumeUnit": "LITER"
>>  },
>>  "name": "ethanol"
>>}
>>```

> ##### Add quantity to ingredient by ID
>> To add quantity to ingredient with specific ID, send a POST request to /api/v1/production/ingredients/{id}/add-quantity
> > endpoint.
>>
>>In following example we want to update an ingredient with id: 1.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "weight","value": 10.0,"weightUnit": "KILOGRAM"}' \
>>     http://localhost:8082/api/v1/production/ingredients/1/add-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 20000.0,
>>    "weightUnit": "GRAM"
>>  },
>>  "name": "sugar"
>>}
>>```

> ##### Add quantity to ingredient by name
>> To add quantity to ingredient with specific name, send a POST request to
> > /api/v1/production/ingredients/name/name/add-quantity endpoint.
>>
>>In following example we want to update an ingredient with name: sugar.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "weight","value": 10.0,"weightUnit": "KILOGRAM"}' \
>>     http://localhost:8082/api/v1/production/ingredients/name/sugar/add-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 20000.0,
>>    "weightUnit": "GRAM"
>>  },
>>  "name": "sugar"
>>}
>>```

> ##### Subtract quantity to ingredient by ID
>> To add subtract to ingredient with specific ID, send a POST request to /api/v1/production/ingredients/{id}/add-subtract
> > endpoint.
>>
>>In following example we want to update an ingredient with id: 1.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "weight","value": 5.0,"weightUnit": "KILOGRAM"}' \
>>     http://localhost:8082/api/v1/production/ingredients/1/subtract-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 5000.0,
>>    "weightUnit": "GRAM"
>>  },
>>  "name": "sugar"
>>}
>>```

> ##### Subtract quantity to ingredient by name
>> To subtract quantity to ingredient with specific name, send a POST request to
> > /api/v1/production/ingredients/name/name/subtract-quantity endpoint.
>>
>>In following example we want to update an ingredient with name: sugar.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "weight","value": 5.0,"weightUnit": "KILOGRAM"}' \
>>     http://localhost:8082/api/v1/production/ingredients/name/sugar/subtract-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 5000.0,
>>    "weightUnit": "GRAM"
>>  },
>>  "name": "sugar"
>>}
>>```

> ##### Delete ingredient by ID
>> To delete an ingredient with specific ID, send a DELETE request to /api/v1/production/ingredients/{id} endpoint
>>
>>In following example we want to delete an ingredient with id: 1.
>
>> Run:
>>```shell
>>curl --request DELETE http://localhost:8082/api/v1/production/ingredients/1
>>```
>>
>>List all ingredients to see that ingredient with id 1 was deleted.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/ingredients
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 2,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 500.0,
>>      "weightUnit": "GRAM"
>>    },
>>    "name": "salt"
>>  },
>>  {
>>    "id": 3,
>>    "quantity": {
>>      "type": "volume",
>>      "value": 500.0,
>>      "volumeUnit": "LITER"
>>    },
>>    "name": "water"
>>  }
>>]
>>```

> ##### Delete ingredient by name
>> To delete an ingredient with specific name, send a DELETE request to /api/v1/production/ingredients/name/{name} endpoint
>>
>>In following example we want to delete an ingredient with name: sugar.
>
>> Run:
>>```shell
>>curl --request DELETE http://localhost:8082/api/v1/production/ingredients/name/sugar
>>```
>>
>>List all ingredients to see that ingredient with id 1 was deleted.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/ingredients
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 2,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 500.0,
>>      "weightUnit": "GRAM"
>>    },
>>    "name": "salt"
>>  },
>>  {
>>    "id": 3,
>>    "quantity": {
>>      "type": "volume",
>>      "value": 500.0,
>>      "volumeUnit": "LITER"
>>    },
>>    "name": "water"
>>  }
>>]
>>```
>>

### GrapeContainers

#### Supported operations:

| Method     | Endpoint                                                                | Description                                                                                            |
|------------|-------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| **GET**    | `/api/v1/production/grape-containers`                                      | Get a list of all GrapeContainers                                                                      |
| **GET**    | `/api/v1/production/grape-containers/{id}`                                 | Get a GrapeContainer by ID                                                                             |
| **GET**    | `/api/v1/production/grape-containers/grapeType-and-year`                   | Get a GrapeContainer by Grape Type and Harvest Year (note: pass `Grape Type and Harvest Year` in body) |
| **PUT**    | `/api/v1/production/grape-containers/{id}`                                 | Update a GrapeContainer by ID                                                                          |
| **PUT**    | `/api/v1/production/grape-containers/grapeType-and-year`                   | Update a GrapeContainer by Grape Type and Harvest Year                                                 |             
| **POST**   | `/api/v1/production/grape-containers`                                      | Add a GrapeContainer                                                                                   |
| **POST**   | `/api/v1/production/grape-containers/{id}/subtract-quantity`               | Subtract quantity to a GrapeContainer by ID                                                            |                      
| **POST**   | `/api/v1/production/grape-containers/{id}/add-quantity`                    | Add quantity to a GrapeContainer by ID                                                                 |                
| **POST**   | `/api/v1/production/grape-containers/grapeType-and-year/subtract-quantity` | Subtract quantity to a GrapeContainer by Grape Type and Harvest Year                                   |                                  
| **POST**   | `/api/v1/production/grape-containers/grapeType-and-year/add-quantity`      | Add quantity to a GrapeContainer by Grape Type and Harvest Year                                        |                            
| **DELETE** | `/api/v1/production/grape-containers/{id}`                                 | Delete a GrapeContainer by ID                                                                          |  
| **DELETE** | `/api/v1/production/grape-containers/grapeType-and-year`                   | Delete a GrapeContainer by Grape Type and Harvest Year                                                 |               

#### Sample data:

For better testing purposes there sample data are currently initialized (this may be removed in following milestones).

```json
[
  {
    "id": 1,
    "grapeType": {
      "name": "Chardonnay",
      "description": "Chardonnay white sweet wine description",
      "color": "WHITE",
      "sweetness": "SWEET"
    },
    "harvestYear": 2020,
    "quantity": {
      "type": "weight",
      "value": 100.0,
      "weightUnit": "KILOGRAM"
    }
  },
  {
    "id": 2,
    "grapeType": {
      "name": "Zweigeltrebe",
      "description": "Zweigeltrebe rose semi-sweet wine description",
      "color": "ROSE",
      "sweetness": "SEMISWEET"
    },
    "harvestYear": 2018,
    "quantity": {
      "type": "weight",
      "value": 200.0,
      "weightUnit": "KILOGRAM"
    }
  },
  {
    "id": 3,
    "grapeType": {
      "name": "Merlot",
      "description": "Merlot red dry wine description",
      "color": "RED",
      "sweetness": "DRY"
    },
    "harvestYear": 2019,
    "quantity": {
      "type": "weight",
      "value": 300.0,
      "weightUnit": "KILOGRAM"
    }
  }
]
```

#### Example scenarios:

Each following scenarios expect to have tha sample data in place to work.

> ##### List all GrapeContainers
>> To get list of all available GrapeContainers send a GET request to /api/v1/production/grape-containers endpoint.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/grape-containers
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 2,
>>    "grapeType": {
>>      "name": "Zweigeltrebe",
>>      "description": "Zweigeltrebe rose semi-sweet wine description",
>>      "color": "ROSE",
>>      "sweetness": "SEMISWEET"
>>    },
>>    "harvestYear": 2018,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 200.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 3,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 300.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  }
>>]
>>```

> ##### Get GrapeContainer by ID
>> To get information only about one GrapeContainers with specific ID, send a GET request to
> > /api/v1/production/grape-containers/{id} endpoint.
>>
>>In following example we want to get a GrapeContainers with id: 1.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/grape-containers/1
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```

> ##### Get GrapeContainer by Grape Type and Harvest Year
>> To get information only about one GrapeContainer with specific Grape Type and Harvest Year, send a GET request to
> > /api/v1/production/grape-containers/grapeType-and-year endpoint.
> > With the Grape Type and Harvest Year in body.
>>
>>In following example we want to get a GrapeContainers with following grape type and harvest year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request GET \
>>     --data '{"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020}' \
>>     http://localhost:8082/api/v1/production/grape-containers/grapeType-and-year
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```

> ##### Add new GrapeContainer
>> To add new GrapeContainer, send a POST request to /api/v1/production/grape-containers endpoint.
>>
>>In following example want to create new GrapeContainer with following data:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Riesling",
>>    "description": "Riesling white dry wine description",
>>    "color": "WHITE",
>>    "sweetness": "DRY"
>>  },
>>  "harvestYear": 2021,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 500.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grapeType": {"name": "Riesling","description": "Riesling white dry wine description","color": "WHITE","sweetness": "DRY"},"harvestYear": 2021,"quantity": {"type": "weight","value": 500.0,"weightUnit": "KILOGRAM"}}' \
>>     http://localhost:8082/api/v1/production/grape-containers
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 4,
>>  "grapeType": {
>>    "name": "Riesling",
>>    "description": "Riesling white dry wine description",
>>    "color": "WHITE",
>>    "sweetness": "DRY"
>>  },
>>  "harvestYear": 2021,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 500.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```

> ##### Update GrapeContainer by ID
>> To update GrapeContainers with specific ID, send a PUT request to /api/v1/production/grape-containers/{id} endpoint.
>>
>>In following example we want to update a GrapeContainers with id: 1, with following data:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "New Chardonnay",
>>    "description": "New Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request PUT \
>>     --data '{"id": 1,"grapeType": {"name": "New Chardonnay","description": "New Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020,"quantity": {"type": "weight","value": 100.0,"weightUnit": "KILOGRAM"}}' \
>>     http://localhost:8082/api/v1/production/grape-containers/1
>>```
>>
>
>> Result:
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "New Chardonnay",
>>        "description": "New Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "weight",
>>        "value": 100.0,
>>        "weightUnit": "KILOGRAM"
>>    }
>>}
>>```
>>We can list all GrapeContainers again to see that the GrapeContainer with id: 1 was updated.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/grape-containers
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "New Chardonnay",
>>      "description": "New Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 2,
>>    "grapeType": {
>>      "name": "Zweigeltrebe",
>>      "description": "Zweigeltrebe rose semi-sweet wine description",
>>      "color": "ROSE",
>>      "sweetness": "SEMISWEET"
>>    },
>>    "harvestYear": 2018,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 200.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 3,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 300.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  }
>>]
>>```
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository
>>

> ##### Update GrapeContainer by Grape Type and Harvest Year
>> To update GrapeContainers with specific Grape Type and Harvest Year, send a PUT request to
> > /api/v1/production/grape-containers/grapeType-and-year endpoint.
>>
>>In following example we want to get a GrapeContainers with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>>
>>By following data:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "New Chardonnay",
>>    "description": "New Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```
>>
>>Therefore, the PUT request's body will be following:
>>```json
>>{
>>  "grapeTypeAndYearGetRequest": {
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "updatedGrapeContainer": {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "New Chardonnay",
>>      "description": "New Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request PUT \
>>     --data '{"grapeTypeAndYearGetRequest": {"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020},"updatedGrapeContainer": {"id": 1,"grapeType": {"name": "New Chardonnay","description": "New Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020,"quantity": {"type": "weight","value": 100.0,"weightUnit": "KILOGRAM"}}}' \
>>     http://localhost:8082/api/v1/production/grape-containers/grapeType-and-year
>>```
>>
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "New Chardonnay",
>>    "description": "New Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```
>>We can list all GrapeContainers again to see that the GrapeContainer with the previous Grape Type and Harvest Year was
> > updated.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/grape-containers
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "New Chardonnay",
>>      "description": "New Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 2,
>>    "grapeType": {
>>      "name": "Zweigeltrebe",
>>      "description": "Zweigeltrebe rose semi-sweet wine description",
>>      "color": "ROSE",
>>      "sweetness": "SEMISWEET"
>>    },
>>    "harvestYear": 2018,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 200.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 3,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 300.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  }
>>]
>>```
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository

> ##### Add quantity to GrapeContainer by ID
>> To add quantity to GrapeContainer with specific ID, send a POST request to
> > /api/v1/production/grape-containers/{id}/add-quantity endpoint.
>>
>>In following example we want to update a GrapeContainer with id: 1.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "weight","value": 100.0,"weightUnit": "KILOGRAM"}' \
>>     http://localhost:8082/api/v1/production/grape-containers/1/add-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 200000.0,
>>    "weightUnit": "GRAM"
>>  }
>>}
>>```

> ##### Add quantity to GrapeContainer by Grape Type and Harvest Year
>> To add quantity to GrapeContainer with specific Grape Type and Harvest Year, send a POST request to
> > /grape-containers/grapeType-and-year/add-quantity endpoint.
>>
>>In following example we want to update a GrapeContainer with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>>and by this quantity:
>>```json
>>{
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>}
>>```
>>
>>Therefore, the POST request's body will be following:
>>```json
>>{
>>  "grapeTypeAndYearGetRequest": {
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "quantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grapeTypeAndYearGetRequest": {"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020},"quantity": {"type": "weight","value": 100.0,"weightUnit": "KILOGRAM"}}' \
>>     http://localhost:8082/api/v1/production/grape-containers/grapeType-and-year/add-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 200000.0,
>>    "weightUnit": "GRAM"
>>  }
>>}
>>```

> ##### Subtract quantity to GrapeContainer by ID
>> To subtract quantity of GrapeContainer with specific ID, send a POST request to
> > /api/v1/production/grape-containers/{id}/add-subtract endpoint.
>>
>>In following example we want to subtract quantity of a GrapeContainer with id: 1.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "weight","value": 50.0,"weightUnit": "KILOGRAM"}' \
>>     http://localhost:8082/api/v1/production/grape-containers/1/subtract-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 50000.0,
>>    "weightUnit": "GRAM"
>>  }
>>}
>>```

> ##### Subtract quantity to GrapeContainer by Grape Type and Harvest Year
>> To subtract quantity of GrapeContainer with specific Grape Type and Harvest Year, send a POST request to
> > /grape-containers/grapeType-and-year/subtract-quantity endpoint.
>>
>>In following example we want to subtract quantity of a GrapeContainer with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>>and by this quantity:
>>```json
>>{
>>  "type": "weight",
>>  "value": 50.0,
>>  "weightUnit": "KILOGRAM"
>>}
>>```
>>
>>Therefore, the POST request's body will be following:
>>```json
>>{
>>  "grapeTypeAndYearGetRequest": {
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "quantity": {
>>    "type": "weight",
>>    "value": 50.0,
>>    "weightUnit": "KILOGRAM"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grapeTypeAndYearGetRequest": {"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020},"quantity": {"type": "weight","value": 50.0,"weightUnit": "KILOGRAM"}}' \
>>     http://localhost:8082/api/v1/production/grape-containers/grapeType-and-year/subtract-quantity
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "weight",
>>    "value": 200000.0,
>>    "weightUnit": "GRAM"
>>  }
>>}
>>```

> ##### Delete GrapeContainer by ID
>> To delete a GrapeContainer with specific ID, send a DELETE request to /api/v1/production/grape-containers/{id} endpoint
>>
>>In following example we want to delete a GrapeContainer with id: 1.
>
>> Run:
>>```shell
>>curl --request DELETE http://localhost:8082/api/v1/production/grape-containers/1
>>```
>>
>>List all ingredients to see that ingredient with id 1 was deleted.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/grape-containers
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 2,
>>    "grapeType": {
>>      "name": "Zweigeltrebe",
>>      "description": "Zweigeltrebe rose semi-sweet wine description",
>>      "color": "ROSE",
>>      "sweetness": "SEMISWEET"
>>    },
>>    "harvestYear": 2018,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 200.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 3,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 300.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  }
>>]
>>```

> ##### Delete GrapeContainer by Grape Type and Harvest Year
>> To delete a GrapeContainer with specific Grape Type and Harvest Year, send a DELETE request to
> > /grape-containers/grapeType-and-year endpoint
> > With Grape Type and Harvest Year in the body.
>>
>>In following example we want to delete a GrapeContainer with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request DELETE \
>>     --data '{"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020}' \
>>     http://localhost:8082/api/v1/production/grape-containers/grapeType-and-year
>>```
>>
>>List all ingredients to see that ingredient with previous Grape Type and Harvest Year was deleted.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/grape-containers
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 2,
>>    "grapeType": {
>>      "name": "Zweigeltrebe",
>>      "description": "Zweigeltrebe rose semi-sweet wine description",
>>      "color": "ROSE",
>>      "sweetness": "SEMISWEET"
>>    },
>>    "harvestYear": 2018,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 200.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  },
>>  {
>>    "id": 3,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>      "type": "weight",
>>      "value": 300.0,
>>      "weightUnit": "KILOGRAM"
>>    }
>>  }
>>]
>>```

### ProductionItems

#### Supported operations:

| Method     | Endpoint                                | Description                        |
|------------|-----------------------------------------|------------------------------------|
| **GET**    | `/api/v1/production/production-items`      | Get a list of all production items |
| **GET**    | `/api/v1/production/production-items/{id}` | Get a production item by ID        |
| **PUT**    | `/api/v1/production/production-items/{id}` | Update a production item by ID     |
| **POST**   | `/api/v1/production/production-items`      | Add a production item              |
| **DELETE** | `/api/v1/production/production-items/{id}` | Delete a production items by ID    |

#### Sample data:

For better testing purposes there sample data are currently initialized (this may be removed in following milestones).

```json
[
  {
    "id": 1,
    "grape": {
      "id": 1,
      "grapeType": {
        "name": "Chardonnay",
        "description": "Chardonnay white sweet wine description",
        "color": "WHITE",
        "sweetness": "SWEET"
      },
      "harvestYear": 2020
    },
    "grapeQuantity": {
      "type": "weight",
      "value": 100.0,
      "weightUnit": "KILOGRAM"
    },
    "ingredients": [
      {
        "name": "sugar",
        "quantity": {
          "type": "weight",
          "value": 10.0,
          "weightUnit": "KILOGRAM"
        }
      },
      {
        "name": "salt",
        "quantity": {
          "type": "weight",
          "value": 5.0,
          "weightUnit": "GRAM"
        }
      },
      {
        "name": "water",
        "quantity": {
          "type": "volume",
          "value": 10.0,
          "volumeUnit": "LITER"
        }
      }
    ],
    "year": 2020
  },
  {
    "id": 2,
    "grape": {
      "id": 2,
      "grapeType": {
        "name": "Zweigeltrebe",
        "description": "Zweigeltrebe rose semi-sweet wine description",
        "color": "ROSE",
        "sweetness": "SEMISWEET"
      },
      "harvestYear": 2018
    },
    "grapeQuantity": {
      "type": "weight",
      "value": 100.0,
      "weightUnit": "KILOGRAM"
    },
    "ingredients": [
      {
        "name": "sugar",
        "quantity": {
          "type": "weight",
          "value": 10.0,
          "weightUnit": "KILOGRAM"
        }
      },
      {
        "name": "salt",
        "quantity": {
          "type": "weight",
          "value": 5.0,
          "weightUnit": "GRAM"
        }
      },
      {
        "name": "water",
        "quantity": {
          "type": "volume",
          "value": 10.0,
          "volumeUnit": "LITER"
        }
      }
    ],
    "year": 2018
  },
  {
    "id": 3,
    "grape": {
      "id": 3,
      "grapeType": {
        "name": "Merlot",
        "description": "Merlot red dry wine description",
        "color": "RED",
        "sweetness": "DRY"
      },
      "harvestYear": 2019
    },
    "grapeQuantity": {
      "type": "weight",
      "value": 100.0,
      "weightUnit": "KILOGRAM"
    },
    "ingredients": [
      {
        "name": "sugar",
        "quantity": {
          "type": "weight",
          "value": 10.0,
          "weightUnit": "KILOGRAM"
        }
      },
      {
        "name": "salt",
        "quantity": {
          "type": "weight",
          "value": 5.0,
          "weightUnit": "GRAM"
        }
      },
      {
        "name": "water",
        "quantity": {
          "type": "volume",
          "value": 10.0,
          "volumeUnit": "LITER"
        }
      }
    ],
    "year": 2019
  }
]
```

#### Example scenarios:

Each following scenarios expect to have tha sample data in place to work.

> ##### List all ProductionItems
>>
>>To get list of all available ProductionItems send a GET request to /api/v1/production/production-items endpoint.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/production-items
>>```
>>
>
>> Result:
>>```json
>>[
>>    {
>>        "id": 1,
>>        "grape": {
>>            "id": 1,
>>            "grapeType": {
>>                "name": "Chardonnay",
>>                "description": "Chardonnay white sweet wine description",
>>                "color": "WHITE",
>>                "sweetness": "SWEET"
>>            },
>>            "harvestYear": 2020
>>        },
>>        "grapeQuantity": {
>>            "type": "weight",
>>            "value": 100.0,
>>            "weightUnit": "KILOGRAM"
>>        },
>>        "ingredients": [
>>            {
>>                "name": "sugar",
>>                "quantity": {
>>                    "type": "weight",
>>                    "value": 10.0,
>>                    "weightUnit": "KILOGRAM"
>>                }
>>            },
>>            {
>>                "name": "salt",
>>                "quantity": {
>>                    "type": "weight",
>>                    "value": 5.0,
>>                    "weightUnit": "GRAM"
>>                }
>>            },
>>            {
>>                "name": "water",
>>                "quantity": {
>>                    "type": "volume",
>>                    "value": 10.0,
>>                    "volumeUnit": "LITER"
>>                }
>>            }
>>        ],
>>        "year": 2020
>>    },
>>    {
>>        "id": 2,
>>        "grape": {
>>            "id": 2,
>>            "grapeType": {
>>                "name": "Zweigeltrebe",
>>                "description": "Zweigeltrebe rose semi-sweet wine description",
>>                "color": "ROSE",
>>                "sweetness": "SEMISWEET"
>>            },
>>            "harvestYear": 2018
>>        },
>>        "grapeQuantity": {
>>            "type": "weight",
>>            "value": 100.0,
>>            "weightUnit": "KILOGRAM"
>>        },
>>        "ingredients": [
>>            {
>>                "name": "sugar",
>>                "quantity": {
>>                    "type": "weight",
>>                    "value": 10.0,
>>                    "weightUnit": "KILOGRAM"
>>                }
>>            },
>>            {
>>                "name": "salt",
>>                "quantity": {
>>                    "type": "weight",
>>                    "value": 5.0,
>>                    "weightUnit": "GRAM"
>>                }
>>            },
>>            {
>>                "name": "water",
>>                "quantity": {
>>                    "type": "volume",
>>                    "value": 10.0,
>>                    "volumeUnit": "LITER"
>>                }
>>            }
>>        ],
>>        "year": 2018
>>    },
>>    {
>>        "id": 3,
>>        "grape": {
>>            "id": 3,
>>            "grapeType": {
>>                "name": "Merlot",
>>                "description": "Merlot red dry wine description",
>>                "color": "RED",
>>                "sweetness": "DRY"
>>            },
>>            "harvestYear": 2019
>>        },
>>        "grapeQuantity": {
>>            "type": "weight",
>>            "value": 100.0,
>>            "weightUnit": "KILOGRAM"
>>        },
>>        "ingredients": [
>>            {
>>                "name": "sugar",
>>                "quantity": {
>>                    "type": "weight",
>>                    "value": 10.0,
>>                    "weightUnit": "KILOGRAM"
>>                }
>>            },
>>            {
>>                "name": "salt",
>>                "quantity": {
>>                    "type": "weight",
>>                    "value": 5.0,
>>                    "weightUnit": "GRAM"
>>                }
>>            },
>>            {
>>                "name": "water",
>>                "quantity": {
>>                    "type": "volume",
>>                    "value": 10.0,
>>                    "volumeUnit": "LITER"
>>                }
>>            }
>>        ],
>>        "year": 2019
>>    }
>>]
>>```

> ##### Get ProductionItem by ID
>> To get information only about one ProductionItem with specific ID, send a GET request to
> > /api/v1/production/production-items/{id} endpoint.
>>
>>In following example we want to get an ProductionItem with id: 1
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/production-items/1
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grape": {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "grapeQuantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  },
>>  "ingredients": [
>>    {
>>      "name": "sugar",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 10.0,
>>        "weightUnit": "KILOGRAM"
>>      }
>>    },
>>    {
>>      "name": "salt",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 5.0,
>>        "weightUnit": "GRAM"
>>      }
>>    },
>>    {
>>      "name": "water",
>>      "quantity": {
>>        "type": "volume",
>>        "value": 10.0,
>>        "volumeUnit": "LITER"
>>      }
>>    }
>>  ],
>>  "year": 2020
>>}
>>```

> ##### Update ProductionItem by ID
>> To update ProductionItem with specific ID, send a PUT request to /api/v1/production/production-items/{id} endpoint.
>>
>>In following example we want to update an ProductionItem with id: 1,
> > with following data:
>>```json
>>{
>>  "id": 1,
>>  "grape": {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "grapeQuantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  },
>>  "ingredients": [
>>    {
>>      "name": "sugar",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 5.0,
>>        "weightUnit": "KILOGRAM"
>>      }
>>    },
>>    {
>>      "name": "salt",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 100.0,
>>        "weightUnit": "GRAM"
>>      }
>>    },
>>    {
>>      "name": "water",
>>      "quantity": {
>>        "type": "volume",
>>        "value": 20.0,
>>        "volumeUnit": "LITER"
>>      }
>>    }
>>  ],
>>  "year": 2020
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request PUT \
>>     --data '{"id": 1,"grape": {"id": 1,"grapeType": {"name": "Merlot","description": "Merlot red dry wine description","color": "RED","sweetness": "DRY"},"harvestYear": 2020},"grapeQuantity": {"type": "weight","value": 100.0,"weightUnit": "KILOGRAM"},"ingredients": [{"name": "sugar","quantity": {"type": "weight","value": 5.0,"weightUnit": "KILOGRAM"}},{"name": "salt","quantity": {"type": "weight","value": 100.0,"weightUnit": "GRAM"}},{"name": "water","quantity": {"type": "volume","value": 20.0,"volumeUnit": "LITER"}}],"year": 2020}' \
>>     http://localhost:8082/api/v1/production/production-items/1
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 1,
>>  "grape": {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "grapeQuantity": {
>>    "type": "weight",
>>    "value": 100.0,
>>    "weightUnit": "KILOGRAM"
>>  },
>>  "ingredients": [
>>    {
>>      "name": "sugar",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 5.0,
>>        "weightUnit": "KILOGRAM"
>>      }
>>    },
>>    {
>>      "name": "salt",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 100.0,
>>        "weightUnit": "GRAM"
>>      }
>>    },
>>    {
>>      "name": "water",
>>      "quantity": {
>>        "type": "volume",
>>        "value": 20.0,
>>        "volumeUnit": "LITER"
>>      }
>>    }
>>  ],
>>  "year": 2020
>>}
>>```
>>We can list all ProductionItems again to see that the ProductionItem with id: 1 was updated.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/production-items
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 1,
>>    "grape": {
>>      "id": 1,
>>      "grapeType": {
>>        "name": "Merlot",
>>        "description": "Merlot red dry wine description",
>>        "color": "RED",
>>        "sweetness": "DRY"
>>      },
>>      "harvestYear": 2020
>>    },
>>    "grapeQuantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    },
>>    "ingredients": [
>>      {
>>        "name": "sugar",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 5.0,
>>          "weightUnit": "KILOGRAM"
>>        }
>>      },
>>      {
>>        "name": "salt",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 100.0,
>>          "weightUnit": "GRAM"
>>        }
>>      },
>>      {
>>        "name": "water",
>>        "quantity": {
>>          "type": "volume",
>>          "value": 20.0,
>>          "volumeUnit": "LITER"
>>        }
>>      }
>>    ],
>>    "year": 2020
>>  },
>>  {
>>    "id": 2,
>>    "grape": {
>>      "id": 2,
>>      "grapeType": {
>>        "name": "Zweigeltrebe",
>>        "description": "Zweigeltrebe rose semi-sweet wine description",
>>        "color": "ROSE",
>>        "sweetness": "SEMISWEET"
>>      },
>>      "harvestYear": 2018
>>    },
>>    "grapeQuantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    },
>>    "ingredients": [
>>      {
>>        "name": "sugar",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 10.0,
>>          "weightUnit": "KILOGRAM"
>>        }
>>      },
>>      {
>>        "name": "salt",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 5.0,
>>          "weightUnit": "GRAM"
>>        }
>>      },
>>      {
>>        "name": "water",
>>        "quantity": {
>>          "type": "volume",
>>          "value": 10.0,
>>          "volumeUnit": "LITER"
>>        }
>>      }
>>    ],
>>    "year": 2018
>>  },
>>  {
>>    "id": 3,
>>    "grape": {
>>      "id": 3,
>>      "grapeType": {
>>        "name": "Merlot",
>>        "description": "Merlot red dry wine description",
>>        "color": "RED",
>>        "sweetness": "DRY"
>>      },
>>      "harvestYear": 2019
>>    },
>>    "grapeQuantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    },
>>    "ingredients": [
>>      {
>>        "name": "sugar",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 10.0,
>>          "weightUnit": "KILOGRAM"
>>        }
>>      },
>>      {
>>        "name": "salt",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 5.0,
>>          "weightUnit": "GRAM"
>>        }
>>      },
>>      {
>>        "name": "water",
>>        "quantity": {
>>          "type": "volume",
>>          "value": 10.0,
>>          "volumeUnit": "LITER"
>>        }
>>      }
>>    ],
>>    "year": 2019
>>  }
>>]
>>```
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository

> ##### Add new ProductionItem
>> To add new ProductionItem, send a POST request to /api/v1/production/production-items endpoint.
>>
>>In following example want to create new ProductionItem with following data:
>>```json
>>{
>>  "grape": {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019
>>  },
>>  "grapeQuantity": {
>>    "type": "weight",
>>    "value": 50.0,
>>    "weightUnit": "KILOGRAM"
>>  },
>>  "ingredients": [
>>    {
>>      "name": "sugar",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 2.0,
>>        "weightUnit": "KILOGRAM"
>>      }
>>    },
>>    {
>>      "name": "salt",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 200.0,
>>        "weightUnit": "GRAM"
>>      }
>>    },
>>    {
>>      "name": "water",
>>      "quantity": {
>>        "type": "volume",
>>        "value": 50.0,
>>        "volumeUnit": "LITER"
>>      }
>>    }
>>  ],
>>  "year": 2021
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grape": {"id": 1,"grapeType": {"name": "Merlot","description": "Merlot red dry wine description","color": "RED","sweetness": "DRY"},"harvestYear": 2019},"grapeQuantity": {"type": "weight","value": 50.0,"weightUnit": "KILOGRAM"},"ingredients": [{"name": "sugar","quantity": {"type": "weight","value": 2.0,"weightUnit": "KILOGRAM"}},{"name": "salt","quantity": {"type": "weight","value": 200.0,"weightUnit": "GRAM"}},{"name": "water","quantity": {"type": "volume","value": 50.0,"volumeUnit": "LITER"}}],"year": 2021}' \
>>     http://localhost:8082/api/v1/production/production-items
>> ```
>
>> Result:
>>```json
>>{
>>  "id": 4,
>>  "grape": {
>>    "id": 1,
>>    "grapeType": {
>>      "name": "Merlot",
>>      "description": "Merlot red dry wine description",
>>      "color": "RED",
>>      "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019
>>  },
>>  "grapeQuantity": {
>>    "type": "weight",
>>    "value": 50.0,
>>    "weightUnit": "KILOGRAM"
>>  },
>>  "ingredients": [
>>    {
>>      "name": "sugar",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 2.0,
>>        "weightUnit": "KILOGRAM"
>>      }
>>    },
>>    {
>>      "name": "salt",
>>      "quantity": {
>>        "type": "weight",
>>        "value": 200.0,
>>        "weightUnit": "GRAM"
>>      }
>>    },
>>    {
>>      "name": "water",
>>      "quantity": {
>>        "type": "volume",
>>        "value": 50.0,
>>        "volumeUnit": "LITER"
>>      }
>>    }
>>  ],
>>  "year": 2021
>>}
>>```
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository of
> > > ingredients and qrapeContianers

> ##### Delete productionItem by ID
>> To delete an ingredient with specific ID, send a DELETE request to /api/v1/production/production-items/{id} endpoint
>>
>>In following example we want to delete an production-items with id: 1.
>
>> Run:
>>```shell
>>curl --request DELETE http://localhost:8082/api/v1/production/production-items/1
>>```
>>
>>List all ingredients to see that productionItem with id 1 was deleted.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/production-items
>> ```
>
>> Result:
>>```json
>>[
>>  {
>>    "id": 2,
>>    "grape": {
>>      "id": 2,
>>      "grapeType": {
>>        "name": "Zweigeltrebe",
>>        "description": "Zweigeltrebe rose semi-sweet wine description",
>>        "color": "ROSE",
>>        "sweetness": "SEMISWEET"
>>      },
>>      "harvestYear": 2018
>>    },
>>    "grapeQuantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    },
>>    "ingredients": [
>>      {
>>        "name": "sugar",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 10.0,
>>          "weightUnit": "KILOGRAM"
>>        }
>>      },
>>      {
>>        "name": "salt",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 5.0,
>>          "weightUnit": "GRAM"
>>        }
>>      },
>>      {
>>        "name": "water",
>>        "quantity": {
>>          "type": "volume",
>>          "value": 10.0,
>>          "volumeUnit": "LITER"
>>        }
>>      }
>>    ],
>>    "year": 2018
>>  },
>>  {
>>    "id": 3,
>>    "grape": {
>>      "id": 3,
>>      "grapeType": {
>>        "name": "Merlot",
>>        "description": "Merlot red dry wine description",
>>        "color": "RED",
>>        "sweetness": "DRY"
>>      },
>>      "harvestYear": 2019
>>    },
>>    "grapeQuantity": {
>>      "type": "weight",
>>      "value": 100.0,
>>      "weightUnit": "KILOGRAM"
>>    },
>>    "ingredients": [
>>      {
>>        "name": "sugar",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 10.0,
>>          "weightUnit": "KILOGRAM"
>>        }
>>      },
>>      {
>>        "name": "salt",
>>        "quantity": {
>>          "type": "weight",
>>          "value": 5.0,
>>          "weightUnit": "GRAM"
>>        }
>>      },
>>      {
>>        "name": "water",
>>        "quantity": {
>>          "type": "volume",
>>          "value": 10.0,
>>          "volumeUnit": "LITER"
>>        }
>>      }
>>    ],
>>    "year": 2019
>>  }
>>]
>>```

### Barrels

#### Supported operations:

| Method     | Endpoint                                                       | Description                                                                                    |
|------------|----------------------------------------------------------------|------------------------------------------------------------------------------------------------|
| **GET**    | `/api/v1/production/barrels`                                      | Get a list of all Barrels                                                                      |
| **GET**    | `/api/v1/production/barrels/{id}`                                 | Get a Barrel by ID                                                                             |
| **GET**    | `/grape-containers/barrels/grapeType-and-year`                 | Get a Barrel by Grape Type and Harvest Year (note: pass `Grape Type and Harvest Year` in body) |
| **PUT**    | `/api/v1/production/barrels/{id}`                                 | Update a Barrel by ID                                                                          |
| **PUT**    | `/api/v1/production/barrels/grapeType-and-year`                   | Update a Barrel by Grape Type and Harvest Year                                                 |             
| **POST**   | `/api/v1/production/barrels`                                      | Add a Barrel                                                                                   |
| **POST**   | `/api/v1/production/barrels/{id}/subtract-quantity`               | Subtract quantity to a Barrel by ID                                                            |                      
| **POST**   | `/api/v1/production/barrels/{id}/add-quantity`                    | Add quantity to a Barrel by ID                                                                 |                
| **POST**   | `/api/v1/production/barrels/grapeType-and-year/subtract-quantity` | Subtract quantity of a Barrel by Grape Type and Harvest Year                                   |                                  
| **POST**   | `/api/v1/production/barrels/grapeType-and-year/add-quantity`      | Add quantity to a Barrel by Grape Type and Harvest Year                                        |                            
| **DELETE** | `/api/v1/production/barrels/{id}`                                 | Delete a Barrel by ID                                                                          |  
| **DELETE** | `/api/v1/production/barrels/grapeType-and-year`                   | Delete a Barrel by Grape Type and Harvest Year                                                 |               

#### Sample data:

For better testing purposes there sample data are currently initialized (this may be removed in following milestones).

```json
[
  {
    "id": 1,
    "grapeType": {
      "name": "Chardonnay",
      "description": "Chardonnay white sweet wine description",
      "color": "WHITE",
      "sweetness": "SWEET"
    },
    "harvestYear": 2020,
    "quantity": {
      "type": "volume",
      "value": 80.0,
      "volumeUnit": "LITER"
    }
  },
  {
    "id": 2,
    "grapeType": {
      "name": "Zweigeltrebe",
      "description": "Zweigeltrebe rose semi-sweet wine description",
      "color": "ROSE",
      "sweetness": "SEMISWEET"
    },
    "harvestYear": 2018,
    "quantity": {
      "type": "volume",
      "value": 90.0,
      "volumeUnit": "LITER"
    }
  },
  {
    "id": 3,
    "grapeType": {
      "name": "Merlot",
      "description": "Merlot red dry wine description",
      "color": "RED",
      "sweetness": "DRY"
    },
    "harvestYear": 2019,
    "quantity": {
      "type": "volume",
      "value": 85.0,
      "volumeUnit": "LITER"
    }
  }
]
```

#### Example scenarios:

Each following scenarios expect to have tha sample data in place to work.

> ##### List all Barrels
>> To get list of all available Barrels send a GET request to /api/v1/production/barrels endpoint.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/barrels
>> ```
>
>> Result:
>> ```json
>> [
>>    {
>>        "id": 1,
>>        "grapeType": {
>>            "name": "Chardonnay",
>>            "description": "Chardonnay white sweet wine description",
>>            "color": "WHITE",
>>            "sweetness": "SWEET"
>>        },
>>        "harvestYear": 2020,
>>        "quantity": {
>>            "type": "volume",
>>            "value": 80.0,
>>            "volumeUnit": "LITER"
>>        }
>>    },
>>    {
>>        "id": 2,
>>        "grapeType": {
>>            "name": "Zweigeltrebe",
>>            "description": "Zweigeltrebe rose semi-sweet wine description",
>>            "color": "ROSE",
>>            "sweetness": "SEMISWEET"
>>        },
>>        "harvestYear": 2018,
>>        "quantity": {
>>            "type": "volume",
>>            "value": 90.0,
>>            "volumeUnit": "LITER"
>>        }
>>    },
>>    {
>>        "id": 3,
>>        "grapeType": {
>>            "name": "Merlot",
>>            "description": "Merlot red dry wine description",
>>            "color": "RED",
>>            "sweetness": "DRY"
>>        },
>>        "harvestYear": 2019,
>>        "quantity": {
>>            "type": "volume",
>>            "value": 85.0,
>>            "volumeUnit": "LITER"
>>        }
>>    }
>> ]
>>```


> ##### Get Barrel by ID
>> To get information only about one Barrel with specific ID, send a GET request to /api/v1/production/barrels/{id}
> > endpoint.
>>
>>In following example we want to get a Barrel with id: 1.
>
>> Run:
>>```shell
>>curl http://localhost:8082/api/v1/production/barrels/1
>> ```
>
>> Result:
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 80.0,
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```

> ##### Get Barrel by Grape Type and Harvest Year
>> To get information only about one Barrel with specific Grape Type and Harvest Year, send a GET request to
> > /api/v1/production/barrels/grapeType-and-year endpoint.
> > With the Grape Type and Harvest Year in body.
>>
>>In following example we want to get a Barrel with following grape type and harvest year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request GET \
>>     --data '{"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020}' \
>>     http://localhost:8082/api/v1/production/barrels/grapeType-and-year
>> ```
>
>> Result:
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 80.0,
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```

> ##### Add new Barrel
>> To add new Barrel, send a POST request to /api/v1/production/barrels endpoint.
>>
>>In following example want to create new Barrel with following data:
>>```json
>>{
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 100.0,
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020,"quantity": {"type": "volume","value": 100.0,"volumeUnit": "LITER"}}' \
>>     http://localhost:8082/api/v1/production/barrels
>> ```
>
>> Result:
>>```json
>>{
>>    "id": 4,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 100.0,
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```

> ##### Update Barrel by ID
>> To update a Barrel with specific ID, send a PUT request to /api/v1/production/barrels/{id} endpoint.
>>
>>In following example we want to update a Barrel with id: 1, with following data:
>>```json
>>    {
>>        "grapeType": {
>>            "name": "Merlot",
>>            "description": "Merlot red dry wine description",
>>            "color": "RED",
>>            "sweetness": "DRY"
>>        },
>>        "harvestYear": 2019,
>>        "quantity": {
>>            "type": "volume",
>>            "value": 42.0,
>>            "volumeUnit": "LITER"
>>        }
>>    }
>>```

> > Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request PUT \
>>     --data '{"grapeType": {"name": "Merlot","description": "Merlot red dry wine description","color": "RED","sweetness": "DRY"},"harvestYear": 2019,"quantity": {"type": "volume","value": 42.0,"volumeUnit": "LITER"}}' \
>>     http://localhost:8082/api/v1/production/barrels/1
>>```
>
>> Result:
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Merlot",
>>        "description": "Merlot red dry wine description",
>>        "color": "RED",
>>        "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 42.0,
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository

> ##### Update Barrel by Grape Type and Harvest Year
>To update Barrel with specific Grape Type and Harvest Year, send a PUT request to
> /api/v1/production/barrels/grapeType-and-year endpoint.
>>
>>In following example we want to get a Barrel with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>>
>>By following data:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Merlot",
>>    "description": "Merlot red dry wine description",
>>    "color": "RED",
>>    "sweetness": "DRY"
>>  },
>>  "harvestYear": 2019,
>>  "quantity": {
>>    "type": "volume",
>>    "value": 42.0,
>>    "volumeUnit": "LITER"
>>  }
>>}
>>```
>>
>>Therefore, the PUT request's body will be following:
>>```json
>>{ 
>>    "grapeTypeAndYearGetRequest": {
>>        "grapeType": {
>>            "name": "Chardonnay",
>>            "description": "Chardonnay white sweet wine description",
>>            "color": "WHITE",
>>            "sweetness": "SWEET"
>>        },
>>        "harvestYear": 2020
>>    },
>>    "updatedBarrel": {
>>        "id": 1,
>>        "grapeType": {
>>            "name": "Merlot",
>>            "description": "Merlot red dry wine description",
>>            "color": "RED",
>>            "sweetness": "DRY"
>>        },
>>        "harvestYear": 2019,
>>        "quantity": {
>>            "type": "volume",
>>            "value": 42.0,
>>            "volumeUnit": "LITER"
>>        }
>>    }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request PUT \
>>     --data '{"grapeTypeAndYearGetRequest": {"grapeType": {"id": 1,"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020},"updatedBarrel": {"id": 1,"grapeType": {"name": "Merlot","description": "Merlot red dry wine description","color": "RED","sweetness": "DRY"},"harvestYear": 2019,"quantity": {"type": "volume","value": 42.0,"volumeUnit": "LITER"}}}' \
>>     http://localhost:8082/api/v1/production/grape-containers/grapeType-and-year
>>```
>>
>
>> Result:
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Merlot",
>>        "description": "Merlot red dry wine description",
>>        "color": "RED",
>>        "sweetness": "DRY"
>>    },
>>    "harvestYear": 2019,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 42.0,
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```

> ##### Add quantity to Barrel by ID
>> To add quantity to Barrel with specific ID, send a POST request to /api/v1/production/barrels/{id}/add-quantity
> > endpoint.
>>
>>In following example we want to update a Barrel with id: 1.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "volume","value": 100.0,"volumeUnit": "LITER"}' \
>>     http://localhost:8082/api/v1/production/barrels/1/add-quantity
>> ```
>
>> Result
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 180000.0,
>>        "volumeUnit": "MILLILITER"
>>    }
>>}
>>```

> ##### Add quantity to Barrel by Grape Type and Harvest Year
>> To add quantity to Barrel with specific Grape Type and Harvest Year, send a POST request to
> > /api/v1/production/barrels/grapeType-and-year/add-quantity endpoint.
>>
>>In following example we want to add quantity to a Barrel with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>>and by this quantity:
>>```json
>>{
>>    "type": "volume",
>>    "value": 100.0,
>>    "volumeUnit": "LITER"
>>}
>>```
>>
>>Therefore, the POST request's body will be following:
>>```json
>>{
>>  "grapeTypeAndYearGetRequest": {
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "quantity": {
>>    "type": "volume",
>>    "value": 100.0,
>>    "volumeUnit": "LITER"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grapeTypeAndYearGetRequest": {"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020},"quantity": {"type": "volume","value": 100.0,"volumeUnit": "LITER"}}' \
>>     http://localhost:8082/api/v1/production/barrels/grapeType-and-year/add-quantity
>> ```
>
>> Result
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 180000.0,
>>        "volumeUnit": "MILLILITER"
>>    }
>>}
>>```

> ##### Subtract quantity of a Barrel by ID
>> To subtract quantity to Barrel with specific ID, send a POST request to
> > /api/v1/production/barrels/{id}/subtract-quantity endpoint.
>>
>>In following example we want to subtract quantity of a Barrel with id: 1.
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"type": "volume","value": 50.0,"volumeUnit": "LITER"}' \
>>     http://localhost:8082/api/v1/production/barrels/1/subtract-quantity
>> ```
>
>> Result
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 30000.0,
>>        "volumeUnit": "MILLILITER"
>>    }
>>}
>>```

> ##### Subtract quantity to Barrel by Grape Type and Harvest Year
>> To subtract quantity to Barrel with specific Grape Type and Harvest Year, send a POST request to
> > /api/v1/production/barrels/grapeType-and-year/subtract-quantity endpoint.
>>
>>In following example we want to subtract quantity of a Barrel with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>>and by this quantity:
>>```json
>>{
>>    "type": "volume",
>>    "value": 50.0,
>>    "volumeUnit": "LITER"
>>}
>>```
>>
>>Therefore, the POST request's body will be following:
>>```json
>>{
>>  "grapeTypeAndYearGetRequest": {
>>    "grapeType": {
>>      "name": "Chardonnay",
>>      "description": "Chardonnay white sweet wine description",
>>      "color": "WHITE",
>>      "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020
>>  },
>>  "quantity": {
>>    "type": "volume",
>>    "value": 50.0,
>>    "volumeUnit": "LITER"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"grapeTypeAndYearGetRequest": {"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020},"quantity": {"type": "volume","value": 50.0,"volumeUnit": "LITER"}}' \
>>     http://localhost:8082/api/v1/production/barrels/grapeType-and-year/subtract-quantity
>> ```
>
>> Result
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay",
>>        "description": "Chardonnay white sweet wine description",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "quantity": {
>>        "type": "volume",
>>        "value": 30000.0,
>>        "volumeUnit": "MILLILITER"
>>    }
>>}
>>```

> ##### Delete Barrel by ID
>> To delete a Barrel with specific ID, send a DELETE request to /api/v1/production/barrels/{id} endpoint.
>>
>>In following example we want to delete a Barrel with id: 1.
>
>> Run:
>>```shell
>>curl --request DELETE http://localhost:8082/api/v1/production/barrels/1
>>```
>>

> ##### Delete Barrel by Grape Type and Harvest Year
>> To delete a Barrel with specific Grape Type and Harvest Year, send a DELETE request to /barrels/grapeType-and-year
> > endpoint
> > With Grape Type and Harvest Year in the body.
>>
>>In following example we want to delete a GrapeContainer with following Grape Type and Harvest Year:
>>```json
>>{
>>  "grapeType": {
>>    "name": "Chardonnay",
>>    "description": "Chardonnay white sweet wine description",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request DELETE \
>>     --data '{"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020}' \
>>     http://localhost:8082/api/v1/production/barrels/grapeType-and-year
>>```
