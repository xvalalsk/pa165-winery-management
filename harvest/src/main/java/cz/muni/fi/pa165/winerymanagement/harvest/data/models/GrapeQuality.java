package cz.muni.fi.pa165.winerymanagement.harvest.data.models;

public enum GrapeQuality {
    POOR,
    AVERAGE,
    EXCELLENT
}
