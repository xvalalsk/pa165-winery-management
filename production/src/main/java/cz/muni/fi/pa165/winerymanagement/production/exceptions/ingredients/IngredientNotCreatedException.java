package cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients;

import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceNotCreatedException;

public class IngredientNotCreatedException extends ResourceNotCreatedException {
    private final String message;

    public IngredientNotCreatedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
