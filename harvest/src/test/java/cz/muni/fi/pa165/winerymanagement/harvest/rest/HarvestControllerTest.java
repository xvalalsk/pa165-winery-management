package cz.muni.fi.pa165.winerymanagement.harvest.rest;

import cz.muni.fi.pa165.winerymanagement.harvest.HarvestApplication;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestInputDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestItemDto;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Color;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeQuality;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Sweetness;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.HarvestNotExistsException;
import cz.muni.fi.pa165.winerymanagement.harvest.facade.HarvestFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureJsonTesters
@WebMvcTest(HarvestController.class)
@ContextConfiguration(classes = HarvestApplication.class)
public class HarvestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HarvestFacade harvestFacade;

    private HarvestDto newHarvest;
    private HarvestDto oldHarvest;

    @Autowired
    private JacksonTester<HarvestDto> jsonHarvest;

    @Autowired
    private JacksonTester<List<HarvestDto>> jsonHarvestList;

    @Autowired
    private JacksonTester<List<HarvestItemDto>> jsonHarvestItemList;

    @Autowired
    private JacksonTester<HarvestItemDto> jsonHarvestItem;

    @Autowired
    private JacksonTester<HarvestDto> jsonHarvestDto;

    @Autowired
    private JacksonTester<HarvestInputDto> jsonHarvestInputDto;

    @Autowired
    private JacksonTester<HarvestItemDto> jsonHarvestItemDto;

    private List<HarvestDto> harvests;
    private List<HarvestItemDto> harvestItems;
    private GrapeType grapeType;

    @BeforeEach
    void setUp() {
        grapeType = new GrapeType();
        grapeType.setName("Merlot");
        grapeType.setDescription("Very good grape");
        grapeType.setColor(Color.ROSE);
        grapeType.setSweetness(Sweetness.SWEET);

        oldHarvest = new HarvestDto();
        oldHarvest.setId(1L);
        oldHarvest.setYear(2019);

        newHarvest = new HarvestDto();
        newHarvest.setId(2L);
        newHarvest.setYear(2023);

        var item = new HarvestItemDto();
        item.setDate(new Date());
        item.setQuality(GrapeQuality.EXCELLENT);
        item.setQuantity(26);

        this.harvests = new ArrayList<>(List.of(oldHarvest, newHarvest));
        this.harvestItems = List.of(item);
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void testFindAll() throws Exception {
        var response = mockMvc.perform(get("/api/v1/harvests"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isEqualTo("[]");
    }

    @Test
    void testFindAllUnauthorized() throws Exception {
        mockMvc.perform(get("/api/v1/harvests"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void findAllExisting() throws Exception {
        List<HarvestDto> harvests = Arrays.asList(oldHarvest, newHarvest);
        given(harvestFacade.findAllHarvests()).willReturn(harvests);

        var response = mockMvc.perform(get("/api/v1/harvests"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isEqualTo(jsonHarvestList.write(harvests).getJson());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void findAllItemsForHarvest() throws Exception {
        given(harvestFacade.findAllItemsForHarvest(1L)).willReturn(this.harvestItems);

        var response = mockMvc.perform(get("/api//v1/harvests/1/items"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isEqualTo(jsonHarvestItemList.write(this.harvestItems).getJson());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void findAllItemForHarvestNonExisting() throws Exception {
        given(harvestFacade.findAllItemsForHarvest(3L)).willThrow(new HarvestNotExistsException());
        mockMvc.perform(get("/api/v1/harvests/3/items")).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void findHarvestExisting() throws Exception {
        given(harvestFacade.findHarvestById(1L)).willReturn(this.harvests.get(0));

        var response = mockMvc.perform(get("/api/v1/harvests/1"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isEqualTo(jsonHarvest.write(this.harvests.get(0)).getJson());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void findHarvestNotExisting() throws Exception {
        given(harvestFacade.findHarvestById(3L)).willThrow(new HarvestNotExistsException());
        mockMvc.perform(get("/api/v1/harvests/3")).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void findHarvestItemByIdExisting() throws Exception {
        given(harvestFacade.findHarvestItemById(1L)).willReturn(this.harvestItems.get(0));

        var response = mockMvc.perform(get("/api/v1/harvests/1/items/1"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isEqualTo(jsonHarvestItem.write(this.harvestItems.get(0)).getJson());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void addHarvest() throws Exception {
        var harvestDto = new HarvestDto();
        harvestDto.setYear(2020);

        mockMvc.perform(post("/api/v1/harvests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonHarvestDto.write(harvestDto).getJson()))
            .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void addHarvestItem() throws Exception {
        var harvestItemDto = new HarvestItemDto();
        harvestItemDto.setQuantity(15);
        harvestItemDto.setQuality(GrapeQuality.EXCELLENT);
        harvestItemDto.setDate(new Date());

        mockMvc.perform(post("/api/v1/harvests/items")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonHarvestItemDto.write(harvestItemDto).getJson()))
            .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void updateHarvest() throws Exception {
        var harvestDto = new HarvestDto();
        harvestDto.setYear(2020);

        mockMvc.perform(put("/api/v1/harvests/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonHarvestDto.write(harvestDto).getJson()))
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void deleteHarvest() throws Exception {
        mockMvc.perform(delete("/api/v1/harvests/1"))
            .andExpect(status().isOk());

        verify(harvestFacade, times(1)).deleteHarvestById(1L);
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void addHarvestWithFutureYear() throws Exception {
        var harvestDto = new HarvestInputDto();
        harvestDto.setYear(3023); // Year in the future

        mockMvc.perform(post("/api/v1/harvests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonHarvestInputDto.write(harvestDto).getJson()))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
    }
}
