package cz.muni.fi.pa165.winerymanagement.production.data.model;

import cz.muni.fi.pa165.winerymanagement.core.converter.QuantityJsonConverter;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Ingredient entity class representing an ingredient with information about
 * its name and quantity.
 */
@Entity
@Table(name = "ingredient")
public class Ingredient implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ingredient")
    private Long id;

    @Column(name = "quantity", columnDefinition = "TEXT")
    @Convert(converter = QuantityJsonConverter.class)
    private Quantity quantity;
    @Column(name = "name", unique = true)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ingredient that)) {
            return false;
        }
        return getName().equals(that.getName());
    }


    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }


    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", name='" + name + '\'' +
                '}';
    }
}
