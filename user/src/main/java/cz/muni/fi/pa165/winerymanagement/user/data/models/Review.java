package cz.muni.fi.pa165.winerymanagement.user.data.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

@Entity
@Table(name = "review")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private Long wineBottleId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    @NotNull
    private int rating;
    @Column(length = 300)
    @NotNull
    private String comment;

    public Review() {
    }

    public Review(long wineBottleId, User user, int rating, String comment) {
        this.wineBottleId = wineBottleId;
        this.user = user;
        this.rating = rating;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWineBottleId() {
        return wineBottleId;
    }

    public void setWineBottleId(Long wineBottleId) {
        this.wineBottleId = wineBottleId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return rating == review.rating && wineBottleId.equals(review.wineBottleId) && user.equals(review.user) && comment.equals(review.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wineBottleId, user, rating, comment);
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", wineBottleId=" + wineBottleId +
                ", user=" + user +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                '}';
    }
}
