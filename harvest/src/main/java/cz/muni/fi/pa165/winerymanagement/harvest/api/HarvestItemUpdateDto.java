package cz.muni.fi.pa165.winerymanagement.harvest.api;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeQuality;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;

import java.util.Date;

public class HarvestItemUpdateDto {
    private GrapeType grape;
    private GrapeQuality quality;
    private double quantity;
    private Date date;

    public GrapeType getGrape() {
        return grape;
    }

    public void setGrape(GrapeType grape) {
        this.grape = grape;
    }

    public GrapeQuality getQuality() {
        return quality;
    }

    public void setQuality(GrapeQuality quality) {
        this.quality = quality;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "HarvestItemUpdateDto{" +
                "grape=" + grape +
                ", quality=" + quality +
                ", quantity=" + quantity +
                ", date=" + date +
                '}';
    }
}

