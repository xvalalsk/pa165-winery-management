import React from 'react';

const StarRating = ({ rating, setRating }) => {
    const handleStarClick = (value) => {
        setRating(value);
    };

    return (
        <div>
            {[1, 2, 3, 4, 5].map((star) => (
                <span
                    key={star}
                    className={rating >= star ? 'text-warning' : 'text-muted'}
                    onClick={() => handleStarClick(star)}
                    style={{ cursor: 'pointer' }}
                >
          ★
        </span>
            ))}
        </div>
    );
};

export default StarRating;