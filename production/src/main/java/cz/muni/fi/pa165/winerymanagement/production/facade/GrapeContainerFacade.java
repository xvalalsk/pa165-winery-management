package cz.muni.fi.pa165.winerymanagement.production.facade;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.api.GrapeContainerDto;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.mappers.grape_container.GrapeContainerMapper;
import cz.muni.fi.pa165.winerymanagement.production.service.GrapeContainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GrapeContainerFacade {
    private final GrapeContainerService grapeContainerService;
    private final GrapeContainerMapper grapeContainerMapper;

    @Autowired
    public GrapeContainerFacade(GrapeContainerService grapeContainerService, GrapeContainerMapper grapeContainerMapper) {
        this.grapeContainerService = grapeContainerService;
        this.grapeContainerMapper = grapeContainerMapper;
    }

    @Transactional(readOnly = true)
    public List<GrapeContainerDto> findAll() {
        return grapeContainerMapper.mapToList(grapeContainerService.findAll());
    }

    @Transactional(readOnly = true)
    public GrapeContainerDto findById(Long id) {
        return grapeContainerMapper.mapToDto(grapeContainerService.findById(id));
    }

    @Transactional(readOnly = true)
    public GrapeContainerDto findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        return grapeContainerMapper.mapToDto(grapeContainerService.findByGrapeTypeAndHarvestYear(grapeType, harvestYear));
    }

    public GrapeContainerDto add(GrapeContainerDto newGrapeContainer) {
        return grapeContainerMapper.mapToDto(grapeContainerService.add(grapeContainerMapper.mapFromDto(newGrapeContainer)));
    }

    public GrapeContainerDto updateById(Long id, GrapeContainerDto updatedGrapeContainer) {
        return grapeContainerMapper.mapToDto(grapeContainerService.updateById(id, grapeContainerMapper.mapFromDto(updatedGrapeContainer)));
    }

    public GrapeContainerDto updateByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear, GrapeContainerDto updatedGrapeContainer) {
        return grapeContainerMapper.mapToDto(grapeContainerService.updateByGrapeTypeAndHarvestYear(grapeType, harvestYear, grapeContainerMapper.mapFromDto(updatedGrapeContainer)));
    }

    public GrapeContainerDto addQuantity(Long id, Quantity quantity) {
        return grapeContainerMapper.mapToDto(grapeContainerService.addQuantity(id, quantity));
    }

    public GrapeContainerDto subtractQuantity(Long id, Quantity quantity) {
        return grapeContainerMapper.mapToDto(grapeContainerService.subtractQuantity(id, quantity));
    }

    public GrapeContainerDto deleteById(Long id) {
        return grapeContainerMapper.mapToDto(grapeContainerService.deleteById(id));
    }

    public GrapeContainerDto deleteByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        return grapeContainerMapper.mapToDto(grapeContainerService.deleteByGrapeTypeAndHarvestYear(grapeType, harvestYear));
    }
}
