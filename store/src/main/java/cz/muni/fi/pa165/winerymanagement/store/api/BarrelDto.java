package cz.muni.fi.pa165.winerymanagement.store.api;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import io.swagger.v3.oas.annotations.media.Schema;


/**
 * Represents a data transfer object for a Barrel.
 */
@Schema(
		name = "BarrelDto",
		description = "Data transfer object representing a barrel used in the production of wine",
		example = """
                  {
                    "grapeType": {
                      "name": "Chardonnay",
                      "description": "Chardonnay white sweet wine description",
                      "color": "WHITE",
                      "sweetness": "SWEET"
                    },
                    "harvestYear": 2020,
                    "quantity": {
                      "type": "volume",
                      "value": 80.0,
                      "volumeUnit": "LITER"
                    }
                  }
                  """
)
public class BarrelDto {
	private Long id;
	private GrapeType grapeType;
	private int harvestYear;
	private VolumeQuantity quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrapeType getGrapeType() {
		return grapeType;
	}

	public void setGrapeType(GrapeType grapeType) {
		this.grapeType = grapeType;
	}

	public int getHarvestYear() {
		return harvestYear;
	}

	public void setHarvestYear(int harvestYear) {
		this.harvestYear = harvestYear;
	}

	public VolumeQuantity getQuantity() {
		return quantity;
	}

	public void setQuantity(VolumeQuantity quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "BarrelDto{" +
				"id=" + id +
				", grapeType=" + grapeType +
				", harvestYear=" + harvestYear +
				", quantity=" + quantity +
				'}';
	}
}
