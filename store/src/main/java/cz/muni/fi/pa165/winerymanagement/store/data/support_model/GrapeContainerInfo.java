package cz.muni.fi.pa165.winerymanagement.store.data.support_model;

/**
 * GrapeContainerInfo class represents the information of a container of grapes for a Production Item.
 * It contains the ID, type of grape, and the harvest year.
 */
public class GrapeContainerInfo {
    private Long id;
    private GrapeType grapeType;
    private int harvestYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeType getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeType grapeType) {
        this.grapeType = grapeType;
    }

    public int getHarvestYear() {
        return harvestYear;
    }

    public void setHarvestYear(int harvestYear) {
        this.harvestYear = harvestYear;
    }

    @Override
    public String toString() {
        return "GrapeContainerInfoDto{" +
                "id=" + id +
                ", grapeType=" + grapeType +
                ", harvestYear=" + harvestYear +
                '}';
    }
}
