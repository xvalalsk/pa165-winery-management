package cz.muni.fi.pa165.winerymanagement.user.facade;

import cz.muni.fi.pa165.winerymanagement.user.api.UserRegistrationDto;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import cz.muni.fi.pa165.winerymanagement.user.mappers.UserMapper;
import cz.muni.fi.pa165.winerymanagement.user.service.AuthorizationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AuthorizationFacadeTest {
    private AuthorizationFacade authorizationFacade;
    private AuthorizationService authorizationService;
    private UserMapper userMapper;
    private User user;
    private UserRegistrationDto userRegistrationDto;

    @BeforeEach
    void setUp() {
        authorizationService = mock(AuthorizationService.class);
        userMapper = mock(UserMapper.class);
        user = mock(User.class);

        authorizationFacade = new AuthorizationFacade(authorizationService, userMapper);

        userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setIssuer("MUNI");
        userRegistrationDto.setExternalId("External_ID");
    }

    @Test
    void registerValidUser() {
        when(authorizationService.register(any())).thenReturn(user);
        when(user.getId()).thenReturn(0L);
        authorizationFacade.register(userRegistrationDto);

        verify(authorizationService, times(1)).register(any());
    }
}
