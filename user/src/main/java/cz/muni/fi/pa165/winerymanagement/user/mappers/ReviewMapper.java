package cz.muni.fi.pa165.winerymanagement.user.mappers;

import cz.muni.fi.pa165.winerymanagement.user.api.ReviewCreateDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewUpdateDto;
import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.UserRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class ReviewMapper {
    @Autowired
    protected UserRepository userRepository;

    public abstract ReviewDto mapToReviewDto(Review review);

    @Mapping(target = "user", expression = "java(userRepository.findById(reviewCreateDto.getUserId()).orElse(null))")
    public abstract Review mapToReview(ReviewCreateDto reviewCreateDto);

    @Mapping(target = "user", expression = "java(userRepository.findById(reviewUpdateDto.getUserId()).orElse(null))")
    public abstract Review mapToReview(ReviewUpdateDto reviewUpdateDto);
}
