package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Barrel;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.BarrelRepository;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelNotCreatedException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelWithGrapeTypeAndYearNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelWithIdNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class BarrelService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BarrelService.class);
    private final BarrelRepository barrelRepository;

    @Autowired
    public BarrelService(BarrelRepository barrelRepository) {
        this.barrelRepository = barrelRepository;
    }

    @Transactional(readOnly = true)
    public List<Barrel> findAll() {
        LOGGER.info("Retrieving all barrels from repository");
        return barrelRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Barrel findById(Long id) {
        LOGGER.info("Retrieving barrel with id {} from repository", id);
        return barrelRepository.findById(id).orElseThrow(() -> new BarrelWithIdNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public Barrel findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        LOGGER.info("Retrieving barrel with grape type {} and harvest year {} from repository", grapeType, harvestYear);
        Barrel barrel = barrelRepository.findByGrapeTypeAndHarvestYear(grapeType, harvestYear);
        if (barrel == null) {
            LOGGER.warn("Barrel with grapeType: {} and harvestYear: {} not found in repository!", grapeType, harvestYear);
            throw new BarrelWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        return barrel;
    }

    public Barrel add(Barrel newBarrel) {
        LOGGER.info("Adding new barrel: {} to repository", newBarrel);
        Barrel barrel = barrelRepository.findByGrapeTypeAndHarvestYear(newBarrel.getGrapeType(), newBarrel.getHarvestYear());
        if (barrel != null) {
            LOGGER.warn("Barrel with grapeType: {} and harvestYear: {} already exists", newBarrel.getGrapeType(), newBarrel.getHarvestYear());
            throw new BarrelAlreadyExistsException(newBarrel.getGrapeType(), newBarrel.getHarvestYear());
        }
        return barrelRepository.save(newBarrel);
    }

    public Barrel updateById(Long id, Barrel updatedBarrel) {
        LOGGER.info("Updating barrel with id {} in repository with new data: {}", id, updatedBarrel);
        Barrel barrel = barrelRepository.findById(id).orElseThrow(() -> new BarrelWithIdNotFoundException(id));
        barrel.setGrapeType(updatedBarrel.getGrapeType());
        barrel.setHarvestYear(updatedBarrel.getHarvestYear());
        barrel.setQuantity(updatedBarrel.getQuantity());
        return barrelRepository.save(barrel);
    }

    public Barrel updateByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear, Barrel updatedBarrel) {
        LOGGER.info("Updating barrel with grape type {} and harvest year {} in repository with new data: {}", grapeType, harvestYear, updatedBarrel);
        Barrel barrel = barrelRepository.findByGrapeTypeAndHarvestYear(grapeType, harvestYear);
        if (barrel == null) {
            LOGGER.warn("Barrel with grapeType: {} and harvestYear: {} not found in repository!", grapeType, harvestYear);
            throw new BarrelWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        barrel.setGrapeType(updatedBarrel.getGrapeType());
        barrel.setHarvestYear(updatedBarrel.getHarvestYear());
        barrel.setQuantity(updatedBarrel.getQuantity());
        return barrelRepository.save(barrel);
    }

    public Barrel addQuantity(Long id, Quantity quantity) {
        LOGGER.info("Adding quantity {} to barrel with id {} in repository", quantity, id);
        isQuantityPositive(quantity.getValue());
        Barrel barrel = barrelRepository.findById(id).orElseThrow(() -> new BarrelWithIdNotFoundException(id));
        barrel.setQuantity((VolumeQuantity) barrel.getQuantity().add(quantity));
        return barrel;
    }

    public Barrel subtractQuantity(Long id, Quantity quantity) {
        LOGGER.info("Subtracting quantity {} from barrel with id {} in repository", quantity, id);
        isQuantityPositive(quantity.getValue());
        Barrel barrel = barrelRepository.findById(id).orElseThrow(() -> new BarrelWithIdNotFoundException(id));
        barrel.setQuantity((VolumeQuantity) barrel.getQuantity().subtract(quantity));
        return barrel;
    }

    public Barrel deleteById(Long id) {
        LOGGER.info("Deleting barrel with id {} from repository", id);
        Barrel barrel = barrelRepository.findById(id).orElseThrow(() -> new BarrelWithIdNotFoundException(id));
        barrelRepository.deleteById(id);
        return barrel;
    }

    public Barrel deleteByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        LOGGER.info("Deleting barrel with grape type {} and harvest year {} from repository", grapeType, harvestYear);
        Barrel barrel = barrelRepository.findByGrapeTypeAndHarvestYear(grapeType, harvestYear);
        if (barrel == null) {
            LOGGER.warn("Barrel with grapeType: {} and harvestYear: {} not found in repository!", grapeType, harvestYear);
            throw new BarrelWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        barrelRepository.deleteById(barrel.getId());
        return barrel;
    }

    private boolean isQuantityPositive(double value) {
        if (value < 0.0) {
            throw new BarrelNotCreatedException("Ingredient's value cannot be negative.");
        }
        return true;
    }
}
