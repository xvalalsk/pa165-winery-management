package cz.muni.fi.pa165.winerymanagement.harvest.mappers;

import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestInputDto;
import cz.muni.fi.pa165.winerymanagement.harvest.api.HarvestDto;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HarvestMapper {
    HarvestDto mapToHarvestDto(Harvest harvest);
    List<HarvestDto> mapToHarvestDtoList(List<Harvest> harvests);
    Harvest mapFromHarvestDto(HarvestDto harvestDto);
    Harvest mapFromHarvestInputDto(HarvestInputDto harvestInputDto);
}
