import {useEffect, useState} from 'react';
import {NavBar} from "@/pages";
import Cookies from "js-cookie";

const Barrels = () => {
    const [token, setToken] = useState(null);
    const [barrels, setBarrels] = useState([]);
    const [productionItems, setProductionItems] = useState([]);
    const [selectedProductionItem, setSelectedProductionItem] = useState(null);
    const [quantityValue, setQuantityValue] = useState('');
    const [quantityUnit, setQuantityUnit] = useState('LITER');
    const [error, setError] = useState('');

    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        fetchBarrels();
        fetchProductionItems();
    }, []);

    const getFilteredProductionItems = () => {
        const usedGrapes = barrels.map((barrel) => ({
            grapeType: barrel.grapeType.name,
            harvestYear: barrel.harvestYear,
        }));

        return productionItems.filter((item) => {
            const grapeType = item.grape.grapeType.name;
            const harvestYear = item.grape.harvestYear;

            return !usedGrapes.some(
                (usedGrape) =>
                    usedGrape.grapeType === grapeType && usedGrape.harvestYear === harvestYear
            );
        });
    };

    const fetchBarrels = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/barrels', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setBarrels(data))
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const fetchProductionItems = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/production-items',{
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                setProductionItems(data)
                console.log("data", data)
            })
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const deleteBarrel = (id) => {
        fetch(`/api/v1/production/barrels/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then(() => {
                setBarrels(barrels.filter((barrel) => barrel.id !== id));
            })
            .catch((error) => setError(error.message));
    };

    const handleInputChange = (event) => {
        const {name, value} = event.target;

        switch (name) {
            case 'productionItem':
                setSelectedProductionItem(
                    productionItems.find((item) => item.id === parseInt(value))
                );
                break;
            case 'quantityValue':
                setQuantityValue(value);
                break;
            case 'quantityUnit':
                setQuantityUnit(value);
                break;
            default:
                break;
        }
    };

    const submitForm = (event) => {
        event.preventDefault();

        if (!selectedProductionItem || !quantityValue) {
            setError('Please fill in all required fields.');
            return;
        }

        const newBarrel = {
            grapeType: selectedProductionItem.grape.grapeType,
            harvestYear: selectedProductionItem.grape.harvestYear,
            quantity: {
                type: 'volume',
                value: parseFloat(quantityValue),
                volumeUnit: quantityUnit,
            },
        };

        fetch('/api/v1/production/barrels', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(newBarrel),
        })
            .then((response) => response.json())
            .then((data) => {
                setBarrels([...barrels, data]);
                setSelectedProductionItem(null);
                setQuantityValue('');
                setError('');
            })
            .catch((error) => setError(error.message));
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar/>
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Barrels</h1>
                <div>
                    <div className="mt-4">
                        <table className="table table-striped table-hover table-bordered">
                            <thead className="bg-secondary bg-gradient">
                            <tr>
                                <th scope="col">Grape Type</th>
                                <th scope="col">Harvest Year</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {barrels.map((barrel) => (
                                <tr key={barrel.id}>
                                    <td>{barrel.grapeType.name}</td>
                                    <td>{barrel.harvestYear}</td>
                                    <td>{`${barrel.quantity.value} ${barrel.quantity.symbol}`}</td>
                                    <td>
                                        <button
                                            className="btn btn-danger"
                                            onClick={() => deleteBarrel(barrel.id)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>

                        <div className="card mt-4">
                            <div className="card-body">
                                <h5 className="card-title">Add a new barrel</h5>
                                <form onSubmit={submitForm}>
                                    <div className="mb-3">
                                        <label htmlFor="productionItem" className="form-label">
                                            Production Item
                                        </label>
                                        <select
                                            className="form-select"
                                            id="productionItem"
                                            name="productionItem"
                                            value={selectedProductionItem?.id || ''}
                                            onChange={handleInputChange}
                                            required
                                        >
                                            <option value="">Select a production item</option>
                                            {getFilteredProductionItems().map((item) => (
                                                <option key={item.id} value={item.id}>
                                                    {item.grape.grapeType.name} ({item.grape.harvestYear})
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label htmlFor="quantityValue" className="form-label">
                                                Quantity Value
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                id="quantityValue"
                                                name="quantityValue"
                                                value={quantityValue}
                                                onChange={handleInputChange}
                                                required
                                            />
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label htmlFor="quantityUnit" className="form-label">
                                                Quantity Unit
                                            </label>
                                            <select
                                                className="form-select"
                                                id="quantityUnit"
                                                name="quantityUnit"
                                                value={quantityUnit}
                                                onChange={handleInputChange}
                                            >
                                                <option value="LITER">LITER</option>
                                                <option value="MILLILITER">MILLILITER</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <button type="submit" className="btn btn-primary">
                                            Add Barrel
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Barrels;