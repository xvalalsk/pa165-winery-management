package cz.muni.fi.pa165.winerymanagement.production.rest;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.api.GrapeContainerDto;
import cz.muni.fi.pa165.winerymanagement.production.facade.GrapeContainerFacade;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearGetRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearModifyQuantityRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearUpdateGrapeContainerRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/production")
@Tag(name = "Grape Containers", description = "API for managing grape containers")
@CrossOrigin(origins = "*")
public class GrapeContainerRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrapeContainerRestController.class);
    private final GrapeContainerFacade grapeContainerFacade;

    @Autowired
    public GrapeContainerRestController(GrapeContainerFacade grapeContainerFacade) {
        this.grapeContainerFacade = grapeContainerFacade;
    }

    @Operation(summary = "Get a list of all grape containers", description = "Returns a list of all grape containers.")
    @ApiResponse(responseCode = "200", description = "List of all grape containers returned successfully.")
    @GetMapping("/grape-containers")
    public ResponseEntity<List<GrapeContainerDto>> getAllGrapeContainers() {
        LOGGER.info("Getting all grape containers");
        return ResponseEntity.ok(grapeContainerFacade.findAll());
    }

    @Operation(summary = "Get a grape container by ID", description = "Returns an grape container with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape container with the specified ID returned successfully."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified ID was not found.")
    })
    @GetMapping("/grape-containers/{id}")
    public ResponseEntity<GrapeContainerDto> getGrapeContainerById(@PathVariable(value = "id") Long id) {
        LOGGER.info("Getting grape container by ID: {}", id);
        GrapeContainerDto grapeContainer = grapeContainerFacade.findById(id);
        return ResponseEntity.ok(grapeContainer);
    }

    @Operation(summary = "Get a grape container by Grape Type and Harvest Year", description = "Returns an grape container with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape container with the specified Grape Type and Harvest Year returned successfully."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified Grape Type and Harvest Year was not found.")
    })
    @GetMapping("/grape-containers/grapeType-and-year")
    public ResponseEntity<GrapeContainerDto> getGrapeContainerByTypeAndYear(@RequestBody GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        LOGGER.info("Getting grape container by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        GrapeContainerDto grapeContainer = grapeContainerFacade.findByGrapeTypeAndHarvestYear(grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        return ResponseEntity.ok(grapeContainer);
    }

    @Operation(summary = "Add a grape container", description = "Adds a new grape container.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Grape container added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input.")
    })
    @PostMapping("/grape-containers")
    public ResponseEntity<GrapeContainerDto> createGrapeContainer(@Valid @RequestBody GrapeContainerDto grapeContainer) {
        LOGGER.info("Creating a new grape container: {}", grapeContainer);
        GrapeContainerDto grapeContainerDto = grapeContainerFacade.add(grapeContainer);
        return ResponseEntity.status(HttpStatus.CREATED).body(grapeContainerDto);
    }

    @Operation(summary = "Update a grape container by ID", description = "Updates a grape container with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape container with the specified ID updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified ID was not found.")
    })
    @PutMapping("/grape-containers/{id}")
    public ResponseEntity<GrapeContainerDto> updateGrapeContainerById(@PathVariable Long id, @Valid @RequestBody GrapeContainerDto updatedGrapeContainer) {
        LOGGER.info("Updating grape container by ID: {} with new data: {}", id, updatedGrapeContainer);
        GrapeContainerDto grapeContainer = grapeContainerFacade.updateById(id, updatedGrapeContainer);
        return ResponseEntity.ok(grapeContainer);
    }

    @Operation(summary = "Update a grape container by Grape Type and Harvest Year", description = "Updates a grape container with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape container with the specified by Grape Type and Harvest Year updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified by Grape Type and Harvest Year was not found.")
    })
    @PutMapping("/grape-containers/grapeType-and-year")
    public ResponseEntity<GrapeContainerDto> updateGrapeContainerByTypeAndYear(@RequestBody GrapeTypeAndYearUpdateGrapeContainerRequest grapeTypeAndYearUpdateGrapeContainerRequest) {
        LOGGER.info("Updating grape container by Grape Type: {} and Harvest Year: {} with new data: {}", grapeTypeAndYearUpdateGrapeContainerRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearUpdateGrapeContainerRequest.getGrapeTypeAndYearGetRequest().getHarvestYear(), grapeTypeAndYearUpdateGrapeContainerRequest.getUpdatedGrapeContainer());
        GrapeContainerDto grapeContainer = grapeContainerFacade.updateByGrapeTypeAndHarvestYear(grapeTypeAndYearUpdateGrapeContainerRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearUpdateGrapeContainerRequest.getGrapeTypeAndYearGetRequest().getHarvestYear(), grapeTypeAndYearUpdateGrapeContainerRequest.getUpdatedGrapeContainer());
        return ResponseEntity.ok(grapeContainer);
    }

    @Operation(summary = "Add quantity to a grape container by ID", description = "Adds the specified quantity to a grape container with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of grape container with the specified ID added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified ID was not found.")
    })
    @PostMapping("/grape-containers/{id}/add-quantity")
    public ResponseEntity<GrapeContainerDto> addQuantityToGrapeContainerById(@PathVariable(value = "id") Long id, @RequestBody Quantity quantity) {
        LOGGER.info("Adding quantity {} to grape container by ID: {}", quantity, id);
        GrapeContainerDto grapeContainer = grapeContainerFacade.findById(id);
        return ResponseEntity.ok(grapeContainerFacade.addQuantity(grapeContainer.getId(), quantity));
    }

    @Operation(summary = "Add quantity to a grape container by Grape Type and Harvest Year", description = "Adds the specified quantity to a grape container with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of grape container with the specified Grape Type and Harvest Year added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified Grape Type and Harvest Year was not found.")
    })
    @PostMapping("/grape-containers/grapeType-and-year/add-quantity")
    public ResponseEntity<GrapeContainerDto> addQuantityToGrapeContainerByTypeAndYear(@RequestBody GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest) {
        LOGGER.info("Adding quantity {} to grape container by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearModifyQuantityRequest.getQuantity(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        GrapeContainerDto grapeContainer = grapeContainerFacade.findByGrapeTypeAndHarvestYear(grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        return ResponseEntity.ok(grapeContainerFacade.addQuantity(grapeContainer.getId(), grapeTypeAndYearModifyQuantityRequest.getQuantity()));
    }

    @Operation(summary = "Subtract quantity to a grape container by ID", description = "Subtracts the specified value to a grape container with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity from grape container with the specified ID subtracted successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified ID was not found.")
    })
    @PostMapping("/grape-containers/{id}/subtract-quantity")
    public ResponseEntity<GrapeContainerDto> subtractQuantityToGrapeContainerById(@PathVariable(value = "id") Long id, @RequestBody Quantity quantity) {
        LOGGER.info("Subtracting quantity {} from grape container by ID: {}", quantity, id);
        GrapeContainerDto grapeContainer = grapeContainerFacade.findById(id);
        return ResponseEntity.ok(grapeContainerFacade.subtractQuantity(grapeContainer.getId(), quantity));
    }

    @Operation(summary = "Subtract quantity to a grape container by Grape Type and Harvest Year", description = "Subtracts the specified quantity to a grape container with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of grape container with the specified Grape Type and Harvest Year subtracted successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified Grape Type and Harvest Year was not found.")
    })
    @PostMapping("/grape-containers/grapeType-and-year/subtract-quantity")
    public ResponseEntity<GrapeContainerDto> subtractQuantityToGrapeContainerByByTypeAndYear(@RequestBody GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest) {
        LOGGER.info("Subtracting quantity {} from grape container by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearModifyQuantityRequest.getQuantity(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        GrapeContainerDto grapeContainer = grapeContainerFacade.findByGrapeTypeAndHarvestYear(grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        return ResponseEntity.ok(grapeContainerFacade.subtractQuantity(grapeContainer.getId(), grapeTypeAndYearModifyQuantityRequest.getQuantity()));
    }

    @Operation(summary = "Delete a grape container by ID", description = "Deletes a grape container with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Grape container with the specified ID deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified ID was not found.")
    })
    @DeleteMapping("/grape-containers/{id}")
    public ResponseEntity<GrapeContainerDto> deleteGrapeContainerById(@PathVariable(value = "id") Long id) {
        LOGGER.info("Deleting grape container by ID: {}", id);
        GrapeContainerDto grapeContainerDto = grapeContainerFacade.deleteById(id);
        return ResponseEntity.ok(grapeContainerDto);
    }

    @Operation(summary = "Delete a grape container by Grape Type and Harvest Year", description = "Deletes a grape container with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Grape container with the specified Grape Type and Harvest Year deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Grape container with the specified Grape Type and Harvest Year was not found.")
    })
    @DeleteMapping("/grape-containers/grapeType-and-year")
    public ResponseEntity<GrapeContainerDto> deleteGrapeContainerByGrapeTypeAndHarvestYear(@RequestBody GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        LOGGER.info("Deleting grape container by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        GrapeContainerDto grapeContainerDto = grapeContainerFacade.deleteByGrapeTypeAndHarvestYear(grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        return ResponseEntity.ok(grapeContainerDto);
    }
}