package cz.muni.fi.pa165.winerymanagement.store.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.store.StoreApplication;
import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleDto;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.facade.WineBottleFacade;
import cz.muni.fi.pa165.winerymanagement.store.rest.exceptionhandling.CustomRestGlobalExceptionHandling;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(WineBottleRestController.class)
@ContextConfiguration(classes = StoreApplication.class)
public class WineBottleRestControllerTest {

	String API_PATH = "/api/v1/store/wine-bottles";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private WineBottleFacade wineBottleFacade;

	private final GrapeType grapeType1 = new GrapeType();
	private final GrapeType grapeType2 = new GrapeType();
	private final GrapeType grapeType3 = new GrapeType();

	VolumeQuantity volumeQuantity1L = new VolumeQuantity(1, VolumeQuantity.VolumeUnit.LITER);
	VolumeQuantity volumeQuantity075L = new VolumeQuantity(0.75, VolumeQuantity.VolumeUnit.LITER);

	private WineBottleDto wineBottleDto1;
	private WineBottleDto wineBottleDto2;
	private WineBottleDto wineBottleDto3;

	@BeforeEach
	void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new WineBottleRestController(wineBottleFacade)).setControllerAdvice(CustomRestGlobalExceptionHandling.class).build();

		grapeType1.setName("Chardonnay");
		grapeType1.setDescription("Chardonnay white sweet wine description");
		grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
		grapeType1.setColor(GrapeType.Color.WHITE);

		grapeType2.setName("Zweigeltrebe");
		grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
		grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
		grapeType2.setColor(GrapeType.Color.ROSE);

		grapeType3.setName("Merlot");
		grapeType3.setDescription("Merlot red dry wine description");
		grapeType3.setSweetness(GrapeType.Sweetness.DRY);
		grapeType3.setColor(GrapeType.Color.RED);

		wineBottleDto1 = new WineBottleDto();
		wineBottleDto1.setId(1L);
		wineBottleDto1.setGrapeType(grapeType1);
		wineBottleDto1.setHarvestYear(2011);
		wineBottleDto1.setVolume(volumeQuantity1L);

		wineBottleDto2 = new WineBottleDto();
		wineBottleDto2.setId(2L);
		wineBottleDto2.setGrapeType(grapeType2);
		wineBottleDto2.setHarvestYear(2022);
		wineBottleDto2.setVolume(volumeQuantity075L);

		wineBottleDto3 = new WineBottleDto();
		wineBottleDto3.setId(3L);
		wineBottleDto3.setGrapeType(grapeType3);
		wineBottleDto3.setHarvestYear(2013);
		wineBottleDto3.setVolume(volumeQuantity075L);
	}

	@Test
	void testGetAllWineBottles() throws Exception{

		List<WineBottleDto> wineBottles = Arrays.asList(wineBottleDto1, wineBottleDto2, wineBottleDto3);

		when(wineBottleFacade.findAll()).thenReturn(wineBottles);
		mockMvc.perform(get(API_PATH))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(3)))

				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].grapeType.name", is("Chardonnay")))
				.andExpect(jsonPath("$[0].grapeType.description", is("Chardonnay white sweet wine description")))
				.andExpect(jsonPath("$[0].grapeType.color", is("WHITE")))
				.andExpect(jsonPath("$[0].grapeType.sweetness", is("SWEET")))
				.andExpect(jsonPath("$[0].harvestYear", is(2011)))
				.andExpect(jsonPath("$[0].volume.type", is("volume")))
				.andExpect(jsonPath("$[0].volume.value", is(1.0)))
				.andExpect(jsonPath("$[0].volume.volumeUnit", is("LITER")))

				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].grapeType.name", is("Zweigeltrebe")))
				.andExpect(jsonPath("$[1].grapeType.description", is("Zweigeltrebe rose semi-sweet wine description")))
				.andExpect(jsonPath("$[1].grapeType.color", is("ROSE")))
				.andExpect(jsonPath("$[1].grapeType.sweetness", is("SEMISWEET")))
				.andExpect(jsonPath("$[1].harvestYear", is(2022)))
				.andExpect(jsonPath("$[1].volume.type", is("volume")))
				.andExpect(jsonPath("$[1].volume.value", is(0.75)))
				.andExpect(jsonPath("$[1].volume.volumeUnit", is("LITER")))

				.andExpect(jsonPath("$[2].id", is(3)))
				.andExpect(jsonPath("$[2].grapeType.name", is("Merlot")))
				.andExpect(jsonPath("$[2].grapeType.description", is("Merlot red dry wine description")))
				.andExpect(jsonPath("$[2].grapeType.color", is("RED")))
				.andExpect(jsonPath("$[2].grapeType.sweetness", is("DRY")))
				.andExpect(jsonPath("$[2].harvestYear", is(2013)))
				.andExpect(jsonPath("$[2].volume.type", is("volume")))
				.andExpect(jsonPath("$[2].volume.value", is(0.75)))
				.andExpect(jsonPath("$[2].volume.volumeUnit", is("LITER")));

	}

	@Test
	void testGetWineBottleById_1() throws Exception {
		long id = wineBottleDto1.getId();

		when(wineBottleFacade.findById(id)).thenReturn(wineBottleDto1);

		mockMvc.perform(get(API_PATH + "/{id}", id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is((int) id)))
				.andExpect(jsonPath("grapeType.name", is("Chardonnay")))
				.andExpect(jsonPath("grapeType.description", is("Chardonnay white sweet wine description")))
				.andExpect(jsonPath("grapeType.color", is("WHITE")))
				.andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
				.andExpect(jsonPath("harvestYear", is(2011)))
				.andExpect(jsonPath("volume.type", is("volume")))
				.andExpect(jsonPath("volume.value", is(1.0)))
				.andExpect(jsonPath("volume.volumeUnit", is("LITER")));

		verify(wineBottleFacade).findById(id);
	}

	@Test
	void testGetWineBottleById_3() throws Exception {
		long id = wineBottleDto3.getId();

		when(wineBottleFacade.findById(id)).thenReturn(wineBottleDto3);

		mockMvc.perform(get(API_PATH + "/{id}", id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is((int) id)))
				.andExpect(jsonPath("grapeType.name", is("Merlot")))
				.andExpect(jsonPath("grapeType.description", is("Merlot red dry wine description")))
				.andExpect(jsonPath("grapeType.color", is("RED")))
				.andExpect(jsonPath("grapeType.sweetness", is("DRY")))
				.andExpect(jsonPath("harvestYear", is(2013)))
				.andExpect(jsonPath("volume.type", is("volume")))
				.andExpect(jsonPath("volume.value", is(0.75)))
				.andExpect(jsonPath("volume.volumeUnit", is("LITER")));

		verify(wineBottleFacade).findById(id);
	}

	@Test
	void testGetWineBottleById_NotFound() throws Exception {
		long id = 1L;

		when(wineBottleFacade.findById(id)).thenThrow(new WineBottleWithIdNotFoundException(id));

		mockMvc.perform(get(API_PATH + "/{id}", id))
				.andExpect(status().isNotFound());
	}

	@Test
	void testCreateWineBottle_Created() throws Exception {
		when(wineBottleFacade.add(ArgumentMatchers.any())).thenReturn(wineBottleDto2);

		mockMvc.perform(post(API_PATH)
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(wineBottleDto2)))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(wineBottleDto2.getId().intValue())))
				.andExpect(jsonPath("grapeType.name", is(wineBottleDto2.getGrapeType().getName())))
				.andExpect(jsonPath("grapeType.description", is(wineBottleDto2.getGrapeType().getDescription())))
				.andExpect(jsonPath("grapeType.color", is(wineBottleDto2.getGrapeType().getColor().toString())))
				.andExpect(jsonPath("grapeType.sweetness", is(wineBottleDto2.getGrapeType().getSweetness().toString())))
				.andExpect(jsonPath("harvestYear", is(2022)))
				.andExpect(jsonPath("volume.value", is(0.75)))
				.andExpect(jsonPath("volume.volumeUnit", is("LITER")));
	}

	@Test
	void testUpdateWineBottleById_Updated() throws Exception {
		long id = wineBottleDto1.getId();

		wineBottleDto1.setHarvestYear(1111);
		when(wineBottleFacade.updateById(ArgumentMatchers.any(long.class), ArgumentMatchers.any())).thenReturn(wineBottleDto1);

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleDto1);

		mockMvc.perform(put(API_PATH + "/{id}", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is((int) id)))
				.andExpect(jsonPath("grapeType.name", is("Chardonnay")))
				.andExpect(jsonPath("grapeType.description", is("Chardonnay white sweet wine description")))
				.andExpect(jsonPath("grapeType.color", is("WHITE")))
				.andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
				.andExpect(jsonPath("harvestYear", is(1111)))
				.andExpect(jsonPath("volume.type", is("volume")))
				.andExpect(jsonPath("volume.value", is(1.0)))
				.andExpect(jsonPath("volume.volumeUnit", is("LITER")));
	}

	@Test
	void testUpdateWineBottleById_NotFound() throws Exception {
		long id = wineBottleDto1.getId();

		wineBottleDto1.setHarvestYear(1111);
		when(wineBottleFacade.updateById(ArgumentMatchers.any(long.class), ArgumentMatchers.any())).thenThrow(new WineBottleWithIdNotFoundException(id));

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleDto1);

		mockMvc.perform(put(API_PATH + "/{id}", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isNotFound());
	}

	@Test
	void testDeleteWineBottleById_Deleted() throws Exception {
		long id = 1L;
		when(wineBottleFacade.deleteById(id)).thenReturn(wineBottleDto1);

		mockMvc.perform(delete(API_PATH + "/{id}", id))
				.andExpect(status().isOk());
	}

	@Test
	void testDeleteWineBottleById_NotFound() throws Exception {
		long id = 1L;
		when(wineBottleFacade.deleteById(id)).thenThrow(new WineBottleWithIdNotFoundException(id));

		mockMvc.perform(delete(API_PATH + "/{id}", id))
				.andExpect(status().isNotFound());
	}
}