package cz.muni.fi.pa165.winerymanagement.core.exceptions;

import java.util.ResourceBundle;

public class WrongQuantityTypeException extends RuntimeException {
    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");

    public WrongQuantityTypeException() {}

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        return messages.getString(key);
    }
}
