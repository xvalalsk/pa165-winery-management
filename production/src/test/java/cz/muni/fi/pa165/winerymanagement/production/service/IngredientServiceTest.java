package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.IngredientRepository;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class IngredientServiceTest {

	@Mock
	private IngredientRepository ingredientRepository;

	@InjectMocks
	private IngredientService ingredientService;

	private Ingredient ingredient1;
	private Ingredient ingredient2;

	@BeforeEach
	public void setUp() {
		ingredient1 = new Ingredient();
		ingredient1.setId(1L);
		ingredient1.setName("Ingredient 1");
		ingredient1.setQuantity(new WeightQuantity(10.0, WeightQuantity.WeightUnit.KILOGRAM));

		ingredient2 = new Ingredient();
		ingredient2.setId(2L);
		ingredient2.setName("Ingredient 2");
		ingredient2.setQuantity(new VolumeQuantity(20.0, VolumeQuantity.VolumeUnit.LITER));
	}

	@Test
	void testFindAll() {
		when(ingredientRepository.findAll()).thenReturn(Arrays.asList(ingredient1, ingredient2));

		List<Ingredient> ingredients = ingredientService.findAll();

		assertEquals(2, ingredients.size());
		assertTrue(ingredients.contains(ingredient1));
		assertTrue(ingredients.contains(ingredient2));

		verify(ingredientRepository, times(1)).findAll();
	}

	@Test
	void testFindById() {
		when(ingredientRepository.findById(1L)).thenReturn(Optional.of(ingredient1));

		Ingredient foundIngredient = ingredientService.findById(1L);

		assertEquals(ingredient1, foundIngredient);
		verify(ingredientRepository, times(1)).findById(1L);
	}

	@Test
	void testFindByIdNotFound() {
		when(ingredientRepository.findById(3L)).thenReturn(Optional.empty());

		assertThrows(IngredientWithIdNotFoundException.class, () -> ingredientService.findById(3L));
		verify(ingredientRepository, times(1)).findById(3L);
	}

	@Test
	void testFindByName() {
		when(ingredientRepository.findByName("Ingredient 1")).thenReturn(Optional.of(ingredient1));

		Ingredient foundIngredient = ingredientService.findByName("Ingredient 1");

		assertEquals(ingredient1, foundIngredient);
		verify(ingredientRepository, times(1)).findByName("Ingredient 1");
	}

	@Test
	void testFindByNameNotFound() {
		when(ingredientRepository.findByName("Ingredient 3")).thenReturn(Optional.empty());

		assertThrows(IngredientWithNameNotFoundException.class, () -> ingredientService.findByName("Ingredient 3"));
		verify(ingredientRepository, times(1)).findByName("Ingredient 3");
	}

	@Test
	void testAdd() {
		when(ingredientRepository.findByName("Ingredient 1")).thenReturn(Optional.empty());
		when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient1);

		Ingredient addedIngredient = ingredientService.add(ingredient1);

		assertEquals(ingredient1, addedIngredient);
		verify(ingredientRepository, times(1)).findByName("Ingredient 1");
		verify(ingredientRepository, times(1)).save(ingredient1);
	}

	@Test
	void testAddWithBlankName() {
		Ingredient ingredientWithBlankName = new Ingredient();
		ingredientWithBlankName.setName("   ");
		ingredientWithBlankName.setQuantity(new WeightQuantity(10.0, WeightQuantity.WeightUnit.KILOGRAM));

		assertThrows(IngredientNotCreatedException.class, () -> ingredientService.add(ingredientWithBlankName));
	}

	@Test
	void testAddWithExistingName() {
		when(ingredientRepository.findByName("Ingredient 1")).thenReturn(Optional.of(ingredient1));

		assertThrows(IngredientAlreadyExistsException.class, () -> ingredientService.add(ingredient1));
		verify(ingredientRepository, times(1)).findByName("Ingredient 1");
	}

	@Test
	void testAddWithNegativeQuantity() {
		Ingredient ingredientWithNegativeQuantity = new Ingredient();
		ingredientWithNegativeQuantity.setName("Ingredient 3");
		ingredientWithNegativeQuantity.setQuantity(new WeightQuantity(-10, WeightQuantity.WeightUnit.KILOGRAM));

		assertThrows(IngredientNotCreatedException.class, () -> ingredientService.add(ingredientWithNegativeQuantity));
	}
	@Test
	void testUpdateById() {
		when(ingredientRepository.findById(1L)).thenReturn(Optional.of(ingredient1));
		when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient1);

		Ingredient updatedIngredient = new Ingredient();
		updatedIngredient.setName("Updated Ingredient 1");
		updatedIngredient.setQuantity(new WeightQuantity(12.0, WeightQuantity.WeightUnit.KILOGRAM));

		Ingredient result = ingredientService.updateById(1L, updatedIngredient);

		assertEquals("Updated Ingredient 1", result.getName());
		assertEquals(12.0, result.getQuantity().getValue());
		verify(ingredientRepository, times(1)).findById(1L);
		verify(ingredientRepository, times(1)).save(ingredient1);
	}

	@Test
	void testUpdateByIdNotFound() {
		when(ingredientRepository.findById(3L)).thenReturn(Optional.empty());

		Ingredient updatedIngredient = new Ingredient();
		updatedIngredient.setName("Updated Ingredient 3");
		updatedIngredient.setQuantity(new WeightQuantity(15.0, WeightQuantity.WeightUnit.KILOGRAM));

		assertThrows(IngredientWithIdNotFoundException.class, () -> ingredientService.updateById(3L, updatedIngredient));
		verify(ingredientRepository, times(1)).findById(3L);
	}

	@Test
	void testUpdateByName() {
		when(ingredientRepository.findByName("Ingredient 1")).thenReturn(Optional.of(ingredient1));
		when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient1);

		Ingredient updatedIngredient = new Ingredient();
		updatedIngredient.setName("Updated Ingredient 1");
		updatedIngredient.setQuantity(new WeightQuantity(12.0, WeightQuantity.WeightUnit.KILOGRAM));

		Ingredient result = ingredientService.updateByName("Ingredient 1", updatedIngredient);

		assertEquals("Updated Ingredient 1", result.getName());
		assertEquals(12.0, result.getQuantity().getValue());
		verify(ingredientRepository, times(1)).findByName("Ingredient 1");
		verify(ingredientRepository, times(1)).save(ingredient1);
	}

	@Test
	void testUpdateByNameNotFound() {
		when(ingredientRepository.findByName("Ingredient 3")).thenReturn(Optional.empty());

		Ingredient updatedIngredient = new Ingredient();
		updatedIngredient.setName("Updated Ingredient 3");
		updatedIngredient.setQuantity(new WeightQuantity(15.0, WeightQuantity.WeightUnit.KILOGRAM));

		assertThrows(IngredientWithNameNotFoundException.class, () -> ingredientService.updateByName("Ingredient 3", updatedIngredient));
		verify(ingredientRepository, times(1)).findByName("Ingredient 3");
	}

	@Test
	void testAddQuantity() {
		when(ingredientRepository.findById(1L)).thenReturn(Optional.of(ingredient1));

		WeightQuantity addedQuantity = new WeightQuantity(5.0, WeightQuantity.WeightUnit.KILOGRAM);
		Ingredient result = ingredientService.addQuantity(1L, addedQuantity);

		assertEquals(15000.0, result.getQuantity().getValue());
		verify(ingredientRepository, times(1)).findById(1L);
	}

	@Test
	void testAddQuantityNotFound() {
		when(ingredientRepository.findById(3L)).thenReturn(Optional.empty());

		WeightQuantity addedQuantity = new WeightQuantity(5.0, WeightQuantity.WeightUnit.KILOGRAM);
		assertThrows(IngredientWithIdNotFoundException.class, () -> ingredientService.addQuantity(3L, addedQuantity));
		verify(ingredientRepository, times(1)).findById(3L);
	}

	@Test
	void testSubtractQuantity() {
		when(ingredientRepository.findById(1L)).thenReturn(Optional.of(ingredient1));

		WeightQuantity subtractedQuantity = new WeightQuantity(5.0, WeightQuantity.WeightUnit.KILOGRAM);
		Ingredient result = ingredientService.subtractQuantity(1L, subtractedQuantity);

		assertEquals(5000.0, result.getQuantity().getValue());
		verify(ingredientRepository, times(1)).findById(1L);
	}

	@Test
	void testSubtractQuantityNotFound() {
		when(ingredientRepository.findById(3L)).thenReturn(Optional.empty());

		WeightQuantity subtractedQuantity = new WeightQuantity(5.0, WeightQuantity.WeightUnit.KILOGRAM);
		assertThrows(IngredientWithIdNotFoundException.class, () -> ingredientService.subtractQuantity(3L, subtractedQuantity));
		verify(ingredientRepository, times(1)).findById(3L);
	}

	@Test
	void testTrySubtractQuantity() {
		when(ingredientRepository.findById(1L)).thenReturn(Optional.of(ingredient1));

		WeightQuantity subtractedQuantity = new WeightQuantity(5.0, WeightQuantity.WeightUnit.KILOGRAM);
		double result = ingredientService.trySubtractQuantity(1L, subtractedQuantity);

		assertEquals(5000.0, result);
		verify(ingredientRepository, times(1)).findById(1L);
	}

	@Test
	void testTrySubtractQuantityNotFound() {
		when(ingredientRepository.findById(3L)).thenReturn(Optional.empty());

		WeightQuantity subtractedQuantity = new WeightQuantity(5.0, WeightQuantity.WeightUnit.KILOGRAM);
		assertThrows(IngredientWithIdNotFoundException.class, () -> ingredientService.trySubtractQuantity(3L, subtractedQuantity));
		verify(ingredientRepository, times(1)).findById(3L);
	}

	@Test
	void testDeleteById() {
		when(ingredientRepository.findById(1L)).thenReturn(Optional.of(ingredient1));

		Ingredient result = ingredientService.deleteById(1L);

		assertEquals(ingredient1, result);
		verify(ingredientRepository, times(1)).findById(1L);
		verify(ingredientRepository, times(1)).deleteById(1L);
	}

	@Test
	void testDeleteByIdNotFound() {
		when(ingredientRepository.findById(3L)).thenReturn(Optional.empty());

		assertThrows(IngredientWithIdNotFoundException.class, () -> ingredientService.deleteById(3L));
		verify(ingredientRepository, times(1)).findById(3L);
	}

	@Test
	void testDeleteByName() {
		when(ingredientRepository.findByName("Ingredient 1")).thenReturn(Optional.of(ingredient1));

		Ingredient result = ingredientService.deleteByName("Ingredient 1");

		assertEquals(ingredient1, result);
		verify(ingredientRepository, times(1)).findByName("Ingredient 1");
		verify(ingredientRepository, times(1)).deleteById(1L);
	}

	@Test
	void testDeleteByNameNotFound() {
		when(ingredientRepository.findByName("Ingredient 3")).thenReturn(Optional.empty());

		assertThrows(IngredientWithNameNotFoundException.class, () -> ingredientService.deleteByName("Ingredient 3"));
		verify(ingredientRepository, times(1)).findByName("Ingredient 3");
	}
}