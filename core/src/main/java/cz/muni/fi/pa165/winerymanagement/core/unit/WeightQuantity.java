package cz.muni.fi.pa165.winerymanagement.core.unit;

import com.fasterxml.jackson.annotation.JsonTypeName;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.WrongQuantityTypeException;

@JsonTypeName("weight")
public class WeightQuantity extends Quantity {
    public enum WeightUnit implements Unit {
        GRAM("g"),
        KILOGRAM("kg");

        private final String symbol;

        WeightUnit(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }

    private WeightUnit weightUnit;

    public WeightQuantity() {
        super();
    }

    public WeightQuantity(double value, WeightUnit weightUnit) {
        super(value, weightUnit.getSymbol());
        this.weightUnit = weightUnit;
        this.value = value;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(WeightUnit weightUnit) {
        this.weightUnit = weightUnit;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String getSymbol() {
        return weightUnit.getSymbol();
    }


    public void convertTo(WeightUnit newWeightUnit) {
        double newValue;
        switch (newWeightUnit) {
            case GRAM:
                newValue = toGram();
                break;
            case KILOGRAM:
                newValue = toKilogram();
                break;
            default:
                throw new IllegalArgumentException("Invalid unit: " + newWeightUnit);
        }
        super.value = newValue;
    }

    private double toGram() {
        switch (weightUnit) {
            case GRAM:
                return getValue();
            case KILOGRAM:
                return getValue() * 1000.0;
            default:
                throw new IllegalStateException("Unknown unit: " + weightUnit);
        }
    }

    private double toKilogram() {
        switch (weightUnit) {
            case GRAM:
                return getValue() / 1000.0;
            case KILOGRAM:
                return getValue();
            default:
                throw new IllegalStateException("Unknown unit: " + weightUnit);
        }
    }

    @Override
    public Quantity add(Quantity other) {
        if (!(other instanceof WeightQuantity otherVolume)) {
            throw new IllegalArgumentException("Cannot add volume to weight or vice versa");
        }
        double newValue = this.toGram() + otherVolume.toGram();
        return new WeightQuantity(newValue, WeightUnit.GRAM);
    }

    @Override
    public Quantity subtract(Quantity other) {
        if (!(other instanceof WeightQuantity otherVolume)) {
            throw new IllegalArgumentException("Cannot add volume to weight or vice versa");
        }
        double newValue = this.toGram() - otherVolume.toGram();
        return new WeightQuantity(newValue, WeightUnit.GRAM);
    }

    @Override
    public double calculateSubtractValue(Quantity other) throws NotEnoughQuantityException {
        WeightQuantity otherWeight = checkQuantityType(other);

        double newValue = this.toGram() - otherWeight.toGram();
        if (newValue < 0) {
            throw new NotEnoughQuantityException("weight", this.toGram(), "grams", otherWeight.toGram(), "grams");
        }
        return newValue;
    }

    private static WeightQuantity checkQuantityType(Quantity other) {
        if (!(other instanceof WeightQuantity otherWeight)) {
            throw new WrongQuantityTypeException();
        }
        return otherWeight;
    }
}
