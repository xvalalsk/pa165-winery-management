package cz.muni.fi.pa165.winerymanagement.user.mappers;

import cz.muni.fi.pa165.winerymanagement.user.api.UserRegistrationDto;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User mapFromRegistrationDto(UserRegistrationDto userRegistrationDto);
}
