package cz.muni.fi.pa165.winerymanagement.production.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import cz.muni.fi.pa165.winerymanagement.production.data.repository.GrapeContainerRepository;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerWithGrapeTypeAndYearNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientNotCreatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GrapeContainerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrapeContainerService.class);
    private final GrapeContainerRepository grapeContainerRepository;

    @Autowired
    public GrapeContainerService(GrapeContainerRepository grapeContainerRepository) {
        this.grapeContainerRepository = grapeContainerRepository;
    }

    @Transactional(readOnly = true)
    public List<GrapeContainer> findAll() {
        LOGGER.info("Retrieving all GrapeContainers from repository");
        return grapeContainerRepository.findAll();
    }

    @Transactional(readOnly = true)
    public GrapeContainer findById(Long id) {
        LOGGER.info("Retrieving GrapeContainer with id {} from repository", id);
        return grapeContainerRepository.findById(id).orElseThrow(() -> new GrapeContainerWithIdNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public GrapeContainer findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        LOGGER.info("Retrieving GrapeContainer with grapeType {} and harvestYear {} from repository", grapeType, harvestYear);
        GrapeContainer grapeContainer = grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType, harvestYear);
        if (grapeContainer == null) {
            LOGGER.warn("GrapeContainer with grapeType {} and harvestYear {} not found in repository!", grapeType, harvestYear);
            throw new GrapeContainerWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        return grapeContainer;
    }

    public GrapeContainer add(GrapeContainer newGrapeContainer) {
        LOGGER.info("Adding new GrapeContainer to repository");
        if (!isGrapeTypeAndHarvestYearAvailable(newGrapeContainer.getGrapeType(), newGrapeContainer.getHarvestYear())) {
            LOGGER.warn("GrapeContainer with grapeType {} and harvestYear {} already exists in repository!", newGrapeContainer.getGrapeType(), newGrapeContainer.getHarvestYear());
            throw new GrapeContainerAlreadyExistsException(newGrapeContainer.getGrapeType(), newGrapeContainer.getHarvestYear());
        }
        isQuantityPositive(newGrapeContainer.getQuantity().getValue());
        return grapeContainerRepository.save(newGrapeContainer);
    }

    public GrapeContainer updateById(Long id, GrapeContainer updatedGrapeContainer) {
        LOGGER.info("Updating GrapeContainer with id {} in repository by new data: {}", id, updatedGrapeContainer);
        GrapeContainer grapeContainer = grapeContainerRepository.findById(id).orElseThrow(() -> new GrapeContainerWithIdNotFoundException(id));
        isGrapeTypeAndHarvestYearAvailable(grapeContainer.getGrapeType(), grapeContainer.getHarvestYear(), updatedGrapeContainer.getGrapeType(), updatedGrapeContainer.getHarvestYear());
        grapeContainer.setGrapeType(updatedGrapeContainer.getGrapeType());
        grapeContainer.setHarvestYear(updatedGrapeContainer.getHarvestYear());
        grapeContainer.setQuantity(updatedGrapeContainer.getQuantity());
        return grapeContainerRepository.save(grapeContainer);
    }

    public GrapeContainer updateByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear, GrapeContainer updatedGrapeContainer) {
        LOGGER.info("Updating GrapeContainer with grapeType {} and harvestYear {} in repository by new data: {}", grapeType, harvestYear, updatedGrapeContainer);
        GrapeContainer grapeContainer = grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType, harvestYear);
        if (grapeContainer == null) {
            LOGGER.warn("GrapeContainer with grapeType {} and harvestYear {} not found in repository!", grapeType, harvestYear);
            throw new GrapeContainerWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        isGrapeTypeAndHarvestYearAvailable(grapeContainer.getGrapeType(), grapeContainer.getHarvestYear(), updatedGrapeContainer.getGrapeType(), updatedGrapeContainer.getHarvestYear());
        grapeContainer.setGrapeType(updatedGrapeContainer.getGrapeType());
        grapeContainer.setHarvestYear(updatedGrapeContainer.getHarvestYear());
        grapeContainer.setQuantity(updatedGrapeContainer.getQuantity());
        return grapeContainerRepository.save(grapeContainer);
    }

    public GrapeContainer addQuantity(Long id, Quantity quantity) {
        LOGGER.info("Adding quantity: {} to GrapeContainer with id {} in repository", quantity, id);
        GrapeContainer grapeContainer = grapeContainerRepository.findById(id).orElseThrow(() -> new GrapeContainerWithIdNotFoundException(id));
        isQuantityPositive(quantity.getValue());
        grapeContainer.setQuantity((WeightQuantity) grapeContainer.getQuantity().add(quantity));
        return grapeContainer;
    }

    public GrapeContainer subtractQuantity(Long id, Quantity quantity) {
        LOGGER.info("Subtracting quantity: {} from GrapeContainer with id {} in repository", quantity, id);
        GrapeContainer grapeContainer = grapeContainerRepository.findById(id).orElseThrow(() -> new GrapeContainerWithIdNotFoundException(id));
        isQuantityPositive(quantity.getValue());
        grapeContainer.setQuantity((WeightQuantity) grapeContainer.getQuantity().subtract(quantity));
        return grapeContainer;
    }

    @Transactional(readOnly = true)
    public double trySubtractQuantity(Long id, Quantity quantity) {
        LOGGER.info("Trying to subtract quantity: {} from GrapeContainer with id {} in repository", quantity, id);
        GrapeContainer grapeContainer = grapeContainerRepository.findById(id).orElseThrow(() -> new GrapeContainerWithIdNotFoundException(id));
        isQuantityPositive(quantity.getValue());
        return grapeContainer.getQuantity().calculateSubtractValue(quantity);
    }

    public GrapeContainer deleteById(Long id) {
        LOGGER.info("Deleting GrapeContainer with id {} from repository", id);
        GrapeContainer grapeContainer = grapeContainerRepository.findById(id).orElseThrow(() -> new GrapeContainerWithIdNotFoundException(id));
        grapeContainerRepository.deleteById(id);
        return grapeContainer;
    }

    public GrapeContainer deleteByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear) {
        LOGGER.info("Deleting GrapeContainer with grapeType {} and harvestYear {} from repository", grapeType, harvestYear);
        GrapeContainer grapeContainer = grapeContainerRepository.findByGrapeTypeAndHarvestYear(grapeType, harvestYear);
        if (grapeContainer == null) {
            throw new GrapeContainerWithGrapeTypeAndYearNotFoundException(grapeType, harvestYear);
        }
        grapeContainerRepository.deleteById(grapeContainer.getId());
        return grapeContainer;
    }

    private boolean isGrapeTypeAndHarvestYearAvailable(GrapeType oldGrapeType, int oldHarvestYear, GrapeType newGrapeType, int newHarvestYear) {
        if (oldGrapeType.equals(newGrapeType) && oldHarvestYear == newHarvestYear) {
            return true;
        }
        return isGrapeTypeAndHarvestYearAvailable(newGrapeType, newHarvestYear);
    }

    private boolean isGrapeTypeAndHarvestYearAvailable(GrapeType newGrapeType, int newHarvestYear) {
        GrapeContainer grapeContainer =  grapeContainerRepository.findByGrapeTypeAndHarvestYear(newGrapeType, newHarvestYear);
        return grapeContainer == null;
    }

    private boolean isQuantityPositive(double value) {
        if (value < 0.0) {
            throw new IngredientNotCreatedException("Ingredient's value cannot be negative.");
        }
        return true;
    }
}
