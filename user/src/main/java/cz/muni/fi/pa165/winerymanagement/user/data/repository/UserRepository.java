package cz.muni.fi.pa165.winerymanagement.user.data.repository;

import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT u FROM User u WHERE u.issuer = ?1 AND u.externalId = ?2")
    Optional<User> findByIssuerAndExternalId(String issuer, String externalId);
}
