package cz.muni.fi.pa165.winerymanagement.core.unit;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.core.exceptions.WrongQuantityTypeException;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = WeightQuantity.class, name = "weight"),
        @JsonSubTypes.Type(value = VolumeQuantity.class, name = "volume")
})
public class Quantity {
    double value;
    String symbol = "";

    public Quantity() {

    }
    protected Quantity(double value, String symbol) {
        this.value = value;
        this.symbol = symbol;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Quantity add(Quantity other) throws WrongQuantityTypeException {
        double newValue = this.value + other.value;
        return new Quantity(newValue, symbol);
    }

    public Quantity subtract(Quantity other) throws NotEnoughQuantityException {
        double newValue = this.value - other.value;
        return new Quantity(newValue, symbol);
    }

    public double calculateSubtractValue(Quantity other) throws NotEnoughQuantityException {
        return this.value - other.value;
    }

    @Override
    public String toString() {
        return String.format("%.2f %s", value, symbol);
    }
}