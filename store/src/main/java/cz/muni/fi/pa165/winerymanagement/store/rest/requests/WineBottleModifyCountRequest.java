package cz.muni.fi.pa165.winerymanagement.store.rest.requests;

import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import io.swagger.v3.oas.annotations.media.Schema;


@Schema(name = "WineBottleModifyCountRequest",
		description = "Data transfer object representing a wineBottleStock with bottleCount used to subtract or add count of wineBottleStock",
		example = """
					{
						"wineBottle": {
							"id": 1,
							"grapeType": {
								"name": "Chardonnay",
								"description": "Chardonnay white sweet wine description",
								"color": "WHITE",
								"sweetness": "SWEET"
							},
							"harvestYear": 2020,
							"quantity": {
								"type": "volume",
								"value": 0.75,
								"volumeUnit": "LITER"
							}
						},
						"bottleCount": 10
					}
				""")
public class WineBottleModifyCountRequest {
	private WineBottle wineBottle;
	private int bottleCount;

	public WineBottle getWineBottle() {
		return wineBottle;
	}

	public void setWineBottle(WineBottle wineBottle) {
		this.wineBottle = wineBottle;
	}

	public int getBottleCount() {
		return bottleCount;
	}

	public void setBottleCount(int bottleCount) {
		this.bottleCount = bottleCount;
	}
}
