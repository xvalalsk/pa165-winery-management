import {Fragment, useEffect, useState} from 'react';
import {NavBar} from "@/pages/index";
import Cookies from "js-cookie";


const Harvests = () => {
    const [token, setToken] = useState(null);
    const [harvests, setHarvests] = useState([]);
    const [error, setError] = useState('');
    const [newYear, setNewYear] = useState('');
    const [form1Error, setForm1Error] = useState('');
    const [form2Error, setForm2Error] = useState('');
    const [form3Error, setForm3Error] = useState('');
    const [grapes, setGrapes] = useState([]);
    const [newItem, setNewItem] = useState({
        grapeId: '',
        quality: '',
        quantity: '',
        harvestId: '',
    });
    const [formData, setFormData] = useState({
        name: '',
        description: '',
        color: 'WHITE',
        sweetness: 'SWEET',
    });

    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        function fetchData() {
            fetch('/api/v1/harvests', {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
            })
                .then((response) => response.json())
                .then((data) => {
                    const sortedData = data.sort((a, b) => a.year - b.year);
                    setHarvests(sortedData);
                })
                .catch((error) => setError(error.message + ". Is the harvest module running?"));

            fetch('/api/v1/harvests/types', {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
            })
                .then((response) => response.json())
                .then((data) => setGrapes(data))
                .catch((error) => setError(error.message + ". Is the harvest module running?"));
        }

        fetchData();
    }, []);

    function ProductionModuleException(message) {
        this.message = message;
        this.name = "ProductionModuleException";
    }

    const handleChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    };
    const deleteHarvestItem = (harvestId, itemId) => {
        fetch(`/api/v1/harvests/${harvestId}/items/${itemId}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Error deleting the harvest item');
                }
                return response.json();
            })
            .then(() => {
                const updatedHarvests = harvests.map((harvest) => {
                    if (harvest.id === harvestId) {
                        return {
                            ...harvest,
                            items: harvest.items.filter((item) => item.id !== itemId),
                        };
                    }
                    return harvest;
                });
                setHarvests(updatedHarvests);
            })
            .catch((error) => {
                console.error('Error deleting harvest item:', error);
            });
    };
    const deleteHarvest = (id) => {
        fetch(`/api/v1/harvests/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    throw new ProductionModuleException('Error deleting the harvest item');
                }
                return response.json();
            })
            .then(() => {
                const updatedHarvests = harvests.filter((harvest) => harvest.id !== id);
                setHarvests(updatedHarvests);
            })
            .catch((error) => {
                console.error('Error deleting harvest item:', error);
            });
    };

    const handleDeleteGrapeType = (id) => {
        fetch(`/api/v1/harvests/types/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => {
                if (response.ok) {
                    setGrapes(grapes.filter((grapeType) => grapeType.id !== id));
                } else {
                    setError('Error deleting grape type.');
                }
            })
            .catch((error) => setError(error.message));
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        setForm1Error('');
        setForm2Error('');
        setForm3Error('');

        if (harvests.some((harvest) => harvest.year === parseInt(newYear))) {
            setForm2Error('Year already exists.');
            return;
        }

        fetch('/api/v1/harvests', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify({year: parseInt(newYear)}),
        })
            .then((response) => response.json())
            .then((data) => {
                const updatedHarvests = [...harvests, data].sort(
                    (a, b) => a.year - b.year
                );
                setHarvests(updatedHarvests);
            })
            .catch((error) => setForm2Error(error.message));

        setNewYear('');
    };

    const handleNewItemSubmit = (e) => {
        setForm1Error('');
        setForm2Error('');
        setForm3Error('');
        newItem.grape = grapes.filter(
            (grape) =>
                parseInt(newItem.grapeId) === parseInt(grape.id)
        )[0];
        newItem.date = new Date();
        console.log("newItem", newItem)
        console.log(JSON.stringify(newItem));
        e.preventDefault();
        fetch('/api/v1/harvests/items', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(newItem),
        })
            .then((response) => {
                console.log(response.status)
                if (response.status !== 201) {
                    throw new Error("Error, check if production module is running")
                }
                return response.json()
            })
            .then((data) => {
                console.log("data", data)
                const updatedHarvests = harvests.map((harvest) => {
                    console.log(harvest.id, newItem.harvestId)
                    if (harvest.id.toString() === newItem.harvestId.toString()) {
                        console.log(harvest.items)
                        if (harvest.items === null) {
                            harvest.items = []
                        }
                        return {
                            ...harvest,
                            items: [...harvest.items, data],
                        };
                    }
                    return harvest;
                });
                console.log("updatedHarvests", updatedHarvests)
                setHarvests(updatedHarvests);
                setNewItem({
                    grapeId: '',
                    quality: '',
                    quantity: '',
                    harvestId: '',
                });
            })
            .catch((error) => {
                console.log(error)
                console.log(error.message)
                setForm3Error(error.message)
            });
    };

    const handleSubmitGrapeType = (e) => {
        e.preventDefault();
        setForm1Error('');
        setForm2Error('');
        setForm3Error('');

        if (grapes.some((grapeType) => grapeType.name === formData.name)) {
            setForm1Error('Grape type name already exists.');
            return;
        }

        fetch('/api/v1/harvests/types', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(formData),
        })
            .then((response) => response.json())
            .then((data) => setGrapes([...grapes, data]))
            .catch((error) => setForm1Error(error.message));

        setFormData({name: '', description: '', color: 'WHITE', sweetness: 'SWEET'});
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar/>
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Harvest module</h1>
                {error ? (
                    <div className="alert alert-danger" role="alert">
                        {error}
                    </div>
                ) : (
                    <div>
                        <div className="mt-4">
                            <h2 className="display-10 mt-5 text-center">Grape Types</h2>
                            <table className="table table-striped table-hover table-bordered">
                                <thead className="bg-secondary bg-gradient">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Color</th>
                                    <th scope="col">Sweetness</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {grapes.map((grapeType) => (
                                    <tr key={grapeType.id}>
                                        <td>{grapeType.name}</td>
                                        <td>{grapeType.description}</td>
                                        <td>{grapeType.color}</td>
                                        <td>{grapeType.sweetness}</td>
                                        <td>
                                            <button
                                                className="btn btn-danger btn-sm"
                                                onClick={() => handleDeleteGrapeType(grapeType.id)}
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>

                        {/* Form for adding a new grape type */}
                        <div className="card mt-4">
                            <div className="card-body">
                                <h5 className="card-title">Add a new grape type</h5>
                                <form onSubmit={handleSubmitGrapeType}>
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">
                                            Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="name"
                                            name="name"
                                            value={formData.name}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="description" className="form-label">
                                            Description
                                        </label>
                                        <textarea
                                            className="form-control"
                                            id="description"
                                            name="description"
                                            rows="3"
                                            value={formData.description}
                                            onChange={handleChange}
                                            required
                                        ></textarea>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="color" className="form-label">
                                            Color
                                        </label>
                                        <select
                                            className="form-select"
                                            id="color"
                                            name="color"
                                            value={formData.color}
                                            onChange={handleChange}
                                            required
                                        >
                                            <option value="WHITE">WHITE</option>
                                            <option value="RED">RED</option>
                                            <option value="ROSE">ROSE</option>
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="sweetness" className="form-label">
                                            Sweetness
                                        </label>
                                        <select
                                            className="form-select"
                                            id="sweetness"
                                            name="sweetness"
                                            value={formData.sweetness}
                                            onChange={handleChange}
                                            required
                                        >
                                            <option value="SWEET">SWEET</option>
                                            <option value="SEMISWEET">SEMISWEET</option>
                                            <option value="DRY">DRY</option>
                                        </select>
                                    </div>
                                    {form1Error && (
                                        <div className="alert alert-danger" role="alert">
                                            {form1Error}
                                        </div>
                                    )}
                                    <button type="submit" className="btn btn-primary">
                                        Add Grape Type
                                    </button>
                                </form>
                            </div>
                        </div>

                        <div className="mt-4">
                            <h2 className="display-10 mt-5 text-center">Harvest Items</h2>
                            <table className="table table-bordered table-striped table-hover">
                                <thead className="bg-secondary bg-gradient">
                                <tr>
                                    <th scope="col">Year</th>
                                    <th scope="col">Grape Name</th>
                                    <th scope="col">Grape Color</th>
                                    <th scope="col">Grape Sweetness</th>
                                    <th scope="col">Quality</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {harvests.map((harvest) => (
                                    <Fragment key={harvest.id}>
                                        <tr className="table-primary">
                                            <th scope="row" colSpan="7">
                                                {harvest.year}
                                                {/* Delete button */}
                                                <button
                                                    type="button"
                                                    className="btn btn-danger btn-sm float-end"
                                                    onClick={() => deleteHarvest(harvest.id)}
                                                >
                                                    Delete
                                                </button>
                                            </th>
                                        </tr>
                                        {harvest.items?.length > 0 ? (
                                            harvest.items.map((item) => (
                                                <tr key={item.id}>
                                                    <td></td>
                                                    <td>{item.grape.name}</td>
                                                    <td>{item.grape.color}</td>
                                                    <td>{item.grape.sweetness}</td>
                                                    <td>{item.quality}</td>
                                                    <td>{`${item.quantity} (Kg)`}</td>
                                                    <td>
                                                        <button
                                                            type="button"
                                                            className="btn btn-danger btn-sm"
                                                            onClick={() => deleteHarvestItem(harvest.id, item.id)}
                                                        >
                                                            Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            ))
                                        ) : (
                                            <tr>
                                                <td colSpan="7">No grapes harvested this year.</td>
                                            </tr>
                                        )}
                                    </Fragment>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                )}
                {/* Form for adding a new year */}
                <div className="card mt-4">
                    <div className="card-body">
                        <h5 className="card-title">Add a new year</h5>
                        <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="year" className="form-label">
                                    New Year
                                </label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="year"
                                    min="1998"
                                    max={new Date().getFullYear()}
                                    value={newYear}
                                    onChange={(e) => setNewYear(e.target.value)}
                                    required
                                />
                            </div>
                            {form2Error && (
                                <div className="alert alert-danger" role="alert">
                                    {form2Error}
                                </div>
                            )}
                            <button type="submit" className="btn btn-primary">
                                Add Year
                            </button>
                        </form>
                    </div>
                </div>
                {/* Form for adding a new year */}
                <div className="card mt-4">
                    <div className="card-body">
                        <h5 className="card-title">Add a new harvest item</h5>
                        <form onSubmit={handleNewItemSubmit}>
                            <div className="mb-3">
                                <label htmlFor="grapeId" className="form-label">
                                    Grape
                                </label>
                                <select
                                    className="form-select"
                                    id="grapeId"
                                    value={newItem.grapeId}
                                    onChange={(e) => {
                                        setNewItem({...newItem, grapeId: e.target.value})
                                    }}
                                    required
                                >
                                    <option value="">Select a grape</option>
                                    {grapes.map((grape) => (
                                        <option key={grape.id} value={grape.id}>
                                            {grape.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="quality" className="form-label">
                                    Quality
                                </label>
                                <select
                                    className="form-control"
                                    id="quality"
                                    name="quality"
                                    value={newItem.quality}
                                    onChange={(e) =>
                                        setNewItem({...newItem, quality: e.target.value})
                                    }
                                    required
                                >
                                    <option value="">Select a quality</option>
                                    <option value="POOR">POOR</option>
                                    <option value="AVERAGE">AVERAGE</option>
                                    <option value="EXCELLENT">EXCELLENT</option>
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="quantity" className="form-label">
                                    Quantity (Kg)
                                </label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="quantity"
                                    value={newItem.quantity}
                                    onChange={(e) =>
                                        setNewItem({...newItem, quantity: e.target.value})
                                    }
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="harvestId" className="form-label">
                                    Harvest
                                </label>
                                <select
                                    className="form-select"
                                    id="harvestId"
                                    value={newItem.harvestId}
                                    onChange={(e) =>
                                        setNewItem({...newItem, harvestId: e.target.value})
                                    }
                                    required
                                >
                                    <option value="">Select a harvest</option>
                                    {harvests.map((harvest) => (
                                        <option key={harvest.id} value={harvest.id}>
                                            {harvest.year}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            {form3Error && (
                                <div className="alert alert-danger" role="alert">
                                    {form3Error}
                                </div>
                            )}
                            <button type="submit" className="btn btn-primary">
                                Add Item
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
};

export default Harvests;
