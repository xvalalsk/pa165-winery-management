import {useEffect, useState} from 'react';
import {NavBar} from "@/pages";
import Cookies from "js-cookie";

const GrapeContainers = () => {
    const [token, setToken] = useState(null);
    const [grapeContainers, setGrapeContainers] = useState([]);
    const [error, setError] = useState('');
    const [grapeTypeName, setGrapeTypeName] = useState('');
    const [grapeTypeDescription, setGrapeTypeDescription] = useState('');
    const [grapeTypeColor, setGrapeTypeColor] = useState('');
    const [grapeTypeSweetness, setGrapeTypeSweetness] = useState('');
    const [harvestYear, setHarvestYear] = useState('');
    const [quantityValue, setQuantityValue] = useState('');
    const [weightUnit, setWeightUnit] = useState('');

    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        fetchGrapeContainers();
    }, []);

    const createGrapeContainer = (e) => {
        e.preventDefault();
        const newGrapeContainer = {
            grapeType: {
                name: grapeTypeName,
                description: grapeTypeDescription,
                color: grapeTypeColor,
                sweetness: grapeTypeSweetness,
            },
            harvestYear: parseInt(harvestYear),
            quantity: {
                type: 'weight',
                value: parseFloat(quantityValue),
                weightUnit: weightUnit,
            },
        };

        fetch('/api/v1/production/grape-containers', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(newGrapeContainer),
        })
            .then((response) => response.json())
            .then((data) => {
                setGrapeContainers([...grapeContainers, data]);
            })
            .catch((error) => setError(error.message));
        setGrapeTypeName('')
        setGrapeTypeDescription('')
        setGrapeTypeColor('')
        setGrapeTypeSweetness('')
        setHarvestYear('')
        setQuantityValue('')
        setWeightUnit('')

        fetchGrapeContainers();
    };


    const fetchGrapeContainers = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/grape-containers', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setGrapeContainers(data))
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const deleteGrapeContainer = (id) => {
        fetch(`/api/v1/production/grape-containers/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then(() => {
                setGrapeContainers(
                    grapeContainers.filter((container) => container.id !== id)
                );
            })
            .catch((error) => setError(error.message));
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar/>
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Grape Containers</h1>
                <div>
                    <div className="mt-4">
                        <table className="table table-striped table-hover table-bordered">
                            <thead className="bg-secondary bg-gradient">
                            <tr>
                                <th scope="col">Grape Type</th>
                                <th scope="col">Description</th>
                                <th scope="col">Color</th>
                                <th scope="col">Sweetness</th>
                                <th scope="col">Harvest Year</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {grapeContainers?.map((container) => (
                                <tr key={container.id}>
                                    <td>{container.grapeType.name}</td>
                                    <td>{container.grapeType.description}</td>
                                    <td>{container.grapeType.color}</td>
                                    <td>{container.grapeType.sweetness}</td>
                                    <td>{container.harvestYear}</td>
                                    <td>{`${container.quantity.value} ${container.quantity.symbol}`}</td>
                                    <td>
                                        <button
                                            className="btn btn-danger"
                                            onClick={() => deleteGrapeContainer(container.id)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                        <div className="card mt-4">
                            <div className="card-body">
                                <h5 className="card-title">Add a new grape container</h5>
                                <form onSubmit={createGrapeContainer} className="mt-4">
                                    <div className="mb-3">
                                        <label htmlFor="grapeTypeName" className="form-label">
                                            Grape Type Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="grapeTypeName"
                                            value={grapeTypeName}
                                            onChange={(e) => setGrapeTypeName(e.target.value)}
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="grapeTypeDescription" className="form-label">
                                            Grape Type Description
                                        </label>
                                        <textarea
                                            className="form-control"
                                            id="grapeTypeDescription"
                                            rows="3"
                                            value={grapeTypeDescription}
                                            onChange={(e) => setGrapeTypeDescription(e.target.value)}
                                            required
                                        ></textarea>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="grapeTypeColor" className="form-label">
                                            Grape Type Color
                                        </label>
                                        <select
                                            className="form-select"
                                            id="grapeTypeColor"
                                            value={grapeTypeColor}
                                            onChange={(e) => setGrapeTypeColor(e.target.value)}
                                            required
                                        >
                                            <option value="">Select Grape Type Color</option>
                                            <option value="WHITE">White</option>
                                            <option value="ROSE">Rose</option>
                                            <option value="RED">Red</option>
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="grapeTypeSweetness" className="form-label">
                                            Grape Type Sweetness
                                        </label>
                                        <select
                                            className="form-select"
                                            id="grapeTypeSweetness"
                                            value={grapeTypeSweetness}
                                            onChange={(e) => setGrapeTypeSweetness(e.target.value)}
                                            required
                                        >
                                            <option value="">Select Grape Type Sweetness</option>
                                            <option value="SWEET">Sweet</option>
                                            <option value="SEMISWEET">Semisweet</option>
                                            <option value="DRY">Dry</option>
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="harvestYear" className="form-label">
                                            Harvest Year
                                        </label>
                                        <input
                                            type="number"
                                            className="form-control"
                                            id="harvestYear"
                                            value={harvestYear}
                                            onChange={(e) => setHarvestYear(e.target.value)}
                                            required
                                        />
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label htmlFor="quantityValue" className="form-label">
                                                Quantity Value
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                id="quantityValue"
                                                value={quantityValue}
                                                onChange={(e) => setQuantityValue(e.target.value)}
                                                required
                                            />
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label htmlFor="weightUnit" className="form-label">
                                                Weight Unit
                                            </label>
                                            <select
                                                className="form-select"
                                                id="weightUnit"
                                                value={weightUnit}
                                                onChange={(e) => setWeightUnit(e.target.value)}
                                                required
                                            >
                                                <option value="">Select Weight Unit</option>
                                                <option value="GRAM">Gram</option>
                                                <option value="KILOGRAM">Kilogram</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-primary">
                                        Create Grape Container
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {error && (
                    <div className="alert alert-danger mt-3" role="alert">
                        {error}
                    </div>
                )}
            </div>
        </>
    )
};

export default GrapeContainers;