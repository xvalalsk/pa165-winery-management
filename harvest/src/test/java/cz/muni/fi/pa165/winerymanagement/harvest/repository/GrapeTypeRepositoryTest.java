package cz.muni.fi.pa165.winerymanagement.harvest.repository;

import cz.muni.fi.pa165.winerymanagement.harvest.HarvestApplication;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Color;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Sweetness;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.GrapeTypeRepository;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = HarvestApplication.class)
@Transactional
public class GrapeTypeRepositoryTest {
    @Autowired
    private GrapeTypeRepository grapeTypeRepository;

    @Autowired
    private EntityManager em;

    private GrapeType grapeType;

    @BeforeEach
    void setUp() {
        grapeType = new GrapeType();
        grapeType.setName("Test");
        grapeType.setDescription("Some testing grape type");
        grapeType.setColor(Color.ROSE);
        grapeType.setSweetness(Sweetness.SWEET);

        em.persist(grapeType);
    }

    @Test
    void findAllGrapeTypes() {
        assertThat(this.grapeTypeRepository.findAll())
                .hasSize(3).contains(grapeType);
    }

    @Test
    void findExistingGrapeTypeById() {
        var grapeType = this.grapeTypeRepository.findById(this.grapeType.getId());
        assertThat(grapeType).isPresent();
        assertThat(grapeType.get()).isEqualTo(this.grapeType);
    }

    @Test
    void findNonExistingGrapeType() {
        assertThat(this.grapeTypeRepository.findById(-1L)).isEmpty();
    }

    @Test
    void addHarvestItem() {
        var newType = new GrapeType();
        newType.setName("New grape");
        newType.setDescription("New grape type");
        newType.setSweetness(Sweetness.DRY);
        newType.setColor(Color.WHITE);

        this.grapeTypeRepository.save(newType);
        assertThat(this.grapeTypeRepository.findGrapeTypeByColorAndSweetnessAndDescriptionAndName(newType.getColor(), newType.getSweetness(), newType.getDescription(), newType.getName())).isNotNull();
    }

    @Test
    void updateHarvestItem() {
        var newType = new GrapeType();
        newType.setName("Updated grape");
        newType.setDescription("Updated grape type");
        newType.setSweetness(Sweetness.DRY);
        newType.setColor(Color.WHITE);

        this.grapeTypeRepository.save(newType);
        assertThat(this.grapeTypeRepository.findGrapeTypeByColorAndSweetnessAndDescriptionAndName(newType.getColor(), newType.getSweetness(), newType.getDescription(), newType.getName())).isNotNull();
    }

    @Test
    void deleteHarvestItemById() {
        this.grapeTypeRepository.deleteById(this.grapeType.getId());
        assertThat(this.grapeTypeRepository.findById(this.grapeType.getId())).isEmpty();
    }
}
