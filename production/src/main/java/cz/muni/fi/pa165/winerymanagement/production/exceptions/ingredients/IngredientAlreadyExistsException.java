package cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients;

import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceAlreadyExistsException;

import java.util.ResourceBundle;

public class IngredientAlreadyExistsException extends ResourceAlreadyExistsException {

    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
    private final String name;

    public IngredientAlreadyExistsException(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        String message = messages.getString(key);
        return String.format(message, name);
    }
}
