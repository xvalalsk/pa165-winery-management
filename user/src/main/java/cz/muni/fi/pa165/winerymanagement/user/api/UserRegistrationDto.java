package cz.muni.fi.pa165.winerymanagement.user.api;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

@Schema(
        description = "User registration DTO",
        example = """
        {
          "issuer": "https://oidc.muni.cz/oidc/",
          "externalId": "123456@muni.cz"
        }
       """
)
public class UserRegistrationDto implements Serializable {

        @ApiModelProperty(
                value = "Issuer of external login",
                example = "John",
                required = true
        )
        private String issuer;

        @ApiModelProperty(
                value = "Specific identifier of external login",
                example = "Doe",
                required = true
        )
        private String externalId;

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRegistrationDto that = (UserRegistrationDto) o;
        return issuer.equals(that.issuer) && externalId.equals(that.externalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(issuer, externalId);
    }
}
