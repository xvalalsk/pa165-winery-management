package cz.muni.fi.pa165.winerymanagement.store.data.model;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * WineBottleStock entity represents a stock of wine bottles with information about
 * its wine bottle, count and price.
 */

@Entity
@Table(name = "wine_bottle_stock")
public class WineBottleStock implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_wine_bottle_stock")
	private Long id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_wine_bottle")
	private WineBottle wineBottle;
	@Column(name = "count")
	private int count;
	@Column(name = "price")
	private double price;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WineBottle getWineBottle() {
		return wineBottle;
	}

	public void setWineBottle(WineBottle wineBottle) {
		this.wineBottle = wineBottle;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WineBottleStock that)) return false;
		return getCount() == that.getCount() && Double.compare(that.getPrice(), getPrice()) == 0 && Objects.equals(getWineBottle(), that.getWineBottle());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getWineBottle(), getCount(), getPrice());
	}

	@Override
	public String toString() {
		return "WineBottleStock{" +
				"id=" + id +
				", wineBottle=" + wineBottle +
				", count=" + count +
				", price=" + price +
				'}';
	}
}
