import {useEffect, useState} from 'react';
import {NavBar} from "@/pages/index";
import Cookies from "js-cookie";

const Store = () => {
    const [token, setToken] = useState(null);
    const [wineBottles, setWineBottles] = useState([]);
    const [wineBottlesInStock, setWineBottlesInStock] = useState([]);
    const [error, setError] = useState('');
    const [isWineBottleInStock, setIsWineBottleInStock] = useState(false);
    const [barrels, setBarrels] = useState([]);
    const [formData, setFormData] = useState({
        grapeType: "",
        bottleVolume: {
            type: 'volume',
            value: 0.75,
            volumeUnit: 'LITER',
        },
        harvestYear: 0,
        bottleCount: 0,
        price: 0
    });


    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        fetchWineBottles();
        fetchWineBottlesInStock();
        fetchBarrels();
    }, []);

    const fetchBarrels = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/barrels',{
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setBarrels(data))
            .catch((error) => setError(error.message + ". Is the Production module is running?"));
    };

    const handleChange = (e) => {
        const {name, value} = e.target;
        if (name === "bottleVolume.value") {
            const bottleVolume = {
                type: 'volume',
                value: value,
                volumeUnit: 'LITER',
            }
            setFormData({...formData, "bottleVolume": bottleVolume});
        } else {
            setFormData({...formData, [name]: value});
        }
    };

    const handleSubmit = async (e) => {
        console.log("formData", formData);
        e.preventDefault();
        try {
            const {price, ...formDataWithoutPrice} = formData;
            const parsedFormData = {
                ...formDataWithoutPrice,
                grapeType: JSON.parse(formData.grapeType),
            };

            await fetch("/api/v1/store/wine-bottles-in-stock", {
                method: "POST",
                headers: {

                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`,
                },
                body: JSON.stringify({
                    wineBottle: {
                        grapeType: JSON.parse(formData.grapeType),
                        harvestYear: formData.harvestYear,
                        volume: formData.bottleVolume,
                    },
                    count: 0,
                    price: formData.price,
                }),
            });


            await fetch(
                '/api/v1/store/wine-bottles-in-stock/fill-wine-bottles',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": `Bearer ${token}`,
                    },
                    body: JSON.stringify(parsedFormData),
                }
            );

            fetchWineBottles();
            fetchWineBottlesInStock();
            fetchBarrels();
            setIsWineBottleInStock(false);
            setFormData({
                grapeType: "",
                bottleVolume: {
                    type: 'volume',
                    value: 0.75,
                    volumeUnit: 'LITER',
                },
                harvestYear: 0,
                bottleCount: 0,
                price: 0
            })
        } catch (error) {
            setError(error.message);
        }
    };


    const deleteWineBottleInStock = async (id) => {
        try {
            await fetch(`/api/v1/store/wine-bottles-in-stock/${id}`, {
                method: 'DELETE',
                headers: {
                    "Authorization": `Bearer ${token}`,
                },
            });
            fetchWineBottles();
            fetchWineBottlesInStock();
            fetchBarrels();
        } catch (error) {
            setError(error.message);
        }
    };

    const fetchWineBottles = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/store/wine-bottles', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log("token", token)
                console.log("data", data)
                setWineBottles(data)
            })
            .catch((error) => setError(error.message +  ". Is the Store module is running?"));
    };

    const fetchWineBottlesInStock = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/store/wine-bottles-in-stock', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setWineBottlesInStock(data))
            .catch((error) => setError(error.message +  ". Is the Store module is running?"));
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar/>
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Store</h1>
                {error && (
                    <div className="alert alert-danger mt-3" role="alert">
                        {error}
                    </div>
                )}
                <div>
                    <div className="mt-4">
                        <h2 className="display-10 mt-5 text-center">Wine Bottles</h2>
                        <table className="table table-striped table-hover table-bordered">
                            <thead className="bg-secondary bg-gradient">
                            <tr>
                                <th scope="col">Grape Name</th>
                                <th scope="col">Color</th>
                                <th scope="col">Sweetness</th>
                                <th scope="col">Harvest Year</th>
                                <th scope="col">Volume</th>
                            </tr>
                            </thead>
                            <tbody>
                            {wineBottles.map((wineBottle) => (
                                <tr key={wineBottle.id}>
                                    <td>{wineBottle.grapeType.name}</td>
                                    <td>{wineBottle.grapeType.color}</td>
                                    <td>{wineBottle.grapeType.sweetness}</td>
                                    <td>{wineBottle.harvestYear}</td>
                                    <td>{`${wineBottle?.volume?.value} ${wineBottle?.volume?.symbol}`}</td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>

                    <div className="mt-4">
                        <h2 className="display-10 mt-5 text-center">Wine Bottles in Stock</h2>
                        <table className="table table-bordered table-striped table-hover">
                            <thead className="bg-secondary bg-gradient">
                            <tr>
                                <th scope="col">Grape Name</th>
                                <th scope="col">Color</th>
                                <th scope="col">Sweetness</th>
                                <th scope="col">Harvest Year</th>
                                <th scope="col">Volume</th>
                                <th scope="col">Count</th>
                                <th scope="col">Price</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {wineBottlesInStock.map((wineBottleInStock) => (
                                <tr key={wineBottleInStock.id}>
                                    <td>{wineBottleInStock.wineBottle.grapeType.name}</td>
                                    <td>{wineBottleInStock.wineBottle.grapeType.color}</td>
                                    <td>{wineBottleInStock.wineBottle.grapeType.sweetness}</td>
                                    <td>{wineBottleInStock.wineBottle.harvestYear}</td>
                                    <td>{`${wineBottleInStock.wineBottle.volume.value} ${wineBottleInStock.wineBottle.volume.symbol}`}</td>
                                    <td>{wineBottleInStock.count}</td>
                                    <td>{`${wineBottleInStock.price} $`}</td>
                                    <td>
                                        <button
                                            className="btn btn-danger"
                                            onClick={() => deleteWineBottleInStock(wineBottleInStock.id)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="card mt-4">
                        <div className="card-body">
                            <h5 className="card-title">Fill Wine Bottles</h5>
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="grapeType">Grape Type (Harvest Year)</label>
                                    <select
                                        className="form-control"
                                        id="grapeType"
                                        name="grapeType"
                                        value={formData.grapeType}
                                        onChange={(e) => {
                                            const grapeTypeValue = e.target.value;
                                            const harvestYearValue = e.target.options[e.target.selectedIndex].getAttribute('data-harvest-year');

                                            const foundWineBottleInStock = wineBottlesInStock.find(
                                                (wineBottleInStock) =>
                                                    JSON.stringify(wineBottleInStock.wineBottle.grapeType) === grapeTypeValue &&
                                                    wineBottleInStock.wineBottle.harvestYear === parseInt(harvestYearValue)
                                            );

                                            if (foundWineBottleInStock) {
                                                setIsWineBottleInStock(true);
                                                setFormData({
                                                    ...formData,
                                                    grapeType: grapeTypeValue,
                                                    harvestYear: parseInt(harvestYearValue),
                                                    price: foundWineBottleInStock.price,
                                                });
                                            } else {
                                                setIsWineBottleInStock(false);
                                                setFormData({
                                                    ...formData,
                                                    grapeType: grapeTypeValue,
                                                    harvestYear: parseInt(harvestYearValue),
                                                });
                                            }
                                        }}

                                        required
                                    >
                                        <option value="">Select Grape Type</option>
                                        {barrels.map((barrel) => (
                                            <option
                                                key={barrel.id}
                                                value={JSON.stringify(barrel.grapeType)}
                                                data-harvest-year={barrel.harvestYear}
                                            >
                                                {barrel.grapeType.name} ({barrel.harvestYear})
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="bottleCount">Bottle Count</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        id="bottleCount"
                                        name="bottleCount"
                                        value={formData.bottleCount}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                {!isWineBottleInStock && (
                                    <>
                                        <div className="form-group">
                                            <label htmlFor="bottleCount">Bottle Volume (l)</label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                id="bottleVolume"
                                                name="bottleVolume.value"
                                                value={formData.bottleVolume.value}
                                                onChange={handleChange}
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="bottleCount">Price ($)</label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                id="price"
                                                name="price"
                                                value={formData.price}
                                                onChange={handleChange}
                                                required
                                            />
                                        </div>
                                    </>
                                )}
                                <div className="mt-2">
                                    <button type="submit" className="btn btn-primary">
                                        Fill Wine Bottles
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Store;