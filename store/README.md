# PA165 - Winery Management

## Store module

### WineBottle

#### Supported operations:

| Method     | Endpoint                                                      | Description                    |
|------------|---------------------------------------------------------------|--------------------------------|
| **GET**    | `/api/v1/store/wine-bottles`                                  | Get a list of all wine bottles |
| **GET**    | `/api/v1/store/wine-bottles/{id}`                             | Get an wine bottle by ID       |
| **PUT**    | `/api/v1/store/wine-bottles/{id}`                             | Update a wine bottle by ID     |
| **POST**   | `/api/v1/store/wine-bottles`                                  | Add a wine bottle              |
| **DELETE** | `/api/v1/store/wine-bottles/{id}`                             | Delete a wine bottle by ID     |

#### Sample data:

For better testing purposes there sample data are currently initialized (this may be removed in following milestones).

```json
[
  {
    "id": 1,
    "grapeType": {
      "name": "Chardonnay",
      "description": "Chardonnay white sweet wine description",
      "color": "WHITE",
      "sweetness": "SWEET"
    },
    "harvestYear": 2011,
    "volume": {
      "type": "volume",
      "value": 0.75,
      "symbol": "l",
      "volumeUnit": "LITER"
    }
  },
  {
    "id": 2,
    "grapeType": {
      "name": "Zweigeltrebe",
      "description": "Zweigeltrebe rose semi-sweet wine description",
      "color": "ROSE",
      "sweetness": "SEMISWEET"
    },
    "harvestYear": 2022,
    "volume": {
      "type": "volume",
      "value": 0.75,
      "symbol": "l",
      "volumeUnit": "LITER"
    }
  }
]
```

#### Example scenarios:

Each following scenarios expect to have tha sample data in place to work.

> ##### List all wine bottles
>> To get list of all available wine bottle send a GET request to /api/v1/store/wine-bottles endpoint.
>
>> Run:
>> ```shell
>> curl http://localhost:8083/api/v1/store/wine-bottles
>> ```
>
>> Result:
>> ```json
>> [
>>   {
>>     "id": 1,
>>     "grapeType": {
>>       "name": "Chardonnay",
>>       "description": "Chardonnay white sweet wine description",
>>       "color": "WHITE",
>>       "sweetness": "SWEET"
>>     },
>>     "harvestYear": 2011,
>>     "volume": {
>>       "type": "volume",
>>       "value": 0.75,
>>       "symbol": "l",
>>       "volumeUnit": "LITER"
>>     }
>>   },
>>   {
>>     "id": 2,
>>     "grapeType": {
>>       "name": "Zweigeltrebe",
>>       "description": "Zweigeltrebe rose semi-sweet wine description",
>>       "color": "ROSE",
>>       "sweetness": "SEMISWEET"
>>     },
>>     "harvestYear": 2022,
>>     "volume": {
>>       "type": "volume",
>>       "value": 0.75,
>>       "symbol": "l",
>>       "volumeUnit": "LITER"
>>     }
>>   }
>> ]
>> ```

> ##### Get wine bottle by ID
>> To get information only about one wine bottle with specific ID, send a GET request to /api/v1/store/wine-bottles/{id}
> > endpoint.
>>
>> In following example we want to get a wine bottle with id: 1
>
>> Run:
>> ```shell
>> curl http://localhost:8083/api/v1/store/wine-bottles/1
>> ```
>
>> Result:
>> ```json
>> {
>>   "id": 1,
>>   "grapeType": {
>>     "name": "Chardonnay",
>>     "description": "Chardonnay white sweet wine description",
>>     "color": "WHITE",
>>     "sweetness": "SWEET"
>>   },
>>   "harvestYear": 2011,
>>   "volume": {
>>     "type": "volume",
>>     "value": 0.75,
>>     "symbol": "l",
>>     "volumeUnit": "LITER"
>>   }
>> }
>> ```

> ##### Update wine bottles by ID
>> To update wine bottles with specific ID, send a PUT request to /api/v1/store/wine-bottles/{id} endpoint.
>>
>> In following example we want to update a wine bottle with id: 1
>
>> Run:
>> ```shell
>> curl --header "Content-Type: application/json" \
>>      --request PUT \
>>      --data '{"id": 1,"grapeType": {"name": "Chardonnay","description": "Chardonnay white sweet wine description","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020,"volume": {"type": "volume","value": 0.75,"volumeUnit": "LITER"} }' \
>>      http://localhost:8083/api/v1/store/wine-bottles/1
>> ```
>
>> Result:
>> ```json
>> {
>>   "id": 1,
>>   "grapeType": {
>>     "name": "Chardonnay",
>>     "description": "Chardonnay white sweet wine description",
>>     "color": "WHITE",
>>     "sweetness": "SWEET"
>>   },
>>   "harvestYear": 2020,
>>   "volume": {
>>     "type": "volume",
>>     "value": 0.75,
>>     "symbol": "l",
>>     "volumeUnit": "LITER"
>>   }
>> }
>> ```
>> We can list all wine bottles again to see that the wine bottle with id: 1 was updated.
>
>> Run:
>> ```shell
>> curl http://localhost:8083/api/v1/store/wine-bottles
>> ```
>
>> Result:
>> ```json
>> [
>>   {
>>     "id": 1,
>>     "grapeType": {
>>       "name": "Chardonnay",
>>       "description": "Chardonnay white sweet wine description",
>>       "color": "WHITE",
>>       "sweetness": "SWEET"
>>     },
>>     "harvestYear": 2020,
>>     "volume": {
>>       "type": "volume",
>>       "value": 0.75,
>>       "symbol": "l",
>>       "volumeUnit": "LITER"
>>     }
>>   },
>>   {
>>     "id": 2,
>>     "grapeType": {
>>       "name": "Zweigeltrebe",
>>       "description": "Zweigeltrebe rose semi-sweet wine description",
>>       "color": "ROSE",
>>       "sweetness": "SEMISWEET"
>>     },
>>     "harvestYear": 2022,
>>     "volume": {
>>       "type": "volume",
>>       "value": 0.75,
>>       "symbol": "l",
>>       "volumeUnit": "LITER"
>>     }
>>   }
>> ]
>> ```
>>
>>> Note in current version the update data aren't validated and may cause some inconsistency in the repository
>>


> ##### Add new wine bottle
>> To add new wine bottle, send a POST request to /api/v1/store/wine-bottles endpoint.
>>
>>In following example want to create new Ingredient with following data:
>>```json
>>{
>>  "id": 1,
>>  "grapeType": {
>>    "name": "Chardonnay2",
>>    "description": "Chardonnay white sweet wine description2",
>>    "color": "WHITE",
>>    "sweetness": "SWEET"
>>  },
>>  "harvestYear": 2020,
>>  "quantity": {
>>    "type": "volume",
>>    "value": 0.75,
>>    "volumeUnit": "LITER"
>>  }
>>}
>>```
>
>> Run:
>>```shell
>>curl --header "Content-Type: application/json" \
>>     --request POST \
>>     --data '{"id": 1,"grapeType": {"name": "Chardonnay2","description": "Chardonnay white sweet wine description2","color": "WHITE","sweetness": "SWEET"},"harvestYear": 2020,"volume": {"type": "volume","value": 0.75,"volumeUnit": "LITER"} }' \
>>     http://localhost:8083/api/v1/store/wine-bottles
>> ```
>
>> Result:
>>```json
>>{
>>    "id": 1,
>>    "grapeType": {
>>        "name": "Chardonnay2",
>>        "description": "Chardonnay white sweet wine description2",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>    },
>>    "harvestYear": 2020,
>>    "volume": {
>>        "type": "volume",
>>        "value": 0.75,
>>        "symbol": "l",
>>        "volumeUnit": "LITER"
>>    }
>>}
>>```


> ##### Delete wine bottle by ID
>> To delete a wine bottle with specific ID, send a DELETE request to /api/v1/store/wine-bottles/{id} endpoint
>>
>> Note: You need to delete wineBottleStock containing this wine bottle at first, if you want to delete a wine bottle.
>>
>>In following example we want to delete an wine bottle with id: 1.
>
>> Run:
>>```shell
>>curl --request DELETE http://localhost:8083/api/v1/store/wine-bottles/1
>>```
>>
>>List all wine bottles to see that wine bottle with id 1 was deleted.
>
>> Run:
>>```shell
>>curl http://localhost:8083/api/v1/store/wine-bottles
>> ```
>
>> Result:
>>```json
>> [
>>   {
>>     "id": 2,
>>     "grapeType": {
>>       "name": "Zweigeltrebe",
>>       "description": "Zweigeltrebe rose semi-sweet wine description",
>>       "color": "ROSE",
>>       "sweetness": "SEMISWEET"
>>     },
>>     "harvestYear": 2022,
>>     "volume": {
>>       "type": "volume",
>>       "value": 0.75,
>>       "symbol": "l",
>>       "volumeUnit": "LITER"
>>     }
>>   }
>> ]
>>```
>>



### WineBottleStock

#### Supported operations:

| Method     | Endpoint                                                        | Description                                                 |
|------------|-----------------------------------------------------------------|-------------------------------------------------------------|
| **GET**    | `/api/v1/store/wine-bottles-in-stock`                           | Get a list of all wine bottles stock                        |
| **GET**    | `/api/v1/store/wine-bottles-in-stock/{id}`                      | Get a wine bottles stock by ID                              |
| **GET**    | `/api/v1/store/wine-bottles-in-stock/winebottle`                | Get a wine bottles stock by wine bottle                     |
| **PUT**    | `/api/v1/store/wine-bottles-in-stock/{id}`                      | Update a wine bottle stock by ID                            |
| **PUT**    | `/api/v1/store/wine-bottles-in-stock/winebottle`                | Update a wine bottle stock by wine bottle                   |
| **POST**   | `/api/v1/store/wine-bottles-in-stock`                           | Add a wine bottle stock                                     |
| **POST**   | `/api/v1/store/wine-bottles-in-stock/{id}/add-bottles`          | Add a wine bottle count to wine bottle stock by ID          |
| **POST**   | `/api/v1/store/wine-bottles-in-stock/{id}/remove-bottles`       | Add a wine bottle count to wine bottle stock by ID          |
| **POST**   | `/api/v1/store/wine-bottles-in-stock/wineBottle/add-bottles`    | Add a wine bottle count to wine bottle stock by wine bottle |
| **POST**   | `/api/v1/store/wine-bottles-in-stock/wineBottle/remove-bottles` | Add a wine bottle count to wine bottle stock by wine bottle |
| **DELETE** | `/api/v1/store/wine-bottles-in-stock/{id}`                      | Delete a wine bottle stock by ID                            |
| **DELETE** | `/api/v1/store/wine-bottles-in-stock/winebottle`                | Delete a wine bottle stock by wine bottle                   |

#### Sample data:

For better testing purposes there sample data are currently initialized (this may be removed in following milestones).

```json
[
  {
    "id": 1,
    "wineBottle": {
      "id": 1,
      "grapeType": {
        "name": "Chardonnay2",
        "description": "Chardonnay white sweet wine description2",
        "color": "WHITE",
        "sweetness": "SWEET"
      },
      "harvestYear": 2020,
      "volume": {
        "type": "volume",
        "value": 0.75,
        "symbol": "l",
        "volumeUnit": "LITER"
      }
    },
    "count": 100,
    "price": 9.99
  },
  {
    "id": 2,
    "wineBottle": {
      "id": 2,
      "grapeType": {
        "name": "Zweigeltrebe",
        "description": "Zweigeltrebe rose semi-sweet wine description",
        "color": "ROSE",
        "sweetness": "SEMISWEET"
      },
      "harvestYear": 2022,
      "volume": {
        "type": "volume",
        "value": 0.75,
        "symbol": "l",
        "volumeUnit": "LITER"
      }
    },
    "count": 80,
    "price": 7.99
  }
]
```

#### Example scenarios:

> ##### List all wine bottles stocks
>> To get list of all available wine bottle stock send a GET request to /api/v1/store/wine-bottles-in-stock endpoint.
>
>> Run:
>> ```shell
>> curl http://localhost:8083/api/v1/store/wine-bottles-in-stock
>> ```
>
>> Result:
>> ```json
>>[
>>  {
>>    "id": 1,
>>    "wineBottle": {
>>      "id": 1,
>>      "grapeType": {
>>        "name": "Chardonnay2",
>>        "description": "Chardonnay white sweet wine description2",
>>        "color": "WHITE",
>>        "sweetness": "SWEET"
>>      },
>>      "harvestYear": 2020,
>>      "volume": {
>>        "type": "volume",
>>        "value": 0.75,
>>        "symbol": "l",
>>        "volumeUnit": "LITER"
>>      }
>>    },
>>    "count": 100,
>>    "price": 9.99
>>  },
>>  {
>>    "id": 2,
>>    "wineBottle": {
>>      "id": 2,
>>      "grapeType": {
>>        "name": "Zweigeltrebe",
>>        "description": "Zweigeltrebe rose semi-sweet wine description",
>>        "color": "ROSE",
>>        "sweetness": "SEMISWEET"
>>      },
>>      "harvestYear": 2022,
>>      "volume": {
>>        "type": "volume",
>>        "value": 0.75,
>>        "symbol": "l",
>>        "volumeUnit": "LITER"
>>      }
>>    },
>>    "count": 80,
>>    "price": 7.99
>>  }
>>]
>> ```
>> 

> ##### Get wine bottle stock by ID
>> To get list of all available wine bottle stock send a GET request to /api/v1/store/wine-bottles-in-stock/{id} endpoint.
>
>> Run:
>> ```shell
>> curl http://localhost:8083/api/v1/store/wine-bottles-in-stock/1
>> ```
>
>> Result:
>> ```json
>> {
>>     "id": 1,
>>     "wineBottle": {
>>         "id": 1,
>>         "grapeType": {
>>             "name": "Chardonnay2",
>>             "description": "Chardonnay white sweet wine description2",
>>             "color": "WHITE",
>>             "sweetness": "SWEET"
>>         },
>>         "harvestYear": 2020,
>>         "volume": {
>>             "type": "volume",
>>             "value": 0.75,
>>             "symbol": "l",
>>             "volumeUnit": "LITER"
>>         }
>>     },
>>     "count": 100,
>>     "price": 9.99
>> }
>> ```

> ##### Delete wine bottle stock by ID
>> To get list of all available wine bottle stock send a DELETE request to /api/v1/store/wine-bottles-in-stock/{id} endpoint.
>
>> Run:
>> ```shell
>> curl --request DELETE http://localhost:8083/api/v1/store/wine-bottles-in-stock/1
>>


> ##### Create wine bottle stock from barrel
>> To fill wine bottle stock from barrel send a POST request to /api/v1/store/wine-bottles-in-stock/fill-wine-bottles endpoint.
>>
>> Note: You need to have the production module running and know what barrels you have in the module
>>
>> In this ecample we are passing following data to the POST request:
>> ```json
>> {
>>   "grapeType": {
>>     "name": "Chardonnay",
>>     "description": "Chardonnay white sweet wine description",
>>     "color": "WHITE",
>>     "sweetness": "SWEET"
>>   },
>>   "bottleVolume": {
>>     "type": "volume",
>>     "value": 0.75,
>>     "volumeUnit": "LITER"
>> },
>>   "harvestYear": 2011,
>>   "bottleCount": 10
>> }
>>```
>>
>
>> Run:
>> ```shell
>> curl -X 'POST' \
>>   'http://localhost:8083/api/v1/store/wine-bottles-in-stock/fill-wine-bottles' \
>>   -H 'accept: */*' \
>>   -H 'Content-Type: application/json' \
>>   -d '{
>>   "grapeType": {
>>     "name": "Chardonnay",
>>     "description": "Chardonnay white sweet wine description",
>>     "color": "WHITE",
>>     "sweetness": "SWEET"
>>   },
>>   "bottleVolume": {
>>     "type": "volume",
>>     "value": 0.75,
>>     "volumeUnit": "LITER"
>>   },
>>   "harvestYear": 2011,
>>   "bottleCount": 10
>> }'
>>```
>>
>
>> Result:
>> ```json
>> {
>>   "id": 1,
>>   "wineBottle": {
>>     "id": 1,
>>     "grapeType": {
>>       "name": "Chardonnay",
>>       "description": "Chardonnay white sweet wine description",
>>       "color": "WHITE",
>>       "sweetness": "SWEET"
>>     },
>>     "harvestYear": 2011,
>>     "volume": {
>>       "type": "volume",
>>       "value": 0.75,
>>       "symbol": "l",
>>       "volumeUnit": "LITER"
>>     }
>>   },
>>   "count": 110,
>>   "price": 9.99
>> }
>> ```
>> 
>> 
>
>> Note: The bottle count was updated. You can also check the production module and observe that the barrel containing requested grape type and harvest year was modified. 

