package cz.muni.fi.pa165.winerymanagement.harvest.service;

import cz.muni.fi.pa165.winerymanagement.core.model.ModuleEnum;
import cz.muni.fi.pa165.winerymanagement.core.service.ModuleUrlService;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.harvest.api.GrapeContainerDto;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.HarvestItem;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.GrapeTypeRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestItemRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.*;
import cz.muni.fi.pa165.winerymanagement.harvest.rest.requests.GrapeTypeAndYearGetRequest;
import cz.muni.fi.pa165.winerymanagement.harvest.rest.requests.GrapeTypeAndYearModifyQuantityRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Transactional
public class HarvestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(HarvestService.class);
    private final HarvestRepository harvestRepository;
    private final HarvestItemRepository harvestItemRepository;
    private final GrapeTypeRepository grapeTypeRepository;

    @Autowired
    public HarvestService(HarvestRepository harvestRepository, HarvestItemRepository harvestItemRepository, GrapeTypeRepository grapeTypeRepository) {
        this.harvestRepository = harvestRepository;
        this.harvestItemRepository = harvestItemRepository;
        this.grapeTypeRepository = grapeTypeRepository;
    }

    // GET
    @Transactional(readOnly = true)
    public List<Harvest> findAllHarvests() {
        LOGGER.info("Retrieving all harvests from repository");
        return harvestRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<HarvestItem> findAllHarvestItems() {
        LOGGER.info("Retrieving all harvest items from repository");
        return harvestItemRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<HarvestItem> findAllItemsForHarvest(Long harvestId) {
        LOGGER.info("Retrieving all items for harvest with ID {} from repository", harvestId);
        harvestRepository.findById(harvestId).orElseThrow(() -> new HarvestNotExistsException(harvestId));
        return harvestItemRepository.findHarvestItemsByHarvestId(harvestId);
    }

    @Transactional(readOnly = true)
    public Harvest findHarvestById(Long id) {
        LOGGER.info("Retrieving harvest with ID {} from repository", id);
        return harvestRepository.findById(id).orElseThrow(() -> new HarvestNotExistsException(id));
    }

    @Transactional(readOnly = true)
    public HarvestItem findHarvestItemById(Long id) {
        LOGGER.info("Retrieving harvest item with ID {} from repository", id);
        return harvestItemRepository.findById(id).orElseThrow(() -> new HarvestItemNotExistsException(id));
    }

    @Transactional(readOnly = true)
    public List<GrapeType> findAllGrapeTypes() {
        LOGGER.info("Retrieving all grape types from repository");
        return grapeTypeRepository.findAll();
    }

    @Transactional(readOnly = true)
    public GrapeType findGrapeTypeById(Long id) {
        LOGGER.info("Retrieving grape type with ID {} from repository", id);
        return grapeTypeRepository.findById(id).orElseThrow(() -> new GrapeTypeNotExistsException(id));
    }


    // POST
    public Harvest addHarvest(Harvest newHarvest) {
        LOGGER.info("Adding new harvest: {} to repository", newHarvest);
        Harvest harvest = harvestRepository.findByHarvestYear(newHarvest.getYear());
        if (harvest != null) {
            throw new HarvestAlreadyExistsException(harvest);
        }
        return harvestRepository.save(newHarvest);
    }

    public HarvestItem addHarvestItem(HarvestItem newItem) {
        LOGGER.info("Adding new harvest item: {} to repository", newItem);
        Harvest harvest = harvestRepository.findById(newItem.getHarvest().getId()).orElseThrow(() -> new HarvestNotExistsException(newItem.getHarvest().getId()));
        HarvestItem item = harvestItemRepository.findSpecificHarvestItem(harvest, newItem.getGrape(), newItem.getDate(), newItem.getQuantity(), newItem.getQuality());
        if (item != null) {
            newItem.setHarvest(item.getHarvest());
            newItem.setQuantity(newItem.getQuantity() + item.getQuantity());
            newItem.setId(item.getId());
        }
        // Post harvestItem to production
        LOGGER.info("Notifying production module that a harvest item will be provided...");
        String getGrapeContainerUrl = ModuleUrlService.buildModuleUrl(ModuleEnum.PRODUCTION, "api/v1/production/grape-containers");

        GrapeContainerDto postedGrapeContainerDto = new GrapeContainerDto();
        postedGrapeContainerDto.setGrapeType(newItem.getGrape());
        postedGrapeContainerDto.setHarvestYear(harvest.getYear());
        postedGrapeContainerDto.setQuantity(new WeightQuantity(0, WeightQuantity.WeightUnit.KILOGRAM));
        RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.postForObject(getGrapeContainerUrl, postedGrapeContainerDto, GrapeContainerDto.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.CONFLICT) {
                LOGGER.info("Received HTTP 406 response from production module, grape container exists, continuing with execution...");
            } else {
                LOGGER.warn("Harvest item post to production item was unsuccessful with exception: {}!", ex.getMessage());
                throw new HarvestItemNotPassedToProductionException(newItem);
            }
        }
        LOGGER.info("Notifying production module that a harvest item will be provided was successful");

        LOGGER.info("Preparing to post harvest item to production module ...");
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setGrapeType(newItem.getGrape());
        grapeTypeAndYearGetRequest.setHarvestYear(harvest.getYear());

        GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest = new GrapeTypeAndYearModifyQuantityRequest();
        grapeTypeAndYearModifyQuantityRequest.setQuantity(new WeightQuantity(newItem.getQuantity(), WeightQuantity.WeightUnit.KILOGRAM));
        grapeTypeAndYearModifyQuantityRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);

        String getGrapeContainerAddUrl = ModuleUrlService.buildModuleUrl(ModuleEnum.PRODUCTION, "api/v1/production/grape-containers/grapeType-and-year/add-quantity");
        RestTemplate getBarrelRestTemplate = new RestTemplate();
        GrapeContainerDto upadtedGrapeContainerDto;
        try {
            upadtedGrapeContainerDto = getBarrelRestTemplate.postForObject(getGrapeContainerAddUrl, grapeTypeAndYearModifyQuantityRequest, GrapeContainerDto.class);
        } catch (RestClientException ex) {
            LOGGER.warn("Harvest item post to production item was unsuccessful with exception: {}!", ex.getMessage());
            throw new HarvestItemNotPassedToProductionException(newItem);
        }
        if (upadtedGrapeContainerDto == null) {
            LOGGER.warn("Harvest item post to production item was unsuccessful!");
            throw new HarvestItemNotPassedToProductionException(newItem);
        }
        LOGGER.info("Harvest item post to production item was successful with {}", upadtedGrapeContainerDto);
        HarvestItem hi = harvestItemRepository.save(newItem);
        LOGGER.info(hi.toString());
        return hi;
    }

    public GrapeType addGrapeType(GrapeType newGrapeType) {
        LOGGER.info("Adding new grape type: {} to repository", newGrapeType);
        GrapeType grapeType = grapeTypeRepository.findGrapeTypeByColorAndSweetnessAndDescriptionAndName(newGrapeType.getColor(), newGrapeType.getSweetness(), newGrapeType.getDescription(), newGrapeType.getName());
        if (grapeType != null) {
            throw new GrapeTypeAlreadyExistsException(grapeType);
        }
        return grapeTypeRepository.save(newGrapeType);
    }


    // PUT
    public Harvest updateHarvest(Long id, Harvest updatedHarvest) {
        LOGGER.info("Updating harvest with ID {} in repository with new data: {}", id, updatedHarvest);
        Harvest harvest = harvestRepository.findById(id).orElseThrow(() -> new HarvestNotExistsException(id));
        harvest.setYear(updatedHarvest.getYear());
        return harvestRepository.save(harvest);
    }

    public HarvestItem updateHarvestItem(Long id, HarvestItem updatedHarvestItem) {
        LOGGER.info("Updating harvest item with ID {} in repository with new data: {}", id, updatedHarvestItem);
        HarvestItem item = harvestItemRepository.findById(id).orElseThrow(() -> new HarvestItemNotExistsException(id));
        item.setDate(updatedHarvestItem.getDate());
        item.setGrape(updatedHarvestItem.getGrape());
        item.setQuantity(updatedHarvestItem.getQuantity());
        item.setQuality(updatedHarvestItem.getQuality());
        return harvestItemRepository.save(item);
    }

    public GrapeType updateGrapeType(Long id, GrapeType updatedGrapeType) {
        LOGGER.info("Updating grape type with ID {} in repository with new data: {}", id, updatedGrapeType);
        GrapeType grapeType = grapeTypeRepository.findById(id).orElseThrow(() -> new GrapeTypeNotExistsException(id));
        grapeType.setColor(updatedGrapeType.getColor());
        grapeType.setSweetness(updatedGrapeType.getSweetness());
        grapeType.setDescription(updatedGrapeType.getDescription());
        grapeType.setName(updatedGrapeType.getName());
        return grapeTypeRepository.save(grapeType);
    }


    // DELETE
    public Harvest deleteHarvestById(Long id) {
        LOGGER.info("Deleting harvest with ID {} from repository", id);
        Harvest harvest = harvestRepository.findById(id).orElseThrow(() -> new HarvestNotExistsException(id));
        harvestRepository.deleteById(id);
        return harvest;
    }

    public HarvestItem deleteHarvestItemById(Long id) {
        LOGGER.info("Deleting harvest item with ID {} from repository", id);
        HarvestItem harvestItem = harvestItemRepository.findById(id).orElseThrow(() -> new HarvestItemNotExistsException(id));
        harvestItemRepository.deleteById(id);
        return harvestItem;
    }

    public GrapeType deleteGrapeTypeById(Long id) {
        LOGGER.info("Deleting grape type with ID {} from repository", id);
        GrapeType grapeType = grapeTypeRepository.findById(id).orElseThrow(() -> new GrapeTypeNotExistsException(id));
        grapeTypeRepository.deleteById(id);
        return grapeType;
    }
}
