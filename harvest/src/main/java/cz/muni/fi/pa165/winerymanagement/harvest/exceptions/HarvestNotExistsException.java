package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;

/**
 * Thrown when the Harvest has not been found.
 */
public class HarvestNotExistsException extends RuntimeException {
    public HarvestNotExistsException() {
      super();
    }

    public HarvestNotExistsException(Long id) {
        super("Harvest with id " + id + " does not exist");
    }

}
