package cz.muni.fi.pa165.winerymanagement.store.facade;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.mappers.WineBottleMapper;
import cz.muni.fi.pa165.winerymanagement.store.service.WineBottleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional

public class WineBottleFacade {
	private final WineBottleService wineBottleService;
	private final WineBottleMapper wineBottleMapper;

	@Autowired
	public WineBottleFacade(WineBottleService wineBottleService, WineBottleMapper wineBottleMapper) {
		this.wineBottleService = wineBottleService;
		this.wineBottleMapper = wineBottleMapper;
	}

	@Transactional(readOnly = true)
	public List<WineBottleDto> findAll() {
		return wineBottleMapper.mapToList(wineBottleService.findAll());
	}

	@Transactional(readOnly = true)
	public WineBottleDto findById(Long id) {
		return wineBottleMapper.mapToDto(wineBottleService.findById(id));
	}

	public WineBottleDto add(WineBottleDto newWineBottle) {
		return wineBottleMapper.mapToDto(wineBottleService.add(wineBottleMapper.mapFromDto(newWineBottle)));
	}

	public WineBottleDto updateById(Long id, WineBottleDto updatedWineBottle) {
		return wineBottleMapper.mapToDto(wineBottleService.updateById(id, wineBottleMapper.mapFromDto(updatedWineBottle)));
	}

	public WineBottleDto deleteById(Long id) {
		return wineBottleMapper.mapToDto(wineBottleService.deleteById(id));
	}
}