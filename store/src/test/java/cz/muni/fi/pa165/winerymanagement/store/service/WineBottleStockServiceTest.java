package cz.muni.fi.pa165.winerymanagement.store.service;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottleStock;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleRepository;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleStockRepository;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.bottle.BottleCountMustBePositiveException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.NotEnoughWineBottlesException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockWithWineBottleNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WineBottleStockServiceTest {

    @Mock
    private WineBottleStockRepository wineBottleStockRepository;

    @Mock
    private WineBottleRepository wineBottleRepository;

    @InjectMocks
    private WineBottleStockService wineBottleStockService;

    private GrapeType grapeType1;
    private GrapeType grapeType2;
    private WineBottle wineBottle1;
    private WineBottle wineBottle2;
    private WineBottleStock wineBottleStock1;
    private WineBottleStock wineBottleStock2;

    @BeforeEach
    public void setUp() {
        grapeType1 = new GrapeType();
        grapeType1.setName("Chardonnay");
        grapeType1.setDescription("description");
        grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType1.setColor(GrapeType.Color.WHITE);

        grapeType2 = new GrapeType();
        grapeType2.setName("Merlot");
        grapeType2.setDescription("description");
        grapeType2.setSweetness(GrapeType.Sweetness.DRY);
        grapeType2.setColor(GrapeType.Color.RED);

        wineBottle1 = new WineBottle();
        wineBottle1.setId(1L);
        wineBottle1.setVolume(new VolumeQuantity(100.0, VolumeQuantity.VolumeUnit.LITER));
        wineBottle1.setHarvestYear(2020);
        wineBottle1.setGrapeType(grapeType1);

        wineBottle2 = new WineBottle();
        wineBottle2.setId(2L);
        wineBottle2.setVolume(new VolumeQuantity(150.0, VolumeQuantity.VolumeUnit.LITER));
        wineBottle2.setHarvestYear(2018);
        wineBottle2.setGrapeType(grapeType2);

        wineBottleStock1 = new WineBottleStock();
        wineBottleStock1.setId(1L);
        wineBottleStock1.setWineBottle(wineBottle1);
        wineBottleStock1.setCount(5);
        wineBottleStock1.setPrice(100);

        wineBottleStock2 = new WineBottleStock();
        wineBottleStock2.setId(2L);
        wineBottleStock2.setWineBottle(wineBottle2);
        wineBottleStock2.setCount(10);
        wineBottleStock2.setPrice(200);
    }

    @Test
    void findAllTest() {
        when(wineBottleStockRepository.findAll()).thenReturn(Arrays.asList(wineBottleStock1, wineBottleStock2));

        List<WineBottleStock> wineBottleStocks = wineBottleStockService.findAll();

        assertNotNull(wineBottleStocks);
        assertEquals(2, wineBottleStocks.size());
        assertTrue(wineBottleStocks.contains(wineBottleStock1));
        assertTrue(wineBottleStocks.contains(wineBottleStock2));

        verify(wineBottleStockRepository, times(1)).findAll();
    }

    @Test
    void findByIdTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.of(wineBottleStock1));

        WineBottleStock foundWineBottleStock = wineBottleStockService.findById(1L);

        assertNotNull(foundWineBottleStock);
        assertEquals(wineBottleStock1, foundWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }

    @Test
    void findByIdNotFoundTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(WineBottleStockWithIdNotFoundException.class, () -> wineBottleStockService.findById(1L));

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }


    @Test
    void findByWineBottleTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(wineBottleStock1);

        WineBottleStock foundWineBottleStock = wineBottleStockService.findByWineBottle(wineBottle1);

        assertNotNull(foundWineBottleStock);
        assertEquals(wineBottleStock1, foundWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
    }

    @Test
    void findByWineBottleNotFoundTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(null);

        assertThrows(WineBottleStockWithWineBottleNotFoundException.class, () -> wineBottleStockService.findByWineBottle(wineBottle1));

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
    }

    @Test
    void addTest() {
        when(wineBottleRepository.save(any(WineBottle.class))).thenReturn(wineBottle1);
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(null);
        when(wineBottleStockRepository.save(any(WineBottleStock.class))).thenReturn(wineBottleStock1);

        WineBottleStock addedWineBottleStock = wineBottleStockService.add(wineBottleStock1);

        assertNotNull(addedWineBottleStock);
        assertEquals(wineBottleStock1, addedWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
        verify(wineBottleStockRepository, times(1)).save(wineBottleStock1);
    }

    @Test
    void addAlreadyExistsTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(wineBottleStock1);
        when(wineBottleRepository.save(any(WineBottle.class))).thenReturn(wineBottle1);

        assertThrows(WineBottleStockAlreadyExistsException.class, () -> wineBottleStockService.add(wineBottleStock1));

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
    }

    @Test
    void updateByIdTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.of(wineBottleStock1));
        when(wineBottleStockRepository.save(any(WineBottleStock.class))).thenReturn(wineBottleStock2);

        WineBottleStock updatedWineBottleStock = wineBottleStockService.updateById(1L, wineBottleStock2);

        assertNotNull(updatedWineBottleStock);
        assertEquals(wineBottleStock2, updatedWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findById(1L);
        verify(wineBottleStockRepository, times(1)).save(wineBottleStock1);
    }

    @Test
    void updateByIdNotFoundTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(WineBottleStockWithIdNotFoundException.class, () -> wineBottleStockService.updateById(1L, wineBottleStock2));

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }

    @Test
    void updateByWineBottleTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(wineBottleStock1);
        when(wineBottleStockRepository.save(any(WineBottleStock.class))).thenReturn(wineBottleStock2);

        WineBottleStock updatedWineBottleStock = wineBottleStockService.updateByWineBottle(wineBottle1, wineBottleStock2);

        assertNotNull(updatedWineBottleStock);
        assertEquals(wineBottleStock2, updatedWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
        verify(wineBottleStockRepository, times(1)).save(wineBottleStock1);
    }

    @Test
    void updateByWineBottleNotFoundTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(null);

        assertThrows(WineBottleStockWithWineBottleNotFoundException.class, () -> wineBottleStockService.updateByWineBottle(wineBottle1, wineBottleStock2));

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
    }

    @Test
    void addBottlesToStockTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.of(wineBottleStock1));

        WineBottleStock updatedWineBottleStock = wineBottleStockService.addBottlesToStock(1L, 5);

        assertNotNull(updatedWineBottleStock);
        assertEquals(10, updatedWineBottleStock.getCount());

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }

    @Test
    void addBottlesToStockNegativeCountTest() {
        assertThrows(BottleCountMustBePositiveException.class, () -> wineBottleStockService.addBottlesToStock(1L, -5));
    }

    @Test
    void removeBottlesFromStockTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.of(wineBottleStock1));

        WineBottleStock updatedWineBottleStock = wineBottleStockService.removeBottlesFromStock(1L, 3);

        assertNotNull(updatedWineBottleStock);
        assertEquals(2, updatedWineBottleStock.getCount());

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }

    @Test
    void removeBottlesFromStockNegativeCountTest() {
        assertThrows(BottleCountMustBePositiveException.class, () -> wineBottleStockService.removeBottlesFromStock(1L, -5));
    }

    @Test
    void removeBottlesFromStockNotEnoughBottlesTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.of(wineBottleStock1));

        assertThrows(NotEnoughWineBottlesException.class, () -> wineBottleStockService.removeBottlesFromStock(1L, 10));

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }

    @Test
    void deleteByIdTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.of(wineBottleStock1));

        WineBottleStock deletedWineBottleStock = wineBottleStockService.deleteById(1L);

        assertNotNull(deletedWineBottleStock);
        assertEquals(wineBottleStock1, deletedWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findById(1L);
        verify(wineBottleStockRepository, times(1)).deleteById(1L);
    }

    @Test
    void deleteByIdNotFoundTest() {
        when(wineBottleStockRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(WineBottleStockWithIdNotFoundException.class, () -> wineBottleStockService.deleteById(1L));

        verify(wineBottleStockRepository, times(1)).findById(1L);
    }

    @Test
    void deleteByWineBottleTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(wineBottleStock1);

        WineBottleStock deletedWineBottleStock = wineBottleStockService.deleteByWineBottle(wineBottle1);

        assertNotNull(deletedWineBottleStock);
        assertEquals(wineBottleStock1, deletedWineBottleStock);

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
        verify(wineBottleStockRepository, times(1)).deleteById(wineBottle1.getId());
    }

    @Test
    void deleteByWineBottleNotFoundTest() {
        when(wineBottleStockRepository.findByWineBottle(wineBottle1)).thenReturn(null);

        assertThrows(WineBottleStockWithWineBottleNotFoundException.class, () -> wineBottleStockService.deleteByWineBottle(wineBottle1));

        verify(wineBottleStockRepository, times(1)).findByWineBottle(wineBottle1);
    }

}
