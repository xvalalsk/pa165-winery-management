package cz.muni.fi.pa165.winerymanagement.production.rest.requests;

import cz.muni.fi.pa165.winerymanagement.production.api.BarrelDto;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "GrapeTypeAndYearUpdateBarrelRequest",
        description = "A request object to update a barrel with grape type and year information",
        example = """
                    {
                        "grapeTypeAndYearGetRequest": {
                            "grapeType": {
                                "name": "Chardonnay",
                                "description": "Chardonnay white sweet wine description",
                                "color": "WHITE",
                                "sweetness": "SWEET"
                            },
                            "harvestYear": 2020
                        },
                        "updatedBarrel": {
                            "id": 1,
                            "grapeType": {
                                "name": "Merlot",
                                "description": "Merlot red dry wine description",
                                "color": "RED",
                                "sweetness": "DRY"
                            },
                            "harvestYear": 2019,
                            "quantity": {
                                "type": "volume",
                                "value": 42.0,
                                "volumeUnit": "LITER"
                            }
                        }
                    }
                """)
public class GrapeTypeAndYearUpdateBarrelRequest {
    private GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest;
    private BarrelDto updatedBarrel;

    public GrapeTypeAndYearGetRequest getGrapeTypeAndYearGetRequest() {
        return grapeTypeAndYearGetRequest;
    }

    public void setGrapeTypeAndYearGetRequest(GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        this.grapeTypeAndYearGetRequest = grapeTypeAndYearGetRequest;
    }

    public BarrelDto getUpdatedBarrel() {
        return updatedBarrel;
    }

    public void setUpdatedBarrel(BarrelDto updatedBarrel) {
        this.updatedBarrel = updatedBarrel;
    }

    @Override
    public String toString() {
        return "GrapeTypeAndYearUpdateBarrelRequest{" +
                "grapeTypeAndYearGetRequest=" + grapeTypeAndYearGetRequest +
                ", updatedBarrel=" + updatedBarrel +
                '}';
    }
}
