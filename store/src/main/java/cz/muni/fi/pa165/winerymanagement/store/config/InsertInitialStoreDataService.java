package cz.muni.fi.pa165.winerymanagement.store.config;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottleStock;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleRepository;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleStockRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is responsible for inserting dummy data into the application.
 * It is a SpringBoot service that is executed during the application startup.
 */
@Service
@Transactional
public class InsertInitialStoreDataService {
	private final WineBottleRepository wineBottleRepository;
	private final WineBottleStockRepository wineBottleStockRepository;

	@Autowired
	public InsertInitialStoreDataService(WineBottleRepository wineBottleRepository, WineBottleStockRepository wineBottleStockRepository ) {
		this.wineBottleRepository = wineBottleRepository;
		this.wineBottleStockRepository = wineBottleStockRepository;
	}

	@PostConstruct
	public void insertDummyData() {
		// Insert GrapeTypes
		GrapeType grapeType1 = new GrapeType();
		grapeType1.setName("Chardonnay");
		grapeType1.setDescription("Chardonnay white sweet wine description");
		grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
		grapeType1.setColor(GrapeType.Color.WHITE);

		GrapeType grapeType2 = new GrapeType();
		grapeType2.setName("Zweigeltrebe");
		grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
		grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
		grapeType2.setColor(GrapeType.Color.ROSE);

		// Insert WineBottles
		VolumeQuantity volumeQuantityBottle = new VolumeQuantity(0.75, VolumeQuantity.VolumeUnit.LITER);

		WineBottle wineBottle1 = new WineBottle();
		wineBottle1.setGrapeType(grapeType1);
		wineBottle1.setHarvestYear(2011);
		wineBottle1.setVolume(volumeQuantityBottle);

		WineBottle wineBottle2 = new WineBottle();
		wineBottle2.setGrapeType(grapeType2);
		wineBottle2.setHarvestYear(2022);
		wineBottle2.setVolume(volumeQuantityBottle);

		//Add wine bottles to stock
		WineBottleStock wineBottleStock1 = new WineBottleStock();
		wineBottleStock1.setWineBottle(wineBottle1);
		wineBottleStock1.setCount(100);
		wineBottleStock1.setPrice(9.99);

		WineBottleStock wineBottleStock2 = new WineBottleStock();
		wineBottleStock2.setWineBottle(wineBottle2);
		wineBottleStock2.setCount(80);
		wineBottleStock2.setPrice(7.99);

		wineBottleStockRepository.save(wineBottleStock1);
		wineBottleStockRepository.save(wineBottleStock2);
	}

    public void deleteAllData() {
		wineBottleStockRepository.deleteAll();
		wineBottleRepository.deleteAll();
    }
}