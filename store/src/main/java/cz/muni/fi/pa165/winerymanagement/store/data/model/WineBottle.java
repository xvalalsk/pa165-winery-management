package cz.muni.fi.pa165.winerymanagement.store.data.model;

import cz.muni.fi.pa165.winerymanagement.core.converter.QuantityJsonConverter;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * WineBottle entity represents a wine bottle with information about
 * its grape type, harvest year, and volume.
 */

@Entity
@Table(name = "wine_bottle")
public class WineBottle implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_wine_bottle")
	private Long id;
	@Embedded
	private GrapeType grapeType;
	@Column(name = "harvest_year")
	private int harvestYear;
	@Column(name = "volume", columnDefinition = "TEXT")
	@Convert(converter = QuantityJsonConverter.class)
	private VolumeQuantity volume;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrapeType getGrapeType() {
		return grapeType;
	}

	public void setGrapeType(GrapeType grapeType) {
		this.grapeType = grapeType;
	}

	public int getHarvestYear() {
		return harvestYear;
	}

	public void setHarvestYear(int harvestYear) {
		this.harvestYear = harvestYear;
	}

	public VolumeQuantity getVolume() {
		return volume;
	}

	public void setVolume(VolumeQuantity volume) {
		this.volume = volume;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof WineBottle that)) return false;
		return Objects.equals(getGrapeType(), that.getGrapeType()) && Objects.equals(getHarvestYear(), that.getHarvestYear()) && Objects.equals(getVolume(), that.getVolume());

	}

	@Override
	public int hashCode() {
		return Objects.hash(getGrapeType(), getHarvestYear(), getVolume());
	}

	@Override
	public String toString() {
		return "WineBottle{" +
				"id=" + id +
				", grapeType=" + grapeType +
				", harvestYear=" + harvestYear +
				", volume=" + volume +
				'}';
	}
}