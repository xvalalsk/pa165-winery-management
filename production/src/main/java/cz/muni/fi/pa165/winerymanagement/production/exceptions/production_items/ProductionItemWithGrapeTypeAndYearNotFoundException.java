package cz.muni.fi.pa165.winerymanagement.production.exceptions.production_items;

import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceNotFoundException;

import java.util.ResourceBundle;

public class ProductionItemWithGrapeTypeAndYearNotFoundException extends ResourceNotFoundException {

    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
    private final GrapeType grapeType;
    private final int harvestYear;

    public ProductionItemWithGrapeTypeAndYearNotFoundException(GrapeType grapeType, int harvestYear) {
        this.grapeType = grapeType;
        this.harvestYear = harvestYear;
    }

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        String message = messages.getString(key);
        return String.format(message, grapeType, harvestYear);
    }
}
