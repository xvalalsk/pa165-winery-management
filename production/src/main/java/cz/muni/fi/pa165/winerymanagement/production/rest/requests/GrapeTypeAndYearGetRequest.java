package cz.muni.fi.pa165.winerymanagement.production.rest.requests;

import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "GrapeTypeAndYearGetRequest",
        description = "Request object for getting grape type and harvest year information",
        example = """
                    {
                        "grapeType": {
                            "name": "Chardonnay",
                            "description": "Chardonnay white sweet wine description",
                            "color": "WHITE",
                            "sweetness": "SWEET"
                        },
                        "harvestYear": 2020
                    }
                """)
public class GrapeTypeAndYearGetRequest {
    private GrapeType grapeType;
    private int harvestYear;

    public GrapeType getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeType grapeType) {
        this.grapeType = grapeType;
    }

    public int getHarvestYear() {
        return harvestYear;
    }

    public void setHarvestYear(int harvestYear) {
        this.harvestYear = harvestYear;
    }
}
