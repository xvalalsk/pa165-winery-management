package cz.muni.fi.pa165.winerymanagement.store.data.model;

import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.converter.QuantityJsonConverter;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;


/**
 * Barrel entity class representing a barrel with information about
 * its harvestYear, grape type and quantity.
 */
@Entity
@Table(name = "barrel")
public class Barrel implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_barrel")
	private Long id;
	@Column(name = "harvest_year")
	private int harvestYear;
	@Embedded
	private GrapeType grapeType;
	@Column(name = "quantity", columnDefinition = "TEXT")
	@Convert(converter = QuantityJsonConverter.class)
	private VolumeQuantity quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getHarvestYear() {
		return harvestYear;
	}

	public void setHarvestYear(int harvestYear) {
		this.harvestYear = harvestYear;
	}

	public GrapeType getGrapeType() {
		return grapeType;
	}

	public void setGrapeType(GrapeType grapeType) {
		this.grapeType = grapeType;
	}

	public VolumeQuantity getQuantity() {
		return quantity;
	}

	public void setQuantity(VolumeQuantity quantity) {
		this.quantity = quantity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Barrel barrel)) return false;
		return Objects.equals(getId(), barrel.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public String toString() {
		return "Barrel{" +
				"id=" + id +
				", harvestYear=" + harvestYear +
				", grapeType=" + grapeType +
				", quantity=" + quantity +
				'}';
	}
}


