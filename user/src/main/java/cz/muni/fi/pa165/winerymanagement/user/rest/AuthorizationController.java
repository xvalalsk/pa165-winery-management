package cz.muni.fi.pa165.winerymanagement.user.rest;

import cz.muni.fi.pa165.winerymanagement.user.api.UserRegistrationDto;
import cz.muni.fi.pa165.winerymanagement.user.facade.AuthorizationFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class AuthorizationController {
    public AuthorizationFacade authorizationFacade;

    @Autowired
    public AuthorizationController(AuthorizationFacade authorizationFacade) {
        this.authorizationFacade = authorizationFacade;
    }

    @PostMapping("/register")
    @Operation(
            summary = "Add user which was logged in to the system with external authentication. Save same basic info f.e. issuer and external identifier.",
            tags = "User"
    )
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "User registered")
    })
    @ResponseStatus(HttpStatus.CREATED)
    public long register(
            @Parameter(description = "User for registration", required = true) @RequestBody UserRegistrationDto userRegistrationDto
    ) {
        try {
            return this.authorizationFacade.register(userRegistrationDto);
        } catch (DataIntegrityViolationException e) {
            // ignore exception
        }
        return 0;
    }
}
