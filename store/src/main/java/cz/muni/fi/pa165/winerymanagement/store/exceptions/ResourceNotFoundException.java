package cz.muni.fi.pa165.winerymanagement.store.exceptions;

public class ResourceNotFoundException extends StoreException {

	public ResourceNotFoundException() {
	}

	@Override
	public String getMessage() {
		return "Resource not found";
	}
}
