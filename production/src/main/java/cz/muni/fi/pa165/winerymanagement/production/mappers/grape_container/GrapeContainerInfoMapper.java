package cz.muni.fi.pa165.winerymanagement.production.mappers.grape_container;

import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.GrapeContainerInfo;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GrapeContainerInfoMapper {
    GrapeContainerInfo mapToDto(GrapeContainer grapeContainer);

    List<GrapeContainerInfo> mapToList(List<GrapeContainerInfo> barrels);

    GrapeContainer mapFromDto(GrapeContainerInfo grapeContainer);
}
