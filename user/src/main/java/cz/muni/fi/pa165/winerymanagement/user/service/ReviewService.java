package cz.muni.fi.pa165.winerymanagement.user.service;

import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.ReviewRepository;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.UserRepository;
import cz.muni.fi.pa165.winerymanagement.user.exceptions.InvalidReviewException;
import cz.muni.fi.pa165.winerymanagement.user.rest.ReviewController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Transactional
public class ReviewService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewService.class);
    private static final int MIN_REVIEW = 0;
    private static final int MAX_REVIEW = 5;

    private final ReviewRepository reviewRepository;
    private final UserRepository userRepository;

    @Autowired
    public ReviewService(ReviewRepository reviewRepository, UserRepository userRepository) {
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
    }

    public Review addReview(Review review) {
        this.validateReview(review);
        return this.reviewRepository.save(review);
    }

    public Set<Review> getWineBottleReviews(Long wineBottleId) {
        return this.reviewRepository.findReviewsByWineBottleId(wineBottleId);
    }

    public Set<Review> getUserReviews(Long userId) {
        return this.reviewRepository.findReviewsByUserId(userId);
    }

    public Review updateReview(Review updatedReview) {
        this.validateReview(updatedReview);

        return this.reviewRepository.save(updatedReview);
    }

    public void deleteReview(Long id) {
        this.reviewRepository.deleteById(id);
    }

    private void validateReview(Review review) {
        if (review.getRating() < MIN_REVIEW || review.getRating() > MAX_REVIEW) {
            throw new InvalidReviewException("Rating must be between " + MIN_REVIEW + " and " + MAX_REVIEW);
        }

        if (review.getComment().isEmpty()) {
            throw new InvalidReviewException("Review comment must not be empty");
        }

        if (this.userRepository.findById(review.getUser().getId()).isEmpty()) {
            throw new InvalidReviewException("User with id " + review.getUser().getId() + " does not exist");
        }
    }
}
