package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.production.ProductionApplication;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Barrel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ProductionApplication.class)
@Transactional
public class BarrelRepositoryTest {

	@Autowired
	private BarrelRepository barrelRepository;

	private Barrel barrel;
	private GrapeType grapeType;

	@BeforeEach
	public void setUp() {
		grapeType = new GrapeType();
		grapeType.setName("Test Grape");
		grapeType.setDescription("Test grape description");
		grapeType.setColor(GrapeType.Color.WHITE);
		grapeType.setSweetness(GrapeType.Sweetness.DRY);

		barrel = new Barrel();
		barrel.setHarvestYear(2020);
		barrel.setGrapeType(grapeType);
		barrel.setQuantity(new VolumeQuantity(100.0, VolumeQuantity.VolumeUnit.LITER));
	}

	@Test
	void findByGrapeTypeAndHarvestYear_shouldReturnBarrel() {
		barrelRepository.save(barrel);

		Barrel foundBarrel = barrelRepository.findByGrapeTypeAndHarvestYear(grapeType, 2020);

		assertThat(foundBarrel).isNotNull();
		assertThat(foundBarrel.getGrapeType()).isEqualTo(grapeType);
		assertThat(foundBarrel.getHarvestYear()).isEqualTo(2020);
		assertThat(foundBarrel.getQuantity().getValue()).isEqualTo(100.0);
		assertThat(foundBarrel.getQuantity().getVolumeUnit()).isEqualTo(VolumeQuantity.VolumeUnit.LITER);
	}

	@Test
	void findByGrapeTypeAndHarvestYear_shouldReturnNull_whenNotFound() {
		barrelRepository.save(barrel);

		Barrel foundBarrel = barrelRepository.findByGrapeTypeAndHarvestYear(grapeType, 2021);

		assertThat(foundBarrel).isNull();
	}

	@Test
	void save_shouldPersistBarrel() {
		// When
		Barrel savedBarrel = barrelRepository.save(barrel);

		// Then
		assertThat(savedBarrel.getId()).isNotNull();
		assertThat(savedBarrel.getGrapeType()).isEqualTo(grapeType);
		assertThat(savedBarrel.getHarvestYear()).isEqualTo(2020);
	}

	@Test
	void findById_shouldReturnBarrel_whenFound() {
		Barrel savedBarrel = barrelRepository.save(barrel);

		Optional<Barrel> foundBarrel = barrelRepository.findById(savedBarrel.getId());

		assertThat(foundBarrel).contains(savedBarrel);
	}

	@Test
	void findById_shouldReturnEmpty_whenNotFound() {
		Optional<Barrel> foundBarrel = barrelRepository.findById(-1L);

		assertThat(foundBarrel).isNotPresent();
	}

	@Test
	void delete_shouldRemoveBarrel() {
		Barrel savedBarrel = barrelRepository.save(barrel);

		barrelRepository.delete(savedBarrel);
		Optional<Barrel> foundBarrel = barrelRepository.findById(savedBarrel.getId());

		assertThat(foundBarrel).isNotPresent();
	}
}