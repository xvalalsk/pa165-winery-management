package cz.muni.fi.pa165.winerymanagement.production.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.api.ProductionItemDto;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.GrapeContainerInfo;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import cz.muni.fi.pa165.winerymanagement.production.facade.ProductionItemFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ProductionItemRestController.class)
@Import(ProductionItemRestController.class)
public class ProductionItemRestControllerTest {

    String API_PATH = "/api/v1/production/production-items";
    WeightQuantity weightQuantity100KG = new WeightQuantity(100, WeightQuantity.WeightUnit.KILOGRAM);
    WeightQuantity weightQuantity500KG = new WeightQuantity(500, WeightQuantity.WeightUnit.KILOGRAM);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductionItemFacade productionItemFacade;
    private GrapeType grapeType1 = new GrapeType();
    private GrapeType grapeType2 = new GrapeType();

    private GrapeContainerInfo grapeContainerInfo1 = new GrapeContainerInfo();
    private GrapeContainerInfo grapeContainerInfo2 = new GrapeContainerInfo();

    private Ingredient ingredient1 = new Ingredient();
    private Ingredient ingredient2 = new Ingredient();
    private Ingredient ingredient3 = new Ingredient();
    private ProductionItemDto productionItemDto1;
    private ProductionItemDto productionItemDto2;

    @BeforeEach
    void setUp() {
        ingredient1.setName("sugar");
        ingredient1.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));

        ingredient2.setName("salt");
        ingredient2.setQuantity(new WeightQuantity(500, WeightQuantity.WeightUnit.GRAM));

        ingredient3.setName("water");
        ingredient3.setQuantity(new VolumeQuantity(500, VolumeQuantity.VolumeUnit.LITER));


        grapeType1.setName("Chardonnay");
        grapeType1.setDescription("Chardonnay white sweet wine description");
        grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType1.setColor(GrapeType.Color.WHITE);

        grapeType2.setName("Zweigeltrebe");
        grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
        grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
        grapeType2.setColor(GrapeType.Color.ROSE);


        grapeContainerInfo1.setGrapeType(grapeType1);
        grapeContainerInfo1.setId(1L);
        grapeContainerInfo1.setHarvestYear((2011));

        grapeContainerInfo2.setGrapeType(grapeType2);
        grapeContainerInfo2.setId(2L);
        grapeContainerInfo2.setHarvestYear((2022));


        productionItemDto1 = new ProductionItemDto();
        productionItemDto1.setId(1L);
        productionItemDto1.setGrape(grapeContainerInfo1);
        productionItemDto1.setGrapeQuantity(weightQuantity100KG);
        productionItemDto1.setYear(2012);
        IngredientRequirement ingredientRequirement1 = new IngredientRequirement();
        ingredientRequirement1.setName(ingredient1.getName());
        ingredientRequirement1.setQuantity(new WeightQuantity(5, WeightQuantity.WeightUnit.KILOGRAM));
        IngredientRequirement ingredientRequirement2 = new IngredientRequirement();
        ingredientRequirement2.setName(ingredient2.getName());
        ingredientRequirement2.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.GRAM));
        IngredientRequirement ingredientRequirement3 = new IngredientRequirement();
        ingredientRequirement3.setName(ingredient3.getName());
        ingredientRequirement3.setQuantity(new VolumeQuantity(15, VolumeQuantity.VolumeUnit.LITER));
        productionItemDto1.setIngredients(List.of(ingredientRequirement1, ingredientRequirement2, ingredientRequirement3));

        productionItemDto2 = new ProductionItemDto();
        productionItemDto2.setId(2L);
        productionItemDto2.setGrape(grapeContainerInfo2);
        productionItemDto2.setGrapeQuantity(weightQuantity500KG);
        productionItemDto2.setYear(2023);
        IngredientRequirement ingredientRequirement21 = new IngredientRequirement();
        ingredientRequirement21.setName(ingredient1.getName());
        ingredientRequirement21.setQuantity(new WeightQuantity(20, WeightQuantity.WeightUnit.KILOGRAM));
        IngredientRequirement ingredientRequirement22 = new IngredientRequirement();
        ingredientRequirement22.setName(ingredient2.getName());
        ingredientRequirement22.setQuantity(new WeightQuantity(25, WeightQuantity.WeightUnit.GRAM));
        IngredientRequirement ingredientRequirement23 = new IngredientRequirement();
        ingredientRequirement23.setName(ingredient3.getName());
        ingredientRequirement23.setQuantity(new VolumeQuantity(30, VolumeQuantity.VolumeUnit.LITER));
        productionItemDto2.setIngredients(List.of(ingredientRequirement21, ingredientRequirement22, ingredientRequirement23));
    }

    @Test
    void testUnauthorized() throws Exception {
        mockMvc.perform(get(API_PATH))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_1"})
    void testForbidden() throws Exception {
        mockMvc.perform(get(API_PATH))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void testGetAllProductionItems() throws Exception {
        List<ProductionItemDto> productionItems = Arrays.asList(productionItemDto1, productionItemDto2);

        when(productionItemFacade.findAll()).thenReturn(productionItems);

        mockMvc.perform(get(API_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].grape.grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("$[0].grape.grapeType.description", is("Chardonnay white sweet wine description")))
                .andExpect(jsonPath("$[0].grape.grapeType.color", is("WHITE")))
                .andExpect(jsonPath("$[0].grape.grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("$[0].grape.harvestYear", is(2011)))

                .andExpect(jsonPath("$[0].grapeQuantity.type", is("weight")))
                .andExpect(jsonPath("$[0].grapeQuantity.value", is(100.0)))
                .andExpect(jsonPath("$[0].grapeQuantity.weightUnit", is("KILOGRAM")))

                .andExpect(jsonPath("$[0].ingredients[0].name", is("sugar")))
                .andExpect(jsonPath("$[0].ingredients[0].quantity.type", is("weight")))
                .andExpect(jsonPath("$[0].ingredients[0].quantity.value", is(5.0)))
                .andExpect(jsonPath("$[0].ingredients[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("$[0].ingredients[1].name", is("salt")))
                .andExpect(jsonPath("$[0].ingredients[1].quantity.type", is("weight")))
                .andExpect(jsonPath("$[0].ingredients[1].quantity.value", is(10.0)))
                .andExpect(jsonPath("$[0].ingredients[1].quantity.weightUnit", is("GRAM")))
                .andExpect(jsonPath("$[0].ingredients[2].name", is("water")))
                .andExpect(jsonPath("$[0].ingredients[2].quantity.type", is("volume")))
                .andExpect(jsonPath("$[0].ingredients[2].quantity.value", is(15.0)))
                .andExpect(jsonPath("$[0].ingredients[2].quantity.volumeUnit", is("LITER")))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].grape.grapeType.name", is("Zweigeltrebe")))
                .andExpect(jsonPath("$[1].grape.grapeType.description", is("Zweigeltrebe rose semi-sweet wine description")))
                .andExpect(jsonPath("$[1].grape.grapeType.color", is("ROSE")))
                .andExpect(jsonPath("$[1].grape.grapeType.sweetness", is("SEMISWEET")))
                .andExpect(jsonPath("$[1].grape.harvestYear", is(2022)))

                .andExpect(jsonPath("$[1].grapeQuantity.type", is("weight")))
                .andExpect(jsonPath("$[1].grapeQuantity.value", is(500.0)))
                .andExpect(jsonPath("$[1].grapeQuantity.weightUnit", is("KILOGRAM")))

                .andExpect(jsonPath("$[1].ingredients[0].name", is("sugar")))
                .andExpect(jsonPath("$[1].ingredients[0].quantity.type", is("weight")))
                .andExpect(jsonPath("$[1].ingredients[0].quantity.value", is(20.0)))
                .andExpect(jsonPath("$[1].ingredients[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("$[1].ingredients[1].name", is("salt")))
                .andExpect(jsonPath("$[1].ingredients[1].quantity.type", is("weight")))
                .andExpect(jsonPath("$[1].ingredients[1].quantity.value", is(25.0)))
                .andExpect(jsonPath("$[1].ingredients[1].quantity.weightUnit", is("GRAM")))
                .andExpect(jsonPath("$[1].ingredients[2].name", is("water")))
                .andExpect(jsonPath("$[1].ingredients[2].quantity.type", is("volume")))
                .andExpect(jsonPath("$[1].ingredients[2].quantity.value", is(30.0)))
                .andExpect(jsonPath("$[1].ingredients[2].quantity.volumeUnit", is("LITER")));

        verify(productionItemFacade).findAll();
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void testGetProductionItemById() throws Exception {
        Long id = productionItemDto1.getId();

        when(productionItemFacade.findById(id)).thenReturn(productionItemDto1);

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grape.grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grape.grapeType.description", is("Chardonnay white sweet wine description")))
                .andExpect(jsonPath("grape.grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grape.grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("grape.harvestYear", is(2011)))

                .andExpect(jsonPath("grapeQuantity.type", is("weight")))
                .andExpect(jsonPath("grapeQuantity.value", is(100.0)))
                .andExpect(jsonPath("grapeQuantity.weightUnit", is("KILOGRAM")))

                .andExpect(jsonPath("ingredients[0].name", is("sugar")))
                .andExpect(jsonPath("ingredients[0].quantity.type", is("weight")))
                .andExpect(jsonPath("ingredients[0].quantity.value", is(5.0)))
                .andExpect(jsonPath("ingredients[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("ingredients[1].name", is("salt")))
                .andExpect(jsonPath("ingredients[1].quantity.type", is("weight")))
                .andExpect(jsonPath("ingredients[1].quantity.value", is(10.0)))
                .andExpect(jsonPath("ingredients[1].quantity.weightUnit", is("GRAM")))
                .andExpect(jsonPath("ingredients[2].name", is("water")))
                .andExpect(jsonPath("ingredients[2].quantity.type", is("volume")))
                .andExpect(jsonPath("ingredients[2].quantity.value", is(15.0)))
                .andExpect(jsonPath("ingredients[2].quantity.volumeUnit", is("LITER")));

        verify(productionItemFacade).findById(id);
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void testCreateProductionItem() throws Exception {
        ProductionItemDto newProductionItem = new ProductionItemDto();
        newProductionItem.setId(1L);
        newProductionItem.setGrape(grapeContainerInfo1);
        newProductionItem.setGrapeQuantity(weightQuantity100KG);
        newProductionItem.setYear(9999);
        IngredientRequirement ingredientRequirement1 = new IngredientRequirement();
        ingredientRequirement1.setName(ingredient1.getName());
        ingredientRequirement1.setQuantity(new WeightQuantity(41, WeightQuantity.WeightUnit.KILOGRAM));

        IngredientRequirement ingredientRequirement2 = new IngredientRequirement();
        ingredientRequirement2.setName(ingredient2.getName());
        ingredientRequirement2.setQuantity(new WeightQuantity(42, WeightQuantity.WeightUnit.GRAM));

        IngredientRequirement ingredientRequirement3 = new IngredientRequirement();
        ingredientRequirement3.setName(ingredient3.getName());
        ingredientRequirement3.setQuantity(new VolumeQuantity(43, VolumeQuantity.VolumeUnit.LITER));
        newProductionItem.setIngredients(List.of(ingredientRequirement1, ingredientRequirement2, ingredientRequirement3));

        when(productionItemFacade.add(ArgumentMatchers.any(ProductionItemDto.class))).thenReturn(newProductionItem);

        mockMvc.perform(post(API_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newProductionItem)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(newProductionItem.getId().intValue())))
                .andExpect(jsonPath("grape.grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grape.grapeType.description", is("Chardonnay white sweet wine description")))
                .andExpect(jsonPath("grape.grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grape.grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("grape.harvestYear", is(2011)))

                .andExpect(jsonPath("grapeQuantity.type", is("weight")))
                .andExpect(jsonPath("grapeQuantity.value", is(100.0)))
                .andExpect(jsonPath("grapeQuantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("ingredients[0].name", is("sugar")))
                .andExpect(jsonPath("ingredients[0].quantity.type", is("weight")))
                .andExpect(jsonPath("ingredients[0].quantity.value", is(41.0)))
                .andExpect(jsonPath("ingredients[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("ingredients[1].name", is("salt")))
                .andExpect(jsonPath("ingredients[1].quantity.type", is("weight")))
                .andExpect(jsonPath("ingredients[1].quantity.value", is(42.0)))
                .andExpect(jsonPath("ingredients[1].quantity.weightUnit", is("GRAM")))
                .andExpect(jsonPath("ingredients[2].name", is("water")))
                .andExpect(jsonPath("ingredients[2].quantity.type", is("volume")))
                .andExpect(jsonPath("ingredients[2].quantity.value", is(43.0)))
                .andExpect(jsonPath("ingredients[2].quantity.volumeUnit", is("LITER")));
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void testUpdateProductionItem() throws Exception {
        Long id = 1L;

        ProductionItemDto newProductionItem = new ProductionItemDto();
        newProductionItem.setId(1L);
        newProductionItem.setGrape(grapeContainerInfo1);
        newProductionItem.setGrapeQuantity(weightQuantity100KG);
        newProductionItem.setYear(9999);
        IngredientRequirement ingredientRequirement1 = new IngredientRequirement();
        ingredientRequirement1.setName(ingredient1.getName());
        ingredientRequirement1.setQuantity(new WeightQuantity(41, WeightQuantity.WeightUnit.KILOGRAM));
        IngredientRequirement ingredientRequirement2 = new IngredientRequirement();
        ingredientRequirement2.setName(ingredient2.getName());
        ingredientRequirement2.setQuantity(new WeightQuantity(42, WeightQuantity.WeightUnit.GRAM));
        IngredientRequirement ingredientRequirement3 = new IngredientRequirement();
        ingredientRequirement3.setName(ingredient3.getName());
        ingredientRequirement3.setQuantity(new VolumeQuantity(43, VolumeQuantity.VolumeUnit.LITER));
        newProductionItem.setIngredients(List.of(ingredientRequirement1, ingredientRequirement2, ingredientRequirement3));

        when(productionItemFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(ProductionItemDto.class))).thenReturn(newProductionItem);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newProductionItem)))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("grape.grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grape.grapeType.description", is("Chardonnay white sweet wine description")))
                .andExpect(jsonPath("grape.grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grape.grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("grape.harvestYear", is(2011)))

                .andExpect(jsonPath("grapeQuantity.type", is("weight")))
                .andExpect(jsonPath("grapeQuantity.value", is(100.0)))
                .andExpect(jsonPath("grapeQuantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("ingredients[0].name", is("sugar")))
                .andExpect(jsonPath("ingredients[0].quantity.type", is("weight")))
                .andExpect(jsonPath("ingredients[0].quantity.value", is(41.0)))
                .andExpect(jsonPath("ingredients[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("ingredients[1].name", is("salt")))
                .andExpect(jsonPath("ingredients[1].quantity.type", is("weight")))
                .andExpect(jsonPath("ingredients[1].quantity.value", is(42.0)))
                .andExpect(jsonPath("ingredients[1].quantity.weightUnit", is("GRAM")))
                .andExpect(jsonPath("ingredients[2].name", is("water")))
                .andExpect(jsonPath("ingredients[2].quantity.type", is("volume")))
                .andExpect(jsonPath("ingredients[2].quantity.value", is(43.0)))
                .andExpect(jsonPath("ingredients[2].quantity.volumeUnit", is("LITER")));
    }

    @Test
    @WithMockUser(authorities = {"SCOPE_test_2"})
    void testDeleteProductionItem() throws Exception {
        Long id = 1L;
        when(productionItemFacade.deleteById(id)).thenReturn(productionItemDto1);

        mockMvc.perform(delete(API_PATH + "/{id}", id))
                .andExpect(status().isOk());
    }
}
