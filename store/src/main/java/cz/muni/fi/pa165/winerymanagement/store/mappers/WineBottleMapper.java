package cz.muni.fi.pa165.winerymanagement.store.mappers;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WineBottleMapper {
	WineBottleDto mapToDto(WineBottle wineBottle);
	List<WineBottleDto> mapToList(List<WineBottle> wineBottles);
	WineBottle mapFromDto(WineBottleDto wineBottleDto);
}


