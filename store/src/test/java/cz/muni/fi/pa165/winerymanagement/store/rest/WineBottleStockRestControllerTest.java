package cz.muni.fi.pa165.winerymanagement.store.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleStockDto;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockWithWineBottleNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.facade.WineBottleStockFacade;
import cz.muni.fi.pa165.winerymanagement.store.rest.exceptionhandling.CustomRestGlobalExceptionHandling;
import cz.muni.fi.pa165.winerymanagement.store.rest.requests.WineBottleUpdateWineBottleStockRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@ExtendWith(MockitoExtension.class)
@WebMvcTest(WineBottleStockRestController.class)
@Import(WineBottleStockRestController.class)
class WineBottleStockRestControllerTest {

	String API_PATH = "/api/v1/store/wine-bottles-in-stock";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private WineBottleStockFacade wineBottleStockFacade;

	VolumeQuantity volumeQuantity1L = new VolumeQuantity(1, VolumeQuantity.VolumeUnit.LITER);
	VolumeQuantity volumeQuantity075L = new VolumeQuantity(0.75, VolumeQuantity.VolumeUnit.LITER);
	private final GrapeType grapeType1 = new GrapeType();
	private final GrapeType grapeType2 = new GrapeType();

	private final WineBottle wineBottle1 = new WineBottle();
	private final WineBottle wineBottle2 = new WineBottle();

	private WineBottleStockDto wineBottleStockDto1;
	private WineBottleStockDto wineBottleStockDto2;

	@BeforeEach
	void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new WineBottleStockRestController(wineBottleStockFacade)).setControllerAdvice(CustomRestGlobalExceptionHandling.class).build();

		grapeType1.setName("Chardonnay");
		grapeType1.setDescription("Chardonnay white sweet wine description");
		grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
		grapeType1.setColor(GrapeType.Color.WHITE);

		grapeType2.setName("Zweigeltrebe");
		grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
		grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
		grapeType2.setColor(GrapeType.Color.ROSE);

		wineBottle1.setId(1L);
		wineBottle1.setGrapeType(grapeType1);
		wineBottle1.setHarvestYear(2011);
		wineBottle1.setVolume(volumeQuantity1L);

		wineBottle2.setId(2L);
		wineBottle2.setGrapeType(grapeType2);
		wineBottle2.setHarvestYear(2022);
		wineBottle2.setVolume(volumeQuantity075L);

		wineBottleStockDto1 = new WineBottleStockDto();
		wineBottleStockDto1.setId(1L);
		wineBottleStockDto1.setWineBottle(wineBottle1);
		wineBottleStockDto1.setCount(100);
		wineBottleStockDto1.setPrice(111.11);

		wineBottleStockDto2 = new WineBottleStockDto();
		wineBottleStockDto2.setId(2L);
		wineBottleStockDto2.setWineBottle(wineBottle2);
		wineBottleStockDto2.setCount(200);
		wineBottleStockDto2.setPrice(222.22);
	}

	@Test
	void testGetAllWineBottlesInStock() throws Exception{
		List<WineBottleStockDto> stock = Arrays.asList(wineBottleStockDto1, wineBottleStockDto2);

		when(wineBottleStockFacade.findAll()).thenReturn(stock);

		mockMvc.perform(get(API_PATH))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(2)))

				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].wineBottle.grapeType.name", is("Chardonnay")))
				.andExpect(jsonPath("$[0].wineBottle.harvestYear", is(2011)))
				.andExpect(jsonPath("$[0].wineBottle.volume.value", is(1.0)))
				.andExpect(jsonPath("$[0].wineBottle.volume.volumeUnit", is("LITER")))
				.andExpect(jsonPath("$[0].count", is(100)))
				.andExpect(jsonPath("$[0].price", is(111.11)))

				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].wineBottle.grapeType.name", is("Zweigeltrebe")))
				.andExpect(jsonPath("$[1].wineBottle.harvestYear", is(2022)))
				.andExpect(jsonPath("$[1].wineBottle.volume.value", is(0.75)))
				.andExpect(jsonPath("$[1].wineBottle.volume.volumeUnit", is("LITER")))
				.andExpect(jsonPath("$[1].count", is(200)))
				.andExpect(jsonPath("$[1].price", is(222.22)));
	}

	@Test
	void testGetWineBottleStockById_2() throws Exception {
		long id = wineBottleStockDto2.getId();

		when(wineBottleStockFacade.findById(id)).thenReturn(wineBottleStockDto2);

		mockMvc.perform(get(API_PATH + "/{id}", id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is((int) id)))
				.andExpect(jsonPath("wineBottle.grapeType.name", is("Zweigeltrebe")))
				.andExpect(jsonPath("wineBottle.harvestYear", is(2022)))
				.andExpect(jsonPath("wineBottle.volume.value", is(0.75)))
				.andExpect(jsonPath("wineBottle.volume.volumeUnit", is("LITER")))
				.andExpect(jsonPath("count", is(200)))
				.andExpect(jsonPath("price", is(222.22)));

		verify(wineBottleStockFacade).findById(id);
	}

	@Test
	void testGetWineBottleStockById_NotFound() throws Exception {
		long id = 1L;

		when(wineBottleStockFacade.findById(id)).thenThrow(new WineBottleStockWithIdNotFoundException(id));

		mockMvc.perform(get(API_PATH + "/{id}", id))
				.andExpect(status().isNotFound());
	}

	@Test
	void testGetWineBottleStockByWineBottle() throws Exception {
		long id = wineBottleStockDto2.getId();
		WineBottle wineBottle = wineBottleStockDto2.getWineBottle();

		when(wineBottleStockFacade.findByWineBottle(any(WineBottle.class))).thenReturn(wineBottleStockDto2);

		String requestBody = new ObjectMapper().writeValueAsString(wineBottle);

		mockMvc.perform(get(API_PATH + "/wineBottle")
					.contentType(MediaType.APPLICATION_JSON)
					.content(requestBody))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is((int) id)))
				.andExpect(jsonPath("wineBottle.id", is(wineBottle.getId().intValue())))
				.andExpect(jsonPath("wineBottle.grapeType.name", is(wineBottle.getGrapeType().getName())))
				.andExpect(jsonPath("wineBottle.grapeType.description", is(wineBottle.getGrapeType().getDescription())))
				.andExpect(jsonPath("wineBottle.grapeType.color", is(wineBottle.getGrapeType().getColor().toString())))
				.andExpect(jsonPath("wineBottle.grapeType.sweetness", is(wineBottle.getGrapeType().getSweetness().toString())))
				.andExpect(jsonPath("wineBottle.harvestYear", is(wineBottle.getHarvestYear())))
				.andExpect(jsonPath("wineBottle.volume.type", is("volume")))
				.andExpect(jsonPath("wineBottle.volume.value", is(wineBottle.getVolume().getValue())))
				.andExpect(jsonPath("wineBottle.volume.volumeUnit", is(wineBottle.getVolume().getVolumeUnit().toString())))
				.andExpect(jsonPath("count", is(200)))
				.andExpect(jsonPath("price", is(222.22)));
	}

	@Test
	void testGetWineBottleStockByWineBottle_NotFound() throws Exception {
		WineBottle wineBottle = wineBottleStockDto1.getWineBottle();

		String requestBody = new ObjectMapper().writeValueAsString(wineBottle);
		when(wineBottleStockFacade.findByWineBottle(any(WineBottle.class))).thenThrow(new WineBottleStockWithWineBottleNotFoundException(wineBottle));

		mockMvc.perform(get(API_PATH + "/wineBottle")
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isNotFound());

	}

	@Test
	void testCreateWineStockBottle_Created() throws Exception {
		when(wineBottleStockFacade.add(ArgumentMatchers.any())).thenReturn(wineBottleStockDto2);

		mockMvc.perform(post(API_PATH)
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(wineBottleStockDto2)))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(wineBottleStockDto2.getId().intValue())))
				.andExpect(jsonPath("wineBottle.grapeType.name", is(wineBottleStockDto2.getWineBottle().getGrapeType().getName())))
				.andExpect(jsonPath("wineBottle.harvestYear", is(wineBottleStockDto2.getWineBottle().getHarvestYear())))
				.andExpect(jsonPath("wineBottle.volume.value", is(wineBottleStockDto2.getWineBottle().getVolume().getValue())))
				.andExpect(jsonPath("wineBottle.volume.volumeUnit", is(wineBottleStockDto2.getWineBottle().getVolume().getVolumeUnit().toString())))
				.andExpect(jsonPath("count", is(wineBottleStockDto2.getCount())))
				.andExpect(jsonPath("price", is(wineBottleStockDto2.getPrice())));
	}

	@Test
	void testUpdateWineBottleStockByID_Updated() throws Exception {
		long id = wineBottleStockDto2.getId();
		wineBottleStockDto2.setCount(999);
		when(wineBottleStockFacade.updateById(ArgumentMatchers.any(long.class), ArgumentMatchers.any(WineBottleStockDto.class))).thenReturn(wineBottleStockDto2);

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleStockDto2);

		mockMvc.perform(put(API_PATH + "/{id}", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is((int) id)))
				.andExpect(jsonPath("wineBottle.grapeType.name", is("Zweigeltrebe")))
				.andExpect(jsonPath("wineBottle.harvestYear", is(2022)))
				.andExpect(jsonPath("wineBottle.volume.value", is(0.75)))
				.andExpect(jsonPath("wineBottle.volume.volumeUnit", is("LITER")))
				.andExpect(jsonPath("count", is(999)))
				.andExpect(jsonPath("price", is(222.22)));
	}

	@Test
	void testUpdateWineBottleStockByWineBottle_Updated() throws Exception {
		WineBottleUpdateWineBottleStockRequest wineBottleUpdateWineBottleStockRequest = new WineBottleUpdateWineBottleStockRequest();
		wineBottleUpdateWineBottleStockRequest.setWineBottle(wineBottleStockDto2.getWineBottle());
		wineBottleUpdateWineBottleStockRequest.setWineBottleStock(wineBottleStockDto2);

		when(wineBottleStockFacade.updateByWineBottle(ArgumentMatchers.any(WineBottle.class), ArgumentMatchers.any(WineBottleStockDto.class))).thenReturn(wineBottleStockDto2);

		wineBottleStockDto2.setCount(999);
		String requestBody = new ObjectMapper().writeValueAsString(wineBottleUpdateWineBottleStockRequest);

		mockMvc.perform(put(API_PATH + "/wineBottle")
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(wineBottleStockDto2.getId().intValue())))
				.andExpect(jsonPath("wineBottle.grapeType.name", is("Zweigeltrebe")))
				.andExpect(jsonPath("wineBottle.harvestYear", is(2022)))
				.andExpect(jsonPath("wineBottle.volume.value", is(0.75)))
				.andExpect(jsonPath("wineBottle.volume.volumeUnit", is("LITER")))
				.andExpect(jsonPath("count", is(999)))
				.andExpect(jsonPath("price", is(222.22)));
	}

	@Test
	void testUpdateWineBottleStockByWineBottle_NotFound() throws Exception {
		WineBottleUpdateWineBottleStockRequest wineBottleUpdateWineBottleStockRequest = new WineBottleUpdateWineBottleStockRequest();
		wineBottleUpdateWineBottleStockRequest.setWineBottle(wineBottleStockDto2.getWineBottle());
		wineBottleUpdateWineBottleStockRequest.setWineBottleStock(wineBottleStockDto2);

		when(wineBottleStockFacade.updateByWineBottle(ArgumentMatchers.any(WineBottle.class), ArgumentMatchers.any(WineBottleStockDto.class))).thenThrow(new WineBottleStockWithWineBottleNotFoundException(wineBottleStockDto2.getWineBottle()));

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleUpdateWineBottleStockRequest);

		mockMvc.perform(put(API_PATH + "/wineBottle")
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isNotFound());
	}

	@Test
	void testUpdateWineBottleStockById_NotFound() throws Exception {
		long id = wineBottleStockDto1.getId();
		wineBottleStockDto1.setCount(999);
		when(wineBottleStockFacade.updateById(ArgumentMatchers.any(long.class), ArgumentMatchers.any(WineBottleStockDto.class))).thenThrow(new WineBottleStockWithIdNotFoundException(id));

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleStockDto1);

		mockMvc.perform(put(API_PATH + "/{id}", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isNotFound());
	}

	@Test
	void testDeleteWineBottleStockById_Deleted() throws Exception {
		long id = 1L;
		when(wineBottleStockFacade.deleteById(id)).thenReturn(wineBottleStockDto1);

		mockMvc.perform(delete(API_PATH + "/{id}", id))
				.andExpect(status().isOk());
	}

	@Test
	void testDeleteWineBottleStockById_NotFound() throws Exception {
		long id = 1L;
		when(wineBottleStockFacade.deleteById(id)).thenThrow(new WineBottleStockWithIdNotFoundException(id));

		mockMvc.perform(delete(API_PATH + "/{id}", id))
				.andExpect(status().isNotFound());
	}

	@Test
	void testDeleteWineBottleStockByWineBottle_Deleted() throws Exception {

		when(wineBottleStockFacade.deleteByWineBottle(any(WineBottle.class))).thenReturn(wineBottleStockDto1);

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleStockDto1);

		mockMvc.perform(delete(API_PATH + "/wineBottle")
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isOk());
	}

	@Test
	void testDeleteWineBottleStockByWineBottle_NotFound() throws Exception {
		when(wineBottleStockFacade.deleteByWineBottle(any(WineBottle.class))).thenThrow(new WineBottleStockWithIdNotFoundException(wineBottle1.getId()));

		String requestBody = new ObjectMapper().writeValueAsString(wineBottleStockDto1);

		mockMvc.perform(delete(API_PATH + "/wineBottle")
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isNotFound());
	}

	@Test
	void addWineBottlesToStockById_Added() throws Exception {
		long id = wineBottleStockDto1.getId();
		int bottleCountAdd = 500;

		when(wineBottleStockFacade.findById(id)).thenReturn(wineBottleStockDto1);
		when(wineBottleStockFacade.addBottlesToStock(id, bottleCountAdd)).thenReturn(wineBottleStockDto1);

		wineBottleStockDto1.setCount(bottleCountAdd);
		String requestBody = new ObjectMapper().writeValueAsString(bottleCountAdd);

		mockMvc.perform(post(API_PATH + "/{id}/add-bottles", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(wineBottleStockDto1.getId().intValue())))
				.andExpect(jsonPath("wineBottle.grapeType.name", is("Chardonnay")))
				.andExpect(jsonPath("wineBottle.harvestYear", is(2011)))
				.andExpect(jsonPath("wineBottle.volume.value", is(1.0)))
				.andExpect(jsonPath("wineBottle.volume.volumeUnit", is("LITER")))
				.andExpect(jsonPath("count", is(500)))
				.andExpect(jsonPath("price", is(111.11)));
	}

	@Test
	void addWineBottlesToStockById_NotFound() throws Exception {
		long id = wineBottleStockDto1.getId();
		int bottleCountAdd = 500;

		when(wineBottleStockFacade.addBottlesToStock(any(Long.class), any(int.class))).thenThrow(new WineBottleWithIdNotFoundException(id));

		wineBottleStockDto1.setCount(bottleCountAdd);
		String requestBody = new ObjectMapper().writeValueAsString(bottleCountAdd);

		mockMvc.perform(post(API_PATH + "/{id}/add-bottles", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isNotFound());
	}

}