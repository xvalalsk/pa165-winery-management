package cz.muni.fi.pa165.winerymanagement.harvest.api;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeQuality;

import java.util.Date;

public class HarvestItemDto {
    private Long id;
    private GrapeTypeDto grape;
    private GrapeQuality quality;
    private double quantity;
    private Date date;
    private Long harvestId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeTypeDto getGrape() {
        return grape;
    }

    public void setGrape(GrapeTypeDto grape) {
        this.grape = grape;
    }

    public GrapeQuality getQuality() {
        return quality;
    }

    public void setQuality(GrapeQuality quality) {
        this.quality = quality;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getHarvestId() {
        return harvestId;
    }

    public void setHarvestId(Long harvestId) {
        this.harvestId = harvestId;
    }

    @Override
    public String toString() {
        return "HarvestItemDto{" +
                "id=" + id +
                ", grape=" + grape +
                ", quality=" + quality +
                ", quantity=" + quantity +
                ", date=" + date +
                ", harvestId=" + harvestId +
                '}';
    }
}
