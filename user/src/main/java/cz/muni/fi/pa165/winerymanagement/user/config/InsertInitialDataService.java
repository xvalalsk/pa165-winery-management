package cz.muni.fi.pa165.winerymanagement.user.config;

import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.ReviewRepository;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InsertInitialDataService {
    private final UserRepository userRepository;
    private final ReviewRepository reviewRepository;

    @Autowired
    public InsertInitialDataService(UserRepository userRepository, ReviewRepository reviewRepository) {
        this.userRepository = userRepository;
        this.reviewRepository = reviewRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        // create user
        var user = new User("https://oidc.muni.cz/oidc/", "492900@muni.cz");
        userRepository.save(user);

        // create two reviews for user
        var review1 = new Review(1, user, 5, "This wine is great!");
        var review2 = new Review(2, user, 3, "I like beer better.");

        reviewRepository.save(review1);
        reviewRepository.save(review2);
    }

    public void deleteAllData() {
        reviewRepository.deleteAll();
        userRepository.deleteAll();
    }
}
