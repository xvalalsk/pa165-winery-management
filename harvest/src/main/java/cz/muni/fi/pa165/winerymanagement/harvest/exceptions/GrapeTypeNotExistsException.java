package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;

/**
 * Thrown when Grape Type has not been found.
 */
public class GrapeTypeNotExistsException extends RuntimeException {
    public GrapeTypeNotExistsException() {
        super();
    }

    public GrapeTypeNotExistsException(Long id) {
        super("Grape type with id " + id + " does not exist");
    }
}
