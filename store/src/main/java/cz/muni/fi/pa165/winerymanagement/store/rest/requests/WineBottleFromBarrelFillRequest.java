package cz.muni.fi.pa165.winerymanagement.store.rest.requests;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        name = "WineBottleFromBarrelFillRequest",
        description = "Data transfer object representing an wine bottle stock creation from a barrel used in the bottling process",
        example = """
                    {
                        "grapeType": {
                            "name": "Chardonnay",
                            "description": "Chardonnay white sweet wine description",
                            "color": "WHITE",
                            "sweetness": "SWEET"
                        },
                        "bottleVolume": {
                            "type": "volume",
                            "value": 0.75,
                            "volumeUnit": "LITER"
                        },
                        "harvestYear": 2011,
                        "bottleCount": 10
                    }
                  """
)
public class WineBottleFromBarrelFillRequest {
    private GrapeType grapeType;
    private int harvestYear;
    private VolumeQuantity bottleVolume;
    private int bottleCount;

    public GrapeType getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeType grapeType) {
        this.grapeType = grapeType;
    }

    public int getBottleCount() {
        return bottleCount;
    }

    public void setBottleCount(int bottleCount) {
        this.bottleCount = bottleCount;
    }

    public int getHarvestYear() {
        return harvestYear;
    }

    public void setHarvestYear(int harvestYear) {
        this.harvestYear = harvestYear;
    }

    public VolumeQuantity getBottleVolume() {
        return bottleVolume;
    }

    public void setBottleVolume(VolumeQuantity bottleVolume) {
        this.bottleVolume = bottleVolume;
    }

    @Override
    public String toString() {
        return "WineBottleFromBarrelFillRequest{" +
                "grapeType=" + grapeType +
                ", bottleCount=" + bottleCount +
                ", harvestYear=" + harvestYear +
                ", bottleVolume=" + bottleVolume +
                '}';
    }
}
