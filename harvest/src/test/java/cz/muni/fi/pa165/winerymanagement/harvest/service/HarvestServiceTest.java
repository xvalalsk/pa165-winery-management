package cz.muni.fi.pa165.winerymanagement.harvest.service;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.*;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.GrapeTypeRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestItemRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestRepository;
import cz.muni.fi.pa165.winerymanagement.harvest.mappers.GrapeTypeMapper;
import cz.muni.fi.pa165.winerymanagement.harvest.mappers.HarvestItemMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class HarvestServiceTest {
    private HarvestService harvestService;
    private HarvestRepository harvestRepository;
    private HarvestItemRepository harvestItemRepository;
    private GrapeTypeRepository grapeTypeRepository;
    private GrapeTypeMapper grapeTypeMapper;
    private HarvestItemMapper harvestItemMapper;
    private GrapeType grapeType;
    private List<Harvest> harvests;
    private List<HarvestItem> harvestItems;

    @BeforeEach
    void setUp() {
        grapeType = new GrapeType();
        grapeType.setName("Merlot");
        grapeType.setDescription("Very good grape");
        grapeType.setColor(Color.ROSE);
        grapeType.setSweetness(Sweetness.SWEET);

        var oldHarvest = new Harvest();
        oldHarvest.setId(1L);
        oldHarvest.setYear(2019);

        var item = new HarvestItem();
        item.setId(1L);
        item.setHarvest(oldHarvest);
        item.setDate(new Date());
        item.setGrape(grapeType);
        item.setQuality(GrapeQuality.EXCELLENT);

        this.harvests = new ArrayList<>(List.of(oldHarvest));
        this.harvestItems = List.of(item);

        harvestRepository = mock(HarvestRepository.class);
        harvestItemRepository = mock(HarvestItemRepository.class);
        grapeTypeRepository = mock(GrapeTypeRepository.class);

        harvestService = new HarvestService(harvestRepository, harvestItemRepository, grapeTypeRepository);
    }

    @Test
    void findAllHarvests() {
        when(harvestRepository.findAll()).thenReturn(this.harvests);
        assertThat(harvestService.findAllHarvests()).isEqualTo(this.harvests);

        verify(harvestRepository, times(1)).findAll();
    }

    @Test
    void findAllHarvestItems() {
        when(harvestItemRepository.findAll()).thenReturn(this.harvestItems);
        assertThat(harvestService.findAllHarvestItems()).isEqualTo(this.harvestItems);

        verify(harvestItemRepository, times(1)).findAll();
    }

    @Test
    void addHarvest() {
        var newHarvest = new Harvest();
        newHarvest.setId(2L);
        newHarvest.setYear(2020);

        harvestService.addHarvest(newHarvest);

        verify(harvestRepository, times(1)).save(newHarvest);
    }

    @Test
    void updateHarvest() {
        var newHarvest = new Harvest();
        newHarvest.setId(1L);
        newHarvest.setYear(2020);

        when(harvestRepository.findById(1L)).thenReturn(Optional.ofNullable(this.harvests.get(0)));
        when(harvestRepository.save(newHarvest)).thenReturn(newHarvest);
        harvestService.updateHarvest(1L, newHarvest);

        verify(harvestRepository, times(1)).save(newHarvest);
    }

    @Test
    void deleteHarvestById() {
        when(harvestRepository.findById(1L)).thenReturn(Optional.ofNullable(this.harvests.get(0)));
        harvestService.deleteHarvestById(1L);

        verify(harvestRepository, times(1)).deleteById(1L);
    }
}
