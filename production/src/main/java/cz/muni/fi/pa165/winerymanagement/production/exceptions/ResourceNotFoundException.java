package cz.muni.fi.pa165.winerymanagement.production.exceptions;

public class ResourceNotFoundException extends ProductionException {

    public ResourceNotFoundException() {
    }

    @Override
    public String getMessage() {
        return "Resource not found";
    }
}
