package cz.muni.fi.pa165.winerymanagement.core.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a grape type for a Barrel, Grape Container and Production Item.
 */
@Embeddable
@Schema(name = "GrapeType", description = "A class representing a grape type with its name, description, color, and sweetness")
public class GrapeType implements Serializable {
    @Schema(description = "The name of the grape type", example = "Cabernet Sauvignon")
    private String name;

    @Schema(description = "A brief description of the grape type", example = "A popular red grape variety")
    private String description;

    @Schema(description = "The color of the grape type", example = "RED")
    private Color color;

    @Schema(description = "The sweetness of the grape type", example = "DRY")
    private Sweetness sweetness;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Sweetness getSweetness() {
        return sweetness;
    }

    public void setSweetness(Sweetness sweetness) {
        this.sweetness = sweetness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GrapeType grapeType)) return false;
        return Objects.equals(getName(), grapeType.getName()) && Objects.equals(getDescription(), grapeType.getDescription()) && getColor() == grapeType.getColor() && getSweetness() == grapeType.getSweetness();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getColor(), getSweetness());
    }

    @Override
    public String toString() {
        return "GrapeType{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", color=" + color +
                ", sweetness=" + sweetness +
                '}';
    }

    public enum Color {
        WHITE("white"),
        RED("red"),

        ROSE("rose");

        private final String wineColor;

        Color(String wineColor) {
            this.wineColor = wineColor;
        }

        public String getColor() {
            return wineColor;
        }
    }

    public enum Sweetness {
        SWEET("sweet"),
        SEMISWEET("semisweet"),

        DRY("dry");

        private final String wineSweetness;

        Sweetness(String wineSweetness) {
            this.wineSweetness = wineSweetness;
        }

        public String getSweetness() {
            return wineSweetness;
        }
    }
}
