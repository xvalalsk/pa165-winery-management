package cz.muni.fi.pa165.winerymanagement.harvest.data.models;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="grape_types")
public class GrapeType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(length = 300)
    private String description;

    private Color color;

    private Sweetness sweetness;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "grape", cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private Set<HarvestItem> harvestItems;

    public GrapeType() {}

    public GrapeType(String name, String description, Color color, Sweetness sweetness) {
        this.name = name;
        this.description = description;
        this.color = color;
        this.sweetness = sweetness;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Sweetness getSweetness() {
        return sweetness;
    }

    public void setSweetness(Sweetness sweetness) {
        this.sweetness = sweetness;
    }

    public Set<HarvestItem> getHarvestItems() {
        return harvestItems;
    }

    public void setHarvestItems(Set<HarvestItem> harvestItems) {
        this.harvestItems = harvestItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrapeType grapeType = (GrapeType) o;
        return Objects.equals(id, grapeType.id) && Objects.equals(name, grapeType.name) && Objects.equals(description, grapeType.description) && color == grapeType.color && sweetness == grapeType.sweetness;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, color, sweetness);
    }

    @Override
    public String toString() {
        return "GrapeType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", color=" + color +
                ", sweetness=" + sweetness +
                '}';
    }
}
