package cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock;

import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.ResourceAlreadyExistsException;

import java.util.ResourceBundle;

public class WineBottleStockAlreadyExistsException extends ResourceAlreadyExistsException {

	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
	private final WineBottle wineBottle;

	public WineBottleStockAlreadyExistsException(WineBottle wineBottle) {
		this.wineBottle = wineBottle;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, wineBottle.toString());
	}
}