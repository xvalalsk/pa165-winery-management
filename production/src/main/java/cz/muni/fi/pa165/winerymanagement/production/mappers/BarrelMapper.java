package cz.muni.fi.pa165.winerymanagement.production.mappers;

import cz.muni.fi.pa165.winerymanagement.production.api.BarrelDto;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Barrel;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BarrelMapper {
    BarrelDto mapToDto(Barrel barrel);

    List<BarrelDto> mapToList(List<Barrel> barrels);

    Barrel mapFromDto(BarrelDto barrelDto);
}
