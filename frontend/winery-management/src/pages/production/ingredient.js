import {useEffect, useState} from 'react';
import {NavBar} from "@/pages";
import Cookies from "js-cookie";

const Ingredients = () => {
    const [token, setToken] = useState(null);
    const [ingredients, setIngredients] = useState([]);
    const [error, setError] = useState('');
    const [name, setName] = useState('');
    const [quantityType, setQuantityType] = useState('volume');
    const [quantityValue, setQuantityValue] = useState('');
    const [quantityUnit, setQuantityUnit] = useState('LITER');

    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        fetchIngredients();
    }, []);

    const fetchIngredients = () => {
        const token = Cookies.get("bearerToken");
        fetch('/api/v1/production/ingredients', {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => setIngredients(data))
            .catch((error) => setError(error.message + ". Is the Production module is running?" ));
    };

    const deleteIngredient = (id) => {
        fetch(`/api/v1/production/ingredients/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
            },
        })
            .then(() => {
                setIngredients(ingredients.filter((ingredient) => ingredient.id !== id));
            })
            .catch((error) => setError(error.message));
    };

    const handleInputChange = (event) => {
        const {name, value} = event.target;

        switch (name) {
            case 'name':
                setName(value);
                break;
            case 'quantityType':
                setQuantityType(value);
                if (value === "volume") {
                    setQuantityUnit("LITER");
                } else {
                    setQuantityUnit("KILOGRAM");
                }
                break;
            case 'quantityValue':
                setQuantityValue(value);
                break;
            case 'quantityUnit':
                setQuantityUnit(value);
                break;
            default:
                break;
        }
    };

    const submitForm = (event) => {
        event.preventDefault();

        if (!name || !quantityValue) {
            setError('Please fill in all required fields.');
            return;
        }

        if (ingredients.some((ingredient) => ingredient.name.toLowerCase() === name.toLowerCase())) {
            setError('Ingredient with the same name already exists.');
            return;
        }

        const newIngredient = {
            name,
            quantity: {
                type: quantityType,
                value: parseFloat(quantityValue),
                [quantityType === 'volume' ? 'volumeUnit' : 'weightUnit']: quantityUnit,
            },
        };

        console.log("newIngredient", newIngredient)
        fetch('/api/v1/production/ingredients', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify(newIngredient),
        })
            .then((response) => response.json())
            .then((data) => {
                setIngredients([...ingredients, data]);
                setName('');
                setQuantityValue('');
                setError('');
            })
            .catch((error) => setError(error.message));
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar/>
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Ingredients</h1>
                <div>
                    <div className="mt-4">
                        <table className="table table-striped table-hover table-bordered">
                            <thead className="bg-secondary bg-gradient">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {ingredients.map((ingredient) => (
                                <tr key={ingredient.id}>
                                    <td>{ingredient.name}</td>
                                    <td>{`${ingredient?.quantity?.value} ${ingredient?.quantity?.symbol}`}</td>
                                    <td>
                                        <button
                                            className="btn btn-danger"
                                            onClick={() => deleteIngredient(ingredient.id)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>

                        {/* Add the form here */}
                        <div className="card mt-4">
                            <div className="card-body">
                                <h5 className="card-title">Add an ingredient</h5>
                                <form onSubmit={submitForm}>
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">
                                            Name
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="name"
                                            name="name"
                                            value={name}
                                            onChange={handleInputChange}
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="quantityType" className="form-label">
                                            Quantity Type
                                        </label>
                                        <select
                                            className="form-select"
                                            id="quantityType"
                                            name="quantityType"
                                            value={quantityType}
                                            onChange={handleInputChange}
                                        >
                                            <option value="volume">Volume</option>
                                            <option value="weight">Weight</option>
                                        </select>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label htmlFor="quantityValue" className="form-label">
                                                Quantity Value
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                id="quantityValue"
                                                name="quantityValue"
                                                value={quantityValue}
                                                onChange={handleInputChange}
                                                required
                                            />
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label htmlFor="quantityUnit" className="form-label">
                                                Quantity Unit
                                            </label>
                                            <select
                                                className="form-select"
                                                id="quantityUnit"
                                                name="quantityUnit"
                                                value={quantityUnit}
                                                onChange={handleInputChange}
                                            >
                                                {quantityType === "volume" ? (
                                                    <>
                                                        <option value="LITER">LITER</option>
                                                        <option value="MILLILITER">MILLILITER</option>
                                                    </>
                                                ) : (
                                                    <>
                                                        <option value="KILOGRAM">KILOGRAM</option>
                                                        <option value="GRAM">GRAM</option>
                                                    </>
                                                )}
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-primary">
                                        Add Ingredient
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {error && (
                    <div className="alert alert-danger mt-3" role="alert">
                        {error}
                    </div>
                )}
            </div>
        </>
    );
};

export default Ingredients;