package cz.muni.fi.pa165.winerymanagement.store.data.repository;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.StoreApplication;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottleStock;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = StoreApplication.class)
@Transactional
public class WineBottleStockRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private WineBottleStockRepository wineBottleStockRepository;

    private WineBottle wineBottle;

    @BeforeEach
    public void setUp() {
        GrapeType grapeType1 = new GrapeType();
        grapeType1.setName("Grape1");
        grapeType1.setDescription("Description1");
        grapeType1.setColor(GrapeType.Color.RED);
        grapeType1.setSweetness(GrapeType.Sweetness.DRY);


        wineBottle = new WineBottle();
        wineBottle.setGrapeType(grapeType1);
        wineBottle.setHarvestYear(2018);
        wineBottle.setVolume(new VolumeQuantity(750, VolumeQuantity.VolumeUnit.LITER));
        entityManager.persist(wineBottle);
    }

    @Test
    void testFindByWineBottle() {
        WineBottleStock wineBottleStock = new WineBottleStock();
        wineBottleStock.setWineBottle(wineBottle);
        wineBottleStock.setCount(10);
        wineBottleStock.setPrice(200);
        entityManager.persist(wineBottleStock);

        WineBottleStock found = wineBottleStockRepository.findByWineBottle(wineBottle);
        assertThat(found).isEqualTo(wineBottleStock);
    }

    @Test
    void testFindAll() {
        WineBottleStock wineBottleStock1 = new WineBottleStock();
        wineBottleStock1.setWineBottle(wineBottle);
        wineBottleStock1.setCount(10);
        wineBottleStock1.setPrice(200);
        entityManager.persist(wineBottleStock1);

        WineBottleStock wineBottleStock2 = new WineBottleStock();
        wineBottleStock2.setWineBottle(wineBottle);
        wineBottleStock2.setCount(20);
        wineBottleStock2.setPrice(400);
        entityManager.persist(wineBottleStock2);

        List<WineBottleStock> wineBottleStocks = wineBottleStockRepository.findAll();
        assertThat(wineBottleStocks).contains(wineBottleStock1, wineBottleStock2);
    }

    @Test
    void testSave() {
        WineBottleStock wineBottleStock = new WineBottleStock();
        wineBottleStock.setWineBottle(wineBottle);
        wineBottleStock.setCount(10);
        wineBottleStock.setPrice(200);

        WineBottleStock saved = wineBottleStockRepository.save(wineBottleStock);
        assertThat(saved).isEqualTo(wineBottleStock);
    }

    @Test
    void testDelete() {
        WineBottleStock wineBottleStock = new WineBottleStock();
        wineBottleStock.setWineBottle(wineBottle);
        wineBottleStock.setCount(10);
        wineBottleStock.setPrice(200);
        entityManager.persist(wineBottleStock);

        wineBottleStockRepository.delete(wineBottleStock);
        WineBottleStock found = entityManager.find(WineBottleStock.class, wineBottleStock.getId());
        assertThat(found).isNull();
    }
}