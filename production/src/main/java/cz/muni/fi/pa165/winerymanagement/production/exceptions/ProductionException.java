package cz.muni.fi.pa165.winerymanagement.production.exceptions;

public class ProductionException extends RuntimeException {

    public ProductionException() {
    }

    @Override
    public String getMessage() {
        return "Something went wrong";
    }
}
