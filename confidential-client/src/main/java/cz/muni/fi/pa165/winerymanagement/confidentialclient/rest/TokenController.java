package cz.muni.fi.pa165.winerymanagement.confidentialclient.rest;

import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class TokenController {
    @GetMapping("/")
    public String index(@RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        return "You have been successfully authenticated! You may close this page";
    }

    @GetMapping("/token")
    public String getToken( @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        return oauth2Client.getAccessToken().getTokenValue();
    }

    @GetMapping("/email")
    public String getPrincipal( @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        return oauth2Client.getPrincipalName();
    }
}
