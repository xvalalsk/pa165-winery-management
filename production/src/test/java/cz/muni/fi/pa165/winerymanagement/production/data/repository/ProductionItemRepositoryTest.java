package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.production.ProductionApplication;
import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.GrapeContainerInfo;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ProductionApplication.class)
@Transactional
public class ProductionItemRepositoryTest {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private ProductionItemRepository productionItemRepository;

	private ProductionItem productionItem;
	private GrapeType grapeType;
	private GrapeContainerInfo grapeContainerInfo;

	@BeforeEach
	public void setUp() {
		grapeType = new GrapeType();
		grapeType.setName("Test Grape");
		grapeType.setDescription("Test Description");
		grapeType.setColor(GrapeType.Color.RED);
		grapeType.setSweetness(GrapeType.Sweetness.DRY);

		grapeContainerInfo = new GrapeContainerInfo();
		grapeContainerInfo.setGrapeType(grapeType);
		grapeContainerInfo.setHarvestYear(2020);

		productionItem = new ProductionItem();
		productionItem.setGrape(grapeContainerInfo);
		productionItem.setYear(2020);

		entityManager.persist(productionItem);
		entityManager.flush();
	}

	@Test
	void findById_shouldReturnProductionItem() {
		Optional<ProductionItem> found = productionItemRepository.findById(productionItem.getId());

		assertThat(found).isPresent();
		assertThat(found.get()).isEqualTo(productionItem);
	}

	@Test
	void findByGrape_GrapeTypeAndYear_shouldReturnProductionItem() {
		ProductionItem found = productionItemRepository.findByGrape_GrapeTypeAndYear(grapeType, 2020);

		assertThat(found).isEqualTo(productionItem);
	}

	@Test
	void findByGrape_GrapeTypeAndYear_shouldReturnNull_whenGrapeTypeNotFound() {
		GrapeType nonExistentGrapeType = new GrapeType();
		nonExistentGrapeType.setName("Non-existent Grape");
		nonExistentGrapeType.setDescription("Non-existent Description");
		nonExistentGrapeType.setColor(GrapeType.Color.WHITE);
		nonExistentGrapeType.setSweetness(GrapeType.Sweetness.SEMISWEET);

		ProductionItem found = productionItemRepository.findByGrape_GrapeTypeAndYear(nonExistentGrapeType, 2020);

		assertThat(found).isNull();
	}

	@Test
	void findByGrape_GrapeTypeAndYear_shouldReturnNull_whenYearNotFound() {
		ProductionItem found = productionItemRepository.findByGrape_GrapeTypeAndYear(grapeType, 2021);

		assertThat(found).isNull();
	}

	@Test
	void save_shouldPersistProductionItem() {
		ProductionItem newProductionItem = new ProductionItem();
		newProductionItem.setGrape(grapeContainerInfo);
		newProductionItem.setYear(2021);

		ProductionItem saved = productionItemRepository.save(newProductionItem);

		assertThat(saved).isEqualTo(newProductionItem);
		assertThat(saved.getId()).isNotNull();
	}

	@Test
	void deleteById_shouldRemoveProductionItem() {
		productionItemRepository.deleteById(productionItem.getId());

		Optional<ProductionItem> found = productionItemRepository.findById(productionItem.getId());

		assertThat(found).isNotPresent();
	}
}