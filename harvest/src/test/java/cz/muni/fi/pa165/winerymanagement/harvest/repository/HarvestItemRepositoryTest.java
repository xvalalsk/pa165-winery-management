package cz.muni.fi.pa165.winerymanagement.harvest.repository;

import cz.muni.fi.pa165.winerymanagement.harvest.HarvestApplication;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.*;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestItemRepository;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = HarvestApplication.class)
@Transactional
public class HarvestItemRepositoryTest {
    @Autowired
    private HarvestItemRepository harvestItemRepository;

    @Autowired
    private EntityManager em;

    private HarvestItem harvestItem;

    private Harvest harvest;

    private GrapeType grapeType;

    @BeforeEach
    void setUp() {
        grapeType = new GrapeType();
        grapeType.setName("Merlot");
        grapeType.setDescription("Very good grape");
        grapeType.setColor(Color.ROSE);
        grapeType.setSweetness(Sweetness.SWEET);

        em.persist(grapeType);

        harvest = new Harvest(2019);
        em.persist(harvest);

        harvestItem = new HarvestItem();
        harvestItem.setHarvest(harvest);
        harvestItem.setDate(new Date());
        harvestItem.setGrape(grapeType);
        harvestItem.setQuality(GrapeQuality.EXCELLENT);

        em.persist(harvestItem);
    }

    @Test
    void findAllItemsForHarvest() {
        assertThat(this.harvestItemRepository.findHarvestItemsByHarvestId(this.harvest.getId()))
                .hasSize(1)
                .isEqualTo(new ArrayList<>(List.of(harvestItem)));
    }

    @Test
    void findAllItems() {
        assertThat(this.harvestItemRepository.findAll())
                .hasSize(3).contains(harvestItem);
    }

    @Test
    void findExistingHarvestItemById() {
        var harvestItem = this.harvestItemRepository.findById(this.harvestItem.getId());
        assertThat(harvestItem).isPresent();
        assertThat(harvestItem.get()).isEqualTo(this.harvestItem);
    }

    @Test
    void findNonExistingHarvestItem() {
        assertThat(this.harvestItemRepository.findById(-1L)).isEmpty();
    }

    @Test
    void addHarvestItem() {
        var newItem = new HarvestItem();
        newItem.setHarvest(this.harvest);
        newItem.setDate(new Date());
        newItem.setGrape(this.grapeType);
        newItem.setQuality(GrapeQuality.POOR);

        this.harvestItemRepository.save(newItem);
        assertThat(this.harvestItemRepository.findHarvestItemsByHarvestId(this.harvest.getId())).hasSize(2).contains(newItem);
    }

    @Test
    void updateHarvestItem() {
        var newItem = new HarvestItem();
        newItem.setId(this.harvestItem.getId());
        newItem.setHarvest(this.harvest);
        newItem.setDate(new Date());
        newItem.setGrape(grapeType);
        newItem.setQuality(GrapeQuality.POOR);

        this.harvestItemRepository.save(newItem);
        assertThat(this.harvestItemRepository.findHarvestItemsByHarvestId(this.harvest.getId())).hasSize(1).contains(newItem);
    }

    @Test
    void deleteHarvestItemById() {
        this.harvestItemRepository.deleteById(this.harvestItem.getId());
        assertThat(this.harvestItemRepository.findHarvestItemsByHarvestId(this.harvest.getId())).isEmpty();
    }

    @Test
    void deleteNonExistingHarvestItemById() {
        this.harvestItemRepository.deleteById(-1L);
        assertEquals(3, this.harvestItemRepository.findAll().size());
    }
}
