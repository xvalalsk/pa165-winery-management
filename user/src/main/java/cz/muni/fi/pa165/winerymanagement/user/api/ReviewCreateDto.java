package cz.muni.fi.pa165.winerymanagement.user.api;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        description = "Review create DTO",
        example = """
        {
          "wineBottleId": 1,
          "userId": 1,
          "rating": 5,
          "comment": "This wine is great!"
        }
        """
)
public class ReviewCreateDto {
    private Long wineBottleId;
    private Long userId;
    private int rating;
    private String comment;

    public long getWineBottleId() {
        return wineBottleId;
    }

    public void setWineBottleId(long wineBottleId) {
        this.wineBottleId = wineBottleId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ReviewCreateDto{" +
                "wineBottleId=" + wineBottleId +
                ", userId=" + userId +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                '}';
    }
}
