package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;

/**
 * Thrown when Grape Type already exists.
 */
public class GrapeTypeAlreadyExistsException extends RuntimeException {
    public GrapeTypeAlreadyExistsException() {
        super();
    }

    public GrapeTypeAlreadyExistsException(GrapeType type) {
        super(type + " already exists");
    }
}
