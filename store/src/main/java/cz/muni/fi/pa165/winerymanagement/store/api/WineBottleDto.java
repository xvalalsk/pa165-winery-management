package cz.muni.fi.pa165.winerymanagement.store.api;

import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Represents a data transfer object for a Wine Bottle.
 */
@Schema(
		name = "WineBottleDto",
		description = "Data transfer object representing a wine bottle to be sold in store",
		example = """
                  {
                    "grapeType": {
                      "name": "Chardonnay",
                      "description": "Chardonnay white sweet wine description",
                      "color": "WHITE",
                      "sweetness": "SWEET"
                    },
                    "harvestYear": 2020,
                    "volume": {
                      "type": "volume",
                      "value": 0.75,
                      "volumeUnit": "LITER"
                    }
                  }
                  """
)
public class WineBottleDto {
	private Long id;
	private GrapeType grapeType;
	private int harvestYear;
	private VolumeQuantity volume;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrapeType getGrapeType() {
		return grapeType;
	}

	public void setGrapeType(GrapeType grapeType) {
		this.grapeType = grapeType;
	}

	public int getHarvestYear() {
		return harvestYear;
	}

	public void setHarvestYear(int harvestYear) {
		this.harvestYear = harvestYear;
	}

	public VolumeQuantity getVolume() {
		return volume;
	}

	public void setVolume(VolumeQuantity volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "WineBottleDto{" +
				"id=" + id +
				", grapeType=" + grapeType +
				", harvestYear=" + harvestYear +
				", volume=" + volume +
				'}';
	}
}
