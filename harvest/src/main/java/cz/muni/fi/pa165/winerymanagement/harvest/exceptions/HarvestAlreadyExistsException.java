package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;

/**
 * Thrown when the Harvest already exists.
 */
public class HarvestAlreadyExistsException extends RuntimeException {
    public HarvestAlreadyExistsException() {
        super();
    }

    public HarvestAlreadyExistsException(Harvest harvest) {
        super(harvest + " already exists");
    }
}
