package cz.muni.fi.pa165.winerymanagement.user.rest;

import cz.muni.fi.pa165.winerymanagement.user.api.ReviewCreateDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewDto;
import cz.muni.fi.pa165.winerymanagement.user.api.ReviewUpdateDto;
import cz.muni.fi.pa165.winerymanagement.user.exceptions.InvalidReviewException;
import cz.muni.fi.pa165.winerymanagement.user.facade.ReviewFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1/reviews")
public class ReviewController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewController.class);
    private final ReviewFacade reviewFacade;

    @Autowired
    public ReviewController(ReviewFacade reviewFacade) {
        this.reviewFacade = reviewFacade;
    }

    @PostMapping("")
    @Operation(
            summary = "Add review",
            tags = "Review"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Review added successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid review body")
    })
    public ResponseEntity<ReviewDto> addReview(@RequestBody ReviewCreateDto reviewCreateDto) {
        try {
            LOGGER.info(reviewCreateDto.toString());
            return ResponseEntity.status(HttpStatus.CREATED).body(this.reviewFacade.addReview(reviewCreateDto));
        } catch (InvalidReviewException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Update review",
            tags = "Review"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Review updated successfully"),
            @ApiResponse(responseCode = "400", description = "Problem updating review")
    })
    public ReviewDto updateReview(
            @Parameter(name="id", required = true, description = "ID of changed review") @PathVariable("id") Long id,
            @RequestBody ReviewUpdateDto reviewDto) {
        try {
            reviewDto.setId(id);
            return this.reviewFacade.updateReview(reviewDto);
        } catch (InvalidReviewException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Delete review",
            tags = "Review"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Review deleted successfully"),
    })
    public void deleteReview(
            @Parameter(name="id", required = true, description = "ID of deleted review") @PathVariable("id") Long id) {
        this.reviewFacade.deleteReview(id);
    }

    @GetMapping("/bottle/{id}")
    @Operation(
            summary = "Get all reviews for bottle",
            tags = "Review"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reviews found"),
    })
    public Iterable<ReviewDto> getAllReviewsForBottle(
            @Parameter(name="id", required = true, description = "ID of bottle") @PathVariable("id") Long id) {
        return this.reviewFacade.getWineBottleReviews(id);
    }

    @GetMapping("/user/{id}")
    @Operation(
            summary = "Get all reviews for user",
            tags = "Review"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reviews found"),
    })
    public Iterable<ReviewDto> getAllReviewsForUser(
            @Parameter(name="id", required = true, description = "ID of user") @PathVariable("id") Long id) {
        return this.reviewFacade.getUserReviews(id);
    }
}
