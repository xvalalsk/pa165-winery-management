package cz.muni.fi.pa165.winerymanagement.core.service;

import cz.muni.fi.pa165.winerymanagement.core.model.ModuleEnum;

public class ModuleUrlService {
    private ModuleUrlService() {}
    public static String buildModuleUrl(ModuleEnum module, String path) {
        boolean runningInDocker = Boolean.parseBoolean(System.getenv("RUNNING_IN_DOCKER"));
        String port = System.getenv(module.getPortName());
        if (port == null) {
            port = String.valueOf(module.getDefaultPort());
        }

        String host = runningInDocker ? module.getDockerName() : "localhost";

        return "http://" + host + ":" + port + "/" + path;
    }
}
