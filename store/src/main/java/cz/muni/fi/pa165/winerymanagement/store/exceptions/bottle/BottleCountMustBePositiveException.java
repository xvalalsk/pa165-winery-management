package cz.muni.fi.pa165.winerymanagement.store.exceptions.bottle;

import cz.muni.fi.pa165.winerymanagement.store.exceptions.StoreException;

import java.util.ResourceBundle;

public class BottleCountMustBePositiveException extends StoreException {
	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
	private final int count;

	public BottleCountMustBePositiveException(int count) {
		this.count = count;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, count);
	}
}