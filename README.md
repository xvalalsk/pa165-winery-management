# PA165 - Winery Management

### Running the project:
Project contains 4 main runnable modules and one module, which contains helper functionality.

Runnable modules are in `harvest`, `store`, `production` and `user` folders.

* **Harvest** (port 8081): functionality regarding grape harvest.
* **Production** (port 8082): contains functionality regarding grape storage and wine production.
* **Store** (port 8083): contains functionality regarding bottles and the number of things in stock.
* **User** (port 8084): contains functionality regarding users, their roles and reviews.


In addition to the runnable modules, the project also includes frontend module in the `frontend/winery-management` folder.
* **Frontend** (port 3000): it provides the user interface for managing winery-related data

### Seeding and clearing data:
Upon starting up, the modules come with sample data.
To clear the data from each module, you must send a POST request with a bearer token obtained from http://localhost:8080/token to the following paths:
- http://localhost:8081/api/v1/harvest/clear
- http://localhost:8082/api/v1/production/clear
- http://localhost:8083/api/v1/store/clear
- http://localhost:8084/api/v1/user/clear

To fill the modules with sample data, you can send a POST request with a bearer token to the following paths:
- http://localhost:8081/api/v1/harvest/seed
- http://localhost:8082/api/v1/production/seed
- http://localhost:8083/api/v1/store/seed
- http://localhost:8084/api/v1/user/seed

Note: Use at your own risk

#### Docker:

Make sure you have `Docker` and `Docker Compose` installed.

* Run `mvn clean install` in the root directory of the project.
* Run `docker-compose up` (or `docker-compose up --build` if you want to rebuild) in the root directory of the project.

If you are running application in Docker you can configure ports for modules from `.env` file.

#### Locally:
* Run `mvn clean install` in the root directory of the project.
* Run `mvn spring-boot:run` in the directory of the module (harvest, production, store and user folders) - alternativelly you can run `mvn spring-boot:run -T 6` in the root folder (this runs all modules).


* Run `npm install` in **frontend/winery-management** folder to install frontend (Note: [Node.js](https://nodejs.org/en) must be installed)
* Run `npm run dev` in **frontend/winery-management** folder start the frontend in development mode


Info: If you want to inspect data in database, you can use `http://localhost:{module-port}/h2-console` (username and password are in module config).

### Metrics
Both prometheus and grafana expect the project running in docker first.
#### Prometheus
Create shared network:
```bash
docker network create metrics
```
Run prometheus in docker:
```bash
docker run --rm --name prometheus -p 9090:9090 --network metrics \
-v ./prometheus.yml:/etc/prometheus/prometheus.yml:Z prom/prometheus:v2.43.0 \
--config.file=/etc/prometheus/prometheus.yml

```
Open prometheus in the browser: http://localhost:9090, navigate to targets under Status in the UI header bar
to observe the modules are running.
![Alt text](img/prometheus.png?raw=true "Title")

#### Grafana
Make sure the docker shared network and prometheus are running and available. Then run Grafana:
```bash
docker run --rm --name grafana --network metrics -p 3001:3000 grafana/grafana:9.1.7
```
1. Open grafana in the browser: http://localhost:3001
2. Login, the default **username:password** is **admin:admin**
3. Add a Datasource. Choose http://prometheus:9090

   ![Alt text](img/grafana-datasource.png?raw=true "Title")
   ![Alt text](img/grafana-add-datasource.png?raw=true "Title")
   ![Alt text](img/grafana-add-datasource-prometheus.png?raw=true "Title")
   ![Alt text](img/grafana-add-datasource-prometheus-setup.png?raw=true "Title")
   ![Alt text](img/grafana-save-and-test.png?raw=true "Title")

4. Navigate to Dashboard, choose import.

   ![Alt text](img/grafana-dashboard.png?raw=true "Title")

   ![Alt text](img/grafana-add-dashboard-new.png?raw=true "Title")

5. Let's import dashboard provided by grafana.com (https://grafana.com/grafana/dashboards/4701)
6. In the "Import via grafana.com" write "4701", click on "Load"

   ![Alt text](img/grafana-load.png?raw=true "Title")

7. Choose the prometheus in next step, click on import.

   ![Alt text](img/grafana-load-choose-prometheus.png?raw=true "Title")

8. Observe the dashboard.

### Team members:

- Bc. Šárka Palkovičová
- Bc. Lucia Baginová
- Bc. Martin Haderka
- Bc. Adam Valalský

### Project description:
Create an information system that will allow a person to manage its own winery in a better way.  The main user role can manage the yearly wine harvest, annotating the quality, quantity of different types of grapes. Then he will manage the wines that can be produced depending on the type of grapes available and other ingredients. The user can also manage the list of wine bottles sold and stocked and available to buyers. Buyers can consult the system to buy quantities of wine bottles, also leaving feedback about the quality of the wine they have bought.

### Diagrams:
#### Use case diagram:
![Alt text](img/diagrams/use-case.png?raw=true "Title")
[Use case diagram](img/diagrams/use-case.png)

#### Class diagram:
![Alt text](img/diagrams/class.png?raw=true "Title")
[Class diagram](img/diagrams/class.png)