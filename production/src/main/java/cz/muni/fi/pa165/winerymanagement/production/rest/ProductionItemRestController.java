package cz.muni.fi.pa165.winerymanagement.production.rest;

import cz.muni.fi.pa165.winerymanagement.production.api.ProductionItemDto;
import cz.muni.fi.pa165.winerymanagement.production.facade.ProductionItemFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/production")
@Tag(name = "Production Item", description = "API for managing production Items")
@CrossOrigin(origins = "*")
public class ProductionItemRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductionItemRestController.class);
    private final ProductionItemFacade productionItemFacade;

    @Autowired
    public ProductionItemRestController(ProductionItemFacade productionItemFacade) {
        this.productionItemFacade = productionItemFacade;
    }

    @Operation(summary = "Get a list of all production items", description = "Returns a list of all production items.")
    @ApiResponse(responseCode = "200", description = "List of all production items returned successfully.")
    @GetMapping("/production-items")
    public ResponseEntity<List<ProductionItemDto>> getAllProductionItems() {
        LOGGER.info("Get all production items.");
        return ResponseEntity.ok(productionItemFacade.findAll());
    }

    @Operation(summary = "Get an production item by ID", description = "Returns an production item with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Production item with the specified ID returned successfully."),
            @ApiResponse(responseCode = "404", description = "Production item with the specified ID was not found.")
    })
    @GetMapping("/production-items/{id}")
    public ResponseEntity<ProductionItemDto> getProductionItemById(@PathVariable(value = "id") Long id) {
        LOGGER.info("Get production item by ID: {}", id);
        ProductionItemDto productionItem = productionItemFacade.findById(id);
        return ResponseEntity.ok(productionItem);
    }

    @Operation(summary = "Add an production item", description = "Adds a new production item.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Production item added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "406", description = "Not having enough ingredients or grape container.")
    })
    @PostMapping("/production-items")
    public ResponseEntity<ProductionItemDto> createProductionItem(@Valid @RequestBody ProductionItemDto productionItem) {
        LOGGER.info("Create a new production item: {}", productionItem);
        ProductionItemDto productionItemDto = productionItemFacade.add(productionItem);
        return ResponseEntity.status(HttpStatus.CREATED).body(productionItemDto);
    }

    @Operation(summary = "Update an production item by ID", description = "Updates a production item with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Production item with the specified ID updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Production item with the specified ID was not found.")
    })
    @PutMapping("/production-items/{id}")
    public ResponseEntity<ProductionItemDto> updateProductionItem(@PathVariable(value = "id") Long id, @Valid @RequestBody ProductionItemDto updatedProductionItem) {
        LOGGER.info("Update production item by ID: {}", id);
        ProductionItemDto productionItem = productionItemFacade.updateById(id, updatedProductionItem);
        return ResponseEntity.ok(productionItem);
    }


    @Operation(summary = "Delete a production item by ID", description = "Deletes a production item with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Production item with the specified ID deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Production item with the specified ID was not found.")
    })
    @DeleteMapping("/production-items/{id}")
    public ResponseEntity<ProductionItemDto> deleteProductionItem(@PathVariable(value = "id") Long id) {
        LOGGER.info("Delete production item by ID: {}", id);
        ProductionItemDto productionItemDto = productionItemFacade.deleteById(id);
        return ResponseEntity.ok(productionItemDto);
    }
}
