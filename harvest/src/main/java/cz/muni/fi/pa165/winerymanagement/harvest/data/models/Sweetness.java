package cz.muni.fi.pa165.winerymanagement.harvest.data.models;

public enum Sweetness {
    SWEET("sweet"),
    SEMISWEET("semisweet"),

    DRY("dry");

    private final String wineSweetness;

    Sweetness(String wineSweetness) {
        this.wineSweetness = wineSweetness;
    }

    public String getSweetness() {
        return wineSweetness;
    }
}
