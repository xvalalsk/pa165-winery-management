package cz.muni.fi.pa165.winerymanagement.confidentialclient;

import cz.muni.fi.pa165.winerymanagement.confidentialclient.rest.requests.UserRegistrationRequest;
import cz.muni.fi.pa165.winerymanagement.core.model.ModuleEnum;
import cz.muni.fi.pa165.winerymanagement.core.service.ModuleUrlService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@SpringBootApplication
public class ConfidentialClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfidentialClientApplication.class, args);
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new SavedRequestAwareAuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth) throws ServletException, IOException {
                if (auth instanceof OAuth2AuthenticationToken token
                        && token.getPrincipal() instanceof OidcUser user) {
                    String url = ModuleUrlService.buildModuleUrl(ModuleEnum.USER, "/api/v1/register");
                    RestTemplate userRegisterRestTemplate = new RestTemplate();

                    var registerDto = new UserRegistrationRequest();
                    registerDto.setIssuer(String.valueOf(user.getIssuer()));
                    registerDto.setExternalId(user.getSubject());

                    try {
                        userRegisterRestTemplate.postForObject(url, registerDto, UserRegistrationRequest.class);
                    } catch (RestClientException e) {
                        throw new ServletException("Error when registering user", e);
                    }
                }
                super.onAuthenticationSuccess(req, res, auth);
            }
        };
    }

}
