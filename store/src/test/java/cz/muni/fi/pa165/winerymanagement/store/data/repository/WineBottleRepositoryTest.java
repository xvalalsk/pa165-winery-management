package cz.muni.fi.pa165.winerymanagement.store.data.repository;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.StoreApplication;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeType;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = StoreApplication.class)
@Transactional
public class WineBottleRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private WineBottleRepository wineBottleRepository;

    private WineBottle wineBottle1;
    private WineBottle wineBottle2;

    @BeforeEach
    public void setUp() {
        GrapeType grapeType1 = new GrapeType();
        grapeType1.setName("Grape1");
        grapeType1.setDescription("Description1");
        grapeType1.setColor(GrapeType.Color.RED);
        grapeType1.setSweetness(GrapeType.Sweetness.DRY);

        wineBottle1 = new WineBottle();
        wineBottle1.setGrapeType(grapeType1);
        wineBottle1.setHarvestYear(2018);
        wineBottle1.setVolume(new VolumeQuantity(750, VolumeQuantity.VolumeUnit.LITER));

        GrapeType grapeType2 = new GrapeType();
        grapeType2.setName("Grape2");
        grapeType2.setDescription("Description2");
        grapeType2.setColor(GrapeType.Color.WHITE);
        grapeType2.setSweetness(GrapeType.Sweetness.SWEET);

        wineBottle2 = new WineBottle();
        wineBottle2.setGrapeType(grapeType2);
        wineBottle2.setHarvestYear(2019);
        wineBottle2.setVolume(new VolumeQuantity(500, VolumeQuantity.VolumeUnit.LITER));

        entityManager.persist(wineBottle1);
        entityManager.persist(wineBottle2);
        entityManager.flush();
    }

    @Test
    void findByGrapeTypeAndHarvestYear() {
        WineBottle foundWineBottle = wineBottleRepository.findByGrapeTypeAndHarvestYear(wineBottle1.getGrapeType(), wineBottle1.getHarvestYear());

        assertThat(foundWineBottle).isEqualTo(wineBottle1);
    }

    @Test
    void findByGrapeTypeAndHarvestYear_NotFound() {
        WineBottle foundWineBottle = wineBottleRepository.findByGrapeTypeAndHarvestYear(wineBottle1.getGrapeType(), 2020);

        assertThat(foundWineBottle).isNull();
    }

    @Test
    void findAll() {
        List<WineBottle> wineBottles = wineBottleRepository.findAll();

        assertThat(wineBottles).contains(wineBottle1, wineBottle2);
    }

    @Test
    void findById() {
        WineBottle foundWineBottle = wineBottleRepository.findById(wineBottle1.getId()).orElse(null);

        assertThat(foundWineBottle).isEqualTo(wineBottle1);
    }

    @Test
    void findById_NotFound() {
        WineBottle foundWineBottle = wineBottleRepository.findById(999L).orElse(null);

        assertThat(foundWineBottle).isNull();
    }

    @Test
    void save() {
        WineBottle newWineBottle = new WineBottle();
        GrapeType grapeType = new GrapeType();
        grapeType.setName("Grape3");
        grapeType.setDescription("Description3");
        grapeType.setColor(GrapeType.Color.ROSE);
        grapeType.setSweetness(GrapeType.Sweetness.SEMISWEET);

        newWineBottle.setGrapeType(grapeType);
        newWineBottle.setHarvestYear(2021);
        newWineBottle.setVolume(new VolumeQuantity(1000, VolumeQuantity.VolumeUnit.LITER));

        WineBottle savedWineBottle = wineBottleRepository.save(newWineBottle);

        assertThat(savedWineBottle).isEqualTo(newWineBottle);
    }

    @Test
    void delete() {
        wineBottleRepository.delete(wineBottle1);

        List<WineBottle> wineBottles = wineBottleRepository.findAll();

        assertThat(wineBottles).contains(wineBottle2);
        assertThat(wineBottles).doesNotContain(wineBottle1);
    }
}
