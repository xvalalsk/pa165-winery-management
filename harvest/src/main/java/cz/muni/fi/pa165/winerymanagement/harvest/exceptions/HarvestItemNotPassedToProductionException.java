package cz.muni.fi.pa165.winerymanagement.harvest.exceptions;


import cz.muni.fi.pa165.winerymanagement.harvest.data.models.HarvestItem;

public class HarvestItemNotPassedToProductionException extends RuntimeException {
    public HarvestItemNotPassedToProductionException() {
        super();
    }

    public HarvestItemNotPassedToProductionException(HarvestItem harvestItem) {
        super("Harvest item " + harvestItem.toString() + " could not be passed to production module");
    }
}
