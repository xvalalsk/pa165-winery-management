package cz.muni.fi.pa165.winerymanagement.store.mappers;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleStockDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottleStock;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WineBottleStockMapper {

	WineBottleStockDto mapToDto(WineBottleStock wineBottleStock);
	List<WineBottleStockDto> mapToList(List<WineBottleStock> stock);
	WineBottleStock mapFromDto(WineBottleStockDto wineBottleStockDto);
}
