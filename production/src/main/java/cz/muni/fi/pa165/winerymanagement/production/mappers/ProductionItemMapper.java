package cz.muni.fi.pa165.winerymanagement.production.mappers;

import cz.muni.fi.pa165.winerymanagement.production.api.ProductionItemDto;
import cz.muni.fi.pa165.winerymanagement.production.data.model.ProductionItem;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductionItemMapper {

    ProductionItemDto mapToDto(ProductionItem productionItem);

    List<ProductionItemDto> mapToList(List<ProductionItem> productionItems);

    ProductionItem mapFromDto(ProductionItemDto productionItemDto);
}
