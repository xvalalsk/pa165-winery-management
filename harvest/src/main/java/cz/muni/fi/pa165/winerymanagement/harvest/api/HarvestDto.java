package cz.muni.fi.pa165.winerymanagement.harvest.api;

import cz.muni.fi.pa165.winerymanagement.harvest.annotations.CurrentYearOrBefore;
import jakarta.validation.constraints.Min;

import java.util.List;

public class HarvestDto {
    private Long id;

    @Min(1998L)
    @CurrentYearOrBefore
    private int year;
    private List<HarvestItemDto> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<HarvestItemDto> getItems() {
        return items;
    }

    public void setItems(List<HarvestItemDto> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "HarvestDto{" +
                "year=" + year +
                ", items=" + items +
                '}';
    }
}
