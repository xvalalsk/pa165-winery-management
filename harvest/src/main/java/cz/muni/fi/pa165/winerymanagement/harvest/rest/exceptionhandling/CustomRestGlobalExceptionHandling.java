package cz.muni.fi.pa165.winerymanagement.harvest.rest.exceptionhandling;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.muni.fi.pa165.winerymanagement.harvest.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomRestGlobalExceptionHandling {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomRestGlobalExceptionHandling.class);
    @ExceptionHandler({HarvestAlreadyExistsException.class, GrapeTypeAlreadyExistsException.class, HarvestItemNotPassedToProductionException.class, MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage handleConflict(Exception ex) {
        LOGGER.error("Throwing exception: {} with 400 response status", ex.getMessage());
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler({HarvestNotExistsException.class, HarvestNotExistsException.class, HarvestItemNotExistsException.class, GrapeTypeNotExistsException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorMessage handleNotFound(Exception ex) {
        LOGGER.error("Throwing exception: {} with 404 response status", ex.getMessage());
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorMessage handleException(Exception ex) {
        LOGGER.error("Throwing exception: {} with 500 response status", ex.getMessage());
        return new ErrorMessage("Internal server error occurred");
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class ErrorMessage {

        private String message;

        public ErrorMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}


