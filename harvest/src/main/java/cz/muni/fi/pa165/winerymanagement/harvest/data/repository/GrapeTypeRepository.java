package cz.muni.fi.pa165.winerymanagement.harvest.data.repository;

import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Color;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Sweetness;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GrapeTypeRepository extends JpaRepository<GrapeType, Long> {
    GrapeType findGrapeTypeByColorAndSweetnessAndDescriptionAndName(Color color, Sweetness sweetness, String description, String name);
}
