package cz.muni.fi.pa165.winerymanagement.production.facade;

import cz.muni.fi.pa165.winerymanagement.core.exceptions.NotEnoughQuantityException;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.api.IngredientDto;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.mappers.IngredientMapper;
import cz.muni.fi.pa165.winerymanagement.production.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class IngredientFacade {
    private final IngredientService ingredientService;
    private final IngredientMapper ingredientMapper;

    @Autowired
    public IngredientFacade(IngredientService ingredientService, IngredientMapper ingredientMapper) {
        this.ingredientService = ingredientService;
        this.ingredientMapper = ingredientMapper;
    }

    @Transactional(readOnly = true)
    public List<IngredientDto> findAll() {
        return ingredientMapper.mapToList(ingredientService.findAll());
    }

    @Transactional(readOnly = true)
    public IngredientDto findById(Long id) {
        return ingredientMapper.mapToDto(ingredientService.findById(id));
    }

    @Transactional(readOnly = true)
    public IngredientDto findByName(String name) {
        return ingredientMapper.mapToDto(ingredientService.findByName(name));
    }

    public IngredientDto add(IngredientDto newIngredient) {
        return ingredientMapper.mapToDto(ingredientService.add(ingredientMapper.mapFromDto(newIngredient)));
    }

    public IngredientDto updateById(Long id, IngredientDto updatedIngredient) {
        return ingredientMapper.mapToDto(ingredientService.updateById(id, ingredientMapper.mapFromDto(updatedIngredient)));
    }

    public IngredientDto updateByName(String name, IngredientDto updatedIngredient) {
        return ingredientMapper.mapToDto(ingredientService.updateByName(name, ingredientMapper.mapFromDto(updatedIngredient)));
    }

    public IngredientDto addQuantity(Long id, Quantity quantity) {
        return ingredientMapper.mapToDto(ingredientService.addQuantity(id, quantity));
    }

    public IngredientDto subtractQuantity(Long id, Quantity quantity) throws NotEnoughQuantityException {
        return ingredientMapper.mapToDto(ingredientService.subtractQuantity(id, quantity));
    }

    public IngredientDto deleteById(Long id) throws IngredientWithIdNotFoundException {
        return ingredientMapper.mapToDto(ingredientService.deleteById(id));
    }

    public IngredientDto deleteByName(String name) throws IngredientWithIdNotFoundException {
        return ingredientMapper.mapToDto(ingredientService.deleteByName(name));
    }
}
