package cz.muni.fi.pa165.winerymanagement.user.repository;

import cz.muni.fi.pa165.winerymanagement.user.UserApplication;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.UserRepository;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserApplication.class)
@Transactional
public class UserRepositoryTest {
    @Autowired
    EntityManager em;
    @Autowired
    private UserRepository userRepository;
    private User user;

    @BeforeEach
    void setUp() {
        user = new User("MUNI", "My_external_id");
        em.persist(user);
    }

    @Test
    void findById() {
        var foundUser = userRepository.findById(user.getId());
        assertThat(foundUser).isPresent();
        assertThat(foundUser.get()).isEqualTo(user);
    }

    @Test
    void findByNonExistingId() {
        assertThat(userRepository.findById(-1L)).isEmpty();
    }

    @Test
    void addUser() {
        var newUser = new User("MUNI", "new external id");
        userRepository.save(newUser);
        var foundUser = userRepository.findById(newUser.getId());
        assertThat(foundUser).isPresent();
        assertThat(foundUser.get()).isEqualTo(newUser);
    }

    @Test
    void updateUser() {
        String newExternalId = "updatedExternalId";

        User userForUpdate = new User();
        userForUpdate.setId(user.getId());
        userForUpdate.setIssuer(user.getIssuer());
        userForUpdate.setExternalId(newExternalId);

        var updatedUser = userRepository.save(userForUpdate);
        assertThat(updatedUser.getExternalId()).isEqualTo(newExternalId);
    }

    @Test
    void deleteUser() {
        userRepository.deleteById(user.getId());
        assertThat(userRepository.findById(user.getId())).isEmpty();
    }
}

