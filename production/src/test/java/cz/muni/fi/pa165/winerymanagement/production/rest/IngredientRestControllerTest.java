package cz.muni.fi.pa165.winerymanagement.production.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.api.IngredientDto;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ingredients.IngredientWithNameNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.facade.IngredientFacade;
import cz.muni.fi.pa165.winerymanagement.production.rest.exceptionhandling.CustomRestGlobalExceptionHandling;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(IngredientRestController.class)
@Import(IngredientRestController.class)
class IngredientRestControllerTest {
    String API_PATH = "/api/v1/production/ingredients";
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private IngredientFacade ingredientFacade;
    private IngredientDto ingredient1Dto;
    private IngredientDto ingredient2Dto;
    private IngredientDto ingredient3Dto;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new IngredientRestController(ingredientFacade)).setControllerAdvice(CustomRestGlobalExceptionHandling.class).build();

        ingredient1Dto = new IngredientDto();
        ingredient1Dto.setId(1L);
        ingredient1Dto.setName("sugar");
        ingredient1Dto.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));

        ingredient2Dto = new IngredientDto();
        ingredient2Dto.setId(2L);
        ingredient2Dto.setName("salt");
        ingredient2Dto.setQuantity(new WeightQuantity(5, WeightQuantity.WeightUnit.GRAM));

        ingredient3Dto = new IngredientDto();
        ingredient3Dto.setId(3L);
        ingredient3Dto.setName("water");
        ingredient3Dto.setQuantity(new VolumeQuantity(30, VolumeQuantity.VolumeUnit.LITER));
    }

    @Test
    void testGetAllIngredients() throws Exception {
        List<IngredientDto> ingredients = Arrays.asList(ingredient1Dto, ingredient2Dto, ingredient3Dto);

        when(ingredientFacade.findAll()).thenReturn(ingredients);

        mockMvc.perform(get(API_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("sugar")))
                .andExpect(jsonPath("$[0].quantity.type", is("weight")))
                .andExpect(jsonPath("$[0].quantity.value", is(10.0)))
                .andExpect(jsonPath("$[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("salt")))
                .andExpect(jsonPath("$[1].quantity.type", is("weight")))
                .andExpect(jsonPath("$[1].quantity.value", is(5.0)))
                .andExpect(jsonPath("$[1].quantity.weightUnit", is("GRAM")))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].name", is("water")))
                .andExpect(jsonPath("$[2].quantity.type", is("volume")))
                .andExpect(jsonPath("$[2].quantity.value", is(30.0)))
                .andExpect(jsonPath("$[2].quantity.volumeUnit", is("LITER")));

        verify(ingredientFacade).findAll();
    }

    @Test
    void testGetIngredientById_First() throws Exception {
        Long id = ingredient1Dto.getId();

        when(ingredientFacade.findById(id)).thenReturn(ingredient1Dto);

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("name", is("sugar")))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(10.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));

        verify(ingredientFacade).findById(id);
    }

    @Test
    void testGetIngredientById_Second() throws Exception {
        Long id = ingredient2Dto.getId();

        when(ingredientFacade.findById(id)).thenReturn(ingredient2Dto);

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("name", is("salt")))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(5.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("GRAM")));

        verify(ingredientFacade).findById(id);
    }

    @Test
    void testGetIngredientById_Third() throws Exception {
        Long id = ingredient3Dto.getId();

        when(ingredientFacade.findById(id)).thenReturn(ingredient3Dto);

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("name", is("water")))
                .andExpect(jsonPath("quantity.type", is("volume")))
                .andExpect(jsonPath("quantity.value", is(30.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")));

        verify(ingredientFacade).findById(id);
    }

    @Test
    void testGetIngredientById_NotFound() throws Exception {
        Long id = 1L;

        when(ingredientFacade.findById(id)).thenThrow(new IngredientWithIdNotFoundException(id));

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetIngredientByName() throws Exception {
        Long id = ingredient1Dto.getId();
        String name = ingredient1Dto.getName();

        when(ingredientFacade.findByName(name)).thenReturn(ingredient1Dto);

        mockMvc.perform(get(API_PATH + "/name/{name}", name))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("name", is(name)))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(10.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));

        verify(ingredientFacade).findByName(name);
    }

    @Test
    void testGetIngredientByName_NotFound() throws Exception {
        String name = "RandomIngredientName";

        when(ingredientFacade.findByName(name)).thenThrow(new IngredientWithNameNotFoundException(name));

        mockMvc.perform(get(API_PATH + "/name/{name}", name))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreateIngredient_Created() throws Exception {
        when(ingredientFacade.add(ArgumentMatchers.any(IngredientDto.class))).thenReturn(ingredient1Dto);

        mockMvc.perform(post(API_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(ingredient1Dto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(ingredient1Dto.getId().intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(10.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));

    }

    @Test
    void testCreateIngredient_Conflict() throws Exception {
        when(ingredientFacade.add(ArgumentMatchers.any(IngredientDto.class))).thenThrow(new IngredientAlreadyExistsException("Ingredient with the name already exists"));

        mockMvc.perform(post(API_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(ingredient1Dto)))
                .andExpect(status().isConflict());
    }

    @Test
    void testUpdateIngredientById() throws Exception {
        Long id = ingredient1Dto.getId();
        ingredient1Dto.setName("Updated Name");
        when(ingredientFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(IngredientDto.class))).thenReturn(ingredient1Dto);

        String requestBody = new ObjectMapper().writeValueAsString(ingredient1Dto);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(10.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));
    }

    @Test
    void testUpdateIngredientById_NotFound() throws Exception {
        Long id = ingredient1Dto.getId();
        ingredient1Dto.setName("Updated Name");

        when(ingredientFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(IngredientDto.class))).thenThrow(new IngredientWithIdNotFoundException(id));

        String requestBody = new ObjectMapper().writeValueAsString(ingredient1Dto);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isNotFound());
    }


    @Test
    void testUpdateIngredientByName() throws Exception {
        String oldName = ingredient1Dto.getName();
        ingredient1Dto.setName("Updated Name");
        when(ingredientFacade.updateByName(ArgumentMatchers.any(String.class), ArgumentMatchers.any(IngredientDto.class))).thenReturn(ingredient1Dto);

        String requestBody = new ObjectMapper().writeValueAsString(ingredient1Dto);

        mockMvc.perform(put(API_PATH + "/name/{name}", oldName)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(ingredient1Dto.getId().intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(10.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));
    }

    @Test
    void testUpdateIngredientByName_NotFound() throws Exception {
        String name = ingredient1Dto.getName();
        ingredient1Dto.setName("Updated Name");

        when(ingredientFacade.updateByName(ArgumentMatchers.any(String.class), ArgumentMatchers.any(IngredientDto.class))).thenThrow(new IngredientWithNameNotFoundException(name));

        String requestBody = new ObjectMapper().writeValueAsString(ingredient1Dto);

        mockMvc.perform(put(API_PATH + "/name/{name}", name)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddQuantityByID() throws Exception {
        IngredientDto updatedIngredient = new IngredientDto();
        updatedIngredient.setId(1L);
        updatedIngredient.setName("sugar");
        updatedIngredient.setQuantity(new WeightQuantity(15, WeightQuantity.WeightUnit.KILOGRAM));

        Quantity quantity = new WeightQuantity(15, WeightQuantity.WeightUnit.KILOGRAM);

        when(ingredientFacade.findById(ArgumentMatchers.any(Long.class))).thenReturn(ingredient1Dto);
        when(ingredientFacade.addQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(updatedIngredient);

        String requestBody = new ObjectMapper().writeValueAsString(quantity);

        mockMvc.perform(post(API_PATH + "/{id}/add-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(ingredient1Dto.getId().intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(15.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));
    }

    @Test
    void testAddQuantityByName() throws Exception {
        IngredientDto updatedIngredient = new IngredientDto();
        updatedIngredient.setId(1L);
        updatedIngredient.setName("sugar");
        updatedIngredient.setQuantity(new WeightQuantity(15, WeightQuantity.WeightUnit.KILOGRAM));

        Quantity quantity = new WeightQuantity(15, WeightQuantity.WeightUnit.KILOGRAM);

        when(ingredientFacade.findByName(ArgumentMatchers.any(String.class))).thenReturn(ingredient1Dto);
        when(ingredientFacade.addQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(updatedIngredient);

        String requestBody = new ObjectMapper().writeValueAsString(quantity);

        mockMvc.perform(post(API_PATH + "/name/{name}/add-quantity", updatedIngredient.getName())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(ingredient1Dto.getId().intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(15.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));
    }

    @Test
    void testSubtractQuantityByID() throws Exception {
        IngredientDto updatedIngredient = new IngredientDto();
        updatedIngredient.setId(1L);
        updatedIngredient.setName("sugar");
        updatedIngredient.setQuantity(new WeightQuantity(5, WeightQuantity.WeightUnit.KILOGRAM));

        Quantity quantity = new WeightQuantity(5, WeightQuantity.WeightUnit.KILOGRAM);

        when(ingredientFacade.findById(ArgumentMatchers.any(Long.class))).thenReturn(ingredient1Dto);
        when(ingredientFacade.subtractQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(updatedIngredient);

        String requestBody = new ObjectMapper().writeValueAsString(quantity);

        mockMvc.perform(post(API_PATH + "/{id}/subtract-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(ingredient1Dto.getId().intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(5.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));
    }

    @Test
    void testSubtractQuantityByName() throws Exception {
        IngredientDto updatedIngredient = new IngredientDto();
        updatedIngredient.setId(1L);
        updatedIngredient.setName("sugar");
        updatedIngredient.setQuantity(new WeightQuantity(15, WeightQuantity.WeightUnit.KILOGRAM));

        Quantity quantity = new WeightQuantity(15, WeightQuantity.WeightUnit.KILOGRAM);

        when(ingredientFacade.findByName(ArgumentMatchers.any(String.class))).thenReturn(ingredient1Dto);
        when(ingredientFacade.subtractQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(updatedIngredient);

        String requestBody = new ObjectMapper().writeValueAsString(quantity);

        mockMvc.perform(post(API_PATH + "/name/{name}/subtract-quantity", updatedIngredient.getName())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(ingredient1Dto.getId().intValue())))
                .andExpect(jsonPath("name", is(ingredient1Dto.getName())))
                .andExpect(jsonPath("quantity.type", is("weight")))
                .andExpect(jsonPath("quantity.value", is(15.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")));
    }

    @Test
    void testDeleteIngredientById() throws Exception {
        Long id = 1L;
        when(ingredientFacade.deleteById(id)).thenReturn(ingredient1Dto);

        mockMvc.perform(delete(API_PATH + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteIngredientByName() throws Exception {
        String name = "Ingredient 1";
        when(ingredientFacade.deleteByName(name)).thenReturn(ingredient1Dto);

        mockMvc.perform(delete(API_PATH + "/name/{name}", name))
                .andExpect(status().isOk());
    }
}