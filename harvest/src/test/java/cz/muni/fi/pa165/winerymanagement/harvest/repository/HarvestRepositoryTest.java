package cz.muni.fi.pa165.winerymanagement.harvest.repository;

import cz.muni.fi.pa165.winerymanagement.harvest.HarvestApplication;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.Harvest;
import cz.muni.fi.pa165.winerymanagement.harvest.data.repository.HarvestRepository;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = HarvestApplication.class)
@Transactional
class HarvestRepositoryTest {
    @Autowired
    private HarvestRepository harvestRepository;

    @Autowired
    private EntityManager em;
    private Harvest harvest;

    @BeforeEach
    void setUp() {
        this.harvest = new Harvest(2023);
        em.persist(this.harvest);
    }

    @Test
    void findAllHarvests() {
        var harvests = harvestRepository.findAll();
        assertThat(harvests).hasSize(3).contains(this.harvest);
    }

    @Test
    void findExistingHarvest() {
        var harvest = this.harvestRepository.findById(this.harvest.getId());
        assertTrue(harvest.isPresent());
        assertEquals(this.harvest, harvest.get());
    }

    @Test
    void findNonExistingHarvest() {
        assertTrue(this.harvestRepository.findById(-1L).isEmpty());
    }

    @Test
    void addHarvest() {
        var newHarvest = new Harvest(2020);

        var inserted = this.harvestRepository.save(newHarvest);
        assertThat(this.harvestRepository.findById(inserted.getId())).isPresent();
    }

    @Test
    void updateHarvest() {
        var newHarvest = new Harvest();
        newHarvest.setId(this.harvest.getId());
        newHarvest.setYear(2020);

        this.harvestRepository.save(newHarvest);
        assertThat(this.harvestRepository.findAll()).hasSize(3).contains(newHarvest);
    }

    @Test
    void deleteHarvestById() {
        this.harvestRepository.deleteById(this.harvest.getId());
        assertEquals(2, this.harvestRepository.findAll().size());
    }

    @Test
    void deleteNonExistingHarvestById() {
        this.harvestRepository.deleteById(-1L);
        assertEquals(3, this.harvestRepository.findAll().size());
    }
}
