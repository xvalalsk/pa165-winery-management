package cz.muni.fi.pa165.winerymanagement.store.rest;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleDto;
import cz.muni.fi.pa165.winerymanagement.store.facade.WineBottleFacade;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/store")
@Tag(name = "Wine Bottles", description = "API for managing wine bottles")
@CrossOrigin(origins = "*")
public class WineBottleRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(WineBottleRestController.class);
	private final WineBottleFacade wineBottleFacade;

	@Autowired
	public WineBottleRestController(WineBottleFacade wineBottleFacade) {
		this.wineBottleFacade = wineBottleFacade;
	}

	@Operation(summary = "Get all wine bottles", description = "Returns a list of all wine bottles.")
	@ApiResponse(responseCode = "200", description = "List of all wine bottles returned successfully.")
	@GetMapping("/wine-bottles")
	public ResponseEntity<List<WineBottleDto>> getAllWineBottles() {
		LOGGER.info("Fetching all wineBottles");
		return ResponseEntity.ok(wineBottleFacade.findAll());
	}

	@Operation(summary = "Get a wine bottle by ID", description = "Returns a wine bottle with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottle with the specified ID returned successfully."),
			@ApiResponse(responseCode = "404", description = "Wine bottle with the specified ID was not found.")
	})
	@GetMapping("/wine-bottles/{id}")
	public ResponseEntity<WineBottleDto> getWineBottleById(@PathVariable(value = "id") Long id) {
		LOGGER.info("Fetching wineBottle with ID: {}", id);
		WineBottleDto wineBottle = wineBottleFacade.findById(id);
		return ResponseEntity.ok(wineBottle);
	}

	@Operation(summary = "Create a wine bottle", description = "Creates a new wine bottle.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Wine bottle createed successfully."),
			@ApiResponse(responseCode = "400", description = "Invalid input.")
	})
	@PostMapping("/wine-bottles")
	public ResponseEntity<WineBottleDto> createWineBottle(@Valid @RequestBody WineBottleDto wineBottle) {
		LOGGER.info("Creating new wineBottle: {}", wineBottle);
		return ResponseEntity.status(HttpStatus.CREATED).body(wineBottleFacade.add(wineBottle));
	}

	@Operation(summary = "Update a wine bottle by ID", description = "Updates a wine bottle with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottle with the specified ID updated successfully."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle with the specified ID was not found.")
	})
	@PutMapping("/wine-bottles/{id}")
	public ResponseEntity<WineBottleDto> updateWineBottleById(@PathVariable Long id, @Valid @RequestBody WineBottleDto updatedWineBottle) {
		LOGGER.info("Updating wine bottle with ID: {} with new data: {}", id, updatedWineBottle);
		WineBottleDto wineBottle = wineBottleFacade.updateById(id, updatedWineBottle);
		return ResponseEntity.ok(wineBottle);
	}

	@Operation(summary = "Delete a wine bottle by ID", description = "Deletes a wine bottle with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "204", description = "Wine bottle with the specified ID deleted successfully."),
			@ApiResponse(responseCode = "404", description = "Wine bottle with the specified ID was not found.")
	})
	@DeleteMapping("/wine-bottles/{id}")
	public ResponseEntity<WineBottleDto> deleteWineBottleById(@PathVariable(value = "id") Long id) {
		LOGGER.info("Deleting wine bottle with ID: {}", id);
		WineBottleDto wineBottle = wineBottleFacade.deleteById(id);
		return ResponseEntity.ok(wineBottle);
	}
}
