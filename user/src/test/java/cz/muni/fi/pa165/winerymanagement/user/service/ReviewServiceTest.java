package cz.muni.fi.pa165.winerymanagement.user.service;

import cz.muni.fi.pa165.winerymanagement.user.data.models.Review;
import cz.muni.fi.pa165.winerymanagement.user.data.models.User;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.ReviewRepository;
import cz.muni.fi.pa165.winerymanagement.user.data.repository.UserRepository;
import cz.muni.fi.pa165.winerymanagement.user.exceptions.InvalidReviewException;
import cz.muni.fi.pa165.winerymanagement.user.service.ReviewService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ReviewServiceTest {
    private ReviewService reviewService;
    private ReviewRepository reviewRepository;
    private UserRepository userRepository;
    private Review review;

    private User user;

    @BeforeEach
    void setUp() {
        reviewRepository = mock(ReviewRepository.class);
        userRepository = mock(UserRepository.class);

        reviewService = new ReviewService(reviewRepository, userRepository);

        user = new User();
        user.setId(0);

        review = new Review();
        review.setId(111L);
        review.setUser(user);
        review.setWineBottleId(222L);
        review.setComment("comment");
        review.setRating(3);
    }

    @Test
    void addValidReview() {
        when(userRepository.findById(0L)).thenReturn(Optional.of(new User()));
        reviewService.addReview(review);
    }

    @Test
    void addReviewInvalidRating() {
        review.setRating(100);
        assertThatThrownBy(() -> reviewService.addReview(review)).isInstanceOf(InvalidReviewException.class);
    }

    @Test
    void addReviewNonExistingUser() {
        assertThatThrownBy(() -> reviewService.addReview(review)).isInstanceOf(InvalidReviewException.class);
    }

    @Test
    void updateValidReview() {
        when(userRepository.findById(0L)).thenReturn(Optional.of(new User()));
        review.setComment("updated comment");
        reviewService.updateReview(review);

        ArgumentCaptor<Review> reviewCaptor = ArgumentCaptor.forClass(Review.class);
        verify(reviewRepository).save(reviewCaptor.capture());

        assertThat(reviewCaptor.getValue().getComment()).isEqualTo("updated comment");
    }

    @Test
    void updateReviewInvalidRating() {
        review.setRating(100);
        assertThatThrownBy(() -> reviewService.updateReview(review)).isInstanceOf(InvalidReviewException.class);
    }

    @Test
    void updateReviewNonExistingUser() {
        assertThatThrownBy(() -> reviewService.updateReview(review)).isInstanceOf(InvalidReviewException.class);
    }
}
