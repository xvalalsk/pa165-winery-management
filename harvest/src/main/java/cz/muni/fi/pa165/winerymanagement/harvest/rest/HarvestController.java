package cz.muni.fi.pa165.winerymanagement.harvest.rest;

import cz.muni.fi.pa165.winerymanagement.harvest.api.*;
import cz.muni.fi.pa165.winerymanagement.harvest.facade.HarvestFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/harvests")
@CrossOrigin(origins = "*")
public class HarvestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HarvestController.class);
    private final HarvestFacade harvestFacade;

    @Autowired
    public HarvestController(HarvestFacade facade) {
        this.harvestFacade = facade;
    }

    @GetMapping
    @Operation(
            summary = "Return all harvests",
            tags = "Harvest"
    )
    @ApiResponse(responseCode = "200", description = "List of all harvests")
    public ResponseEntity<List<HarvestDto>> findAllHarvests() {
        LOGGER.info("Finding all harvests");
        return ResponseEntity.ok(harvestFacade.findAllHarvests());
    }

    @GetMapping("/items")
    @Operation(
            summary = "Return all harvest items",
            tags = "Harvest"
    )
    @ApiResponse(responseCode = "200", description = "List of all harvest items")
    public ResponseEntity<List<HarvestItemDto>> findAllHarvestItems() {
        LOGGER.info("Finding all harvest items");
        return ResponseEntity.ok(harvestFacade.findAllHarvestItems());
    }

    @GetMapping("/types")
    @Operation(
            summary = "Return grape types",
            tags = "Harvest"
    )
    @ApiResponse(responseCode = "200", description = "List of all grape types")
    public ResponseEntity<List<GrapeTypeDto>> findAllGrapeTypes() {
        LOGGER.info("Finding all grape types");
        return ResponseEntity.ok(harvestFacade.findAllGrapeTypes());
    }

    @GetMapping("/{id}/items")
    @Operation(
            summary = "Return all items for specific harvest",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of items for specific harvest"),
            @ApiResponse(responseCode = "404", description = "Harvest with specific ID wasn't found")
    })
    public ResponseEntity<List<HarvestItemDto>> findAllItemsForHarvest(@PathVariable("id") Long harvestId) {
        LOGGER.info("Finding all items for specific harvest with ID: {}", harvestId);
        return ResponseEntity.ok(harvestFacade.findAllItemsForHarvest(harvestId));
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Return harvest by ID",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Specific harvest"),
            @ApiResponse(responseCode = "404", description = "Harvest with specific ID wasn't found")
    })
    public ResponseEntity<HarvestDto> findHarvestById(@PathVariable("id") Long id) {
        LOGGER.info("Finding harvest by ID: {}", id);
        return ResponseEntity.ok(harvestFacade.findHarvestById(id));
    }

    @GetMapping("/{harvestId}/items/{id}")
    @Operation(
            summary = "Return harvest item by ID",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Specific harvest item"),
            @ApiResponse(responseCode = "404", description = "Harvest item with specific ID wasn't found")
    })
    public ResponseEntity<HarvestItemDto> findHarvestItemById(@PathVariable("id") Long id) {
        LOGGER.info("Finding harvest item by ID: {}", id);
        return ResponseEntity.ok(harvestFacade.findHarvestItemById(id));
    }

    @GetMapping("/types/{id}")
    @Operation(
            summary = "Return grape type by ID",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Specific grape type"),
            @ApiResponse(responseCode = "404", description = "Grape type with specific ID wasn't found")
    })
    public ResponseEntity<GrapeTypeDto> findGrapeTypeById(@PathVariable("id") Long id) {
        LOGGER.info("Finding grape type by ID: {}", id);
        return ResponseEntity.ok(harvestFacade.findGrapeTypeById(id));
    }

    @PostMapping
    @Operation(
            summary = "Create new harvest",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Harvest was successfully created"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    public ResponseEntity<HarvestDto> addHarvest(@Valid @RequestBody HarvestInputDto harvestInputDto) {
        LOGGER.info("Adding new harvest: {}", harvestInputDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(harvestFacade.addHarvest(harvestInputDto));
    }

    @PostMapping("/items")
    @Operation(
            summary = "Create new harvest item",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Harvest item was successfully created"),
            @ApiResponse(responseCode = "404", description = "Harvest with specific ID wasn't found"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    public ResponseEntity<HarvestItemDto> addHarvestItem(@Valid @RequestBody HarvestItemCreateDto harvestItemCreateDto) {
        LOGGER.info("Adding new harvest item: {}", harvestItemCreateDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(harvestFacade.addHarvestItem(harvestItemCreateDto));
    }

    @PostMapping("/types")
    @Operation(
            summary = "Create new grape type",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Grape type was successfully created"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    public ResponseEntity<GrapeTypeDto> addGrapeType(@Valid @RequestBody GrapeTypeInputDto grapeTypeInputDto) {
        LOGGER.info("Adding new grape type: {}", grapeTypeInputDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(harvestFacade.addGrapeType(grapeTypeInputDto));
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Update existing harvest",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Harvest was successfully updated"),
            @ApiResponse(responseCode = "404", description = "Harvest with specific ID wasn't found")
    })
    public ResponseEntity<HarvestDto> updateHarvest(@PathVariable("id") Long id, @Valid @RequestBody HarvestInputDto harvestInputDto) {
        LOGGER.info("Updating harvest with ID: {} with new data: {}", id, harvestInputDto);
        return ResponseEntity.ok(harvestFacade.updateHarvest(id, harvestInputDto));
    }

    @PutMapping("/items/{id}")
    @Operation(
            summary = "Update existing harvest item",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Harvest item was successfully updated"),
            @ApiResponse(responseCode = "404", description = "Harvest item with specific ID wasn't found")
    })
    public ResponseEntity<HarvestItemDto> updateHarvestItem(@PathVariable("id") Long id, @Valid @RequestBody HarvestItemUpdateDto harvestItemUpdateDto) {
        LOGGER.info("Updating harvest item with ID: {} with new data: {}", id, harvestItemUpdateDto);
        return ResponseEntity.ok(harvestFacade.updateHarvestItem(id, harvestItemUpdateDto));
    }

    @PutMapping("/types/{id}")
    @Operation(
            summary = "Update existing grape type",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape type was successfully updated"),
            @ApiResponse(responseCode = "404", description = "Grape type with specific ID wasn't found")
    })
    public ResponseEntity<GrapeTypeDto> updateGrapeType(@PathVariable("id") Long id, @Valid @RequestBody GrapeTypeInputDto grapeTypeInputDto) {
        LOGGER.info("Updating grape type with ID: {} with new data: {}", id, grapeTypeInputDto);
        return ResponseEntity.ok(harvestFacade.updateGrapeType(id, grapeTypeInputDto));
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Delete existing harvest",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Harvest was successfully deleted"),
            @ApiResponse(responseCode = "404", description = "Harvest with specific ID wasn't found")
    })
    public ResponseEntity<HarvestDto> deleteHarvestById(@PathVariable("id") Long id) {
        LOGGER.info("Deleting harvest by ID: {}", id);
        return ResponseEntity.ok(harvestFacade.deleteHarvestById(id));
    }

    @DeleteMapping("/{harvestId}/items/{id}")
    @Operation(
            summary = "Delete existing harvest item",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Harvest item was successfully deleted"),
            @ApiResponse(responseCode = "404", description = "Harvest item with specific ID wasn't found")
    })
    public ResponseEntity<HarvestItemDto> deleteHarvestItemById(@PathVariable("id") Long id) {
        LOGGER.info("Deleting harvest item by ID: {}", id);
        return ResponseEntity.ok(harvestFacade.deleteHarvestItemById(id));
    }

    @DeleteMapping("/types/{id}")
    @Operation(
            summary = "Delete existing grape type",
            tags = "Harvest"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape type was successfully deleted"),
            @ApiResponse(responseCode = "404", description = "Grape type with specific ID wasn't found")
    })
    public ResponseEntity<GrapeTypeDto> deleteGrapeTypeById(@PathVariable("id") Long id) {
        LOGGER.info("Deleting grape type by ID: {}", id);
        return ResponseEntity.ok(harvestFacade.deleteGrapeTypeById(id));
    }
}
