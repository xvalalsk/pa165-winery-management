package cz.muni.fi.pa165.winerymanagement.harvest.api;

import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.harvest.data.models.GrapeType;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Represents a data transfer object for a Grape Container.
 */
@Schema(
        name = "GrapeContainerDto",
        description = "Data transfer object representing a grape container used in the production of wine",
        example = """
                  {
                      "id": 1,
                      "grapeType": {
                        "name": "Chardonnay",
                        "description": "Chardonnay white sweet wine description",
                        "color": "WHITE",
                        "sweetness": "SWEET"
                      },
                      "harvestYear": 2020,
                      "quantity": {
                        "type": "weight",
                        "value": 100.0,
                        "weightUnit": "KILOGRAM"
                      }
                    }
                  """
)
public class GrapeContainerDto {
    private Long id;
    private GrapeType grapeType;
    private int harvestYear;
    private WeightQuantity quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeType getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeType grapeType) {
        this.grapeType = grapeType;
    }

    public int getHarvestYear() {
        return harvestYear;
    }

    public void setHarvestYear(int harvestYear) {
        this.harvestYear = harvestYear;
    }

    public WeightQuantity getQuantity() {
        return quantity;
    }

    public void setQuantity(WeightQuantity quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "GrapeContainerDto{" +
                "id=" + id +
                ", grapeType=" + grapeType +
                ", harvestYear=" + harvestYear +
                ", quantity=" + quantity +
                '}';
    }
}
