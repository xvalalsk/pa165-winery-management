package cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels;

import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceNotFoundException;

import java.util.ResourceBundle;

public class BarrelWithIdNotFoundException extends ResourceNotFoundException {

    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
    private final long id;

    public BarrelWithIdNotFoundException(long id) {
        this.id = id;
    }

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        String message = messages.getString(key);
        return String.format(message, id);
    }
}
