package cz.muni.fi.pa165.winerymanagement.production.facade;

import cz.muni.fi.pa165.winerymanagement.production.api.ProductionItemDto;
import cz.muni.fi.pa165.winerymanagement.production.mappers.ProductionItemMapper;
import cz.muni.fi.pa165.winerymanagement.production.service.ProductionItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductionItemFacade {
    private final ProductionItemService productionItemService;
    private final ProductionItemMapper productionItemMapper;

    @Autowired
    public ProductionItemFacade(ProductionItemService productionItemService, ProductionItemMapper productionItemMapper) {
        this.productionItemService = productionItemService;
        this.productionItemMapper = productionItemMapper;
    }

    @Transactional(readOnly = true)
    public List<ProductionItemDto> findAll() {
        return productionItemMapper.mapToList(productionItemService.findAll());
    }

    @Transactional(readOnly = true)
    public ProductionItemDto findById(Long id) {
        return productionItemMapper.mapToDto(productionItemService.findById(id));
    }

    public ProductionItemDto add(ProductionItemDto newProductionItem) {
        return productionItemMapper.mapToDto(productionItemService.add(productionItemMapper.mapFromDto(newProductionItem)));
    }

    public ProductionItemDto updateById(Long id, ProductionItemDto updatedProductionItem) {
        return productionItemMapper.mapToDto(productionItemService.updateById(id, productionItemMapper.mapFromDto(updatedProductionItem)));
    }

    public ProductionItemDto deleteById(Long id) {
        return productionItemMapper.mapToDto(productionItemService.deleteById(id));
    }
}
