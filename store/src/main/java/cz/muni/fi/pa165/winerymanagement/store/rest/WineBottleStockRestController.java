package cz.muni.fi.pa165.winerymanagement.store.rest;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleStockDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.facade.WineBottleStockFacade;
import cz.muni.fi.pa165.winerymanagement.store.rest.requests.WineBottleFromBarrelFillRequest;
import cz.muni.fi.pa165.winerymanagement.store.rest.requests.WineBottleModifyCountRequest;
import cz.muni.fi.pa165.winerymanagement.store.rest.requests.WineBottleUpdateWineBottleStockRequest;
import cz.muni.fi.pa165.winerymanagement.store.service.WineBottleStockService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/store")
@Tag(name = "Wine Bottles In Stock", description = "API for managing wine bottles in stock")
@CrossOrigin(origins = "*")
public class WineBottleStockRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(WineBottleStockRestController.class);
	private final WineBottleStockFacade wineBottleStockFacade;

	@Autowired
	public WineBottleStockRestController(WineBottleStockFacade wineBottleStockFacade) {
		this.wineBottleStockFacade = wineBottleStockFacade;
	}

	@Operation(summary = "Get all wine bottles in stock", description = "Returns a list of all wine bottles in stock.")
	@ApiResponse(responseCode = "200", description = "List of all wine bottles in stock returned successfully.")
	@GetMapping("/wine-bottles-in-stock")
	public ResponseEntity<List<WineBottleStockDto>> getAllWineBottlesInStock() {
		LOGGER.info("Getting all wineBottleStock");
		return ResponseEntity.ok(wineBottleStockFacade.findAll());
	}

	@Operation(summary = "Get a wine bottle in stock by ID", description = "Returns a wine bottle in stock with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottle in stock with the specified ID returned successfully."),
			@ApiResponse(responseCode = "404", description = "Wine bottle in stock with the specified ID was not found.")
	})
	@GetMapping("/wine-bottles-in-stock/{id}")
	public ResponseEntity<WineBottleStockDto> getWineBottleStockById(@PathVariable(value = "id") Long id) {
		LOGGER.info("Getting wineBottleStock by ID: {}", id);
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.findById(id);
		return ResponseEntity.ok(wineBottleStock);
	}

	@Operation(summary = "Get a wine bottle stock by wine bottle", description = "Returns wine bottle stock with the specified wine bottles.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottle stock with the specified wine bottles returned successfully."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified wine bottles was not found.")
	})
	@GetMapping("/wine-bottles-in-stock/wineBottle")
	public ResponseEntity<WineBottleStockDto> getWineBottleStockByWineBottle(@RequestBody WineBottle wineBottle) {
		LOGGER.info("Getting wineBottleStock by wineBottle: {}", wineBottle);
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.findByWineBottle(wineBottle);
		return ResponseEntity.ok(wineBottleStock);
	}

	@Operation(summary = "Create a wine bottle in stock", description = "Creates a new wine bottle in stock.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Wine bottle created successfully in stock."),
			@ApiResponse(responseCode = "400", description = "Invalid input.")
	})
	@PostMapping("/wine-bottles-in-stock")
	public ResponseEntity<WineBottleStockDto> createWineBottleStock(@Valid @RequestBody WineBottleStockDto wineBottleStock) {
		LOGGER.info("Creating new wineBottleStock: {}", wineBottleStock);
		WineBottleStockDto wineBottleStockDto = wineBottleStockFacade.add(wineBottleStock);
		return ResponseEntity.status(HttpStatus.CREATED).body(wineBottleStockDto);
	}

	@Operation(summary = "Update a wine bottle in stock by ID", description = "Updates a wine bottle in stock with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottle in stock with the specified ID updated successfully."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle in stock with the specified ID was not found.")
	})
	@PutMapping("/wine-bottles-in-stock/{id}")
	public ResponseEntity<WineBottleStockDto> updateWineBottleStockById(@PathVariable Long id, @Valid @RequestBody WineBottleStockDto updatedWineBottleStock) {
		LOGGER.info("Updating wineBottleStock with ID: {} with new data: {}", id, updatedWineBottleStock);
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.updateById(id, updatedWineBottleStock);
		return ResponseEntity.ok(wineBottleStock);
	}

	@Operation(summary = "Update a wine bottle stock by wine bottle", description = "Updates wine bottle stock with the specified wine bottles.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottle stock with the specified wine bottles updated successfully."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified wine bottles was not found.")
	})
	@PutMapping("/wine-bottles-in-stock/wineBottle")
	public ResponseEntity<WineBottleStockDto> updateWineBottleStockByWineBottle(@RequestBody WineBottleUpdateWineBottleStockRequest wineBottleUpdateWineBottleStockRequest) {
		LOGGER.info("Updating wineBottleStock with wineBottle: {} with new data: {}", wineBottleUpdateWineBottleStockRequest.getWineBottle(), wineBottleUpdateWineBottleStockRequest.getWineBottleStock());
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.updateByWineBottle(wineBottleUpdateWineBottleStockRequest.getWineBottle(), wineBottleUpdateWineBottleStockRequest.getWineBottleStock());
		return ResponseEntity.ok(wineBottleStock);
	}

	@Operation(summary = "Add wine bottles to stock by ID", description = "Adds the number of wine bottles to stock with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottles were added successfully to stock."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified ID was not found.")
	})
	@PostMapping("/wine-bottles-in-stock/{id}/add-bottles")
	public ResponseEntity<WineBottleStockDto> addWineBottlesToStockById(@PathVariable(value = "id") Long id, @RequestBody int bottleCount) {
		LOGGER.info("Adding bottles (count: {}) to wineBottleStock with ID: {}", id, bottleCount);
		return ResponseEntity.ok(wineBottleStockFacade.addBottlesToStock(id, bottleCount));
	}

	@Operation(summary = "Add wine bottles to stock by wine bottle", description = "Adds wine bottles to stock with the specified wine bottle.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottles were added successfully to stock."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified wine bottle was not found.")
	})
	@PostMapping("/wine-bottles-in-stock/wineBottle/add-bottles")
	public ResponseEntity<WineBottleStockDto> addWineBottlesToStockByWineBottle(@RequestBody WineBottleModifyCountRequest wineBottleModifyCountRequest) {
		LOGGER.info("Adding bottles (count: {}) to wineBottleStock with wineBottle: {}", wineBottleModifyCountRequest.getWineBottle(), wineBottleModifyCountRequest.getBottleCount());
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.findByWineBottle(wineBottleModifyCountRequest.getWineBottle());
		return ResponseEntity.ok(wineBottleStockFacade.addBottlesToStock(wineBottleStock.getId(), wineBottleModifyCountRequest.getBottleCount()));
	}

	@Operation(summary = "Remove wine bottles from stock by ID", description = "Subtracts the number of wine bottles from stock with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottles were removed successfully from stock."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified ID was not found.")
	})
	@PostMapping("/wine-bottles-in-stock/{id}/remove-bottles")
	public ResponseEntity<WineBottleStockDto> removeWineBottlesFromStockById(@PathVariable(value = "id") Long id, @RequestBody int bottleCount) {
		LOGGER.info("Subtracting bottles (count: {}) to wineBottleStock with ID: {}", id, bottleCount);
		return ResponseEntity.ok(wineBottleStockFacade.removeBottlesFromStock(id, bottleCount));
	}

	@Operation(summary = "Remove wine bottles from stock by wine bottle", description = "Subtracts the number of wine bottles from stock with the specified wine bottle.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottles were removed successfully from stock."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified wine bottle was not found.")
	})
	@PostMapping("/wine-bottles-in-stock/wineBottle/remove-bottles")
	public ResponseEntity<WineBottleStockDto> removeWineBottlesFromStockByWineBottle(@RequestBody WineBottleModifyCountRequest wineBottleModifyCountRequest) {
		LOGGER.info("Subtracting bottles (count: {}) to wineBottleStock with wineBottle: {}", wineBottleModifyCountRequest.getWineBottle(), wineBottleModifyCountRequest.getBottleCount());
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.findByWineBottle(wineBottleModifyCountRequest.getWineBottle());
		return ResponseEntity.ok(wineBottleStockFacade.removeBottlesFromStock(wineBottleStock.getId(), wineBottleModifyCountRequest.getBottleCount()));
	}

	@Operation(summary = "Delete a wine bottle in stock by ID", description = "Deletes a wine bottle in stock with the specified ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "204", description = "Wine bottle in stock with the specified ID deleted successfully."),
			@ApiResponse(responseCode = "404", description = "Wine bottle in stock with the specified ID was not found.")
	})
	@DeleteMapping("/wine-bottles-in-stock/{id}")
	public ResponseEntity<WineBottleStockDto> deleteWineBottleStockById(@PathVariable(value = "id") Long id) {
		LOGGER.info("Deleting wineBottleStock with ID: {}", id);
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.deleteById(id);
		return ResponseEntity.ok(wineBottleStock);
	}

	@Operation(summary = "Delete a wine bottle stock by wine bottle", description = "Deletes a wine bottle stock with the specified wine bottle.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "204", description = "Wine bottle stock with the specified wine bottle deleted successfully."),
			@ApiResponse(responseCode = "404", description = "Wine bottle stock with the specified wine bottle was not found.")
	})
	@DeleteMapping("/wine-bottles-in-stock/wineBottle")
	public ResponseEntity<WineBottleStockDto> deleteWineBottleStockByWineBottle(@RequestBody WineBottle wineBottle) {
		LOGGER.info("Deleting wineBottleStock with wineBottle: {}", wineBottle);
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.deleteByWineBottle(wineBottle);
		return ResponseEntity.ok(wineBottleStock);
	}

	@Operation(summary = "Fill wine from barrel into wine bottles", description = "Fills wine from barrel into wine bottles")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Wine bottles were filled successfully and added to stock."),
			@ApiResponse(responseCode = "400", description = "Invalid input."),
			@ApiResponse(responseCode = "404", description = "Not enough wine for bottling in barrel.")
	})
	@PostMapping("/wine-bottles-in-stock/fill-wine-bottles")
	public ResponseEntity<WineBottleStockDto> fillBottles(@Valid @RequestBody WineBottleFromBarrelFillRequest wineBottleFromBarrelFillRequest){
		LOGGER.info("Filling {} wineBottles with bottleVolume: {} from barrel with grapeType: {} and harvestYear: {}", wineBottleFromBarrelFillRequest.getBottleCount(), wineBottleFromBarrelFillRequest.getBottleVolume(), wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
		WineBottleStockDto wineBottleStock = wineBottleStockFacade.fillBottles(wineBottleFromBarrelFillRequest);
		return ResponseEntity.ok(wineBottleStock);
	}

	//prepared for next milestone
//	@Operation(summary = "Buy wine bottles from stock", description = "User buys wine bottles from stock.")
//	@ApiResponses(value = {
//			@ApiResponse(responseCode = "200", description = "Wine bottles were successfully bought."),
//			@ApiResponse(responseCode = "400", description = "Invalid input."),
//			@ApiResponse(responseCode = "404", description = "Specified wine bottles are not available in stock.")
//	})
//	@PostMapping("/wine-bottles-in-stock/wineBottle/buy-wine-bottles")
//	public ResponseEntity<?> buyWineBottlesFromStock(@RequestBody Map<WineBottle,Integer> wineItems) {
//		for (Map.Entry<WineBottle, Integer> wineItem : wineItems.entrySet()){
//			try {
//				WineBottleStockDto wineBottleStock = wineBottleStockFacade.findByWineBottle(wineItem.getKey());
//				wineBottleStockFacade.removeBottlesFromStock(wineBottleStock.getId(), wineItem.getValue());
//			}
//			catch (Exception ex){
//				return ResponseEntity.notFound().build();
//			}
//		}
//		return ResponseEntity.ok().build();
//	}
}
