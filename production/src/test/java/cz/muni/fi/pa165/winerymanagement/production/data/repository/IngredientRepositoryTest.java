package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.ProductionApplication;
import cz.muni.fi.pa165.winerymanagement.production.data.model.Ingredient;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ProductionApplication.class)
@Transactional
class IngredientRepositoryTest {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private IngredientRepository ingredientRepository;

	private Ingredient ingredient1;
	private Ingredient ingredient2;

	@BeforeEach
	void setUp() {
		ingredient1 = new Ingredient();
		ingredient1.setName("Sugar");
		ingredient1.setQuantity(new WeightQuantity(10, WeightQuantity.WeightUnit.KILOGRAM));

		ingredient2 = new Ingredient();
		ingredient2.setName("Salt");
		ingredient2.setQuantity(new WeightQuantity(20, WeightQuantity.WeightUnit.KILOGRAM));

		entityManager.persist(ingredient1);
		entityManager.persist(ingredient2);
		entityManager.flush();
	}

	@Test
	void findById() {
		Optional<Ingredient> found = ingredientRepository.findById(ingredient1.getId());
		assertThat(found).isPresent();
		assertThat(found.get()).isEqualTo(ingredient1);
	}

	@Test
	void findByName() {
		Optional<Ingredient> found = ingredientRepository.findByName(ingredient1.getName());
		assertThat(found).isPresent();
		assertThat(found.get()).isEqualTo(ingredient1);
	}

	@Test
	void findByNameNotFound() {
		Optional<Ingredient> found = ingredientRepository.findByName("NonExistingIngredient");
		assertThat(found).isNotPresent();
	}

	@Test
	void save() {
		Ingredient ingredient3 = new Ingredient();
		ingredient3.setName("Flour");
		ingredient3.setQuantity(new VolumeQuantity(30, VolumeQuantity.VolumeUnit.LITER));

		Ingredient saved = ingredientRepository.save(ingredient3);
		assertThat(saved.getId()).isNotNull();
		assertThat(saved.getName()).isEqualTo(ingredient3.getName());
		assertThat(saved.getQuantity()).isEqualTo(ingredient3.getQuantity());
	}

	@Test
	void deleteById() {
		ingredientRepository.deleteById(ingredient1.getId());
		Optional<Ingredient> found = ingredientRepository.findById(ingredient1.getId());
		assertThat(found).isNotPresent();
	}

	@Test
	void findAll() {
		Iterable<Ingredient> ingredients = ingredientRepository.findAll();
		assertThat(ingredients).contains(ingredient1, ingredient2);
	}
}