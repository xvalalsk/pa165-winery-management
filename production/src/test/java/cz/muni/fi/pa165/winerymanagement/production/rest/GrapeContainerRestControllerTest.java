package cz.muni.fi.pa165.winerymanagement.production.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.api.GrapeContainerDto;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers.GrapeContainerWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.facade.GrapeContainerFacade;
import cz.muni.fi.pa165.winerymanagement.production.rest.exceptionhandling.CustomRestGlobalExceptionHandling;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearGetRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearModifyQuantityRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearUpdateGrapeContainerRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(GrapeContainerRestController.class)
@Import(GrapeContainerRestController.class)
public class GrapeContainerRestControllerTest {

    String API_PATH = "/api/v1/production/grape-containers";
    WeightQuantity weightQuantity100KG = new WeightQuantity(100, WeightQuantity.WeightUnit.KILOGRAM);
    WeightQuantity weightQuantity500KG = new WeightQuantity(500, WeightQuantity.WeightUnit.KILOGRAM);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private GrapeContainerFacade grapeContainerFacade;
    private GrapeContainerDto grapeContainerDto1;
    private GrapeContainerDto grapeContainerDto2;
    private GrapeType grapeType1 = new GrapeType();
    private GrapeType grapeType2 = new GrapeType();

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new GrapeContainerRestController(grapeContainerFacade)).setControllerAdvice(CustomRestGlobalExceptionHandling.class).build();

        grapeType1.setName("Chardonnay");
        grapeType1.setDescription("Chardonnay white sweet wine description");
        grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType1.setColor(GrapeType.Color.WHITE);

        grapeType2.setName("Zweigeltrebe");
        grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
        grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
        grapeType2.setColor(GrapeType.Color.ROSE);

        grapeContainerDto1 = new GrapeContainerDto();
        grapeContainerDto1.setGrapeType(grapeType1);
        grapeContainerDto1.setId(1L);
        grapeContainerDto1.setQuantity(weightQuantity100KG);
        grapeContainerDto1.setHarvestYear((2011));

        grapeContainerDto2 = new GrapeContainerDto();
        grapeContainerDto2.setGrapeType(grapeType2);
        grapeContainerDto2.setId(2L);
        grapeContainerDto2.setQuantity(weightQuantity500KG);
        grapeContainerDto2.setHarvestYear((2022));
    }

    @Test
    void testGetAllGrapeContainers() throws Exception {
        List<GrapeContainerDto> grapeContainers = Arrays.asList(grapeContainerDto1, grapeContainerDto2);

        when(grapeContainerFacade.findAll()).thenReturn(grapeContainers);

        mockMvc.perform(get(API_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("$[0].grapeType.color", is("WHITE")))
                .andExpect(jsonPath("$[0].grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("$[0].quantity.value", is(100.0)))
                .andExpect(jsonPath("$[0].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("$[0].harvestYear", is(2011)))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].grapeType.name", is("Zweigeltrebe")))
                .andExpect(jsonPath("$[1].grapeType.color", is("ROSE")))
                .andExpect(jsonPath("$[1].grapeType.sweetness", is("SEMISWEET")))
                .andExpect(jsonPath("$[1].quantity.value", is(500.0)))
                .andExpect(jsonPath("$[1].quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("$[1].harvestYear", is(2022)));

        verify(grapeContainerFacade).findAll();
    }

    @Test
    void testGetGrapeContainerById_1() throws Exception {
        Long id = grapeContainerDto1.getId();

        when(grapeContainerFacade.findById(id)).thenReturn(grapeContainerDto1);

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));

        verify(grapeContainerFacade).findById(id);
    }

    @Test
    void testGetGrapeContainerById_NotFound() throws Exception {
        Long id = 1L;

        when(grapeContainerFacade.findById(id)).thenThrow(new GrapeContainerWithIdNotFoundException(id));

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetGrapeContainerByTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setHarvestYear((2011));
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);

        when(grapeContainerFacade.findByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(get(API_PATH + "/grapeType-and-year")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearGetRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testCreateGrapeContainer_Created() throws Exception {
        when(grapeContainerFacade.add(ArgumentMatchers.any())).thenReturn(grapeContainerDto1);

        mockMvc.perform(post(API_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeContainerDto1)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(grapeContainerDto1.getId().intValue())))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testUpdateGrapeContainerById() throws Exception {
        Long id = grapeContainerDto1.getId();
        grapeContainerDto1.setHarvestYear((9999));

        when(grapeContainerFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any())).thenReturn(grapeContainerDto1);

        String requestBody = new ObjectMapper().writeValueAsString(grapeContainerDto1);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(9999)));
    }

    @Test
    void testUpdateGrapeContainerByTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setHarvestYear((2011));
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);

        grapeContainerDto1.setHarvestYear((9999));

        GrapeTypeAndYearUpdateGrapeContainerRequest grapeTypeAndYearUpdateGrapeContainerRequest = new GrapeTypeAndYearUpdateGrapeContainerRequest();
        grapeTypeAndYearUpdateGrapeContainerRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);
        grapeTypeAndYearUpdateGrapeContainerRequest.setUpdatedGrapeContainer(grapeContainerDto1);

        when(grapeContainerFacade.updateByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class), ArgumentMatchers.any(GrapeContainerDto.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(put(API_PATH + "/grapeType-and-year")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearUpdateGrapeContainerRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(9999)));
    }

    @Test
    void testUpdateGrapeContainerById_NotFound() throws Exception {
        Long id = grapeContainerDto2.getId();
        grapeContainerDto2.setHarvestYear((8888));

        when(grapeContainerFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any())).thenThrow(new GrapeContainerWithIdNotFoundException(id));

        String requestBody = new ObjectMapper().writeValueAsString(grapeContainerDto2);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddQuantityToGrapeContainerById() throws Exception {
        Quantity quantity = new WeightQuantity(50, WeightQuantity.WeightUnit.KILOGRAM);

        Long id = grapeContainerDto1.getId();
        grapeContainerDto1.setQuantity(new WeightQuantity(150, WeightQuantity.WeightUnit.KILOGRAM));

        when(grapeContainerFacade.findById(id)).thenReturn(grapeContainerDto1);
        when(grapeContainerFacade.addQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(post(API_PATH + "/{id}/add-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(quantity)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(150.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));

    }

    @Test
    void testAddQuantityToGrapeContainerByGrapeTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setHarvestYear((2011));
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);

        GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest = new GrapeTypeAndYearModifyQuantityRequest();
        grapeTypeAndYearModifyQuantityRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);
        grapeTypeAndYearModifyQuantityRequest.setQuantity(new WeightQuantity(50, WeightQuantity.WeightUnit.KILOGRAM));

        grapeContainerDto1.setQuantity(new WeightQuantity(150, WeightQuantity.WeightUnit.KILOGRAM));

        when(grapeContainerFacade.findByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(grapeContainerDto1);
        when(grapeContainerFacade.addQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(post(API_PATH + "/grapeType-and-year/add-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearModifyQuantityRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(150.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testSubtractQuantityToGrapeContainerById() throws Exception {
        Quantity quantity = new WeightQuantity(50, WeightQuantity.WeightUnit.KILOGRAM);

        Long id = grapeContainerDto1.getId();
        grapeContainerDto1.setQuantity(new WeightQuantity(50, WeightQuantity.WeightUnit.KILOGRAM));

        when(grapeContainerFacade.findById(id)).thenReturn(grapeContainerDto1);
        when(grapeContainerFacade.subtractQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(post(API_PATH + "/{id}/subtract-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(quantity)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(50.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));

    }

    @Test
    void testSubtractQuantityToGrapeContainerByGrapeTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setHarvestYear((2011));
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);

        GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest = new GrapeTypeAndYearModifyQuantityRequest();
        grapeTypeAndYearModifyQuantityRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);
        grapeTypeAndYearModifyQuantityRequest.setQuantity(new WeightQuantity(50, WeightQuantity.WeightUnit.KILOGRAM));

        grapeContainerDto1.setQuantity(new WeightQuantity(50, WeightQuantity.WeightUnit.KILOGRAM));

        when(grapeContainerFacade.findByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(grapeContainerDto1);
        when(grapeContainerFacade.subtractQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(post(API_PATH + "/grapeType-and-year/subtract-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearModifyQuantityRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(50.0)))
                .andExpect(jsonPath("quantity.weightUnit", is("KILOGRAM")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }


    @Test
    void testDeleteGrapeContainerById() throws Exception {
        Long id = 1L;
        when(grapeContainerFacade.deleteById(id)).thenReturn(grapeContainerDto1);

        mockMvc.perform(delete(API_PATH + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteGrapeContainerById_NotFound() throws Exception {
        Long id = 1L;
        when(grapeContainerFacade.deleteById(id)).thenThrow(new GrapeContainerWithIdNotFoundException(id));

        mockMvc.perform(delete(API_PATH + "/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testDeleteGrapeContainerByGrapeTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setHarvestYear((2011));
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);

        when(grapeContainerFacade.deleteByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(grapeContainerDto1);

        mockMvc.perform(delete(API_PATH + "/grapeType-and-year")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearGetRequest)))
                .andExpect(status().isOk());
    }
}
