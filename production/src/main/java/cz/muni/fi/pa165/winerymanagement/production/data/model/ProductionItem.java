package cz.muni.fi.pa165.winerymanagement.production.data.model;

import cz.muni.fi.pa165.winerymanagement.core.converter.QuantityJsonConverter;
import cz.muni.fi.pa165.winerymanagement.core.unit.WeightQuantity;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.GrapeContainerInfo;
import cz.muni.fi.pa165.winerymanagement.production.data.support_model.IngredientRequirement;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * ProductionItem entity class representing a production with information about
 * its grape container information, quantity, list of ingredients and production year.
 */
@Entity
@Table(name = "production_item")
public class ProductionItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_production_item")
    private Long id;
    @Embedded
    private GrapeContainerInfo grape;
    @Column(name = "quantity", columnDefinition = "TEXT")
    @Convert(converter = QuantityJsonConverter.class)
    private WeightQuantity grapeQuantity;
    @Column(name = "production_year")
    private int year;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "production_item_id")
    private List<IngredientRequirement> ingredients;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrapeContainerInfo getGrape() {
        return grape;
    }

    public void setGrape(GrapeContainerInfo grape) {
        this.grape = grape;
    }

    public WeightQuantity getGrapeQuantity() {
        return grapeQuantity;
    }

    public void setGrapeQuantity(WeightQuantity grapeQuantity) {
        this.grapeQuantity = grapeQuantity;
    }

    public List<IngredientRequirement> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientRequirement> ingredients) {
        this.ingredients = ingredients;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductionItem that)) return false;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "ProductionItem{" +
                "id=" + id +
                ", grape=" + grape +
                ", grapeQuantity=" + grapeQuantity +
                ", ingredients=" + ingredients +
                ", year=" + year +
                '}';
    }
}
