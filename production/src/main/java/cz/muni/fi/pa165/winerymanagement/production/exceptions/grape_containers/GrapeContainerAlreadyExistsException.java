package cz.muni.fi.pa165.winerymanagement.production.exceptions.grape_containers;

import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.ResourceAlreadyExistsException;

import java.util.ResourceBundle;

public class GrapeContainerAlreadyExistsException extends ResourceAlreadyExistsException {

    private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
    private final GrapeType grapeType;
    private final int harvestYear;

    public GrapeContainerAlreadyExistsException(GrapeType grapeType, int harvestYear) {
        this.grapeType = grapeType;
        this.harvestYear = harvestYear;
    }

    @Override
    public String getMessage() {
        String key = getClass().getSimpleName() + ".message";
        String message = messages.getString(key);
        return String.format(message, grapeType, harvestYear);
    }
}
