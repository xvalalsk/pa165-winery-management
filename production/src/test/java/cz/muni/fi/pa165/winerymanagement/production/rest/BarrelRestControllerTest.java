package cz.muni.fi.pa165.winerymanagement.production.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.production.api.BarrelDto;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import cz.muni.fi.pa165.winerymanagement.production.exceptions.barrels.BarrelWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.production.facade.BarrelFacade;
import cz.muni.fi.pa165.winerymanagement.production.rest.exceptionhandling.CustomRestGlobalExceptionHandling;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearGetRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearModifyQuantityRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearUpdateBarrelRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(BarrelRestController.class)
@Import(BarrelRestController.class)
public class BarrelRestControllerTest {

    String API_PATH = "/api/v1/production/barrels";
    VolumeQuantity volumeQuantity100L = new VolumeQuantity(100, VolumeQuantity.VolumeUnit.LITER);
    VolumeQuantity volumeQuantity500L = new VolumeQuantity(500, VolumeQuantity.VolumeUnit.LITER);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private BarrelFacade barrelFacade;
    private BarrelDto barrelDto1;
    private BarrelDto barrelDto2;
    private GrapeType grapeType1 = new GrapeType();
    private GrapeType grapeType2 = new GrapeType();

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new BarrelRestController(barrelFacade)).setControllerAdvice(CustomRestGlobalExceptionHandling.class).build();

        grapeType1.setName("Chardonnay");
        grapeType1.setDescription("Chardonnay white sweet wine description");
        grapeType1.setSweetness(GrapeType.Sweetness.SWEET);
        grapeType1.setColor(GrapeType.Color.WHITE);

        grapeType2.setName("Zweigeltrebe");
        grapeType2.setDescription("Zweigeltrebe rose semi-sweet wine description");
        grapeType2.setSweetness(GrapeType.Sweetness.SEMISWEET);
        grapeType2.setColor(GrapeType.Color.ROSE);

        barrelDto1 = new BarrelDto();
        barrelDto1.setId(1L);
        barrelDto1.setGrapeType(grapeType1);
        barrelDto1.setQuantity(volumeQuantity100L);
        barrelDto1.setHarvestYear((2011));

        barrelDto2 = new BarrelDto();
        barrelDto2.setId(2L);
        barrelDto2.setGrapeType(grapeType2);
        barrelDto2.setQuantity(volumeQuantity500L);
        barrelDto2.setHarvestYear((2022));
    }

    @Test
    void testGetAllBarrels() throws Exception {
        List<BarrelDto> barrels = Arrays.asList(barrelDto1, barrelDto2);

        when(barrelFacade.findAll()).thenReturn(barrels);

        mockMvc.perform(get(API_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("$[0].grapeType.color", is("WHITE")))
                .andExpect(jsonPath("$[0].grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("$[0].quantity.value", is(100.0)))
                .andExpect(jsonPath("$[0].quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("$[0].harvestYear", is(2011)))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].grapeType.name", is("Zweigeltrebe")))
                .andExpect(jsonPath("$[1].grapeType.color", is("ROSE")))
                .andExpect(jsonPath("$[1].grapeType.sweetness", is("SEMISWEET")))
                .andExpect(jsonPath("$[1].quantity.value", is(500.0)))
                .andExpect(jsonPath("$[1].quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("$[1].harvestYear", is(2022)));

        verify(barrelFacade).findAll();
    }

    @Test
    void testGetBarrelById_1() throws Exception {
        Long id = barrelDto1.getId();

        when(barrelFacade.findById(id)).thenReturn(barrelDto1);

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));

        verify(barrelFacade).findById(id);
    }

    @Test
    void testGetBarrelById_NotFound() throws Exception {
        Long id = 1L;

        when(barrelFacade.findById(id)).thenThrow(new BarrelWithIdNotFoundException(id));

        mockMvc.perform(get(API_PATH + "/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetBarrelByTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);
        grapeTypeAndYearGetRequest.setHarvestYear((2011));

        when(barrelFacade.findByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(barrelDto1);

        mockMvc.perform(post(API_PATH + "/grapeType-and-year")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearGetRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));

        verify(barrelFacade).findByGrapeTypeAndHarvestYear(grapeType1, (2011));
    }

    @Test
    void testCreateBarrel_Created() throws Exception {
        when(barrelFacade.add(ArgumentMatchers.any())).thenReturn(barrelDto1);

        mockMvc.perform(post(API_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(barrelDto1)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(barrelDto1.getId().intValue())))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testUpdateBarrelById() throws Exception {
        Long id = barrelDto1.getId();
        barrelDto1.setHarvestYear((9999));

        when(barrelFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any())).thenReturn(barrelDto1);

        String requestBody = new ObjectMapper().writeValueAsString(barrelDto1);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(id.intValue())))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(9999)));
    }

    @Test
    void testUpdateBarrelByTypeAndYear() throws Exception {
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);
        grapeTypeAndYearGetRequest.setHarvestYear((2011));

        GrapeTypeAndYearUpdateBarrelRequest grapeTypeAndYearUpdateBarrelRequest = new GrapeTypeAndYearUpdateBarrelRequest();
        grapeTypeAndYearUpdateBarrelRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);
        grapeTypeAndYearUpdateBarrelRequest.setUpdatedBarrel(barrelDto1);

        when(barrelFacade.updateByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class), ArgumentMatchers.any(BarrelDto.class))).thenReturn(barrelDto1);

        barrelDto1.setHarvestYear((9999));

        mockMvc.perform(put(API_PATH + "/grapeType-and-year")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearUpdateBarrelRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(100.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(9999)));
    }

    @Test
    void testUpdateBarrelById_NotFound() throws Exception {
        Long id = barrelDto2.getId();
        barrelDto2.setHarvestYear((8888));

        when(barrelFacade.updateById(ArgumentMatchers.any(Long.class), ArgumentMatchers.any())).thenThrow(new BarrelWithIdNotFoundException(id));

        String requestBody = new ObjectMapper().writeValueAsString(barrelDto2);

        mockMvc.perform(put(API_PATH + "/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddQuantityByID() throws Exception {
        Quantity quantity = new VolumeQuantity(5, VolumeQuantity.VolumeUnit.LITER);

        when(barrelFacade.findById(ArgumentMatchers.any(Long.class))).thenReturn(barrelDto1);

        barrelDto1.setQuantity(new VolumeQuantity(105, VolumeQuantity.VolumeUnit.LITER));
        when(barrelFacade.addQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(barrelDto1);

        String requestBody = new ObjectMapper().writeValueAsString(quantity);

        mockMvc.perform(post(API_PATH + "/{id}/add-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(105.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testSubtractQuantityByID() throws Exception {
        Quantity quantity = new VolumeQuantity(5, VolumeQuantity.VolumeUnit.LITER);

        when(barrelFacade.findById(ArgumentMatchers.any(Long.class))).thenReturn(barrelDto1);

        barrelDto1.setQuantity(new VolumeQuantity(95, VolumeQuantity.VolumeUnit.LITER));
        when(barrelFacade.subtractQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(barrelDto1);

        String requestBody = new ObjectMapper().writeValueAsString(quantity);

        mockMvc.perform(post(API_PATH + "/{id}/subtract-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(95.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testAddQuantityByTypeAndYear() throws Exception {
        Quantity quantity = new VolumeQuantity(5, VolumeQuantity.VolumeUnit.LITER);
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);
        grapeTypeAndYearGetRequest.setHarvestYear((2011));

        GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest = new GrapeTypeAndYearModifyQuantityRequest();
        grapeTypeAndYearModifyQuantityRequest.setQuantity(quantity);
        grapeTypeAndYearModifyQuantityRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);

        when(barrelFacade.findByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(barrelDto1);

        barrelDto1.setQuantity(new VolumeQuantity(105, VolumeQuantity.VolumeUnit.LITER));
        when(barrelFacade.addQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(barrelDto1);

        String requestBody = new ObjectMapper().writeValueAsString(grapeTypeAndYearModifyQuantityRequest);

        mockMvc.perform(post(API_PATH + "/grapeType-and-year/add-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(105.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testSubtractQuantityByTypeAndYear() throws Exception {
        Quantity quantity = new VolumeQuantity(5, VolumeQuantity.VolumeUnit.LITER);
        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);
        grapeTypeAndYearGetRequest.setHarvestYear((2011));

        GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest = new GrapeTypeAndYearModifyQuantityRequest();
        grapeTypeAndYearModifyQuantityRequest.setQuantity(quantity);
        grapeTypeAndYearModifyQuantityRequest.setGrapeTypeAndYearGetRequest(grapeTypeAndYearGetRequest);

        when(barrelFacade.findByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(barrelDto1);

        barrelDto1.setQuantity(new VolumeQuantity(95, VolumeQuantity.VolumeUnit.LITER));
        when(barrelFacade.subtractQuantity(ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Quantity.class))).thenReturn(barrelDto1);

        String requestBody = new ObjectMapper().writeValueAsString(grapeTypeAndYearModifyQuantityRequest);

        mockMvc.perform(post(API_PATH + "/grapeType-and-year/subtract-quantity", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(1)))
                .andExpect(jsonPath("grapeType.name", is("Chardonnay")))
                .andExpect(jsonPath("grapeType.color", is("WHITE")))
                .andExpect(jsonPath("grapeType.sweetness", is("SWEET")))
                .andExpect(jsonPath("quantity.value", is(95.0)))
                .andExpect(jsonPath("quantity.volumeUnit", is("LITER")))
                .andExpect(jsonPath("harvestYear", is(2011)));
    }

    @Test
    void testDeleteBarrelById() throws Exception {
        Long id = 1L;
        when(barrelFacade.deleteById(id)).thenReturn(barrelDto1);

        mockMvc.perform(delete(API_PATH + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteBarrelById_NotFound() throws Exception {
        Long id = 1L;
        when(barrelFacade.deleteById(id)).thenThrow(new BarrelWithIdNotFoundException(id));

        mockMvc.perform(delete(API_PATH + "/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testDeleteByGrapeTypeAndHarvestYear() throws Exception {

        GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest = new GrapeTypeAndYearGetRequest();
        grapeTypeAndYearGetRequest.setGrapeType(grapeType1);
        grapeTypeAndYearGetRequest.setHarvestYear((2011));

        when(barrelFacade.deleteByGrapeTypeAndHarvestYear(ArgumentMatchers.any(GrapeType.class), ArgumentMatchers.any(int.class))).thenReturn(barrelDto1);

        mockMvc.perform(delete(API_PATH + "/grapeType-and-year")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(grapeTypeAndYearGetRequest)))
                .andExpect(status().isOk());

    }

}
