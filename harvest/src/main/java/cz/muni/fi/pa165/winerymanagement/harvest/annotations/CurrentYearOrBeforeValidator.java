package cz.muni.fi.pa165.winerymanagement.harvest.annotations;

import jakarta.validation.ConstraintValidator;

import jakarta.validation.ConstraintValidatorContext;
import java.time.Year;

public class CurrentYearOrBeforeValidator implements ConstraintValidator<CurrentYearOrBefore, Integer> {
    @Override
    public void initialize(CurrentYearOrBefore constraintAnnotation) {
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return value <= Year.now().getValue();
    }
}
