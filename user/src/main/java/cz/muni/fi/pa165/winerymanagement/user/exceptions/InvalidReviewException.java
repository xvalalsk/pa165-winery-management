package cz.muni.fi.pa165.winerymanagement.user.exceptions;

public class InvalidReviewException extends RuntimeException{
    public InvalidReviewException(String message) {
        super(message);
    }
}
