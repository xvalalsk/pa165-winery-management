package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.production.data.model.GrapeContainer;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrapeContainerRepository extends JpaRepository<GrapeContainer, Long> {
    GrapeContainer findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear);
}
