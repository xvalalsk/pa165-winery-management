package cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock;

import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.ResourceNotFoundException;

import java.util.ResourceBundle;

public class WineBottleStockWithWineBottleNotFoundException extends ResourceNotFoundException {

	private static final ResourceBundle messages = ResourceBundle.getBundle("messages");
	private final WineBottle wineBottle;

	public WineBottleStockWithWineBottleNotFoundException(WineBottle wineBottle) {
		this.wineBottle = wineBottle;
	}

	@Override
	public String getMessage() {
		String key = getClass().getSimpleName() + ".message";
		String message = messages.getString(key);
		return String.format(message, wineBottle.toString());
	}

}