package cz.muni.fi.pa165.winerymanagement.production.rest;


import cz.muni.fi.pa165.winerymanagement.production.config.InsertInitialDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/production")
public class DataController {

    private final InsertInitialDataService insertInitialDataService;

    @Autowired
    public DataController(InsertInitialDataService insertInitialDataService) {
        this.insertInitialDataService = insertInitialDataService;
    }

    @PostMapping("/seed")
    public void seedData() {
        insertInitialDataService.insertDummyData();
    }

    @PostMapping("/clear")
    public void clearData() {
        insertInitialDataService.deleteAllData();
    }
}
