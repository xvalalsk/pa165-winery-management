package cz.muni.fi.pa165.winerymanagement.confidentialclient.rest.requests;

public class UserRegistrationRequest {
    private String issuer;

    private String externalId;

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}

