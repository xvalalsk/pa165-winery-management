package cz.muni.fi.pa165.winerymanagement.production.data.repository;

import cz.muni.fi.pa165.winerymanagement.production.data.model.Barrel;
import cz.muni.fi.pa165.winerymanagement.core.model.GrapeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarrelRepository extends JpaRepository<Barrel, Long> {
    Barrel findByGrapeTypeAndHarvestYear(GrapeType grapeType, int harvestYear);
}
