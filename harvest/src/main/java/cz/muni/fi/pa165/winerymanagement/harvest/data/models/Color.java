package cz.muni.fi.pa165.winerymanagement.harvest.data.models;

public enum Color {
    WHITE("white"),
    RED("red"),

    ROSE("rose");

    private final String wineColor;

    Color(String wineColor) {
        this.wineColor = wineColor;
    }

    public String getColor() {
        return wineColor;
    }
}
