package cz.muni.fi.pa165.winerymanagement.store.facade;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleStockDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.mappers.WineBottleStockMapper;
import cz.muni.fi.pa165.winerymanagement.store.rest.requests.WineBottleFromBarrelFillRequest;
import cz.muni.fi.pa165.winerymanagement.store.service.WineBottleStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WineBottleStockFacade {
	private final WineBottleStockService wineBottleStockService;
	private final WineBottleStockMapper wineBottleStockMapper;

	@Autowired
	public WineBottleStockFacade(WineBottleStockService wineBottleStockService, WineBottleStockMapper wineBottleStockMapper) {
		this.wineBottleStockService = wineBottleStockService;
		this.wineBottleStockMapper = wineBottleStockMapper;
	}

	@Transactional(readOnly = true)
	public List<WineBottleStockDto> findAll() {
		return wineBottleStockMapper.mapToList(wineBottleStockService.findAll());
	}

	@Transactional(readOnly = true)
	public WineBottleStockDto findById(Long id) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.findById(id));
	}

	@Transactional(readOnly = true)
	public WineBottleStockDto findByWineBottle(WineBottle wineBottle) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.findByWineBottle(wineBottle));
	}

	public WineBottleStockDto add(WineBottleStockDto newWineBottleStock) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.add(wineBottleStockMapper.mapFromDto(newWineBottleStock)));
	}

	public WineBottleStockDto updateById(Long id, WineBottleStockDto updatedWineBottleStock) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.updateById(id, wineBottleStockMapper.mapFromDto(updatedWineBottleStock)));
	}

	public WineBottleStockDto updateByWineBottle(WineBottle wineBottle, WineBottleStockDto updatedWineBottleStock) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.updateByWineBottle(wineBottle, wineBottleStockMapper.mapFromDto(updatedWineBottleStock)));
	}

	public WineBottleStockDto addBottlesToStock(Long id, int bottleCount) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.addBottlesToStock(id, bottleCount));
	}

	public WineBottleStockDto removeBottlesFromStock(Long id, int bottleCount) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.removeBottlesFromStock(id, bottleCount));
	}

	public WineBottleStockDto deleteById(Long id) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.deleteById(id));
	}

	public WineBottleStockDto deleteByWineBottle(WineBottle wineBottle){
		return wineBottleStockMapper.mapToDto(wineBottleStockService.deleteByWineBottle(wineBottle));
	}

	public WineBottleStockDto fillBottles(WineBottleFromBarrelFillRequest wineBottleFromBarrelFillRequest) {
		return wineBottleStockMapper.mapToDto(wineBottleStockService.fillBottles(wineBottleFromBarrelFillRequest));
	}
}
