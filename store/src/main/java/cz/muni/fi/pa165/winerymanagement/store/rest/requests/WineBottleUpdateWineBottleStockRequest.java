package cz.muni.fi.pa165.winerymanagement.store.rest.requests;

import cz.muni.fi.pa165.winerymanagement.store.api.WineBottleStockDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
		name = "WineBottleUpdateWineBottleStockRequest",
		description = "Data transfer object representing a WineBottleStock and WineBottle used in updating a WineBottleStock",
		example = """
                  {
                    "wineBottle": {
						"id": 1,
						"grapeType": {
						  "name": "Chardonnay2",
						  "description": "Chardonnay2 white sweet wine description",
						  "color": "WHITE",
						  "sweetness": "SWEET"
						},
						"harvestYear": 2021,
						"quantity": {
						  "type": "volume",
						  "value": 0.75,
						  "volumeUnit": "LITER"
						}
					},
                    "wineBottleStock": {
						"id": 1,
						"wineBottle": {
							"id": 1,
							"grapeType": {
							  "name": "Chardonnay",
							  "description": "Chardonnay white sweet wine description",
							  "color": "WHITE",
							  "sweetness": "SWEET"
							},
							"harvestYear": 2020,
							"quantity": {
							  "type": "volume",
							  "value": 0.75,
							  "volumeUnit": "LITER"
							}
						},
						"count": 250,
						"price": 8.99
				  	}
                  }
                  """
)
public class WineBottleUpdateWineBottleStockRequest {
	private WineBottle wineBottle;
	private WineBottleStockDto wineBottleStock;

	public WineBottle getWineBottle() {
		return wineBottle;
	}

	public void setWineBottle(WineBottle wineBottle) {
		this.wineBottle = wineBottle;
	}

	public WineBottleStockDto getWineBottleStock() {
		return wineBottleStock;
	}

	public void setWineBottleStock(WineBottleStockDto wineBottleStock) {
		this.wineBottleStock = wineBottleStock;
	}
}
