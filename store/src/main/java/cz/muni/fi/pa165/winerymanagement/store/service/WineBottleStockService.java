package cz.muni.fi.pa165.winerymanagement.store.service;

import cz.muni.fi.pa165.winerymanagement.core.model.ModuleEnum;
import cz.muni.fi.pa165.winerymanagement.core.service.ModuleUrlService;
import cz.muni.fi.pa165.winerymanagement.core.unit.VolumeQuantity;
import cz.muni.fi.pa165.winerymanagement.store.api.BarrelDto;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottle;
import cz.muni.fi.pa165.winerymanagement.store.data.model.WineBottleStock;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleRepository;
import cz.muni.fi.pa165.winerymanagement.store.data.repository.WineBottleStockRepository;
import cz.muni.fi.pa165.winerymanagement.store.data.support_model.GrapeContainerInfo;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.barrel.BarrelCouldNotBeFetchedException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.barrel.BarrelCouldNotBeSubtractedException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.barrel.BarrelQuantityNotAvailableException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.bottle.BottleCountMustBePositiveException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.NotEnoughWineBottlesException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle.WineBottleWithGrapeTypeHarvestYearVolumeNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockAlreadyExistsException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockWithIdNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.exceptions.wine_bottle_stock.WineBottleStockWithWineBottleNotFoundException;
import cz.muni.fi.pa165.winerymanagement.store.rest.requests.WineBottleFromBarrelFillRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Transactional
public class WineBottleStockService {
	private static final Logger LOGGER = LoggerFactory.getLogger(WineBottleStockService.class);
	private final WineBottleStockRepository wineBottleStockRepository;
	private final WineBottleRepository wineBottleRepository;

	@Autowired
	public WineBottleStockService(WineBottleStockRepository wineBottleStockRepository, WineBottleRepository wineBottleRepository) {
		this.wineBottleStockRepository = wineBottleStockRepository;
		this.wineBottleRepository = wineBottleRepository;
	}

	@Transactional(readOnly = true)
	public List<WineBottleStock> findAll() {
		LOGGER.info("Retrieving all wineBottleStocks from repository.");
		return wineBottleStockRepository.findAll();
	}

	@Transactional(readOnly = true)
	public WineBottleStock findById(Long id) {
		LOGGER.info("Retrieving wineBottleStock with id: {} from repository.", id);
		return wineBottleStockRepository.findById(id).orElseThrow(() -> new WineBottleStockWithIdNotFoundException(id));
	}

	@Transactional(readOnly = true)
	public WineBottleStock findByWineBottle(WineBottle wineBottle) {
		LOGGER.info("Retrieving all wineBottleStock with wineBottle: {} from repository.", wineBottle);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findByWineBottle(wineBottle);
		if (wineBottleStock == null) {
			LOGGER.warn("WineBottleStock with wineBottle: {} wasn't found in repository.", wineBottle);
			throw new WineBottleStockWithWineBottleNotFoundException(wineBottle);
		}
		return wineBottleStock;
	}

	public WineBottleStock add(WineBottleStock newWineBottleStock) {
		LOGGER.info("Adding new wineBottleStock: {} to repository", newWineBottleStock);
		WineBottle wineBottle = wineBottleRepository.findByGrapeTypeAndHarvestYear(newWineBottleStock.getWineBottle().getGrapeType(),newWineBottleStock.getWineBottle().getHarvestYear());
		if (wineBottle == null) {
			wineBottle = wineBottleRepository.save(newWineBottleStock.getWineBottle());
		}
		newWineBottleStock.setWineBottle(wineBottle);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findByWineBottle(newWineBottleStock.getWineBottle());
		if (wineBottleStock != null) {
			LOGGER.warn("WineBottleStock with wineBottle: {} already exists in the repository with id: {}!", newWineBottleStock.getWineBottle(), wineBottleStock.getId());
			throw new WineBottleStockAlreadyExistsException(newWineBottleStock.getWineBottle());
		}
		return wineBottleStockRepository.save(newWineBottleStock);
	}

	public WineBottleStock updateById(Long id, WineBottleStock updatedWineBottleStock) {
		LOGGER.info("Updating wineBottleStock with id: {} from repository with following wineBottleStock: {}.", id, updatedWineBottleStock);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findById(id).orElseThrow(() -> new WineBottleStockWithIdNotFoundException(id));
		wineBottleStock.setWineBottle(updatedWineBottleStock.getWineBottle());
		wineBottleStock.setCount(updatedWineBottleStock.getCount());
		wineBottleStock.setPrice(updatedWineBottleStock.getPrice());
		return wineBottleStockRepository.save(wineBottleStock);
	}

	public WineBottleStock updateByWineBottle(WineBottle wineBottle, WineBottleStock updatedWineBottleStock) {
		LOGGER.info("Updating wineBottleStock with wineBottle: {} from repository with following wineBottleStock: {}.", wineBottle, updatedWineBottleStock);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findByWineBottle(wineBottle);
		if (wineBottleStock == null) {
			LOGGER.warn("WineBottleStock with wineBottle: {} wasn't found in repository!", wineBottle);
			throw new WineBottleStockWithWineBottleNotFoundException(wineBottle);
		}
		wineBottleStock.setWineBottle(updatedWineBottleStock.getWineBottle());
		wineBottleStock.setCount(updatedWineBottleStock.getCount());
		wineBottleStock.setPrice(updatedWineBottleStock.getPrice());
		return wineBottleStockRepository.save(wineBottleStock);
	}

	public WineBottleStock addBottlesToStock(Long id, int bottleCount) {
		LOGGER.info("Adding bottles (count: {}) to wineBottleStock with id: {}.", bottleCount, id);
		checkIfBottleCountPositive(bottleCount);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findById(id).orElseThrow(() -> new WineBottleStockWithIdNotFoundException(id));
		wineBottleStock.setCount(wineBottleStock.getCount() + bottleCount);
		return wineBottleStock;
	}

	public WineBottleStock removeBottlesFromStock(Long id, int bottleCount){
		LOGGER.info("Removing bottles (count: {}) from wineBottleStock with id: {}.", bottleCount, id);
		checkIfBottleCountPositive(bottleCount);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findById(id).orElseThrow(() -> new WineBottleStockWithIdNotFoundException(id));
		int newBottleCount = wineBottleStock.getCount() - bottleCount;
		if (newBottleCount < 0) {
			throw new NotEnoughWineBottlesException(wineBottleStock.getCount());
		}
		wineBottleStock.setCount(newBottleCount);
		return wineBottleStock;
	}

	public WineBottleStock deleteById(Long id) {
		LOGGER.info("Deleting wineBottleStock with id: {}.", id);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findById(id).orElseThrow(() -> new WineBottleStockWithIdNotFoundException(id));
		wineBottleStockRepository.deleteById(id);
		return wineBottleStock;
	}

	public WineBottleStock deleteByWineBottle(WineBottle wineBottle) {
		LOGGER.info("Deleting wineBottleStock with wineBottle: {}.", wineBottle);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findByWineBottle(wineBottle);
		if (wineBottleStock == null){
			throw new WineBottleStockWithWineBottleNotFoundException(wineBottle);
		}
		wineBottleStockRepository.deleteById(wineBottle.getId());
		return wineBottleStock;
	}

	private static void checkIfBottleCountPositive(int bottleCount) {
		if (bottleCount < 0) {
			throw new BottleCountMustBePositiveException(bottleCount);
		}
	}

	public WineBottleStock fillBottles(WineBottleFromBarrelFillRequest wineBottleFromBarrelFillRequest) {
		// Check if requested wineBottle exists
		LOGGER.info("Looking for wineBottle with grapeType: {}, harvestYear: {}, bottleVolume: {} ...", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), wineBottleFromBarrelFillRequest.getBottleVolume());
		WineBottle wineBottle = wineBottleRepository.findByGrapeTypeAndHarvestYearAndVolume(wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), wineBottleFromBarrelFillRequest.getBottleVolume());
		if (wineBottle == null) {
			LOGGER.info("WineBottle with grapeType: {}, harvestYear: {}, bottleVolume: {} not found. Creating one ...", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), wineBottleFromBarrelFillRequest.getBottleVolume());
			WineBottle newWineBottle = new WineBottle();
			newWineBottle.setGrapeType(wineBottleFromBarrelFillRequest.getGrapeType());
			newWineBottle.setHarvestYear(wineBottleFromBarrelFillRequest.getHarvestYear());
			newWineBottle.setVolume(wineBottleFromBarrelFillRequest.getBottleVolume());
			wineBottleRepository.save(newWineBottle);
			LOGGER.info("WineBottle with grapeType: {}, harvestYear: {}, bottleVolume: {} created!", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), wineBottleFromBarrelFillRequest.getBottleVolume());
		} else {
			LOGGER.info("WineBottle with grapeType: {}, harvestYear: {}, bottleVolume: {} found!", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), wineBottleFromBarrelFillRequest.getBottleVolume());
		}

		// Check if requested wineBottleStock exists
		LOGGER.info("Looking for wineBottleStock with wineBottle: {} ...", wineBottle);
		WineBottleStock wineBottleStock = wineBottleStockRepository.findByWineBottle(wineBottle);
		if (wineBottleStock == null) {
			LOGGER.warn("WineBottleStock with wineBottle {} doesn't exist", wineBottle);
			throw new WineBottleStockWithWineBottleNotFoundException(wineBottle);
		}
		LOGGER.info("WineBottleStock with wineBottle: {} found!", wineBottle);

		// Fetch barrel with desired grapeType and harvestYear
		LOGGER.info("Preparing to fetch barrel from production module ...");
		GrapeContainerInfo grapeContainerInfo = new GrapeContainerInfo();
		grapeContainerInfo.setGrapeType(wineBottleFromBarrelFillRequest.getGrapeType());
		grapeContainerInfo.setHarvestYear(wineBottleFromBarrelFillRequest.getHarvestYear());
		final String getBarrelUrl = ModuleUrlService.buildModuleUrl(ModuleEnum.PRODUCTION, "api/v1/production/barrels/grapeType-and-year");
		RestTemplate getBarrelRestTemplate = new RestTemplate();
		BarrelDto barrelDTO;
		try {
			LOGGER.info("Fetching barrel with grapeType: {}, harvestYear: {} from production module ...", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
			barrelDTO = getBarrelRestTemplate.postForObject(getBarrelUrl, grapeContainerInfo, BarrelDto.class);
		} catch (RestClientException ex) {
			LOGGER.warn("Barrel with grapeType: {}, harvestYear: {} could not be fetched", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
			throw new BarrelCouldNotBeFetchedException(wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
		}
		if (barrelDTO == null) {
			LOGGER.warn("Barrel with grapeType: {}, harvestYear: {} could not be fetched", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
			throw new BarrelCouldNotBeFetchedException(wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
		}
		LOGGER.info("Barrel with grapeType: {}, harvestYear: {} from production module was fetched successfully!", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
		LOGGER.info("Received barrel: {}", barrelDTO);

		// Check if requested quantity is available
		LOGGER.info("Checking if requested quantity is available in the barrel ...");
		double availableQuantity = barrelDTO.getQuantity().convertValueTo(VolumeQuantity.VolumeUnit.MILLILITER);
		double requestedQuantity = wineBottleFromBarrelFillRequest.getBottleVolume().convertValueTo(VolumeQuantity.VolumeUnit.MILLILITER) * wineBottleFromBarrelFillRequest.getBottleCount();
		if (availableQuantity < requestedQuantity) {
			LOGGER.warn("Not enough quantity in Barrel with grapeType: {}, harvestYear: {}, available: {}, requested: {}", wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), availableQuantity, requestedQuantity);
			throw new BarrelQuantityNotAvailableException(availableQuantity, requestedQuantity);
		}
		LOGGER.info("Requested quantity is available!");

		// Subtract required volume from barrel
		LOGGER.info("Subtracting requested quantity: {} from barrel: {} in production module ...", requestedQuantity, barrelDTO);
		final String subtractQuantityFromBarrelUrl = ModuleUrlService.buildModuleUrl(ModuleEnum.PRODUCTION, "api/v1/production/barrels/" + barrelDTO.getId() + "/subtract-quantity");

		VolumeQuantity volumeQuantity = new VolumeQuantity(requestedQuantity, VolumeQuantity.VolumeUnit.MILLILITER);
		RestTemplate subtractQauntityFromBarrelRestTemplate = new RestTemplate();
		try {
			subtractQauntityFromBarrelRestTemplate.postForObject(subtractQuantityFromBarrelUrl, volumeQuantity, BarrelDto.class);
		} catch (RestClientException ex) {
			LOGGER.warn("Requested volume: {} could not be subtracted from barrel with grapeType: {}, harvestYear: {}", requestedQuantity, wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear());
			throw new BarrelCouldNotBeSubtractedException(wineBottleFromBarrelFillRequest.getGrapeType(), wineBottleFromBarrelFillRequest.getHarvestYear(), volumeQuantity);
		}
		LOGGER.info("Requested quantity: {} from barrel: {} in production module was subtracted successfully!", requestedQuantity, barrelDTO);

		// Set new bottle count on wineBottleStock
		int wineBottleStockPreviousCount = wineBottleStock.getCount();
		wineBottleStock.setCount(wineBottleStockPreviousCount + wineBottleFromBarrelFillRequest.getBottleCount());
		wineBottleStockRepository.save(wineBottleStock);
		LOGGER.info("WineBottleStock with wineBottle: {}, price: {}, with previous bottle count: {}, has been updated, now has {} bottles.", wineBottleStock.getWineBottle(), wineBottleStock.getPrice(), wineBottleStockPreviousCount, wineBottleStock.getCount());

		return wineBottleStock;
	}
}