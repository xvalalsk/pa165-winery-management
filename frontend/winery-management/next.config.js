const isDocker = process.env.DOCKER === 'true' || process.env.NEXT_PUBLIC_DOCKER_ENV === 'true'
global.isDocker = isDocker;

module.exports = {
    serverRuntimeConfig: {
        DOCKER: process.env.DOCKER,
    },
    publicRuntimeConfig: {
        NEXT_PUBLIC_DOCKER_ENV: process.env.NEXT_PUBLIC_DOCKER_ENV,
    },
  async rewrites() {
    return isDocker ?
        [
            {
                source: '/api/v1/harvests/:path*',
                destination: 'http://harvest:8081/api/v1/harvests/:path*'
            },

            {
                source: '/api/v1/production/:path*',
                destination: 'http://production:8082/api/v1/production/:path*',
            },

            {
                source: '/api/v1/store/:path*',
                destination: 'http://store:8083/api/v1/store/:path*'
            },

            {
                source: '/api/v1/user/:path*',
                destination: 'http://user:8084/api/v1/user/:path*'
            },
            {
                source: '/api/v1/reviews/:path*',
                destination: 'http://user:8084/api/v1/reviews/:path*'
            },
        ]
      :
        [
            {
                source: '/api/v1/harvests/:path*',
                destination: 'http://localhost:8081/api/v1/harvests/:path*'
            },

            {
                source: '/api/v1/production/:path*',
                destination: 'http://localhost:8082/api/v1/production/:path*',
            },

            {
                source: '/api/v1/store/:path*',
                destination: 'http://localhost:8083/api/v1/store/:path*'
            },

            {
                source: '/api/v1/user/:path*',
                destination: 'http://localhost:8084/api/v1/user/:path*'
            },
            {
                source: '/api/v1/reviews/:path*',
                destination: 'http://localhost:8084/api/v1/reviews/:path*'
            },
        ]
  },
};
