package cz.muni.fi.pa165.winerymanagement.production.rest;

import cz.muni.fi.pa165.winerymanagement.core.unit.Quantity;
import cz.muni.fi.pa165.winerymanagement.production.api.BarrelDto;
import cz.muni.fi.pa165.winerymanagement.production.facade.BarrelFacade;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearGetRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearModifyQuantityRequest;
import cz.muni.fi.pa165.winerymanagement.production.rest.requests.GrapeTypeAndYearUpdateBarrelRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/production")
@Tag(name = "Barrels", description = "API for managing barrels")
@CrossOrigin(origins = "*")
public class BarrelRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BarrelRestController.class);
    private final BarrelFacade barrelFacade;

    @Autowired
    public BarrelRestController(BarrelFacade barrelFacade) {
        this.barrelFacade = barrelFacade;
    }

    @Operation(summary = "Get a list of all Barrels", description = "Returns a list of all barrels.")
    @ApiResponse(responseCode = "200", description = "List of all barrels returned successfully.")
    @GetMapping("/barrels")
    public ResponseEntity<List<BarrelDto>> getAllBarrels() {
        LOGGER.info("Getting all barrels");
        return ResponseEntity.ok(barrelFacade.findAll());
    }

    @Operation(summary = "Get a Barrel by ID", description = "Returns a barrel with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Barrel with the specified ID returned successfully."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified ID was not found.")
    })
    @GetMapping(path = "/barrels/{id}")
    public ResponseEntity<BarrelDto> getBarrelById(@PathVariable("id") Long id) {
        LOGGER.info("Getting barrel by ID: {}", id);
        BarrelDto barrelDto = barrelFacade.findById(id);
        return ResponseEntity.ok(barrelDto);
    }

    @Operation(summary = "Get a Barrel by Grape Type and Harvest Year", description = "Returns a barrel with the specified by Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Barrel with the specified by Grape Type and Harvest Year returned successfully."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified by Grape Type and Harvest Year was not found.")
    })
    @PostMapping(path = "/barrels/grapeType-and-year")
    public ResponseEntity<BarrelDto> getBarrelByTypeAndYear(@RequestBody GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        LOGGER.info("Getting barrel by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        BarrelDto barrelDto = barrelFacade.findByGrapeTypeAndHarvestYear(grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        return ResponseEntity.ok(barrelDto);
    }

    @Operation(summary = "Add a Barrel", description = "Adds a new barrel.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Barrel added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input.")
    })
    @PostMapping("/barrels")
    public ResponseEntity<BarrelDto> createBarrel(@Valid @RequestBody BarrelDto newBarrel) {
        LOGGER.info("Creating a new barrel: {}", newBarrel);
        return ResponseEntity.status(HttpStatus.CREATED).body(barrelFacade.add(newBarrel));
    }

    @Operation(summary = "Update a Barrel by ID", description = "Updates a barrel with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Barrel with the specified ID updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified ID was not found.")
    })
    @PutMapping("/barrels/{id}")
    public ResponseEntity<BarrelDto> updateBarrelById(@PathVariable Long id, @Valid @RequestBody BarrelDto updatedBarrel) {
        LOGGER.info("Updating barrel with ID: {} with new data: {}", id, updatedBarrel);
        BarrelDto barrel = barrelFacade.updateById(id, updatedBarrel);
        return ResponseEntity.ok(barrel);
    }

    @Operation(summary = "Update a Barrel by Grape Type and Harvest Year", description = "Updates a barrel with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Barrel with the specified by Grape Type and Harvest Year updated successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified by Grape Type and Harvest Year was not found.")
    })
    @PutMapping("/barrels/grapeType-and-year")
    public ResponseEntity<BarrelDto> updateBarrelByTypeAndYear(@RequestBody GrapeTypeAndYearUpdateBarrelRequest grapeTypeAndYearUpdateBarrelRequest) {
        LOGGER.info("Updating barrel by Grape Type: {} and Harvest Year: {} with new data: {}", grapeTypeAndYearUpdateBarrelRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearUpdateBarrelRequest.getGrapeTypeAndYearGetRequest().getHarvestYear(), grapeTypeAndYearUpdateBarrelRequest.getUpdatedBarrel());
        BarrelDto barrel = barrelFacade.updateByGrapeTypeAndHarvestYear(grapeTypeAndYearUpdateBarrelRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearUpdateBarrelRequest.getGrapeTypeAndYearGetRequest().getHarvestYear(), grapeTypeAndYearUpdateBarrelRequest.getUpdatedBarrel());
        return ResponseEntity.ok(barrel);
    }

    @Operation(summary = "Add quantity to a Barrel by ID", description = "Adds the specified quantity to a barrel with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of barrel with the specified ID added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified ID was not found.")
    })
    @PostMapping("/barrels/{id}/add-quantity")
    public ResponseEntity<BarrelDto> addQuantityToBarrelById(@PathVariable(value = "id") Long id, @RequestBody Quantity quantity) {
        LOGGER.info("Adding quantity {} to barrel with ID: {}", quantity, id);
        return ResponseEntity.ok(barrelFacade.addQuantity(id, quantity));
    }

    @Operation(summary = "Add quantity to a Barrel by Grape Type and Harvest Year", description = "Adds the specified quantity to a barrel with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of barrel with the specified Grape Type and Harvest Year added successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified Grape Type and Harvest Year was not found.")
    })
    @PostMapping("/barrels/grapeType-and-year/add-quantity")
    public ResponseEntity<BarrelDto> addQuantityToBarrelByTypeAndYear(@RequestBody GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest) {
        LOGGER.info("Adding quantity {} to barrel by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearModifyQuantityRequest.getQuantity(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        BarrelDto barrel = barrelFacade.findByGrapeTypeAndHarvestYear(grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        return ResponseEntity.ok(barrelFacade.addQuantity(barrel.getId(), grapeTypeAndYearModifyQuantityRequest.getQuantity()));
    }

    @Operation(summary = "Subtract quantity to a Barrel by ID", description = "Subtracts the specified quantity to a barrel with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of barrel with the specified ID subtracted successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified ID was not found.")
    })
    @PostMapping("/barrels/{id}/subtract-quantity")
    public ResponseEntity<BarrelDto> subtractQuantityOfBarrelById(@PathVariable(value = "id") Long id, @RequestBody Quantity quantity) {
        LOGGER.info("Subtracting quantity {} from barrel with ID: {}", quantity, id);
        return ResponseEntity.ok(barrelFacade.subtractQuantity(id, quantity));
    }

    @Operation(summary = "Subtract quantity of a Barrel by Grape Type and Harvest Year", description = "Subtracts the specified quantity to a barrel with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quantity of barrel with the specified Grape Type and Harvest Year subtracted successfully."),
            @ApiResponse(responseCode = "400", description = "Invalid input."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified Grape Type and Harvest Year was not found.")
    })

    @PostMapping("/barrels/grapeType-and-year/subtract-quantity")
    public ResponseEntity<BarrelDto> subtractQuantityOfBarrelByTypeAndYear(@RequestBody GrapeTypeAndYearModifyQuantityRequest grapeTypeAndYearModifyQuantityRequest) {
        LOGGER.info("Subtracting quantity {} from barrel by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearModifyQuantityRequest.getQuantity(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        BarrelDto barrel = barrelFacade.findByGrapeTypeAndHarvestYear(grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getGrapeType(), grapeTypeAndYearModifyQuantityRequest.getGrapeTypeAndYearGetRequest().getHarvestYear());
        return ResponseEntity.ok(barrelFacade.subtractQuantity(barrel.getId(), grapeTypeAndYearModifyQuantityRequest.getQuantity()));
    }

    @Operation(summary = "Delete a Barrel by ID", description = "Deletes a barrel with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Barrel with the specified ID deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified ID was not found.")
    })
    @DeleteMapping("/barrels/{id}")
    public ResponseEntity<BarrelDto> deleteBarrelById(@PathVariable(value = "id") Long id) {
        LOGGER.info("Deleting barrel with ID: {}", id);
        BarrelDto barrelDto = barrelFacade.deleteById(id);
        return ResponseEntity.ok(barrelDto);
    }

    @Operation(summary = "Delete a Barrel by Grape Type and Harvest Year", description = "Deletes a barrel with the specified Grape Type and Harvest Year.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Barrel with the specified Grape Type and Harvest Year deleted successfully."),
            @ApiResponse(responseCode = "404", description = "Barrel with the specified Grape Type and Harvest Year was not found.")
    })
    @DeleteMapping("/barrels/grapeType-and-year")
    public ResponseEntity<BarrelDto> deleteBarrelByGrapeTypeAndHarvestYear(@RequestBody GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        LOGGER.info("Deleting barrel by Grape Type: {} and Harvest Year: {}", grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        BarrelDto barrelDto = barrelFacade.deleteByGrapeTypeAndHarvestYear(grapeTypeAndYearGetRequest.getGrapeType(), grapeTypeAndYearGetRequest.getHarvestYear());
        return ResponseEntity.ok(barrelDto);
    }
}
