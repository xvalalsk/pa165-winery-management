package cz.muni.fi.pa165.winerymanagement.user.rest;

import cz.muni.fi.pa165.winerymanagement.user.UserApplication;
import cz.muni.fi.pa165.winerymanagement.user.api.UserRegistrationDto;
import cz.muni.fi.pa165.winerymanagement.user.facade.AuthorizationFacade;
import cz.muni.fi.pa165.winerymanagement.user.rest.AuthorizationController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureJsonTesters
@WebMvcTest(AuthorizationController.class)
@ContextConfiguration(classes = UserApplication.class)
class AuthorizationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorizationFacade authorizationFacade;

    @Autowired
    private JacksonTester<UserRegistrationDto> jsonUserRegistrationDto;

    @Test
    void registerUser() throws Exception {
        var userRegisterDto = new UserRegistrationDto();

        when(authorizationFacade.register(any())).thenReturn(0L);

        mockMvc.perform(post("/api/v1/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUserRegistrationDto.write(userRegisterDto).getJson()))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        verify(authorizationFacade, times(1)).register(any());
    }
}
