package cz.muni.fi.pa165.winerymanagement.harvest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = HarvestApplication.class)
class HarvestApplicationTests {

    @Test
    void contextLoads() {
    }

}
