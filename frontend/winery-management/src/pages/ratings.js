import { Fragment, useEffect, useState } from 'react';
import { NavBar } from '@/pages';
import Cookies from 'js-cookie';
import StarRating from '../components/StarRating';

const WineRatings = () => {
    const [token, setToken] = useState(null);
    const [userID, setUserID] = useState(null);
    const [wineBottles, setWineBottles] = useState([]);
    const [reviews, setReviews] = useState([]);
    const [error, setError] = useState('');

    const [selectedWineBottle, setSelectedWineBottle] = useState(null);
    const [rating, setRating] = useState('');
    const [comment, setComment] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch('/api/v1/reviews', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    wineBottleId: selectedWineBottle,
                    userId: userID,
                    rating: parseInt(rating),
                    comment: comment,
                }),
            });

            if (response.ok) {
                // Refresh the reviews
                fetchWineBottles();
            } else {
                setError('Failed to submit review. Please try again.');
            }
        } catch (error) {
            setError(error.message + '. Is the Reviews module running?');
        }
    };

    useEffect(() => {
        const token = Cookies.get("bearerToken");
        setToken(token);
        const userID = Cookies.get("userID");
        setUserID(userID);
        fetchWineBottles();
    }, []);

    const fetchWineBottles = async () => {
        const token = Cookies.get("bearerToken");
        try {
            const response = await fetch('/api/v1/store/wine-bottles', {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            const data = await response.json();
            setWineBottles(data);
            fetchReviews(data);
        } catch (error) {
            setError(error.message + '. Is the Store module running?');
        }
    };

    const fetchReviews = async (wineBottles) => {
        const token = Cookies.get("bearerToken");
        try {
            const reviewsData = await Promise.all(
                wineBottles.map(async (wineBottle) => {
                    const response = await fetch(`/api/v1/reviews/bottle/${wineBottle.id}`, {
                        method: 'GET',
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    });
                    const data = await response.json();
                    return { bottleId: wineBottle.id, reviews: data };
                })
            );
            setReviews(reviewsData);
        } catch (error) {
            setError(error.message + '. Is the Reviews module running?');
        }
    };

    const displayStars = (rating) => {
        return '★'.repeat(rating) + '☆'.repeat(5 - rating);
    };

    if (token === null) {
        return (
            <>
                <NavBar />
                <div>Login first</div>
            </>
        );
    }

    return (
        <>
            <NavBar />
            <div className="container mt-5">
                <h1 className="display-4 mb-5 text-center">Wine Ratings</h1>
                <div>
                    <div className="mt-4">
                        <table className="table table-striped table-hover table-bordered no-border">
                            <thead className="bg-secondary bg-gradient">
                            <tr>
                                <th scope="col">Wine</th>
                                <th scope="col">Volume</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Comment</th>
                            </tr>
                            </thead>
                            <tbody>
                            {wineBottles.map((wineBottle) => (
                                <Fragment key={wineBottle.id}>
                                    <tr className="table-primary">
                                        <th scope="row" colSpan="1">
                                            {wineBottle.grapeType.name} {wineBottle.harvestYear}
                                        </th>
                                        <th scope="row" colSpan="3">
                                            {`${wineBottle.volume.value} ${wineBottle.volume.symbol}`}
                                        </th>
                                    </tr>

                                    {reviews
                                        .filter((review) => review.bottleId === wineBottle.id)
                                        .map((review) =>
                                            review.reviews.map((rating, index) => (
                                                <tr key={index}>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{displayStars(rating.rating)}</td>
                                                    <td>{rating.comment}</td>
                                                </tr>
                                            ))
                                        )}
                                </Fragment>
                            ))}
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <h2 className="mb-3">Add a Review</h2>
                        <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="wineBottleSelect" className="form-label">
                                    Wine Bottle
                                </label>
                                <select
                                    className="form-select"
                                    id="wineBottleSelect"
                                    value={selectedWineBottle}
                                    onChange={(e) => setSelectedWineBottle(e.target.value)}
                                    required
                                >
                                    <option value="">Select a wine bottle</option>
                                    {wineBottles.map((wineBottle) => (
                                        <option key={wineBottle.id} value={wineBottle.id}>
                                            {wineBottle.grapeType.name} - {wineBottle.harvestYear}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="ratingInput" className="form-label">
                                    Rating
                                </label>
                                <StarRating rating={rating} setRating={setRating} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="commentInput" className="form-label">
                                    Comment
                                </label>
                                <textarea
                                    className="form-control"
                                    id="commentInput"
                                    rows="3"
                                    value={comment}
                                    onChange={(e) => setComment(e.target.value)}
                                    required
                                ></textarea>
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Submit Review
                            </button>
                        </form>
                    </div>
                </div>
                {error && (
                    <div className="alert alert-danger mt-3" role="alert">
                        {error}
                    </div>
                )}
            </div>
        </>
    );
};

export default WineRatings;