package cz.muni.fi.pa165.winerymanagement.production.rest.requests;

import cz.muni.fi.pa165.winerymanagement.production.api.GrapeContainerDto;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "GrapeTypeAndYearUpdateGrapeContainerRequest",
        description = "A request object containing information about grape type and year to update a grape container.",
        example = """
                    {
                        "grapeTypeAndYearGetRequest": {
                            "grapeType": {
                                "name": "Chardonnay",
                                "description": "Chardonnay white sweet wine description",
                                "color": "WHITE",
                                "sweetness": "SWEET"
                            },
                            "harvestYear": 2020
                        },
                        "updatedGrapeContainer": {
                            "id": 1,
                            "grapeType": {
                                "name": "New Chardonnay",
                                "description": "New Chardonnay white sweet wine description",
                                "color": "WHITE",
                                "sweetness": "SWEET"
                            },
                            "harvestYear": 2020,
                            "quantity": {
                                "type": "weight",
                                "value": 100.0,
                                "weightUnit": "KILOGRAM"
                            }
                        }
                    }
                """
)
public class GrapeTypeAndYearUpdateGrapeContainerRequest {
    private GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest;
    private GrapeContainerDto updatedGrapeContainer;

    public GrapeTypeAndYearGetRequest getGrapeTypeAndYearGetRequest() {
        return grapeTypeAndYearGetRequest;
    }

    public void setGrapeTypeAndYearGetRequest(GrapeTypeAndYearGetRequest grapeTypeAndYearGetRequest) {
        this.grapeTypeAndYearGetRequest = grapeTypeAndYearGetRequest;
    }

    public GrapeContainerDto getUpdatedGrapeContainer() {
        return updatedGrapeContainer;
    }

    public void setUpdatedGrapeContainer(GrapeContainerDto updatedGrapeContainer) {
        this.updatedGrapeContainer = updatedGrapeContainer;
    }

    @Override
    public String toString() {
        return "GrapeTypeAndYearUpdateGrapeContainerRequest{" +
                "grapeTypeAndYearGetRequest=" + grapeTypeAndYearGetRequest +
                ", updatedGrapeContainer=" + updatedGrapeContainer +
                '}';
    }
}
