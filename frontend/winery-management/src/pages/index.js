import Link from 'next/link'
import {useEffect, useState} from 'react';
import Cookies from "js-cookie";
import {useRouter} from "next/navigation";

export function NavBar() {
    const [isOpen, setIsOpen] = useState(false);
    const [logged, setLogged] = useState(false);
    const [loading, setLoading] = useState(false);

    const router = useRouter();
    const toggleDropdown = () => setIsOpen(!isOpen);

    function delay(milliseconds){
        return new Promise(resolve => {
            setTimeout(resolve, milliseconds);
        });
    }
    async function getToken() {
        setLoading(true);
        try {
            const tokenResponse = await fetch("http://localhost:8080/token", {
                method: "GET",
                credentials: "include",
            });

            if (tokenResponse.ok) {
                const data = await tokenResponse.text();
                Cookies.set("bearerToken", data, {expires: 1}); // expires in 1 day;
                const emailResponse = await fetch("http://localhost:8080/email", {
                    method: "GET",
                    credentials: "include",
                });
                const email= await emailResponse.text();
                await delay(1000);
                const registrationResponse = await fetch("/api/v1/user/register", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${data}`,
                    },
                    body: JSON.stringify({
                        issuer: "https://oidc.muni.cz/oidc/",
                        externalId: email
                    }),
                });
                const userID = await registrationResponse.json();
                Cookies.set("userID", userID, {expires: 1}); // expires in 1 day;
                setLogged(true);
                router.push('/');
            } else {
                console.error("login error")
            }
        } catch (error) {
            console.error("Error fetching token:", error);
            window.open("http://localhost:8080", "_blank");
            window.addEventListener('focus', async () => {
                await delay(1000);
                await getToken();
                router.refresh()
            });
        }
        setLoading(false);
    }

    async function logout() {
        Cookies.remove("bearerToken");
        Cookies.remove("userID");
        setLogged(false);
        router.push('/');
    }

    useEffect(() => {
        setLogged(Cookies.get("bearerToken") !== undefined)
    }, [])

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <Link className="navbar-brand" href="/">
                    Winery Management
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ms-auto">
                        <li className="nav-item">
                            <Link className="nav-link" href="/harvest">
                                Harvest
                            </Link>
                        </li>
                        <li className="nav-item dropdown">
                            <button
                                className="nav-link dropdown-toggle border-0 bg-light"
                                onClick={toggleDropdown}
                            >
                                Production
                            </button>
                            <ul
                                className={`dropdown-menu${isOpen ? ' show' : ''}`}
                                aria-labelledby="navbarDropdown"
                            >
                                <li>
                                    <Link className="dropdown-item" href="/production/ingredient">
                                        Ingredient
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/production/grape-container" className="dropdown-item">
                                        Grape Container
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/production/production-items" className="dropdown-item">
                                        Production Items
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/production/barrels" className="dropdown-item">
                                        Barrels
                                    </Link>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" href="/store">
                                Store
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" href="/ratings">
                                Ratings
                            </Link>
                        </li>
                        <li className="nav-item">
                            {logged ? (
                                    <button className="nav-link btn btn-danger btn-outline-danger text-decoration-none text-white" onClick={logout}>
                                        Logout
                                    </button>

                            ) : (
                                <button className="nav-link btn btn-primary btn-outline-primary text-decoration-none text-white" onClick={getToken}>
                                    Login
                                </button>
                            )}
                        </li>
                    </ul>
                </div>
            </div>
            {loading && <div>Loading...</div>}
        </nav>
    );
}


export default function Home() {
    return (
        <div>
            <NavBar/>
            <div className="container my-5">
                <h1>Welcome to Winery Management!</h1>
                <p>This page allows the winery owners to record the harvests of grapes.</p>
                <p>The production module allows to record grape containers, which are grapes stored in containers, also
                    it allows to add ingredients which are used to create a production item, from grape containers and
                    at the end of the process it allows to create a barrel of wine.</p>
                <p>The store module allows to fill bottles from the barrels.</p>
            </div>
        </div>
    )
}
